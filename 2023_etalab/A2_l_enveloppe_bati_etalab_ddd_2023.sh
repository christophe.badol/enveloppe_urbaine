#!/bin/bash
# A0 Préparation à Enveloppe Urbaine
# 26/02/2022

echo "-----------------------------------------------------"
echo "A.2] Travail sur la table nationale Enveloppe Urbaine"
echo "-----------------------------------------------------"
export PGPASSWORD="tunelesaurapas!"
# Paramètres
commande="psql -h hp-geomatique -p 5432 -d ceremabase -c "
millesime="2023"

##############################################################
# Option 1.1 : Depuis le parcellaire express ==> pepci
#trigramme="pepci"
# Option 1.2 : Depuis le cadastre de data.gouv.fr ==> etalab
trigramme="etalab"
# Option 1.3 : Depuis le bati de la bdtopo v3 ==> bdtopo
#trigramme="bdtopo"
# Fin Option 1
##############################################################

schemacadastre="r_cadastre_"$trigramme"_"$millesime
schematravail="p_enveloppe_bati_"$millesime
# Obligation du cadastre meme si le bati agrégé n a pas été intégré :
batiorigine=$schemacadastre".n_batiments_"$trigramme"_000_"$millesime 


# Important :
# Pour tout logger taper :
# su postgres
# time sh /home/administrateur/00_gitlab/prod/enveloppe_urbaine/2022_etalab/A2_l_enveloppe_bati_etalab_ddd_2022.sh 1>/mnt/data/A2_enveloppe_bati_etalab_ddd_2021.log 2>&1

$commande "

CREATE TABLE "$schematravail".temp_departement_buffer_200m_000_"$millesime" AS
	SELECT '0' || insee_dep AS code_dep, ST_Multi(ST_Buffer(ST_Union(ST_Transform(geom,2154)),200))::geometry(MultiPolygon,2154) as geom
	FROM r_admin_express.n_adm_exp_cog_departement_000
	GROUP BY insee_dep;

CREATE INDEX temp_departement_buffer_200m_000_"$millesime"_code_dep_idx
    ON "$schematravail".temp_departement_buffer_200m_000_"$millesime" USING brin
    (code_dep)
    TABLESPACE index;

CREATE INDEX temp_departement_buffer_200m_000_"$millesime"_geom_gist
    ON "$schematravail".temp_departement_buffer_200m_000_"$millesime" USING gist
    (geom)
    TABLESPACE index;

ALTER TABLE "$schematravail".temp_departement_buffer_200m_000_"$millesime"
    CLUSTER ON temp_departement_buffer_200m_000_"$millesime"_geom_gist;

"

#DEBUG
#for dpt in '090'
for dpt in '001' '002' '003' '004' '005' '006' '007' '008' '009' '010' '011' '012' '013' '014' '015' '016' '017' '018' '019' '02b' '02a' '021' '022' '023' '024' '025' '026' '027' '028' '029' '030' '031' '032' '033' '034' '035' '036' '037' '038' '039' '040' '041' '042' '043' '044' '045' '046' '047' '048' '049' '050' '051' '052' '053' '054' '055' '056' '057' '058' '059' '060' '061' '062' '063' '064' '065' '066' '067' '068' '069' '070' '071' '072' '073' '074' '075' '076' '077' '078' '079' '080' '081' '082' '083' '084' '085' '086' '087' '088' '089' '090' '091' '092' '093' '094' '095'
do

tabletravail="l_enveloppe_bati_"$trigramme"_"$dpt"_"$millesime	
now=$(date +"%T")
echo "--------------------"
echo "Début à $now"
echo "Département $dpt"
echo "--------------------"
echo "A.1.1] Création de la table à partir d’une dilatation de 50 m / fusion / érosion de 40 m"
$commande "
DROP TABLE IF EXISTS "$schematravail"."$tabletravail";
CREATE TABLE "$schematravail"."$tabletravail"
(
	id serial,
	nb_batidur integer,
	nb_batileg integer,
	surf_batidur integer,
	surf_batileg integer,
	code_dep character(3) DEFAULT '"$dpt"',  
	geom geometry(Polygon,2154)
);
"
#DEBUG
# Obligation de séparer ces trois commandes car si 2a 2b l’ALTER SEQUENCE echoue et annule les 4 commandes
$commande "
ALTER SEQUENCE "$schematravail"."$tabletravail"_id_seq
	RESTART WITH "$dpt"000001;
"
#DEBUG
# Obligation de séparer ces trois commandes car si 2a 2b l’ALTER SEQUENCE echoue et annule les 4 commandes
$commande "
INSERT INTO "$schematravail"."$tabletravail" (geom)
	SELECT (ST_Dump(ST_Buffer(ST_Union(ST_Buffer(t1.geom,50)),-40))).geom::geometry(Polygon,2154) as geom
	FROM "$batiorigine" as t1
	JOIN "$schematravail".temp_departement_buffer_200m_000_"$millesime" as t2
	ON ST_Intersects (t1.geom, t2.geom)
	WHERE t2.code_dep = upper('"$dpt"');
"
echo "A.1.2] Clé + Index + Cluster + Optimisation"
$commande "
ALTER TABLE "$schematravail"."$tabletravail"
    ADD CONSTRAINT "$tabletravail"_pk PRIMARY KEY (id)
    USING INDEX TABLESPACE index;

ALTER TABLE "$schematravail"."$tabletravail"
    ADD CONSTRAINT enforce_dims_geom CHECK (ST_ndims(geom) = 2);

ALTER TABLE "$schematravail"."$tabletravail"
	ADD CONSTRAINT enforce_srid_geom CHECK (ST_Srid(geom)= 2154);

ALTER TABLE "$schematravail"."$tabletravail"
    ADD CONSTRAINT enforce_geotype_geom CHECK (geometrytype(geom) = 'POLYGON'::text);

ALTER TABLE "$schematravail"."$tabletravail"
	ADD CONSTRAINT enforce_no_self_intersection_geom CHECK (st_issimple(geom));

ALTER TABLE "$schematravail"."$tabletravail"
	ADD CONSTRAINT enforce_no_zero_area_geom CHECK ((st_area(geom) > (0)::double precision));

ALTER TABLE "$schematravail"."$tabletravail"
	ADD CONSTRAINT enforce_validite_geom CHECK (st_isvalid(geom));

CREATE INDEX "$tabletravail"_code_dep_idx ON "$schematravail"."$tabletravail"
	USING brin (code_dep) TABLESPACE index;

CREATE INDEX "$tabletravail"_geom ON "$schematravail"."$tabletravail"
	USING gist (geom) TABLESPACE index;
ALTER TABLE "$schematravail"."$tabletravail" CLUSTER ON "$tabletravail"_geom;
"

echo "A.1.3] Peuplement du champs nb_batidur + index"
$commande "
UPDATE "$schematravail"."$tabletravail" as t1 SET nb_batidur = (
	SELECT count(*)
	FROM "$batiorigine" as t2
	-- Option sans partition	
	-- FROM "$batiorigine" as t2
	WHERE 	t1.code_dep = '"$dpt"'
		and (t2.type='Bâtiment en dur' or t2.type='Bâti dur' or t2.type='Bati dur' or t2.type='01')
		and ST_Intersects(t1.geom, t2.geom))
	WHERE 	t1.code_dep = '"$dpt"';
"
$commande "
CREATE INDEX "$tabletravail"_nb_batidur_idx ON "$schematravail"."$tabletravail"
	USING brin (nb_batidur) TABLESPACE index;
"

echo "A.1.4] Peuplement du champs nb_batileg + index"
$commande "
UPDATE "$schematravail"."$tabletravail" as t1 SET nb_batileg = (
	SELECT count(*)
	FROM "$batiorigine" as t2
	-- Option sans partition	
	-- FROM "$batiorigine" as t2
	WHERE 	t1.code_dep = '"$dpt"'
		and (type='Construction légère' or type='Bâti léger' or type='Bati leger' or type='02')
		and ST_Intersects(t1.geom, t2.geom))
	WHERE 	t1.code_dep = '"$dpt"';
"
$commande "
CREATE INDEX "$tabletravail"_nb_batileg_idx ON "$schematravail"."$tabletravail"
	USING brin (nb_batileg) TABLESPACE index;
"

echo "A.1.5] Peuplement du champs surf_batidur + index"
$commande "
UPDATE "$schematravail"."$tabletravail" as t1 SET surf_batidur = (
	SELECT sum(ST_Area(t2.geom))
	FROM "$batiorigine" as t2
	WHERE 	
		ST_Intersects(t1.geom, t2.geom)
		AND 
		(t2.type='01' or t2.type='Bâtiment en dur' or t2.type='Bâti dur' or t2.type='Bati dur')
	);
"
$commande "
CREATE INDEX "$tabletravail"_surf_batidur_idx ON "$schematravail"."$tabletravail"
	USING brin (surf_batidur);
"

echo "A.1.6] Peuplement du champs surf_batileg + index"
$commande "	
UPDATE "$schematravail"."$tabletravail" as t1 SET surf_batileg = (
	SELECT sum(ST_Area(t2.geom))
	FROM "$batiorigine" as t2
	WHERE 	
		ST_Intersects(t1.geom, t2.geom)
		AND
		(t2.type='02' or t2.type='Construction légère' or t2.type='Bâti léger' or t2.type='Bati leger')
	);	
"
$commande "
CREATE INDEX "$tabletravail"_surf_batileg_idx ON "$schematravail"."$tabletravail"
	USING brin (surf_batileg);
"		

echo "A.1.7] Commentaires"
$commande "
COMMENT ON TABLE "$schematravail"."$tabletravail"
  IS 'Enveloppe du bati au 1er janvier 2021 pour le département ''"$dpt"''.

Il s’agit de fusionner tous les bâtiments issu du cadastre "$trigramme" par une dilatation de 50 m, puis une fusion et ensuite une érosion de 40 m';

COMMENT ON COLUMN "$schematravail"."$tabletravail".id
    IS 'Identifiant unique selon méthode COVADIS : n°département.000.000 + id.';

COMMENT ON COLUMN "$schematravail"."$tabletravail".nb_batidur
    IS 'Nombre de bâtiments qualifiés de dur par la DGFiP intégrés dans cette enveloppe du bâti.';

COMMENT ON COLUMN "$schematravail"."$tabletravail".nb_batileg
    IS 'Nombre de bâtiments qualifiés de legers par la DGFiP intégrés dans cette enveloppe du bâti.';

COMMENT ON COLUMN "$schematravail"."$tabletravail".surf_batidur
    IS 'Surface totale en m2 de tous les bâtiments qualifiés de dur par la DGFiP et présents dans cette enveloppe du bâti.';

COMMENT ON COLUMN "$schematravail"."$tabletravail".surf_batileg
   IS 'Surface totale en m2 de tous les bâtiments qualifiés de leger par la DGFiP et présents dans cette enveloppe du bâti.';	

COMMENT ON COLUMN "$schematravail"."$tabletravail".code_dep
    IS 'Numéro INSEE du département sur 3 charactères.';

COMMENT ON COLUMN "$schematravail"."$tabletravail".geom
    IS 'Champs géométrique : Monopolygones en Lambert93 (EPSG:2154).';
"

done
