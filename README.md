# Enveloppe Urbaine

## Recapitulatif du travail annuel de création de l'enveloppe urbaine du bâti cadastral.

- Classement par année + origine du cadastre
	- fichier sql : synthèse du travail
	- fichiers sh : automatisation de certaines taches
	- 80_log : log des fichiers d'automatisation

