#!/bin/bash
# A0 Préparation à Enveloppe Urbaine
commande="psql -d ceremabase -c "

$commande "
CREATE SCHEMA tache_urbaine_2017;
"

$commande "
DROP TABLE IF EXISTS tache_urbaine_2017.temp_departement_buffer_200m_000_2017;
CREATE TABLE tache_urbaine_2017.temp_departement_buffer_200m_000_2017 AS
	SELECT '0'||insee_dep as code_dep, ST_Multi(ST_Union(ST_Buffer(ST_Transform(geom,2154),200)))::geometry(MultiPolygon,2154) as geom
	FROM r_admin_express.n_admin_express_departement_fra_2020
	GROUP BY code_dep;

CREATE INDEX temp_departement_buffer_200m_000_2017_code_dep_idx
    ON tache_urbaine_2017.temp_departement_buffer_200m_000_2017 USING brin
    (code_dep)
    TABLESPACE index;

CREATE INDEX temp_departement_buffer_200m_000_2017_geom_gist
    ON tache_urbaine_2017.temp_departement_buffer_200m_000_2017 USING gist
    (geom)
    TABLESPACE index;

ALTER TABLE tache_urbaine_2017.temp_departement_buffer_200m_000_2017
    CLUSTER ON temp_departement_buffer_200m_000_2017_geom_gist;
"
