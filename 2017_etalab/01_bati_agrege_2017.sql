echo "====================================================================================="
echo "	CREATION BATI AGREGE"
echo ""
echo "             CEREMABASE"
echo ""
echo "	Version 1 du 3 octobre 2020"
echo "	c.badol"
echo "	Finalisé oui| | / non |X|"
echo "====================================================================================="
echo " Rq : ---- pour les commentaires / -- pour les commandes optionnelles, debuger"

CREATE SCHEMA p_enveloppe_bati_2017 AUTHORIZATION postgres;
COMMENT ON SCHEMA p_enveloppe_bati_2017 IS 'Enveloppe du bâti de 2017.
Création par fusion d un buffer de 50 m autour des batiments et érosion de -40 m.

Le référentiel utilisé est dans le nom de la table :
- pepci : Parcellaire Express de l IGN,
- etalab : cadastre disponible sur le site data.gouv.fr,
- bdtopo : batiments de la BDTOPO de l IGN.

Les scripts de ce travail sont disponibles ici : https://gitlab.com/christophe.badol/enveloppe_urbaine .';

echo "--------------------------------
echo "A1] Bâti agrégé 2017
echo "--------------------------------

#!/bin/bash
# Tache Urbaine
commande="psql -d ceremabase -c "
millesime="2017"
cadastre="etalab"
couchebaticadastre="r_cadastre_"$cadastre"_"$millesime".n_bati_etalab"

#DEBUG
#echo $couchebaticadastre

# Important :
# Pour tout logger taper :
# time sh /home/administrateur/gitlab/enveloppe_urbaine/2017_etalab/01_l_bati_agrege_etalab_2017.sh 1>/mnt/data/01_l_bati_agrege_etalab_2017.log 2>&1

# A] Bati Agrege
#for dpt in '090'
for dpt in '001' '002' '003' '004' '005' '006' '007' '008' '009' '010' '011' '012' '013' '014' '015' '016' '017' '018' '019' '021' '022' '023' '024' '025' '026' '027' '028' '029' '030' '031' '032' '033' '034' '035' '036' '037' '038' '039' '040' '041' '042' '043' '044' '045' '046' '047' '048' '049' '050' '051' '052' '053' '054' '055' '056' '057' '058' '059' '060' '061' '062' '063' '064' '065' '066' '067' '068' '069' '070' '071' '072' '073' '074' '075' '076' '077' '078' '079' '080' '081' '082' '083' '084' '085' '086' '087' '088' '089' '090' '091' '092' '093' '094' '095'
#for dpt in '02b' '02a'

do

echo $dpt
echo '----'

# A.1] Création des tables Départementales
$commande "
DROP TABLE IF EXISTS tache_urbaine_"$millesime".l_bati_agrege_etalab_"$dpt"_"$millesime";
"
$commande "
CREATE TABLE tache_urbaine_"$millesime".l_bati_agrege_etalab_"$dpt"_"$millesime"
	(
	id serial,
	nb_batidur integer,
	nb_batileg integer,
	code_dep character(3) DEFAULT '"$dpt"',  
	geom geometry(Polygon,2154)
	);
"

$commande "
ALTER SEQUENCE tache_urbaine_"$millesime".l_bati_agrege_etalab_"$dpt"_"$millesime"_id_seq
	RESTART WITH "$dpt"000001;
"

# A.2] Buffer +0,01, ST_Union, Buffer -0,01, ST_Dump
$commande "
INSERT INTO tache_urbaine_"$millesime".l_bati_agrege_etalab_"$dpt"_"$millesime" (geom)
	SELECT (ST_Dump(ST_Buffer(ST_Union(ST_Buffer(geom,0,01)),-0,01))).geom::geometry(Polygon,2154) as geom
	FROM "$couchebaticadastre"_"$dpt"_"$millesime";
"

# A.3] Mise à jour des champs nb_batidur & nb_batileg
$commande "
UPDATE tache_urbaine_"$millesime".l_bati_agrege_etalab_"$dpt"_"$millesime" as t1 SET nb_batidur = (
	SELECT count(*)
	FROM "$couchebaticadastre"_"$dpt"_"$millesime" as t2
	WHERE 	t1.code_dep = '"$dpt"'
		and (t2.type='Bâtiment en dur' or t2.type='Bâti dur' or t2.type='Bati dur' or t2.type='01')
		and ST_Intersects(t1.geom, t2.geom))
	WHERE 	t1.code_dep = '"$dpt"';
"

$commande "
UPDATE tache_urbaine_"$millesime".l_bati_agrege_etalab_"$dpt"_"$millesime" as t1 SET nb_batileg = (
	SELECT count(*)
	FROM "$couchebaticadastre"_"$dpt"_"$millesime" as t2
	WHERE 	t1.code_dep = '"$dpt"'
		and (type='Construction légère' or type='Bâti léger' or type='Bati leger' or type='02')
		and ST_Intersects(t1.geom, t2.geom))
	WHERE 	t1.code_dep = '"$dpt"';
"

# A.4] Optimisation 
# Clé primaire 
$commande "
ALTER TABLE tache_urbaine_"$millesime".l_bati_agrege_etalab_"$dpt"_"$millesime"
    ADD CONSTRAINT l_bati_agrege_etalab_"$dpt"_"$millesime"_pk PRIMARY KEY (id)
    USING INDEX TABLESPACE index;
"
# Contraintes Géométriques
$commande "
ALTER TABLE tache_urbaine_"$millesime".l_bati_agrege_etalab_"$dpt"_"$millesime"
    ADD CONSTRAINT enforce_dims_geom CHECK (ST_ndims(geom) = 2);
"
$commande "
ALTER TABLE tache_urbaine_"$millesime".l_bati_agrege_etalab_"$dpt"_"$millesime"
	ADD CONSTRAINT enforce_srid_geom CHECK (ST_Srid(geom)= 2154);
"
$commande "
ALTER TABLE tache_urbaine_"$millesime".l_bati_agrege_etalab_"$dpt"_"$millesime"
    ADD CONSTRAINT enforce_geotype_geom CHECK (geometrytype(geom) = 'POLYGON'::text);
"
$commande "
ALTER TABLE tache_urbaine_"$millesime".l_bati_agrege_etalab_"$dpt"_"$millesime"
	ADD CONSTRAINT enforce_validite_geom CHECK (ST_IsValid(geom));
"
$commande "
ALTER TABLE tache_urbaine_"$millesime".l_bati_agrege_etalab_"$dpt"_"$millesime"
	ADD CONSTRAINT enforce_no_self_intersection_geom CHECK (ST_IsSimple(geom));
"
$commande "
ALTER TABLE tache_urbaine_"$millesime".l_bati_agrege_etalab_"$dpt"_"$millesime"
	ADD CONSTRAINT enforce_no_zero_area_geom CHECK (ST_Area(geom) > 0);
"
# Index attributaires
$commande "
CREATE INDEX l_bati_agrege_etalab_"$dpt"_"$millesime"_nb_batidur_idx ON tache_urbaine_"$millesime".l_bati_agrege_etalab_"$dpt"_"$millesime"
	USING brin (nb_batidur) TABLESPACE index;
"
$commande "
CREATE INDEX l_bati_agrege_etalab_"$dpt"_"$millesime"_nb_batileg_idx ON tache_urbaine_"$millesime".l_bati_agrege_etalab_"$dpt"_"$millesime"
	USING brin (nb_batileg) TABLESPACE index;
"
$commande "
CREATE INDEX l_bati_agrege_etalab_"$dpt"_"$millesime"_code_dep_idx ON tache_urbaine_"$millesime".l_bati_agrege_etalab_"$dpt"_"$millesime"
	USING brin (code_dep) TABLESPACE index;
"
# Index Géomérique + Cluster
$commande "
CREATE INDEX l_bati_agrege_etalab_"$dpt"_"$millesime"_geom_gist ON tache_urbaine_"$millesime".l_bati_agrege_etalab_"$dpt"_"$millesime"
	USING gist (geom) TABLESPACE index;
"
$commande "
ALTER TABLE tache_urbaine_"$millesime".l_bati_agrege_etalab_"$dpt"_"$millesime"
	CLUSTER ON l_bati_agrege_etalab_"$dpt"_"$millesime"_geom_gist;
"

# A.5] Commentaires
# Table
$commande "
COMMENT ON TABLE tache_urbaine_"$millesime".l_bati_agrege_etalab_"$dpt"_"$millesime"
  IS 'Bâti agrégé de "$millesime" pour le département ''"$dpt"''.

Il s’agit de fusionner tous les bâtiments du cadastre "$cadastre" qui sont séparés de moins de 1 cm par un Buffer +0,01, ST_Union, Buffer -0,01, ST_Dump';
"
# Champs
$commande "
COMMENT ON COLUMN tache_urbaine_"$millesime".l_bati_agrege_etalab_"$dpt"_"$millesime".id
    IS 'Identifiant unique selon méthode COVADIS : n°département.000.000 + id.';
"
$commande "
COMMENT ON COLUMN tache_urbaine_"$millesime".l_bati_agrege_etalab_"$dpt"_"$millesime".nb_batidur
    IS 'Nombre de bâtiments qualifiés de dur par la DGFiP intégrés dans cette agrégation de bâtiments.';
"
$commande "
COMMENT ON COLUMN tache_urbaine_"$millesime".l_bati_agrege_etalab_"$dpt"_"$millesime".nb_batileg
    IS 'Nombre de bâtiments qualifiés de legers par la DGFiP intégrés dans cette agrégation de bâtiments.';
"
$commande "
COMMENT ON COLUMN tache_urbaine_"$millesime".l_bati_agrege_etalab_"$dpt"_"$millesime".code_dep
    IS 'Numéro INSEE du département sur 3 charactères.';
"
$commande "
COMMENT ON COLUMN tache_urbaine_"$millesime".l_bati_agrege_etalab_"$dpt"_"$millesime".geom
    IS 'Champs géométrique : Monopolygones en Lambert93 (EPSG:2154).';
"

done

time sh /home/administrateur/gitlab/enveloppe_urbaine/2017_etalab/01_l_bati_agrege_etalab_2017.sh 1>/mnt/data/01_l_bati_agrege_etalab_2017.log 2>&1
/*
real	25200m54,707s
user	2m1,250s
sys	0m27,607s
*/
time sh /home/administrateur/gitlab/enveloppe_urbaine/2017_etalab/01_l_bati_agrege_etalab_2017.sh 1>/mnt/data/01_l_bati_agrege_etalab_2017_corse.log 2>&1
/*
real	34m14,224s
user	0m2,422s
sys	0m0,537s
*/

/*
echo "------------------------------------------------------------"
echo "A.2] Partition "
echo "------------------------------------------------------------"
echo "A.2.1] Partition Nationale "
$commande "
update tache_urbaine_2017.l_bati_agrege_etalab_02a_2017 set code_dep = '02A';
--> Updated Rows	83931
update tache_urbaine_2017.l_bati_agrege_etalab_02b_2017 set code_dep = '02B';
--> Updated Rows	90684
select w_adl_delegue.set_partition_attach_metropole_et_regions('tache_urbaine_2017.l_bati_agrege_etalab','_2017');
--> 2020-10-21 - tache_urbaine_2017.l_bati_agrege_etalab_000_2017 reste visible
"

echo "A.2.2] Commentaires "
$commande "
DO $$
DECLARE
liste_valeur 				CHARACTER VARYING[];
req 						text;
BEGIN
liste_valeur := ARRAY  ['000','r11','r24','r27','r28','r32','r44','r52','r53','r75','r76','r84','r93','r94'];

FOR i_table IN 1..array_length(liste_valeur, 1) LOOP
		req := '
				COMMENT ON TABLE tache_urbaine_2017.l_bati_agrege_etalab_'|| liste_valeur[i_table] ||'_2017
				  IS ''Bâti agrégé de 2017.
				
				Il s’agit de fusionner tous les bâtiments du parcellaire express qui sont séparés de moins de 1 cm par un Buffer +0,01, ST_Union, Buffer -0,01, ST_Dump.'';
				COMMENT ON COLUMN tache_urbaine_2017.l_bati_agrege_etalab_'|| liste_valeur[i_table] ||'_2017.id
				IS ''Identifiant unique selon méthode COVADIS : n°département.000.000 + id.'';
				COMMENT ON COLUMN tache_urbaine_2017.l_bati_agrege_etalab_'|| liste_valeur[i_table] ||'_2017.nb_batidur
				IS ''Nombre de bâtiments qualifiés de dur par la DGFiP intégrés dans cette agrégation de bâtiments.'';
				COMMENT ON COLUMN tache_urbaine_2017.l_bati_agrege_etalab_'|| liste_valeur[i_table] ||'_2017.nb_batileg
				IS ''Nombre de bâtiments qualifiés de legers par la DGFiP intégrés dans cette agrégation de bâtiments.'';
				COMMENT ON COLUMN tache_urbaine_2017.l_bati_agrege_etalab_'|| liste_valeur[i_table] ||'_2017.code_dep
				IS ''Numéro INSEE du département sur 3 charactères.'';
				COMMENT ON COLUMN tache_urbaine_2017.l_bati_agrege_etalab_'|| liste_valeur[i_table] ||'_2017.geom
				IS ''Champs géométrique : Monopolygones en Lambert93 (EPSG:2154).''; 
			'; 
		RAISE NOTICE '%', req;
		EXECUTE(req);
		COMMIT;
   END LOOP;
END $$;
"
--> Updated Rows	0

echo "------------------------------------
echo "A3] Correction des erreurs de nom
echo " 25/03/2023
echo "------------------------------------
DO $$
DECLARE
liste_valeur 				CHARACTER VARYING[];
req 						text;
BEGIN
liste_valeur := ARRAY  ['000', 'r11','r24','r27','r28','r32','r44','r52','r53','r75','r76','r84','r93','r94',
						'001','002','003','004','005','006','007','008','009','010',
						'011','012','013','014','015','016','017','018','019','02a','02b',
						'021','022','023','024','025','026','027','028','029','030',
						'031','032','033','034','035','036','037','038','039','040',
						'041','042','043','044','045','046','047','048','049','050',
						'051','052','053','054','055','056','057','058','059','060',
						'061','062','063','064','065','066','067','068','069','070',
						'071','072','073','074','075','076','077','078','079','080',
						'081','082','083','084','085','086','087','088','089','090',
						'091','092','093','094','095'
						];
FOR i_table IN 1..array_length(liste_valeur, 1) LOOP
		req := 'ALTER TABLE tache_urbaine_2017.l_bati_agrege_pepci_'|| liste_valeur[i_table] ||'_2017 RENAME TO l_bati_agrege_etalab_'|| liste_valeur[i_table] ||'_2017;';
		RAISE NOTICE '%', req;
		EXECUTE(req);
		--ROLLBACK;
		COMMIT;
END LOOP;
END $$;
--> OK

ALTER SCHEMA tache_urbaine_2017 RENAME TO p_enveloppe_bati_2017;
--> Updated Rows	0
