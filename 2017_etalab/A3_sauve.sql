# 20/10/2020
# A] Sauvegardes

# A.1] Sauvegarde Schéma :
time pg_dump --port=5432 --dbname="ceremabase" --schema="tache_urbaine_2017" --username "postgres" --role "postgres" --format=plain --encoding UTF8 --inserts --column-inserts --verbose --quote-all-identifiers --file="/mnt/data/sauve_tache_urbaine_2017.sql" 1>/mnt/data/A3_sauve_enveloppe_urbaine_2017.log 2>&1
#real	19m29,226s
#user	2m1,780s
#sys	1m24,994s



# A.2] Sauvegarde de la table finale
time pg_dump --port=5432 --dbname="ceremabase" --table="tache_urbaine_2017.l_enveloppe_bati_etalab_000_2017" --username "postgres" --role "postgres" --format=plain --encoding UTF8 --inserts --column-inserts --verbose --quote-all-identifiers --file="/mnt/data/sauve_tache_urbaine_2017.l_enveloppe_bati_etalab_000_2017.sql" 1>/mnt/data/sauve_tache_urbaine_2017.l_enveloppe_bati_etalab_000_2017.log 2>&1
#real	2m57,126s
#user	0m20,367s
#sys	0m14,819s




