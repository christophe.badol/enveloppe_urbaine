# 20/10/2020
# A] Sauvegardes

# A.1] Sauvegarde Schéma :
time pg_dump --port=5432 --dbname="ceremabase" --schema="tache_urbaine_2020" --username "postgres" --role "postgres" --format=plain --encoding UTF8 --inserts --column-inserts --verbose --quote-all-identifiers --file="/mnt/data/sauve_tache_urbaine_2020.sql" 1>/mnt/data/A3_sauve_enveloppe_urbaine_2020.log 2>&1
#real	9m26,074s
#user	1m35,467s
#sys	1m8,502s


# A.2] Sauvegarde de la table finale
time pg_dump --port=5432 --dbname="ceremabase" --table="tache_urbaine_2020.l_enveloppe_bati_etalab_000_2020" --username "postgres" --role "postgres" --format=plain --encoding UTF8 --inserts --column-inserts --verbose --quote-all-identifiers --file="/mnt/data/sauve_tache_urbaine_2020.l_enveloppe_bati_etalab_000_2020.sql" 1>/mnt/data/A3_l_enveloppe_bati_etalab_000_2020.log 2>&1
#real	1m51,522s
#user	0m16,965s
#sys	0m14,285s


# Test
time pg_dump --port=5432 --dbname="ceremabase" --table="tache_urbaine_2020.l_enveloppe_bati_etalab_000_2020" --username "postgres" --role "postgres" --format=plain --encoding UTF8 --inserts --column-inserts --verbose --quote-all-identifiers  | 7z a -an -txz -si -so > /mnt/data/sauve_tache_urbaine_2020.l_enveloppe_bati_etalab_000_2020.sql.xz
#real	32m58,186s
#user	135m14,384s
#sys	0m54,641s

