#!/bin/bash
# Tache Urbaine
commande="psql -d ceremabase -c "
millesime="2020"
cadastre="etalab"
couchebaticadastre="r_cadastre_etalab_2020.n_bati_etalab"

# Important :
# Pour tout logger taper :
# time sh /home/administrateur/gitlab/enveloppe_urbaine/2020_etalab/A1_l_bati_agrege_etalab_2020.sh 1>/mnt/data/A1_l_bati_agrege_etalab_2020.log 2>&1

# A] Bati Agrege

#for dpt in '090' '039' '025' '001' '002' '003' '004' '005' '006' '007' '008' '009' '010' '011' '012' '013' '014' '015' '016' '017' '018' '019' '021' '022' '023' '024' '026' '027' '028' '029' '030' '031' '032'
#for dpt in '033' '034' '035' '036' '037' '038' '040' '041' '042' '043' '044' '045' '046'
for dpt in '047' '048' '049' '050' '051' '052' '053' '054' '055' '056' '057' '058' '059' '060' '061' '062' '063' '064' '065' '066' '067' '068' '069' '070' '071' '072' '073' '074' '075' '076' '077' '078' '079' '080' '081' '082' '083' '084' '085' '086' '087' '088' '089' '091' '092' '093' '094' '095'
#for dpt in '02b' '02a'

do

echo $dpt
echo '----'

# A.1] Création des tables Départementales
$commande "
DROP TABLE IF EXISTS tache_urbaine_"$millesime".l_bati_agrege_etalab_"$dpt"_"$millesime";
"
$commande "
CREATE TABLE tache_urbaine_"$millesime".l_bati_agrege_etalab_"$dpt"_"$millesime"
	(
	id serial,
	nb_batidur integer,
	nb_batileg integer,
	code_dep character(3) DEFAULT '"$dpt"',  
	geom geometry(Polygon,2154)
	);
"

$commande "
ALTER SEQUENCE tache_urbaine_"$millesime".l_bati_agrege_etalab_"$dpt"_"$millesime"_id_seq
	RESTART WITH "$dpt"000001;
"

# A.2] Buffer +0,01, ST_Union, Buffer -0,01, ST_Dump
$commande "
INSERT INTO tache_urbaine_"$millesime".l_bati_agrege_etalab_"$dpt"_"$millesime" (geom)
	SELECT (ST_Dump(ST_Buffer(ST_Union(ST_Buffer(geom,0,01)),-0,01))).geom::geometry(Polygon,2154) as geom
	FROM "$couchebaticadastre"_"$dpt"_"$millesime";
"

# A.3] Mise à jour des champs nb_batidur & nb_batileg
$commande "
UPDATE tache_urbaine_"$millesime".l_bati_agrege_etalab_"$dpt"_"$millesime" as t1 SET nb_batidur = (
	SELECT count(*)
	FROM "$couchebaticadastre"_"$dpt"_"$millesime" as t2
	WHERE 	t1.code_dep = '"$dpt"'
		and (t2.type='Bâtiment en dur' or t2.type='Bâti dur' or t2.type='Bati dur' or t2.type='01')
		and ST_Intersects(t1.geom, t2.geom))
	WHERE 	t1.code_dep = '"$dpt"';
"

$commande "
UPDATE tache_urbaine_"$millesime".l_bati_agrege_etalab_"$dpt"_"$millesime" as t1 SET nb_batileg = (
	SELECT count(*)
	FROM "$couchebaticadastre"_"$dpt"_"$millesime" as t2
	WHERE 	t1.code_dep = '"$dpt"'
		and (type='Construction légère' or type='Bâti léger' or type='Bati leger' or type='02')
		and ST_Intersects(t1.geom, t2.geom))
	WHERE 	t1.code_dep = '"$dpt"';
"

# A.4] Optimisation 
# Clé primaire 
$commande "
ALTER TABLE tache_urbaine_"$millesime".l_bati_agrege_etalab_"$dpt"_"$millesime"
    ADD CONSTRAINT l_bati_agrege_etalab_"$dpt"_"$millesime"_pk PRIMARY KEY (id)
    USING INDEX TABLESPACE index;
"
# Contraintes Géométriques
$commande "
ALTER TABLE tache_urbaine_"$millesime".l_bati_agrege_etalab_"$dpt"_"$millesime"
    ADD CONSTRAINT enforce_dims_geom CHECK (ST_ndims(geom) = 2);
"
$commande "
ALTER TABLE tache_urbaine_"$millesime".l_bati_agrege_etalab_"$dpt"_"$millesime"
	ADD CONSTRAINT enforce_srid_geom CHECK (ST_Srid(geom)= 2154);
"
$commande "
ALTER TABLE tache_urbaine_"$millesime".l_bati_agrege_etalab_"$dpt"_"$millesime"
    ADD CONSTRAINT enforce_geotype_geom CHECK (geometrytype(geom) = 'POLYGON'::text);
"
# Index attributaires
$commande "
CREATE INDEX l_bati_agrege_etalab_"$dpt"_"$millesime"_nb_batidur_idx ON tache_urbaine_"$millesime".l_bati_agrege_etalab_"$dpt"_"$millesime"
	USING brin (nb_batidur) TABLESPACE index;
"
$commande "
CREATE INDEX l_bati_agrege_etalab_"$dpt"_"$millesime"_nb_batileg_idx ON tache_urbaine_"$millesime".l_bati_agrege_etalab_"$dpt"_"$millesime"
	USING brin (nb_batileg) TABLESPACE index;
"
$commande "
CREATE INDEX l_bati_agrege_etalab_"$dpt"_"$millesime"_code_dep_idx ON tache_urbaine_"$millesime".l_bati_agrege_etalab_"$dpt"_"$millesime"
	USING brin (code_dep) TABLESPACE index;
"
# Index Géomérique + Cluster
$commande "
CREATE INDEX l_bati_agrege_etalab_"$dpt"_"$millesime"_geom_gist ON tache_urbaine_"$millesime".l_bati_agrege_etalab_"$dpt"_"$millesime"
	USING gist (geom) TABLESPACE index;
"
$commande "
ALTER TABLE tache_urbaine_"$millesime".l_bati_agrege_etalab_"$dpt"_"$millesime"
	CLUSTER ON l_bati_agrege_etalab_"$dpt"_"$millesime"_geom_gist;
"

# A.5] Commentaires
# Table
$commande "
COMMENT ON TABLE tache_urbaine_"$millesime".l_bati_agrege_etalab_"$dpt"_"$millesime"
  IS 'Bâti agrégé de "$millesime" pour le département ''"$dpt"''.

Il s’agit de fusionner tous les bâtiments du cadastre "$cadastre" qui sont séparés de moins de 1 cm par un Buffer +0,01, ST_Union, Buffer -0,01, ST_Dump';
"
# Champs
$commande "
COMMENT ON COLUMN tache_urbaine_"$millesime".l_bati_agrege_etalab_"$dpt"_"$millesime".id
    IS 'Identifiant unique selon méthode COVADIS : n°département.000.000 + id.';
"
$commande "
COMMENT ON COLUMN tache_urbaine_"$millesime".l_bati_agrege_etalab_"$dpt"_"$millesime".nb_batidur
    IS 'Nombre de bâtiments qualifiés de dur par la DGFiP intégrés dans cette agrégation de bâtiments.';
"
$commande "
COMMENT ON COLUMN tache_urbaine_"$millesime".l_bati_agrege_etalab_"$dpt"_"$millesime".nb_batileg
    IS 'Nombre de bâtiments qualifiés de legers par la DGFiP intégrés dans cette agrégation de bâtiments.';
"
$commande "
COMMENT ON COLUMN tache_urbaine_"$millesime".l_bati_agrege_etalab_"$dpt"_"$millesime".code_dep
    IS 'Numéro INSEE du département sur 3 charactères.';
"
$commande "
COMMENT ON COLUMN tache_urbaine_"$millesime".l_bati_agrege_etalab_"$dpt"_"$millesime".geom
    IS 'Champs géométrique : Monopolygones en Lambert93 (EPSG:2154).';
"

done
