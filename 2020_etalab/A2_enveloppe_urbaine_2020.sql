#!/bin/bash
# "A.2] Travail sur la table nationale Enveloppe Urbaine
commande="psql -d ceremabase -c "
millesime="2020"
schemaorigine="tache_urbaine_"$millesime

# Important :
# Pour tout logger taper :
# time sh /home/administrateur/Documents/A2_enveloppe_urbaine_2020.sh 1>/mnt/data/02_enveloppe_urbaine_2020_A2.log 2>&1

echo "A.2.1] Création de la table temporaire d addition des départements"
now=$(date +"%T")
echo "Début à $now"
echo "-------------------------------------------------------------------"
$commande "
DROP TABLE IF EXISTS "$schemaorigine".temp_enveloppe_bati_etalab_000_"$millesime"
"
$commande "
CREATE TABLE "$schemaorigine".temp_enveloppe_bati_etalab_000_"$millesime"
(
	code_dep character(3),
	geom geometry(Polygon,2154)
);
"

echo "A.2.2] Ajout des départements dans la table nationale temporaire"
#DEBUG
#for dpt in '001' '038' '069'
for dpt in '001' '002' '003' '004' '005' '006' '007' '008' '009' '010' '011' '012' '013' '014' '015' '016' '017' '018' '019' '02b' '02a' '021' '022' '023' '024' '025' '026' '027' '028' '029' '030' '031' '032' '033' '034' '035' '036' '037' '038' '039' '040' '041' '042' '043' '044' '045' '046' '047' '048' '049' '050' '051' '052' '053' '054' '055' '056' '057' '058' '059' '060' '061' '062' '063' '064' '065' '066' '067' '068' '069' '070' '071' '072' '073' '074' '075' '076' '077' '078' '079' '080' '081' '082' '083' '084' '085' '086' '087' '088' '089' '090' '091' '092' '093' '094' '095'

do

echo "Département $dpt"
$commande "
INSERT INTO "$schemaorigine".temp_enveloppe_bati_etalab_000_"$millesime" (code_dep, geom)
	SELECT code_dep, geom
	FROM "$schemaorigine".l_enveloppe_bati_etalab_"$dpt"_"$millesime";
"

done

echo "A.2.3] Préparation de la phase A.2.4"
now=$(date +"%T")
echo "Début à $now"
echo "-------------------------------------"
echo "A.2.3.1] Index Géomérique"

$commande "
CREATE INDEX temp_enveloppe_bati_etalab_000_"$millesime"_geom_gist ON "$schemaorigine".temp_enveloppe_bati_etalab_000_"$millesime"
	USING gist (geom) TABLESPACE index;
"
echo "A.2.3.2] Identifiant pour la phase A.3"
$commande "
ALTER TABLE "$schemaorigine".temp_enveloppe_bati_etalab_000_"$millesime"
	ADD COLUMN id serial;
"
echo "A.2.3.3] Sauvegarde en cas d echec de la A.3"
$commande "
DROP TABLE IF EXISTS "$schemaorigine".sauve_temp_enveloppe_bati_etalab_000_"$millesime";
CREATE TABLE "$schemaorigine".sauve_temp_enveloppe_bati_etalab_000_"$millesime" AS
	SELECT * FROM "$schemaorigine".temp_enveloppe_bati_etalab_000_"$millesime";
"


echo "A.2.4] Création de la table nationale"
now=$(date +"%T")
echo "Début à $now"
echo "------------------------------------"
$commande "
DROP TABLE IF EXISTS "$schemaorigine".l_enveloppe_bati_etalab_000_"$millesime";
CREATE TABLE "$schemaorigine".l_enveloppe_bati_etalab_000_"$millesime"
(
	id_enveloppe serial,
	nb_batidur integer,
	nb_batileg integer,
	surf_batidur integer,
	surf_batileg integer,
	list_code_dep varchar,
	geom geometry(Polygon,2154)
);
"

echo "A.2.4.1] Sélection des enveloppes qui s intersectent"
now=$(date +"%T")
echo "Début à $now"
echo "------------------------------------"
$commande "
DROP TABLE IF EXISTS "$schemaorigine".stintersects_enveloppe_bati_etalab_000_"$millesime";
CREATE TABLE "$schemaorigine".stintersects_enveloppe_bati_etalab_000_"$millesime" AS
	SELECT ST_Union(t1.geom,t2.geom)::geometry(Polygon,2154) as geometrie_union
		FROM "$schemaorigine".temp_enveloppe_bati_etalab_000_"$millesime" as t1
		JOIN "$schemaorigine".temp_enveloppe_bati_etalab_000_"$millesime" as t2
		ON ST_Intersects(t1.geom, t2.geom)
		WHERE t1.id < t2.id;
-->
" 
$commande "
CREATE INDEX stintersects_enveloppe_bati_etalab_000_"$millesime"_geom_gist
	ON "$schemaorigine".stintersects_enveloppe_bati_etalab_000_"$millesime"
	USING gist (geometrie_union) TABLESPACE index;
"

echo "A.2.4.2] Peuplement par agregation des zones de chevauchement interdépartementales"
now=$(date +"%T")
echo "Début à $now"
echo "------------------------------------"
$commande "
INSERT INTO "$schemaorigine".l_enveloppe_bati_etalab_000_"$millesime" (geom)
SELECT (ST_Dump(ST_Union(geometrie_union))).geom::geometry(Polygon,2154) as geom
		FROM "$schemaorigine".stintersects_enveloppe_bati_etalab_000_"$millesime";
--> INSERT 0 25040 / Requête exécutée avec succès en 43 hr 58 min.
"

echo "A.2.4.3] Suppression des zones de des zones de"
echo "         chevauchement interdépartementales que l on vient d inserer"
now=$(date +"%T")
echo "Début à $now"
echo "---------------------------------------------------------------------"
$commande "
DELETE FROM "$schemaorigine".temp_enveloppe_bati_etalab_000_"$millesime" WHERE id IN (
		(SELECT t1.id
		FROM "$schemaorigine".temp_enveloppe_bati_etalab_000_"$millesime" as t1
		JOIN "$schemaorigine".temp_enveloppe_bati_etalab_000_"$millesime" as t2
		ON ST_Intersects(t1.geom, t2.geom)
		WHERE t1.id < t2.id)
	UNION ALL
		(SELECT t2.id
		FROM "$schemaorigine".temp_enveloppe_bati_etalab_000_"$millesime" as t1
		JOIN "$schemaorigine".temp_enveloppe_bati_etalab_000_"$millesime" as t2
		ON ST_Intersects(t1.geom, t2.geom)
		WHERE t1.id < t2.id)
);
--> 
"

echo "---------------------------------------------------------"
echo "A.2.4.4] Insersion du reste : ce qui ne se supperpose pas :"
echo "         On vérifie qu'il n'y a rien qui s intersecte"
echo "---------------------------------------------------------"
$commande "
CREATE INDEX l_enveloppe_bati_etalab_000_"$millesime"_geom_gist
	ON "$schemaorigine".l_enveloppe_bati_etalab_000_"$millesime"
	USING gist (geom) TABLESPACE index;

--SELECT *
--FROM "$schemaorigine".temp_enveloppe_bati_etalab_000_"$millesime" as t1
--JOIN "$schemaorigine".l_enveloppe_bati_etalab_000_"$millesime" as t2
--ON ST_Intersects(t1.geom, t2.geom);
-->
"
echo "---------------------------------------------------------"
$commande "
DROP INDEX "$schemaorigine".l_enveloppe_bati_etalab_000_"$millesime"_geom_gist;
"

echo "A.2.4.5] On insere !"
$commande "
INSERT INTO "$schemaorigine".l_enveloppe_bati_etalab_000_"$millesime" (geom)
	SELECT geom::geometry(Polygon,2154) as geom
	FROM "$schemaorigine".temp_enveloppe_bati_etalab_000_"$millesime";
--> INSERT 0 1535920 / Requête exécutée avec succès en 2 min 29 secs.
"

echo "A.2.4.6] Index Géomérique + Cluster"
$commande "
CREATE INDEX l_enveloppe_bati_etalab_000_"$millesime"_geom_gist ON "$schemaorigine".l_enveloppe_bati_etalab_000_"$millesime"
	USING gist (geom) TABLESPACE index;
ALTER TABLE "$schemaorigine".l_enveloppe_bati_etalab_000_"$millesime"
	CLUSTER ON l_enveloppe_bati_etalab_000_"$millesime"_geom_gist;
"
$commande "
CREATE INDEX sauve_temp_enveloppe_bati_etalab_000_"$millesime"_geom_gist ON "$schemaorigine".sauve_temp_enveloppe_bati_etalab_000_"$millesime"
	USING gist (geom) TABLESPACE index;
ALTER TABLE "$schemaorigine".sauve_temp_enveloppe_bati_etalab_000_"$millesime"
	CLUSTER ON sauve_temp_enveloppe_bati_etalab_000_"$millesime"_geom_gist;
"
#DEBUG : Pas fait sur le referentiel cadastre ETALAB 2020 ! 
#$commande "
#CREATE INDEX n_bati_etalab_000_"$millesime"_type_idx
#    ON r_cadastre_etalab_"$millesime".n_bati_etalab_000_"$millesime" USING brin
#    (type)
#    TABLESPACE index;
#"

echo "A.2.5] Mise à jour des champs"
echo "A.2.5.1] Mise à jour des champs nb_batidur"
now=$(date +"%T")
echo "Début à $now"
echo "------------------------------------"
$commande "
UPDATE "$schemaorigine".l_enveloppe_bati_etalab_000_"$millesime" as t1 SET nb_batidur = (
	SELECT count(*)
	FROM r_cadastre_etalab_"$millesime".n_bati_etalab_000_"$millesime" as t2
	WHERE	ST_Intersects(t1.geom, t2.geom)
	AND (t2.type='Bâtiment en dur' or t2.type='Bâti dur' or t2.type='Bati dur' or t2.type='01')
	);
--> 
"

echo "A.2.5.2] Mise à jour des champs nb_batileg"
now=$(date +"%T")
echo "Début à $now"
echo "------------------------------------"
$commande "
UPDATE "$schemaorigine".l_enveloppe_bati_etalab_000_"$millesime" as t1 SET nb_batileg = (
	SELECT count(*)
	FROM r_cadastre_etalab_"$millesime".n_bati_etalab_000_"$millesime" as t2
	WHERE	ST_Intersects(t1.geom, t2.geom)
	AND	(t2.type='Construction légère' or t2.type='Bâti léger' or t2.type='Bati leger' or t2.type='02')
	);
"

echo "A.2.5.3 Peuplement du champs surf_batidur + index"
$commande "
UPDATE "$schemaorigine".l_enveloppe_bati_etalab_000_"$millesime" as t1 SET surf_batidur = (
	SELECT sum(ST_Area(t2.geom))
	FROM r_cadastre_etalab_"$millesime".n_bati_etalab_000_"$millesime" as t2
	WHERE 	
		ST_Intersects(t1.geom, t2.geom)
		AND 
		(t2.type='01' or t2.type='Bâtiment en dur' or t2.type='Bâti dur' or t2.type='Bati dur')
	);
"

echo "A.2.5.4  Peuplement du champs surf_batileg + index"
$commande "
UPDATE "$schemaorigine".l_enveloppe_bati_etalab_000_"$millesime" as t1 SET surf_batileg = (
	SELECT sum(ST_Area(t2.geom))
	FROM r_cadastre_etalab_"$millesime".n_bati_etalab_000_"$millesime" as t2
	WHERE 	
		ST_Intersects(t1.geom, t2.geom)
		AND
		(t2.type='02' or t2.type='Construction légère' or t2.type='Bâti léger' or t2.type='Bati leger')
	);
"

echo "A.2.5.5] Mise à jour du champs list_code_dep"
now=$(date +"%T")
echo "Début à $now"
echo "------------------------------------"
#DEBUG : si plusieurs taches fusionnées d une même département on a un doublon
#$commande "
#UPDATE "$schemaorigine".l_enveloppe_bati_etalab_000_"$millesime" as t1 SET list_code_dep = (
#	SELECT array_agg(code_dep)::varchar
#	FROM "$schemaorigine".sauve_temp_enveloppe_bati_etalab_000_"$millesime" as t2
#	WHERE	ST_Intersects(t1.geom, t2.geom)
#	);
#--> UPDATE 38360 / Requête exécutée avec succès en 1 min 25 secs.
#"

#DEBUG : Enleve les doublons de départements, mais on perds l information de la fusion de plusieurs tache dans le même département
$commande "
UPDATE "$schemaorigine".l_enveloppe_bati_etalab_000_"$millesime" as t1 SET list_code_dep = (
	WITH resultat1 AS 
		(SELECT DISTINCT t2.code_dep
		FROM "$schemaorigine".sauve_temp_enveloppe_bati_etalab_000_"$millesime" as t2
		WHERE ST_Intersects(t1.geom, t2.geom)
		ORDER BY t2.code_dep)
	SELECT array_agg(code_dep)::varchar FROM resultat1
	);
--> 
"

echo "A.2.6] Optimisations"
now=$(date +"%T")
echo "Début à $now"
echo "------------------------------------"
echo "A.2.6.1] Clé primaire"
$commande "
ALTER TABLE "$schemaorigine".l_enveloppe_bati_etalab_000_"$millesime"
    ADD CONSTRAINT l_enveloppe_bati_etalab_000_"$millesime"_id_enveloppe_pk PRIMARY KEY (id_enveloppe)
    USING INDEX TABLESPACE index;
ALTER TABLE "$schemaorigine".l_enveloppe_bati_etalab_000_"$millesime"
    ADD CONSTRAINT enforce_dims_geom CHECK (ST_ndims(geom) = 2);
ALTER TABLE "$schemaorigine".l_enveloppe_bati_etalab_000_"$millesime"
	ADD CONSTRAINT enforce_srid_geom CHECK (ST_Srid(geom)= 2154);
ALTER TABLE "$schemaorigine".l_enveloppe_bati_etalab_000_"$millesime"
    ADD CONSTRAINT enforce_geotype_geom CHECK (geometrytype(geom) = 'POLYGON'::text);
"
echo "A.2.6.2] Index attributaires"
$commande "
CREATE INDEX l_enveloppe_bati_etalab_000_"$millesime"_nb_batidur_idx ON "$schemaorigine".l_enveloppe_bati_etalab_000_"$millesime"
	USING brin (nb_batidur) TABLESPACE index;
CREATE INDEX l_enveloppe_bati_etalab_000_"$millesime"_nb_batileg_idx ON "$schemaorigine".l_enveloppe_bati_etalab_000_"$millesime"
	USING brin (nb_batileg) TABLESPACE index;
CREATE INDEX l_enveloppe_bati_etalab_000_"$millesime"_surf_batidur_idx ON "$schemaorigine".l_enveloppe_bati_etalab_000_"$millesime"
	USING brin (surf_batidur) TABLESPACE index;
CREATE INDEX l_enveloppe_bati_etalab_000_"$millesime"_surf_batileg_idx ON "$schemaorigine".l_enveloppe_bati_etalab_000_"$millesime"
	USING brin (surf_batileg) TABLESPACE index;
CREATE INDEX l_enveloppe_bati_etalab_000_"$millesime"_list_code_dep_idx ON "$schemaorigine".l_enveloppe_bati_etalab_000_"$millesime"
	USING brin (list_code_dep) TABLESPACE index;
"

echo "A.2.6.3] Index Géomérique + Cluster"
#DEBUG Déjà fait en partie # B.2.1]

echo "A.2.6.4] Commentaires"
$commande "
COMMENT ON TABLE "$schemaorigine".l_enveloppe_bati_etalab_000_"$millesime"
  IS 'Enveloppe nationale du bâti de 2020.

Il s’agit de fusionner tous les bâtiments issu du cadastre ETALAB qui sont séparés de moins de 50 m par un Buffer +50, ST_Union, Erosion de 40m';
"
$commande "
COMMENT ON COLUMN "$schemaorigine".l_enveloppe_bati_etalab_000_"$millesime".id_enveloppe
    IS 'Identifiant national unique pour une même enveloppe du bati';
COMMENT ON COLUMN "$schemaorigine".l_enveloppe_bati_etalab_000_"$millesime".nb_batidur
    IS 'Nombre de bâtiments qualifiés de dur par la DGFiP intégrés dans cette enveloppe du bâti.';
COMMENT ON COLUMN "$schemaorigine".l_enveloppe_bati_etalab_000_"$millesime".nb_batileg
    IS 'Nombre de bâtiments qualifiés de leger par la DGFiP intégrés dans cette enveloppe du bâti.';
COMMENT ON COLUMN "$schemaorigine".l_enveloppe_bati_etalab_000_"$millesime".surf_batidur
    IS 'Surface totale en m2 de tous les bâtiments qualifiés de dur par la DGFiP et présents dans cette enveloppe du bâti.';
COMMENT ON COLUMN "$schemaorigine".l_enveloppe_bati_etalab_000_"$millesime".surf_batileg
    IS 'Surface totale en m2 de tous les bâtiments qualifiés de leger par la DGFiP et présents dans cette enveloppe du bâti.';	
COMMENT ON COLUMN "$schemaorigine".l_enveloppe_bati_etalab_000_"$millesime".list_code_dep
    IS 'Liste des départements concernés par cette enveloppe du bâti (doublons enlevés)';
COMMENT ON COLUMN "$schemaorigine".l_enveloppe_bati_etalab_000_"$millesime".geom
    IS 'Champs géométrique : Monopolygone en Lambert93 (EPSG:2154).';
"

echo "A.2.7] Netoyage des tables temporaires"
now=$(date +"%T")
echo "Début à $now"
echo "------------------------------------"
$commande "
DROP TABLE IF EXISTS "$schemaorigine".temp_departement_buffer_200m_000_"$millesime";
----#DEBUG : on garde pour vérifier 
--DROP TABLE IF EXISTS "$schemaorigine".temp_enveloppe_bati_etalab_000_"$millesime";
DROP TABLE IF EXISTS "$schemaorigine".stintersects_enveloppe_bati_etalab_000_"$millesime";
"

# time sh /home/administrateur/Documents/A2_enveloppe_urbaine_2020.sh 1>/mnt/data/02_enveloppe_urbaine_2020_A2.log 2>&1
# fait en deux partie car impossible de faire A.2.5.4  Peuplement du champs surf_batileg + index"
# OK le 3 avril 2020

# Quelques corrections :
COMMENT ON TABLE tache_urbaine_2020.l_enveloppe_bati_etalab_000_2020 IS 'Enveloppe nationale du bâti de 2020.
Il s’agit de fusionner tous les bâtiments issu du cadastre ETALAB qui sont séparés de moins de 50 m par un Buffer +50, ST_Union, Erosion de 40m.

Reste cet UPDATE à faire :
UPDATE tache_urbaine_2020.l_enveloppe_bati_etalab_000_2020 as t1 SET surf_batileg = (
	SELECT sum(ST_Area(t2.geom))
	FROM r_cadastre_etalab_2020.n_bati_etalab_000_2020 as t2
	WHERE 	
		ST_Intersects(t1.geom, t2.geom)
		AND
		(t2.type=''02'' or t2.type=''Construction légère'' or t2.type=''Bâti léger'' or t2.type=''Bati leger'')
	);

Il s’agit de fusionner tous les bâtiments issu du cadastre ETALAB qui sont séparés de moins de 50 m par un Buffer +50, ST_Union, Erosion de 40m';

COMMENT ON COLUMN tache_urbaine_2020.l_enveloppe_bati_etalab_000_2020.list_code_dep
    IS 'Liste des départements concernés par cette enveloppe du bâti (doublons enlevés)';
   
# 16 mai 2020
# Mise aux normes des tables départementales :
DO $$
DECLARE
liste_valeur 				CHARACTER VARYING[];
req 						text;
BEGIN
--liste_valeur := ARRAY  ['090'];
liste_valeur := ARRAY  [
						'001','002','003','004','005','006','007','008','009','010',
						'011','012','013','014','015','016','017','018','019','02a','02b',
						'021','022','023','024','025','026','027','028','029','030',
						'031','032','033','034','035','036','037','038','039','040',
						'041','042','043','044','045','046','047','048','049','050',
						'051','052','053','054','055','056','057','058','059','060',
						'061','062','063','064','065','066','067','068','069','070',
						'071','072','073','074','075','076','077','078','079','080',
						'081','082','083','084','085','086','087','088','089',
						'091','092','093','094','095'
						];
FOR i_table IN 1..array_length(liste_valeur, 1)
LOOP
		req := '
				ALTER TABLE tache_urbaine_2020.l_enveloppe_bati_etalab_'|| liste_valeur[i_table] ||'_2020 RENAME COLUMN nbatidur TO nb_batidur;
				ALTER TABLE tache_urbaine_2020.l_enveloppe_bati_etalab_'|| liste_valeur[i_table] ||'_2020 RENAME COLUMN nbatileg TO nb_batileg;
				ALTER TABLE tache_urbaine_2020.l_enveloppe_bati_etalab_'|| liste_valeur[i_table] ||'_2020
							ADD column surf_batidur integer,
							ADD column surf_batileg integer;
				
				COMMENT ON COLUMN tache_urbaine_2020.l_enveloppe_bati_etalab_'|| liste_valeur[i_table] ||'_2020.surf_batidur
				    IS ''Surface totale en m2 de tous les bâtiments qualifiés de dur par la DGFiP et présents dans cette enveloppe du bâti.'';
				COMMENT ON COLUMN tache_urbaine_2020.l_enveloppe_bati_etalab_'|| liste_valeur[i_table] ||'_2020.surf_batileg
				    IS ''Surface totale en m2 de tous les bâtiments qualifiés de leger par la DGFiP et présents dans cette enveloppe du bâti.'';	

				UPDATE tache_urbaine_2020.l_enveloppe_bati_etalab_'|| liste_valeur[i_table] ||'_2020 as t1 SET surf_batidur = (
					SELECT sum(ST_Area(t2.geom))
					FROM r_cadastre_etalab_2020.n_bati_etalab_000_2020 as t2
					WHERE 	
						ST_Intersects(t1.geom, t2.geom)
						AND 
						(t2.type=''01'' or t2.type=''Bâtiment en dur'' or t2.type=''Bâti dur'' or t2.type=''Bati dur'')
					);

				UPDATE tache_urbaine_2020.l_enveloppe_bati_etalab_'|| liste_valeur[i_table] ||'_2020 as t1 SET surf_batileg = (
					SELECT sum(ST_Area(t2.geom))
					FROM r_cadastre_etalab_2020.n_bati_etalab_000_2020 as t2
					WHERE 	
						ST_Intersects(t1.geom, t2.geom)
						AND
						(t2.type=''02'' or t2.type=''Construction légère'' or t2.type=''Bâti léger'' or t2.type=''Bati leger'')
					);				

				CREATE INDEX l_enveloppe_bati_etalab_'|| liste_valeur[i_table] ||'_2020_surf_batidur_idx
						ON tache_urbaine_2020.l_enveloppe_bati_etalab_'|| liste_valeur[i_table] ||'_2020 USING brin (surf_batidur);
				CREATE INDEX l_enveloppe_bati_etalab_'|| liste_valeur[i_table] ||'_2020_surf_batileg_idx
						ON tache_urbaine_2020.l_enveloppe_bati_etalab_'|| liste_valeur[i_table] ||'_2020 USING brin (surf_batileg);
				';
		RAISE NOTICE '%', req;
		EXECUTE(req);
		COMMIT;
END LOOP;
END $$;
# Finish time	Sun May 17 10:11:25 CEST 2020

---- 27 juin 2020
---- Ajout nouvelle contraintes Géométriques :
DO $$
DECLARE
liste_valeur 				CHARACTER VARYING[];
req 						text;
BEGIN
liste_valeur := ARRAY  [--'000',
						--'r11','r24','r27','r28','r32','r44','r52','r53','r75','r76','r84','r93','r94',
						'001','002','003','004','005','006','007','008','009','010',
						'011','012','013','014','015','016','017','018','019','02a','02b',
						'021','022','023','024','025','026','027','028','029','030',
						'031','032','033','034','035','036','037','038','039','040',
						'041','042','043','044','045','046','047','048','049','050',
						'051','052','053','054','055','056','057','058','059','060',
						'061','062','063','064','065','066','067','068','069','070',
						'071','072','073','074','075','076','077','078','079','080',
						'081','082','083','084','085','086','087','088','089','090',
						'091','092','093','094','095'--,'971','972','973','974','976'
						];
FOR i_table IN 1..array_length(liste_valeur, 1) LOOP
		req := '
				ALTER TABLE tache_urbaine_2020.l_enveloppe_bati_etalab_'|| liste_valeur[i_table] ||'_2020 ADD CONSTRAINT enforce_validite_geom CHECK (ST_IsValid(geom));
				ALTER TABLE tache_urbaine_2020.l_enveloppe_bati_etalab_'|| liste_valeur[i_table] ||'_2020 ADD CONSTRAINT enforce_no_self_intersection_geom CHECK (ST_IsSimple(geom));
				ALTER TABLE tache_urbaine_2020.l_enveloppe_bati_etalab_'|| liste_valeur[i_table] ||'_2020 ADD CONSTRAINT enforce_no_zero_area_geom CHECK (ST_Area(geom) > 0);
				';
		RAISE NOTICE '%', req;
		EXECUTE(req);
		COMMIT;
END LOOP;
END $$;

------------------------------------------------------------
---- B] Synthèse
------------------------------------------------------------
---- 08/05/2021
---- https://gitlab.cerema.fr/centre-est/admin_ceremabase/-/blob/master/fonctions/synthese_geometrie_schema.sql
CREATE table tache_urbaine_2020.synthese_geometrie_schema AS
SELECT * FROM w_fonctions.synthese_geometrie_schema('tache_urbaine_2020');
--> Updated Rows	207