#!/bin/bash
# "A.2] Travail sur la table nationale Enveloppe Urbaine
commande="psql -d ceremabase -c "
millesime="2020"
schemaorigine="tache_urbaine_"$millesime

# Important :
# Pour tout logger taper :
# time sh /home/administrateur/Documents/A2_enveloppe_urbaine_2020.sh 1>/mnt/data/02_enveloppe_urbaine_2020_A2.log 2>&1

echo "A.2.5.4  Peuplement du champs surf_batileg + index"
$commande "
---- Combien de tache
---- select count(geom) from tache_urbaine_2020.l_enveloppe_bati_etalab_000_2020;
--> 1589625

DROP INDEX tache_urbaine_2020.l_enveloppe_bati_etalab_000_2020_surf_batileg_idx;

UPDATE tache_urbaine_2020.l_enveloppe_bati_etalab_000_2020 as t1 SET surf_batileg = (
	SELECT sum(ST_Area(t2.geom))
	FROM r_cadastre_etalab_2020.n_bati_etalab_000_2020 as t2
	WHERE 	
		ST_Intersects(t1.geom, t2.geom)
		AND
		(t2.type='02' or t2.type='Construction légère' or t2.type='Bâti léger' or t2.type='Bati leger')
	)
	where t1.id_enveloppe < 500000;
--> Updated Rows	499999
UPDATE tache_urbaine_2020.l_enveloppe_bati_etalab_000_2020 as t1 SET surf_batileg = (
	SELECT sum(ST_Area(t2.geom))
	FROM r_cadastre_etalab_2020.n_bati_etalab_000_2020 as t2
	WHERE 	
		ST_Intersects(t1.geom, t2.geom)
		AND
		(t2.type='02' or t2.type='Construction légère' or t2.type='Bâti léger' or t2.type='Bati leger')
	)
	where t1.id_enveloppe > 1000000;
--> UUpdated Rows	589625	
UPDATE tache_urbaine_2020.l_enveloppe_bati_etalab_000_2020 as t1 SET surf_batileg = (
	SELECT sum(ST_Area(t2.geom))
	FROM r_cadastre_etalab_2020.n_bati_etalab_000_2020 as t2
	WHERE 	
		ST_Intersects(t1.geom, t2.geom)
		AND
		(t2.type='02' or t2.type='Construction légère' or t2.type='Bâti léger' or t2.type='Bati leger')
	)
	where t1.id_enveloppe > 499999 and t1.id_enveloppe < 1000001;
--> Updated Rows	500001	

CREATE INDEX l_enveloppe_bati_etalab_000_2020_surf_batileg_idx ON tache_urbaine_2020.l_enveloppe_bati_etalab_000_2020 USING brin (surf_batileg);

"

echo "A.2.5.5] Mise à jour du champs list_code_dep"
now=$(date +"%T")
echo "Début à $now"
echo "------------------------------------"
#DEBUG : si plusieurs taches fusionnées d'une même département on a un doublon
#$commande "
#UPDATE "$schemaorigine".l_enveloppe_bati_etalab_000_"$millesime" as t1 SET list_code_dep = (
#	SELECT array_agg(code_dep)::varchar
#	FROM "$schemaorigine".sauve_temp_enveloppe_bati_etalab_000_"$millesime" as t2
#	WHERE	ST_Intersects(t1.geom, t2.geom)
#	);
#--> UPDATE 38360 / Requête exécutée avec succès en 1 min 25 secs.
#"

#DEBUG : Enleve les doublons de départements, mais on perds l'information de la fusion de plusieurs tache dans le même département
$commande "
UPDATE "$schemaorigine".l_enveloppe_bati_etalab_000_"$millesime" as t1 SET list_code_dep = (
	WITH resultat1 AS 
		(SELECT DISTINCT t2.code_dep
		FROM "$schemaorigine".sauve_temp_enveloppe_bati_etalab_000_"$millesime" as t2
		WHERE ST_Intersects(t1.geom, t2.geom)
		ORDER BY t2.code_dep)
	SELECT array_agg(code_dep)::varchar FROM resultat1
	);
--> 
"

echo "A.2.6] Optimisations"
now=$(date +"%T")
echo "Début à $now"
echo "------------------------------------"
echo "A.2.6.1] Clé primaire"
$commande "
ALTER TABLE "$schemaorigine".l_enveloppe_bati_etalab_000_"$millesime"
    ADD CONSTRAINT l_enveloppe_bati_etalab_000_"$millesime"_id_enveloppe_pk PRIMARY KEY (id_enveloppe)
    USING INDEX TABLESPACE index;
ALTER TABLE "$schemaorigine".l_enveloppe_bati_etalab_000_"$millesime"
    ADD CONSTRAINT enforce_dims_geom CHECK (ST_ndims(geom) = 2);
ALTER TABLE "$schemaorigine".l_enveloppe_bati_etalab_000_"$millesime"
	ADD CONSTRAINT enforce_srid_geom CHECK (ST_Srid(geom)= 2154);
ALTER TABLE "$schemaorigine".l_enveloppe_bati_etalab_000_"$millesime"
    ADD CONSTRAINT enforce_geotype_geom CHECK (geometrytype(geom) = 'POLYGON'::text);
"
echo "A.2.6.2] Index attributaires"
$commande "
CREATE INDEX l_enveloppe_bati_etalab_000_"$millesime"_nb_batidur_idx ON "$schemaorigine".l_enveloppe_bati_etalab_000_"$millesime"
	USING brin (nb_batidur) TABLESPACE index;
CREATE INDEX l_enveloppe_bati_etalab_000_"$millesime"_nb_batileg_idx ON "$schemaorigine".l_enveloppe_bati_etalab_000_"$millesime"
	USING brin (nb_batileg) TABLESPACE index;
CREATE INDEX l_enveloppe_bati_etalab_000_"$millesime"_surf_batidur_idx ON "$schemaorigine".l_enveloppe_bati_etalab_000_"$millesime"
	USING brin (surf_batidur) TABLESPACE index;
CREATE INDEX l_enveloppe_bati_etalab_000_"$millesime"_surf_batileg_idx ON "$schemaorigine".l_enveloppe_bati_etalab_000_"$millesime"
	USING brin (surf_batileg) TABLESPACE index;
CREATE INDEX l_enveloppe_bati_etalab_000_"$millesime"_list_code_dep_idx ON "$schemaorigine".l_enveloppe_bati_etalab_000_"$millesime"
	USING brin (list_code_dep) TABLESPACE index;
"

echo "A.2.6.3] Index Géomérique + Cluster"
#DEBUG Déjà fait en partie # B.2.1]

echo "A.2.6.4] Commentaires"
$commande "
COMMENT ON TABLE "$schemaorigine".l_enveloppe_bati_etalab_000_"$millesime"
  IS 'Enveloppe nationale du bâti de 2017.

Il s’agit de fusionner tous les bâtiments issu du cadastre ETALAB qui sont séparés de moins de 50 m par un Buffer +50, ST_Union, Erosion de 40m';
"
$commande "
COMMENT ON COLUMN "$schemaorigine".l_enveloppe_bati_etalab_000_"$millesime".id_enveloppe
    IS 'Identifiant national unique pour une même enveloppe du bati';
COMMENT ON COLUMN "$schemaorigine".l_enveloppe_bati_etalab_000_"$millesime".nb_batidur
    IS 'Nombre de bâtiments qualifiés de dur par la DGFiP intégrés dans cette enveloppe du bâti.';
COMMENT ON COLUMN "$schemaorigine".l_enveloppe_bati_etalab_000_"$millesime".nb_batileg
    IS 'Nombre de bâtiments qualifiés de leger par la DGFiP intégrés dans cette enveloppe du bâti.';
COMMENT ON COLUMN "$schemaorigine".l_enveloppe_bati_etalab_000_"$millesime".surf_batidur
    IS 'Surface totale en m2 de tous les bâtiments qualifiés de dur par la DGFiP et présents dans cette enveloppe du bâti.';
COMMENT ON COLUMN "$schemaorigine".l_enveloppe_bati_etalab_000_"$millesime".surf_batileg
    IS 'Surface totale en m2 de tous les bâtiments qualifiés de leger par la DGFiP et présents dans cette enveloppe du bâti.';	
COMMENT ON COLUMN "$schemaorigine".l_enveloppe_bati_etalab_000_"$millesime".geom
    IS 'Champs géométrique : Monopolygone en Lambert93 (EPSG:2154).';
"

echo "A.2.7] Netoyage des tables temporaires"
now=$(date +"%T")
echo "Début à $now"
echo "------------------------------------"
$commande "
DROP TABLE IF EXISTS "$schemaorigine".temp_departement_buffer_200m_000_"$millesime";
----#DEBUG : on garde pour vérifier 
--DROP TABLE IF EXISTS "$schemaorigine".temp_enveloppe_bati_etalab_000_"$millesime";
DROP TABLE IF EXISTS "$schemaorigine".stintersects_enveloppe_bati_etalab_000_"$millesime";
"

