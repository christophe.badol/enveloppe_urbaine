#!/bin/bash
# Tache Urbaine

#psql ceremabase -c "
#CREATE TABLE tache_urbaine_2019.temp_departement_buffer_200m_000_2019 AS
#	SELECT code_dep, ST_Multi(ST_Buffer(ST_Union(geom),200))::geometry(MultiPolygon,2154) as geom
#	FROM r_parcellaire_express.n_commune_pepci_000
#	GROUP BY code_dep;

#CREATE INDEX temp_departement_buffer_200m_000_2019_code_dep_idx
#    ON tache_urbaine_2019.temp_departement_buffer_200m_000_2019 USING brin
#    (code_dep)
#    TABLESPACE index;

#CREATE INDEX temp_departement_buffer_200m_000_2019_geom_gist
#    ON tache_urbaine_2019.temp_departement_buffer_200m_000_2019 USING gist
#    (geom)
#    TABLESPACE index;

#ALTER TABLE tache_urbaine_2019.temp_departement_buffer_200m_000_2019
#    CLUSTER ON temp_departement_buffer_200m_000_2019_geom_gist;
#"

# B] Tache Urbaine
# B.1] Création de la table temporaire d'addition des départements
psql ceremabase -c "DROP TABLE IF EXISTS tache_urbaine_2019.temp_enveloppe_bati_pepci_2019;"
psql ceremabase -c "CREATE TABLE tache_urbaine_2019.temp_enveloppe_bati_pepci_2019
(
	geom geometry(Polygon,2154)
);"

#for dpt in '090' '039' '025' 
for dpt in '001' '002' '003' '004' '005' '006' '007' '008' '009' '010' '011' '012' '013' '014' '015' '016' '017' '018' '019' '02b' '02a' '021' '022' '023' '024' '025' '026' '027' '028' '029' '030' '031' '032' '033' '034' '035' '036' '037' '038' '039' '040' '041' '042' '043' '044' '045' '046' '047' '048' '049' '050' '051' '052' '053' '054' '055' '056' '057' '058' '059' '060' '061' '062' '063' '064' '065' '066' '067' '068' '069' '070' '071' '072' '073' '074' '075' '076' '077' '078' '079' '080' '081' '082' '083' '084' '085' '086' '087' '088' '089' '090' '091' '092' '093' '094' '095'

do

# Buffer de +50m / Aggregation / Erosion de 40m pour chaque département
psql ceremabase -c "
INSERT INTO tache_urbaine_2019.temp_enveloppe_bati_pepci_2019 (geom)
	SELECT (ST_Dump(ST_Buffer(ST_Union(ST_Buffer(t1.geom,50)),-40))).geom::geometry(Polygon,2154) as geom
	FROM r_parcellaire_express.n_batiment_pepci_000 as t1
	JOIN tache_urbaine_2019.temp_departement_buffer_200m_000_2019 as t2
	ON ST_Intersects (t1.geom, t2.geom)
	WHERE t2.code_dep = '"$dpt"';
"

done

# Spécificité pour le 59/Nord - Trop Gros département
psql ceremabase -c "
CREATE TABLE tache_urbaine_2019.temp_commune_buffer_100m_059_2019 AS
	SELECT code_insee, ST_Multi(ST_Buffer(geom,100))::geometry(MultiPolygon,2154) as geom
	FROM r_parcellaire_express.n_commune_pepci_059_2019;
--> SELECT 651

CREATE INDEX temp_commune_buffer_100m_059_2019_geom_gist
    ON tache_urbaine_2019.temp_commune_buffer_100m_059_2019 USING gist
    (geom)
    TABLESPACE index;
--> CREATE INDEX / Requête exécutée avec succès en 271 msec.


CREATE TABLE tache_urbaine_2019.temp1_buffer_059_2019 AS 
SELECT t2.code_insee, ST_multi(ST_Buffer(t1.geom,50))::geometry(MultiPolygon,2154) as geom	
	FROM r_parcellaire_express.n_batiment_pepci_000 as t1
	JOIN tache_urbaine_2019.temp_commune_buffer_100m_059_2019 as t2
	ON ST_Intersects (t1.geom, t2.geom)
	ORDER BY t2.code_insee;
--> SELECT 1936961 / Requête exécutée avec succès en 5 min 38 secs. 

Create table tache_urbaine_2019.temp2_union_059_2019 as
SELECT ST_multi(ST_Union(geom))::geometry(MultiPolygon,2154) as geom
FROM tache_urbaine_2019.temp1_buffer_059_2019
GROUP BY code_insee;
--> SELECT 648 / Requête exécutée avec succès en 9 min 47 secs.

INSERT INTO tache_urbaine_2019.temp_enveloppe_bati_pepci_2019 (geom)
SELECT (ST_Dump(ST_Buffer(ST_Union(geom),-40))).geom::geometry(Polygon,2154) as geom
FROM tache_urbaine_2019.temp2_union_059_2019
--> INSERT 0 17848 / Requête exécutée avec succès en 40 min 4 secs.
"

# Index Géomérique
psql ceremabase -c "
CREATE INDEX temp_enveloppe_bati_pepci_2019_geom_gist ON tache_urbaine_2019.temp_enveloppe_bati_pepci_2019
	USING gist (geom) TABLESPACE index;
"
# Identifiant pour la phase B.2
psql ceremabase -c "
ALTER TABLE tache_urbaine_2019.temp_enveloppe_bati_pepci_2019
	ADD COLUMN id serial;
"
# Sauvegarde en cas d'echec de la B.2
psql ceremabase -c "
CREATE TABLE tache_urbaine_2019.sauve_temp_enveloppe_bati_pepci_2019 AS
	SELECT * FROM tache_urbaine_2019.temp_enveloppe_bati_pepci_2019;
"
# B.2] Création de la table nationale
psql ceremabase -c "
CREATE TABLE tache_urbaine_2019.l_enveloppe_bati_pepci_2019
(
    id_enveloppe serial,
    nbatidur integer,
    nbatileg integer,
    geom geometry(Polygon,2154)
);
"
# B.2.1] Sélection des enveloppes qui s'intersectent
psql ceremabase -c "
CREATE TABLE tache_urbaine_2019.stintersects_enveloppe_bati_pepci_2019 AS
	SELECT ST_Union(t1.geom,t2.geom)::geometry(Polygon,2154) as geometrie_union
		FROM tache_urbaine_2019.temp_enveloppe_bati_pepci_2019 as t1
		JOIN tache_urbaine_2019.temp_enveloppe_bati_pepci_2019 as t2
		ON ST_Intersects(t1.geom, t2.geom)
		WHERE t1.id < t2.id;

CREATE INDEX stintersects_enveloppe_bati_pepci_2019_geom_gist
	ON tache_urbaine_2019.stintersects_enveloppe_bati_pepci_2019
	USING gist (geometrie_union) TABLESPACE index;
"


# B.2.3] Peuplement par agregation des zones de chevauchement interdépartementales
psql ceremabase -c "
INSERT INTO tache_urbaine_2019.l_enveloppe_bati_pepci_2019 (geom)
SELECT (ST_Dump(ST_Union(geometrie_union))).geom::geometry(Polygon,2154) as geom
		FROM tache_urbaine_2019.stintersects_enveloppe_bati_pepci_2019;
--> INSERT 0 25040 / Requête exécutée avec succès en 43 hr 58 min.
"
#ENCOURS
# Suppression des zones de des zones de chevauchement interdépartementales que l'on vient d'inserer
psql ceremabase -c "
DELETE FROM tache_urbaine_2019.temp_enveloppe_bati_pepci_2019 WHERE id IN (
		(SELECT t1.id
		FROM tache_urbaine_2019.temp_enveloppe_bati_pepci_2019 as t1
		JOIN tache_urbaine_2019.temp_enveloppe_bati_pepci_2019 as t2
		ON ST_Intersects(t1.geom, t2.geom)
		WHERE t1.id < t2.id)
	UNION ALL
		(SELECT t2.id
		FROM tache_urbaine_2019.temp_enveloppe_bati_pepci_2019 as t1
		JOIN tache_urbaine_2019.temp_enveloppe_bati_pepci_2019 as t2
		ON ST_Intersects(t1.geom, t2.geom)
		WHERE t1.id < t2.id)
);
--> DELETE 51457 / Requête exécutée avec succès en 11 min 12 secs.
"

# B.2.4] Insersion du reste : ce qui ne se supperpose pas :
# B.2.4.1] On vérifie qu'il n'y a rien qui s'intersecte
psql ceremabase -c "
CREATE INDEX l_enveloppe_bati_pepci_2019_geom_gist
	ON tache_urbaine_2019.l_enveloppe_bati_pepci_2019
	USING gist (geom) TABLESPACE index;

SELECT *
FROM tache_urbaine_2019.temp_enveloppe_bati_pepci_2019 as t1
JOIN tache_urbaine_2019.l_enveloppe_bati_pepci_2019 as t2
ON ST_Intersects(t1.geom, t2.geom);
--> Exécution réussie. Temps total :  3 min 35 secs / 0 lignes affectées.

DROP INDEX tache_urbaine_2019.l_enveloppe_bati_pepci_2019_geom_gist;
"
# B.2.4.2] On insere !
psql ceremabase -c "
INSERT INTO tache_urbaine_2019.l_enveloppe_bati_pepci_2019 (geom)
	SELECT geom::geometry(Polygon,2154) as geom
	FROM tache_urbaine_2019.temp_enveloppe_bati_pepci_2019;
--> INSERT 0 1535920 / Requête exécutée avec succès en 2 min 29 secs.
"

# Index Géomérique + Cluster
psql ceremabase -c "
CREATE INDEX l_enveloppe_bati_pepci_2019_geom_gist ON tache_urbaine_2019.l_enveloppe_bati_pepci_2019
	USING gist (geom) TABLESPACE index;
ALTER TABLE tache_urbaine_2019.l_enveloppe_bati_pepci_2019
	CLUSTER ON l_enveloppe_bati_pepci_2019_geom_gist;
"

# B.2.5] Mise à jour des champs nbatidur & nbatileg
psql ceremabase -c "
UPDATE tache_urbaine_2019.l_enveloppe_bati_pepci_2019 as t1 SET nbatidur = (
	SELECT count(*)
	FROM r_parcellaire_express.n_batiment_pepci_000 as t2
	WHERE	(t2.type='Bâtiment en dur' or t2.type='Bâti dur' or t2.type='Bati dur' or t2.type='01')
			and ST_Intersects(t1.geom, t2.geom));
--> UPDATE 1560960 / Requête exécutée avec succès en 4 hr 42 min.
"
#ENCOURS
psql ceremabase -c "
UPDATE tache_urbaine_2019.l_enveloppe_bati_pepci_2019 as t1 SET nbatileg = (
	SELECT count(*)
	FROM r_parcellaire_express.n_batiment_pepci_000 as t2
	WHERE 	(type='Construction légère' or type='Bâti léger' or type='Bati leger' or type='02')
		and ST_Intersects(t1.geom, t2.geom));
"
# B.2.3] Optimisations
# Clé primaire 
psql ceremabase -c "
ALTER TABLE tache_urbaine_2019.l_enveloppe_bati_pepci_2019
    ADD CONSTRAINT l_enveloppe_bati_pepci_2019_id_enveloppe_pk PRIMARY KEY (id_enveloppe)
    USING INDEX TABLESPACE index;
"
# Contraintes Géométriques
psql ceremabase -c "
ALTER TABLE tache_urbaine_2019.l_enveloppe_bati_pepci_2019
    ADD CONSTRAINT enforce_dims_geom CHECK (ST_ndims(geom) = 2);
"
psql ceremabase -c "
ALTER TABLE tache_urbaine_2019.l_enveloppe_bati_pepci_2019
	ADD CONSTRAINT enforce_srid_geom CHECK (ST_Srid(geom)= 2154);
"
psql ceremabase -c "
ALTER TABLE tache_urbaine_2019.l_enveloppe_bati_pepci_2019
    ADD CONSTRAINT enforce_geotype_geom CHECK (geometrytype(geom) = 'POLYGON'::text);
"
# Index attributaires
psql ceremabase -c "
CREATE INDEX l_enveloppe_bati_pepci_2019_nbatidur_idx ON tache_urbaine_2019.l_enveloppe_bati_pepci_2019
	USING brin (nbatidur) TABLESPACE index;
"
psql ceremabase -c "
CREATE INDEX l_enveloppe_bati_pepci_2019_nbatileg_idx ON tache_urbaine_2019.l_enveloppe_bati_pepci_2019
	USING brin (nbatileg) TABLESPACE index;
"

# Index Géomérique + Cluster
# Déjà fait en partie # B.2.1]

# B.2.4] Commentaires
# Table
psql ceremabase -c "
COMMENT ON TABLE tache_urbaine_2019.l_enveloppe_bati_pepci_2019
  IS 'Enveloppe nationale du bâti de 2019.

Il s’agit de fusionner tous les bâtiments du parcellaire express qui sont séparés de moins de 50 m par un Buffer +50, ST_Union, Erosion de 40m';
"
# Champs
psql ceremabase -c "
COMMENT ON COLUMN tache_urbaine_2019.l_enveloppe_bati_pepci_2019.id_enveloppe
    IS 'Identifiant national unique pour une même enveloppe du bati';
"
psql ceremabase -c "
COMMENT ON COLUMN tache_urbaine_2019.l_enveloppe_bati_pepci_2019.nbatidur
    IS 'Nombre de bâtiments qualifiés de dur par la DGFiP intégrés dans cette enveloppe du bati.';
"
psql ceremabase -c "
COMMENT ON COLUMN tache_urbaine_2019.l_enveloppe_bati_pepci_2019.nbatileg
    IS 'Nombre de bâtiments qualifiés de legers par la DGFiP intégrés dans cette enveloppe du bati.';
"
psql ceremabase -c "
COMMENT ON COLUMN tache_urbaine_2019.l_enveloppe_bati_pepci_2019.geom
    IS 'Champs géométrique : Monopolygones en Lambert93 (EPSG:2154).';
"
#psql ceremabase -c "DROP TABLE IF EXISTS tache_urbaine_2019.temp_departement_buffer_200m_000_2019;"
#psql ceremabase -c "DROP TABLE IF EXISTS tache_urbaine_2019.temp_enveloppe_bati_pepci_2019;"
#psql ceremabase -c "DROP TABLE IF EXISTS tache_urbaine_2019.stintersects_enveloppe_bati_pepci_2019;"
