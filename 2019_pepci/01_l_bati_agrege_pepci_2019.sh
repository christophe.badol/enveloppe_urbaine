#!/bin/bash
# Tache Urbaine

# A] Bati Agrege

for dpt in '090' '039' '025' '001' '002' '003' '004' '005' '006' '007' '008' '009' '010' '011' '012' '013' '014' '015' '016' '017' '018' '019' '021' '022' '023' '024' '026' '027' '028' '029' '030' '031' '032' '033' '034' '035' '036' '037' '038' '040' '041' '042' '043' '044' '045' '046' '047' '048' '049' '050' '051' '052' '053' '054' '055' '056' '057' '058' '059' '060' '061' '062' '063' '064' '065' '066' '067' '068' '069' '070' '071' '072' '073' '074' '075' '076' '077' '078' '079' '080' '081' '082' '083' '084' '085' '086' '087' '088' '089' '091' '092' '093' '094' '095'
#for dpt in '02b' '02a'

#  Execution :
# time sh /home/administrateur/gitlab/enveloppe_urbaine/2018_etalab/A1_l_bati_agrege_etalab_2018.sh 1>/mnt/data/A1_l_bati_agrege_etalab_2018.log 2>&1

do

# A.1] Création des tables Départementales
psql -p 5444 -d ceremabase -c "DROP TABLE IF EXISTS tache_urbaine_2019.l_bati_agrege_pepci_"$dpt"_2019;"
psql -p 5444 -d ceremabase -c "
CREATE TABLE tache_urbaine_2019.l_bati_agrege_pepci_"$dpt"_2019
	(
	id serial,
	nbatidur integer,
	nbatileg integer,
	code_dep character(3) DEFAULT '"$dpt"',  
	geom geometry(Polygon,2154)
	);
"

psql -p 5444 -d ceremabase -c "
ALTER SEQUENCE tache_urbaine_2019.l_bati_agrege_pepci_"$dpt"_2019_id_seq
	RESTART WITH "$dpt"000001;
"

# A.2] Buffer +0,01, ST_Union, Buffer -0,01, ST_Dump
psql -p 5444 -d ceremabase -c "
INSERT INTO tache_urbaine_2019.l_bati_agrege_pepci_"$dpt"_2019 (geom)
	SELECT (ST_Dump(ST_Buffer(ST_Union(ST_Buffer(geom,0,01)),-0,01))).geom::geometry(Polygon,2154) as geom
	FROM r_parcellaire_express.n_batiment_pepci_"$dpt"_2019;
"

# A.3] Mise à jour des champs nbatidur & nbatileg
psql -p 5444 -d ceremabase -c "
UPDATE tache_urbaine_2019.l_bati_agrege_pepci_"$dpt"_2019 as t1 SET nbatidur = (
	SELECT count(*)
	FROM r_parcellaire_express.n_batiment_pepci_"$dpt"_2019 as t2
	WHERE 	t1.code_dep = '"$dpt"'
		and (t2.type='Bâtiment en dur' or t2.type='Bâti dur' or t2.type='Bati dur' or t2.type='01')
		and ST_Intersects(t1.geom, t2.geom))
	WHERE 	t1.code_dep = '"$dpt"';
"

psql -p 5444 -d ceremabase -c "
UPDATE tache_urbaine_2019.l_bati_agrege_pepci_"$dpt"_2019 as t1 SET nbatileg = (
	SELECT count(*)
	FROM r_parcellaire_express.n_batiment_pepci_"$dpt"_2019 as t2
	WHERE 	t1.code_dep = '"$dpt"'
		and (type='Construction légère' or type='Bâti léger' or type='Bati leger' or type='02')
		and ST_Intersects(t1.geom, t2.geom))
	WHERE 	t1.code_dep = '"$dpt"';
"

# A.4] Optimisation 
# Clé primaire 
psql -p 5444 -d ceremabase -c "
ALTER TABLE tache_urbaine_2019.l_bati_agrege_pepci_"$dpt"_2019
    ADD CONSTRAINT l_bati_agrege_pepci_"$dpt"_2019_pk PRIMARY KEY (id)
    USING INDEX TABLESPACE index;
"
# Contraintes Géométriques
psql -p 5444 -d ceremabase -c "
ALTER TABLE tache_urbaine_2019.l_bati_agrege_pepci_"$dpt"_2019
    ADD CONSTRAINT enforce_dims_geom CHECK (ST_ndims(geom) = 2);
"
psql -p 5444 -d ceremabase -c "
ALTER TABLE tache_urbaine_2019.l_bati_agrege_pepci_"$dpt"_2019
	ADD CONSTRAINT enforce_srid_geom CHECK (ST_Srid(geom)= 2154);
"
psql -p 5444 -d ceremabase -c "
ALTER TABLE tache_urbaine_2019.l_bati_agrege_pepci_"$dpt"_2019
    ADD CONSTRAINT enforce_geotype_geom CHECK (geometrytype(geom) = 'POLYGON'::text);
"
# Index attributaires
psql -p 5444 -d ceremabase -c "
CREATE INDEX l_bati_agrege_pepci_"$dpt"_2019_nbatidur_idx ON tache_urbaine_2019.l_bati_agrege_pepci_"$dpt"_2019
	USING brin (nbatidur) TABLESPACE index;
"
psql -p 5444 -d ceremabase -c "
CREATE INDEX l_bati_agrege_pepci_"$dpt"_2019_nbatileg_idx ON tache_urbaine_2019.l_bati_agrege_pepci_"$dpt"_2019
	USING brin (nbatileg) TABLESPACE index;
"
psql -p 5444 -d ceremabase -c "
CREATE INDEX l_bati_agrege_pepci_"$dpt"_2019_code_dep_idx ON tache_urbaine_2019.l_bati_agrege_pepci_"$dpt"_2019
	USING brin (code_dep) TABLESPACE index;
"
# Index Géomérique + Cluster
psql -p 5444 -d ceremabase -c "
CREATE INDEX l_bati_agrege_pepci_"$dpt"_2019_geom_gist ON tache_urbaine_2019.l_bati_agrege_pepci_"$dpt"_2019
	USING gist (geom) TABLESPACE index;
"
psql -p 5444 -d ceremabase -c "
ALTER TABLE tache_urbaine_2019.l_bati_agrege_pepci_"$dpt"_2019
	CLUSTER ON l_bati_agrege_pepci_"$dpt"_2019_geom_gist;
"

# A.5] Commentaires
# Table
psql -p 5444 -d ceremabase -c "
COMMENT ON TABLE tache_urbaine_2019.l_bati_agrege_pepci_"$dpt"_2019
  IS 'Bâti agrégé de 2019 pour le département ''"$dpt"''.

Il s’agit de fusionner tous les bâtiments du parcellaire express qui sont séparés de moins de 1 cm par un Buffer +0,01, ST_Union, Buffer -0,01, ST_Dump';
"
# Champs
psql -p 5444 -d ceremabase -c "
COMMENT ON COLUMN tache_urbaine_2019.l_bati_agrege_pepci_"$dpt"_2019.id
    IS 'Identifiant unique selon méthode COVADIS : n°département.000.000 + id.';
"
psql -p 5444 -d ceremabase -c "
COMMENT ON COLUMN tache_urbaine_2019.l_bati_agrege_pepci_"$dpt"_2019.nbatidur
    IS 'Nombre de bâtiments qualifiés de dur par la DGFiP intégrés dans cette agrégation de bâtiments.';
"
psql -p 5444 -d ceremabase -c "
COMMENT ON COLUMN tache_urbaine_2019.l_bati_agrege_pepci_"$dpt"_2019.nbatileg
    IS 'Nombre de bâtiments qualifiés de legers par la DGFiP intégrés dans cette agrégation de bâtiments.';
"
psql -p 5444 -d ceremabase -c "
COMMENT ON COLUMN tache_urbaine_2019.l_bati_agrege_pepci_"$dpt"_2019.code_dep
    IS 'Numéro INSEE du département sur 3 charactères.';
"
psql -p 5444 -d ceremabase -c "
COMMENT ON COLUMN tache_urbaine_2019.l_bati_agrege_pepci_"$dpt"_2019.geom
    IS 'Champs géométrique : Monopolygones en Lambert93 (EPSG:2154).';
"

done
