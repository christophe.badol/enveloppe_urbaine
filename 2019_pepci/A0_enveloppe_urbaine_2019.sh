#!/bin/bash
# A0 Préparation à Enveloppe Urbaine

# Paramètres
commande="psql -d ceremabase -c "
millesime="2019"
trigramme="pepci"
schematravail="tache_urbaine_"$millesime
schemacadastre="r_parcellaire_express"

# Important :
# Pour tout logger taper :
# time sh /home/administrateur/Documents/A0_enveloppe_urbaine_2019.sh 1>/mnt/data/A0_enveloppe_urbaine_2019.log 2>&1

$commande "
CREATE SCHEMA $schematravail;
"
$commande "
CREATE TABLE "$schematravail".temp_departement_buffer_200m_000_"$millesime" AS
	SELECT code_dep, ST_Multi(ST_Buffer(ST_Union(geom),200))::geometry(MultiPolygon,2154) as geom
	FROM "$schemacadastre".n_commune_"$trigramme"_000
	GROUP BY code_dep;

CREATE INDEX temp_departement_buffer_200m_000_"$millesime"_code_dep_idx
    ON "$schematravail".temp_departement_buffer_200m_000_"$millesime" USING brin
    (code_dep)
    TABLESPACE index;

CREATE INDEX temp_departement_buffer_200m_000_"$millesime"_geom_gist
    ON "$schematravail".temp_departement_buffer_200m_000_"$millesime" USING gist
    (geom)
    TABLESPACE index;

ALTER TABLE "$schematravail".temp_departement_buffer_200m_000_"$millesime"
    CLUSTER ON temp_departement_buffer_200m_000_"$millesime"_geom_gist;
"
