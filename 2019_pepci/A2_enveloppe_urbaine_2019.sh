#!/bin/bash
# "A.2] Travail sur la table nationale Enveloppe Urbaine
commande="psql -d ceremabase -c "

# Important :
# Pour tout logger taper :
# time sh /home/administrateur/gitlab/enveloppe_urbaine/2019_pepci/A2_enveloppe_urbaine_2019.sh 1>/mnt/data/A2_enveloppe_urbaine_2019.log 2>&1

echo "A.2.1] Création de la table temporaire d addition des départements"
now=$(date +"%T")
echo "Début à $now"
echo "-------------------------------------------------------------------"
$commande "
DROP TABLE IF EXISTS tache_urbaine_2019.temp_enveloppe_bati_pepci_000_2019
"
$commande "
CREATE TABLE tache_urbaine_2019.temp_enveloppe_bati_pepci_000_2019
(
	code_dep character(3),
	geom geometry(Polygon,2154)
);
"

echo "A.2.2] Ajout des départements dans la table nationale temporaire"
#DEBUG
#for dpt in '001' '038' '069'
for dpt in '001' '002' '003' '004' '005' '006' '007' '008' '009' '010' '011' '012' '013' '014' '015' '016' '017' '018' '019' '02b' '02a' '021' '022' '023' '024' '025' '026' '027' '028' '029' '030' '031' '032' '033' '034' '035' '036' '037' '038' '039' '040' '041' '042' '043' '044' '045' '046' '047' '048' '049' '050' '051' '052' '053' '054' '055' '056' '057' '058' '059' '060' '061' '062' '063' '064' '065' '066' '067' '068' '069' '070' '071' '072' '073' '074' '075' '076' '077' '078' '079' '080' '081' '082' '083' '084' '085' '086' '087' '088' '089' '090' '091' '092' '093' '094' '095'

do

echo "Département $dpt"
$commande "
INSERT INTO tache_urbaine_2019.temp_enveloppe_bati_pepci_000_2019 (code_dep, geom)
	SELECT code_dep, geom
	FROM tache_urbaine_2019.l_enveloppe_bati_pepci_"$dpt"_2019;
"

done

echo "A.2.3] Préparation de la phase A.2.4"
now=$(date +"%T")
echo "Début à $now"
echo "-------------------------------------"
echo "A.2.3.1] Index Géomérique"

$commande "
CREATE INDEX temp_enveloppe_bati_pepci_000_2019_geom_gist ON tache_urbaine_2019.temp_enveloppe_bati_pepci_000_2019
	USING gist (geom) TABLESPACE index;
"
echo "A.2.3.2] Identifiant pour la phase A.3"
$commande "
ALTER TABLE tache_urbaine_2019.temp_enveloppe_bati_pepci_000_2019
	ADD COLUMN id serial;
"
echo "A.2.3.3] Sauvegarde en cas d'echec de la A.3"
$commande "
DROP TABLE IF EXISTS tache_urbaine_2019.sauve_temp_enveloppe_bati_pepci_000_2019;
CREATE TABLE tache_urbaine_2019.sauve_temp_enveloppe_bati_pepci_000_2019 AS
	SELECT * FROM tache_urbaine_2019.temp_enveloppe_bati_pepci_000_2019;
"


echo "A.2.4] Création de la table nationale"
now=$(date +"%T")
echo "Début à $now"
echo "------------------------------------"
$commande "
DROP TABLE IF EXISTS tache_urbaine_2019.l_enveloppe_bati_pepci_000_2019;
CREATE TABLE tache_urbaine_2019.l_enveloppe_bati_pepci_000_2019
(
    id_enveloppe serial,
	nb_batidur int4,
	nb_batileg int4,
	surf_batidur int4,
	surf_batileg int4,
	list_code_dep varchar,
    geom geometry(Polygon,2154)
);
"

echo "A.2.4.1] Sélection des enveloppes qui s'intersectent"
now=$(date +"%T")
echo "Début à $now"
echo "------------------------------------"
$commande "
DROP TABLE IF EXISTS tache_urbaine_2019.stintersects_enveloppe_bati_pepci_000_2019;
CREATE TABLE tache_urbaine_2019.stintersects_enveloppe_bati_pepci_000_2019 AS
	SELECT ST_Union(t1.geom,t2.geom)::geometry(Polygon,2154) as geometrie_union
		FROM tache_urbaine_2019.temp_enveloppe_bati_pepci_000_2019 as t1
		JOIN tache_urbaine_2019.temp_enveloppe_bati_pepci_000_2019 as t2
		ON ST_Intersects(t1.geom, t2.geom)
		WHERE t1.id < t2.id;
-->
" 
$commande "
CREATE INDEX stintersects_enveloppe_bati_pepci_000_2019_geom_gist
	ON tache_urbaine_2019.stintersects_enveloppe_bati_pepci_000_2019
	USING gist (geometrie_union) TABLESPACE index;
"

echo "A.2.4.2] Peuplement par agregation des zones de chevauchement interdépartementales"
now=$(date +"%T")
echo "Début à $now"
echo "------------------------------------"
$commande "
INSERT INTO tache_urbaine_2019.l_enveloppe_bati_pepci_000_2019 (geom)
SELECT (ST_Dump(ST_Union(geometrie_union))).geom::geometry(Polygon,2154) as geom
		FROM tache_urbaine_2019.stintersects_enveloppe_bati_pepci_000_2019;
--> INSERT 0 25040 / Requête exécutée avec succès en 43 hr 58 min.
"

echo "A.2.4.3] Suppression des zones de des zones de"
echo "         chevauchement interdépartementales que l'on vient d'inserer"
now=$(date +"%T")
echo "Début à $now"
echo "---------------------------------------------------------------------"
$commande "
DELETE FROM tache_urbaine_2019.temp_enveloppe_bati_pepci_000_2019 WHERE id IN (
		(SELECT t1.id
		FROM tache_urbaine_2019.temp_enveloppe_bati_pepci_000_2019 as t1
		JOIN tache_urbaine_2019.temp_enveloppe_bati_pepci_000_2019 as t2
		ON ST_Intersects(t1.geom, t2.geom)
		WHERE t1.id < t2.id)
	UNION ALL
		(SELECT t2.id
		FROM tache_urbaine_2019.temp_enveloppe_bati_pepci_000_2019 as t1
		JOIN tache_urbaine_2019.temp_enveloppe_bati_pepci_000_2019 as t2
		ON ST_Intersects(t1.geom, t2.geom)
		WHERE t1.id < t2.id)
);
--> 
"

echo "---------------------------------------------------------"
echo "A.2.4.4] Insersion du reste : ce qui ne se supperpose pas :"
echo "         On vérifie qu'il n'y a rien qui s'intersecte"
echo "---------------------------------------------------------"
$commande "
CREATE INDEX l_enveloppe_bati_pepci_000_2019_geom_gist
	ON tache_urbaine_2019.l_enveloppe_bati_pepci_000_2019
	USING gist (geom) TABLESPACE index;

--SELECT *
--FROM tache_urbaine_2019.temp_enveloppe_bati_pepci_000_2019 as t1
--JOIN tache_urbaine_2019.l_enveloppe_bati_pepci_000_2019 as t2
--ON ST_Intersects(t1.geom, t2.geom);
-->
"
echo "---------------------------------------------------------"
$commande "
DROP INDEX tache_urbaine_2019.l_enveloppe_bati_pepci_000_2019_geom_gist;
"

echo "A.2.4.5] On insere !"
$commande "
INSERT INTO tache_urbaine_2019.l_enveloppe_bati_pepci_000_2019 (geom)
	SELECT geom::geometry(Polygon,2154) as geom
	FROM tache_urbaine_2019.temp_enveloppe_bati_pepci_000_2019;
--> INSERT 0 1535920 / Requête exécutée avec succès en 2 min 29 secs.
"

echo "A.2.4.6] Index Géomérique + Cluster"
$commande "
CREATE INDEX l_enveloppe_bati_pepci_000_2019_geom_gist ON tache_urbaine_2019.l_enveloppe_bati_pepci_000_2019
	USING gist (geom) TABLESPACE index;
ALTER TABLE tache_urbaine_2019.l_enveloppe_bati_pepci_000_2019
	CLUSTER ON l_enveloppe_bati_pepci_000_2019_geom_gist;
"
$commande "
CREATE INDEX sauve_temp_enveloppe_bati_pepci_000_2019_geom_gist ON tache_urbaine_2019.sauve_temp_enveloppe_bati_pepci_000_2019
	USING gist (geom) TABLESPACE index;
ALTER TABLE tache_urbaine_2019.sauve_temp_enveloppe_bati_pepci_000_2019
	CLUSTER ON sauve_temp_enveloppe_bati_pepci_000_2019_geom_gist;
"
#DEBUG : Pas fait sur le referentiel cadastre pepci 2019 ! 
#$commande "
#CREATE INDEX n_bati_pepci_000_2019_type_idx
#    ON r_cadastre_pepci_2019.n_bati_pepci_000_2019 USING brin
#    (type)
#    TABLESPACE index;
#"

echo "A.2.5] Mise à jour des champs"
echo "A.2.5.1] Mise à jour des champs nb_batidur"
now=$(date +"%T")
echo "Début à $now"
echo "------------------------------------"
$commande "
UPDATE tache_urbaine_2019.l_enveloppe_bati_pepci_000_2019 as t1 SET nb_batidur = (
	SELECT count(*)
	FROM r_parcellaire_express.n_batiment_pepci_000_2019 as t2
	WHERE	ST_Intersects(t1.geom, t2.geom)
	AND (t2.type='Bâtiment en dur' or t2.type='Bâti dur' or t2.type='Bati dur' or t2.type='01')
	);
--> 
"

echo "A.2.5.2] Mise à jour des champs nb_batileg"
now=$(date +"%T")
echo "Début à $now"
echo "------------------------------------"
$commande "
UPDATE tache_urbaine_2019.l_enveloppe_bati_pepci_000_2019 as t1 SET nb_batileg = (
	SELECT count(*)
	FROM r_parcellaire_express.n_batiment_pepci_000_2019 as t2
	WHERE	ST_Intersects(t1.geom, t2.geom)
	AND	(t2.type='Construction légère' or t2.type='Bâti léger' or t2.type='Bati leger' or t2.type='02')
	);
--> Requête exécutée avec succès en 2 min 8 secs.
"
echo "A.2.5.3] Mise à jour des champs surf_batidur"
now=$(date +"%T")
echo "Début à $now"
echo "------------------------------------"
$commande "
UPDATE tache_urbaine_2019.l_enveloppe_bati_pepci_000_2019 as t1 SET surf_batidur = (
	SELECT sum(ST_Area(t2.geom))
	FROM r_parcellaire_express.n_batiment_pepci_000_2019 as t2
	WHERE 	
		ST_Intersects(t1.geom, t2.geom)
		AND 
		(t2.type='01' or t2.type='Bâtiment en dur' or t2.type='Bâti dur' or t2.type='Bati dur')
	);
"
echo "A.2.5.4] Mise à jour des champs surf_batileg"
now=$(date +"%T")
echo "Début à $now"
echo "------------------------------------"
$commande "
---- echo "A.2.5.4] Mise à jour des champs surf_batileg"
---- Combien de tache
select count(geom) from tache_urbaine_2019.l_enveloppe_bati_pepci_000_2019;
--> 1550223

UPDATE tache_urbaine_2019.l_enveloppe_bati_pepci_000_2019 as t1 SET surf_batileg = (
	SELECT sum(ST_Area(t2.geom))
	FROM r_parcellaire_express.n_batiment_pepci_000_2019 as t2
	WHERE 	
		ST_Intersects(t1.geom, t2.geom)
		AND
		(t2.type='02' or t2.type='Construction légère' or t2.type='Bâti léger' or t2.type='Bati leger')
	)
	where t1.id_enveloppe < 500000;
--> Updated Rows	499999
UPDATE tache_urbaine_2019.l_enveloppe_bati_pepci_000_2019 as t1 SET surf_batileg = (
	SELECT sum(ST_Area(t2.geom))
	FROM r_parcellaire_express.n_batiment_pepci_000_2019 as t2
	WHERE 	
		ST_Intersects(t1.geom, t2.geom)
		AND
		(t2.type='02' or t2.type='Construction légère' or t2.type='Bâti léger' or t2.type='Bati leger')
	)
	where t1.id_enveloppe > 1000000;
--> Updated Rows	550223
UPDATE tache_urbaine_2019.l_enveloppe_bati_pepci_000_2019 as t1 SET surf_batileg = (
	SELECT sum(ST_Area(t2.geom))
	FROM r_parcellaire_express.n_batiment_pepci_000_2019 as t2
	WHERE 	
		ST_Intersects(t1.geom, t2.geom)
		AND
		(t2.type='02' or t2.type='Construction légère' or t2.type='Bâti léger' or t2.type='Bati leger')
	)
	where t1.id_enveloppe > 499999 and t1.id_enveloppe < 1000001;
--> Updated Rows	500001
"
echo "A.2.5.5] Mise à jour du champs list_code_dep"
now=$(date +"%T")
echo "Début à $now"
echo "------------------------------------"
#DEBUG : si plusieurs taches fusionnées d une même département on a un doublon
#$commande "
#UPDATE tache_urbaine_2019.l_enveloppe_bati_pepci_000_2019 as t1 SET list_code_dep = (
#	SELECT array_agg(code_dep)::varchar
#	FROM tache_urbaine_2019.sauve_temp_enveloppe_bati_pepci_000_2019 as t2
#	WHERE	ST_Intersects(t1.geom, t2.geom)
#	);
#--> UPDATE 38360 / Requête exécutée avec succès en 1 min 25 secs.
#"

#DEBUG : Enleve les doublons de départements, mais on perds l information de la fusion de plusieurs tache dans le même département
$commande "
UPDATE tache_urbaine_2019.l_enveloppe_bati_pepci_000_2019 as t1 SET list_code_dep = (
	WITH resultat1 AS 
		(SELECT DISTINCT t2.code_dep
		FROM tache_urbaine_2019.sauve_temp_enveloppe_bati_pepci_000_2019 as t2
		WHERE ST_Intersects(t1.geom, t2.geom)
		ORDER BY t2.code_dep)
	SELECT array_agg(code_dep)::varchar FROM resultat1
	);
--> 
"

echo "A.2.6] Optimisations"
now=$(date +"%T")
echo "Début à $now"
echo "------------------------------------"
echo "A.2.6.1] Clé primaire"
$commande "
ALTER TABLE tache_urbaine_2019.l_enveloppe_bati_pepci_000_2019
    ADD CONSTRAINT l_enveloppe_bati_pepci_000_2019_id_enveloppe_pk PRIMARY KEY (id_enveloppe)
    USING INDEX TABLESPACE index;
ALTER TABLE tache_urbaine_2019.l_enveloppe_bati_pepci_000_2019
    ADD CONSTRAINT enforce_dims_geom CHECK (ST_ndims(geom) = 2);
ALTER TABLE tache_urbaine_2019.l_enveloppe_bati_pepci_000_2019
	ADD CONSTRAINT enforce_srid_geom CHECK (ST_Srid(geom)= 2154);
ALTER TABLE tache_urbaine_2019.l_enveloppe_bati_pepci_000_2019
    ADD CONSTRAINT enforce_geotype_geom CHECK (geometrytype(geom) = 'POLYGON'::text);
"
echo "A.2.6.2] Index attributaires"
$commande "
CREATE INDEX l_enveloppe_bati_pepci_000_2019_nb_batidur_idx ON tache_urbaine_2019.l_enveloppe_bati_pepci_000_2019
	USING brin (nb_batidur) TABLESPACE index;
CREATE INDEX l_enveloppe_bati_pepci_000_2019_nbatileg_idx ON tache_urbaine_2019.l_enveloppe_bati_pepci_000_2019
	USING brin (nb_batileg) TABLESPACE index;
CREATE INDEX l_enveloppe_bati_pepci_000_2019_surf_batidur_idx ON tache_urbaine_2019.l_enveloppe_bati_pepci_000_2019
	USING brin (surf_batidur) TABLESPACE index;
CREATE INDEX l_enveloppe_bati_pepci_000_2019_surf_batileg_idx ON tache_urbaine_2019.l_enveloppe_bati_pepci_000_2019
	USING brin (surf_batileg) TABLESPACE index;
CREATE INDEX l_enveloppe_bati_pepci_000_2019_list_code_dep_idx ON tache_urbaine_2019.l_enveloppe_bati_pepci_000_2019
	USING brin (list_code_dep) TABLESPACE index;
"

echo "A.2.6.3] Index Géomérique + Cluster"
#DEBUG Déjà fait en partie # B.2.1]

echo "A.2.6.4] Commentaires"
$commande "
COMMENT ON TABLE tache_urbaine_2019.l_enveloppe_bati_pepci_000_2019
  IS 'Enveloppe nationale du bâti de 2019.

Il s’agit de fusionner tous les bâtiments issu du cadastre PCI qui sont séparés de moins de 50 m par un Buffer +50, ST_Union, Erosion de 40m';
"
$commande "
COMMENT ON COLUMN tache_urbaine_2019.l_enveloppe_bati_pepci_000_2019.id_enveloppe
    IS 'Identifiant national unique pour une même enveloppe du bati';
COMMENT ON COLUMN tache_urbaine_2019.l_enveloppe_bati_pepci_000_2019.nb_batidur
    IS 'Nombre de bâtiments qualifiés de dur par la DGFiP intégrés dans cette enveloppe du bati.';
COMMENT ON COLUMN tache_urbaine_2019.l_enveloppe_bati_pepci_000_2019.nb_batileg
    IS 'Nombre de bâtiments qualifiés de legers par la DGFiP intégrés dans cette enveloppe du bati.';
COMMENT ON COLUMN tache_urbaine_2019.l_enveloppe_bati_pepci_000_2019.surf_batidur
    IS 'Surface totale en m2 de tous les bâtiments qualifiés de dur par la DGFiP et présents dans cette enveloppe du bâti.';
COMMENT ON COLUMN tache_urbaine_2019.l_enveloppe_bati_pepci_000_2019.surf_batileg
    IS 'Surface totale en m2 de tous les bâtiments qualifiés de leger par la DGFiP et présents dans cette enveloppe du bâti.';	
COMMENT ON COLUMN tache_urbaine_2019.l_enveloppe_bati_pepci_000_2019.list_code_dep
    IS 'Liste des départements à l³origine de cette enveloppe du bati.';	
COMMENT ON COLUMN tache_urbaine_2019.l_enveloppe_bati_pepci_000_2019.geom
    IS 'Champs géométrique : Monopolygone en Lambert93 (EPSG:2154).';
"

echo "A.2.7] Netoyage des tables temporaires"
now=$(date +"%T")
echo "Début à $now"
echo "------------------------------------"
$commande "
DROP TABLE IF EXISTS tache_urbaine_2019.temp_departement_buffer_200m_000_2019;
----#DEBUG : on garde pour vérifier 
--DROP TABLE IF EXISTS tache_urbaine_2019.temp_enveloppe_bati_pepci_000_2019;
DROP TABLE IF EXISTS tache_urbaine_2019.stintersects_enveloppe_bati_pepci_000_2019;
"
