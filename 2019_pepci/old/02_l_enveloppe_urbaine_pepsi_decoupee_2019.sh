#!/bin/bash
# Tache Urbaine
# B.3] Découpe à la commune
# B.3.1] Création de la table nationale
psql ceremabase -c "
DROP TABLE IF EXISTS tache_urbaine_2019.l_enveloppe_bati_pepci_decoupee_2019;
CREATE TABLE tache_urbaine_2019.l_enveloppe_bati_pepci_decoupee_2019 
		(
		id serial,
		id_enveloppe integer,
		nbatidur integer,
		nbatileg integer,
		code_dep character(3),
		code_insee character(5),
		geom geometry(MultiPolygon,2154)
		);
"
# B.3.2] Découpe à partir des communes de la BDTOPO
psql ceremabase -c "
INSERT INTO tache_urbaine_2019.l_enveloppe_bati_pepci_decoupee_2019
	(id_enveloppe, nbatidur, nbatileg, code_dep, code_insee, geom)
SELECT
	env.id_enveloppe,
	null::integer,
	null::integer,
	com.code_dep::character(3),
	com.code_insee,
	ST_Multi(ST_Buffer(ST_Intersection(env.geom, com.geom),0.0))::geometry(MultiPolygon,2154)
FROM
	tache_urbaine_2019.l_enveloppe_bati_pepci_2019 as env
INNER JOIN
	r_bdtopo_2019.n_commune_bdt_000 as com
	--r_parcellaire_express.n_commune_pepci_000 as com -- problème de géométrie (lacunes et chevauchements)
ON
	ST_Intersects(env.geom, com.geom)
WHERE NOT
	ST_IsEmpty(ST_Buffer(ST_Intersection(env.geom, com.geom),0.0));
"

# Index Géomérique + Cluster
psql ceremabase -c "
CREATE INDEX l_enveloppe_bati_pepci_decoupee_2019_geom_gist ON tache_urbaine_2019.l_enveloppe_bati_pepci_decoupee_2019
	USING gist (geom) TABLESPACE index;
ALTER TABLE tache_urbaine_2019.l_enveloppe_bati_pepci_decoupee_2019
	CLUSTER ON l_enveloppe_bati_pepci_decoupee_2019_geom_gist;
"

# B.3.3] Mise à jour des champs nbatidur & nbatileg
psql ceremabase -c "
UPDATE tache_urbaine_2019.l_enveloppe_bati_pepci_decoupee_2019 as t1 SET nbatidur = (
	SELECT count(*)
	FROM r_parcellaire_express.n_batiment_pepci_000 as t2
	WHERE
		t1.code_dep = t2.code_dep and
		ST_Intersects(t1.geom, t2.geom)	
		and (t2.type='Bâtiment en dur' or t2.type='Bâti dur' or t2.type='Bati dur' or t2.type='01')
		);
"

psql ceremabase -c "
UPDATE tache_urbaine_2019.l_enveloppe_bati_pepci_decoupee_2019 as t1 SET nbatileg = (
	SELECT count(*)
	FROM r_parcellaire_express.n_batiment_pepci_000 as t2
	WHERE
		t1.code_dep = t2.code_dep and
		ST_Intersects(t1.geom, t2.geom)	
		and (type='Construction légère' or type='Bâti léger' or type='Bati leger' or type='02')
		);
"

# B.3.4] Optimisations
# Clé primaire 
psql ceremabase -c "
ALTER TABLE tache_urbaine_2019.l_enveloppe_bati_pepci_decoupee_2019
    ADD CONSTRAINT l_enveloppe_bati_pepci_decoupee_2019_id_pk PRIMARY KEY (id)
    USING INDEX TABLESPACE index;
"
# Contraintes Géométriques
psql ceremabase -c "
ALTER TABLE tache_urbaine_2019.l_enveloppe_bati_pepci_decoupee_2019
    ADD CONSTRAINT enforce_dims_geom CHECK (ST_ndims(geom) = 2);
"
psql ceremabase -c "
ALTER TABLE tache_urbaine_2019.l_enveloppe_bati_pepci_decoupee_2019
	ADD CONSTRAINT enforce_srid_geom CHECK (ST_Srid(geom)= 2154);
"
psql ceremabase -c "
ALTER TABLE tache_urbaine_2019.l_enveloppe_bati_pepci_decoupee_2019
    ADD CONSTRAINT enforce_geotype_geom CHECK (geometrytype(geom) = 'MULTIPOLYGON'::text);
"
# Index attributaires
psql ceremabase -c "
CREATE INDEX l_enveloppe_bati_pepci_decoupee_2019_id_enveloppe_idx ON tache_urbaine_2019.l_enveloppe_bati_pepci_decoupee_2019
	USING brin (id_enveloppe) TABLESPACE index;
"
psql ceremabase -c "
CREATE INDEX ll_enveloppe_bati_pepci_decoupee_2019_nbatidur_idx ON tache_urbaine_2019.l_enveloppe_bati_pepci_decoupee_2019
	USING brin (nbatidur) TABLESPACE index;
"
psql ceremabase -c "
CREATE INDEX l_enveloppe_bati_pepci_decoupee_2019_nbatileg_idx ON tache_urbaine_2019.l_enveloppe_bati_pepci_decoupee_2019
	USING brin (nbatileg) TABLESPACE index;
"
psql ceremabase -c "
CREATE INDEX l_enveloppe_bati_pepci_decoupee_2019_code_dep_idx ON tache_urbaine_2019.l_enveloppe_bati_pepci_decoupee_2019
	USING brin (code_dep) TABLESPACE index;
"
psql ceremabase -c "
CREATE INDEX l_enveloppe_bati_pepci_decoupee_2019_code_insee_idx ON tache_urbaine_2019.l_enveloppe_bati_pepci_decoupee_2019
	USING brin (code_insee) TABLESPACE index;
"
# Index Géomérique + Cluster
# Déjà fait en partie # B.3.2]

# B.3.5] Commentaires
# Table
psql ceremabase -c "
COMMENT ON TABLE tache_urbaine_2019.l_enveloppe_bati_pepci_decoupee_2019
  IS 'Enveloppe nationale du bâti de 2019 découpée selon les communes de la BDTOPO.

Il s’agit de fusionner tous les bâtiments du parcellaire express qui sont séparés de moins de 50 m par un Buffer +50, ST_Union, Erosion de 40m';
"
# Champs
psql ceremabase -c "
COMMENT ON COLUMN tache_urbaine_2019.l_enveloppe_bati_pepci_decoupee_2019.id
    IS 'Identifiant national unique pour un objet découpé';
COMMENT ON COLUMN tache_urbaine_2019.l_enveloppe_bati_pepci_decoupee_2019.id_enveloppe
    IS 'Identifiant national unique pour une même enveloppe du bati même découpée.';
COMMENT ON COLUMN tache_urbaine_2019.l_enveloppe_bati_pepci_decoupee_2019.nbatidur
    IS 'Nombre de bâtiments qualifiés de dur par la DGFiP intégrés dans cette enveloppe du bati.';
COMMENT ON COLUMN tache_urbaine_2019.l_enveloppe_bati_pepci_decoupee_2019.nbatileg
    IS 'Nombre de bâtiments qualifiés de legers par la DGFiP intégrés dans cette enveloppe du bati.';
COMMENT ON COLUMN tache_urbaine_2019.l_enveloppe_bati_pepci_decoupee_2019.code_insee
    IS 'Code INSEE de la commune où se trouve l’enveloppe du bati.';
COMMENT ON COLUMN tache_urbaine_2019.l_enveloppe_bati_pepci_decoupee_2019.code_dep
    IS 'Code INSEE du département où se trouve l’enveloppe du bati.';
COMMENT ON COLUMN tache_urbaine_2019.l_enveloppe_bati_pepci_decoupee_2019.geom
    IS 'Champs géométrique : Monopolygones en Lambert93 (EPSG:2154).';
" 

