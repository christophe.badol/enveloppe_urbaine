#!/bin/bash
# Tache Urbaine

#psql ceremabase -c "
#CREATE TABLE tache_urbaine_2019.temp_departement_buffer_200m_000_2019 AS
#	SELECT code_dep, ST_Multi(ST_Buffer(ST_Union(geom),200))::geometry(MultiPolygon,2154) as geom
#	FROM r_parcellaire_express.n_commune_pepci_000
#	GROUP BY code_dep;

#CREATE INDEX temp_departement_buffer_200m_000_2019_code_dep_idx
#    ON tache_urbaine_2019.temp_departement_buffer_200m_000_2019 USING brin
#    (code_dep)
#    TABLESPACE index;

#CREATE INDEX temp_departement_buffer_200m_000_2019_geom_gist
#    ON tache_urbaine_2019.temp_departement_buffer_200m_000_2019 USING gist
#    (geom)
#    TABLESPACE index;

#ALTER TABLE tache_urbaine_2019.temp_departement_buffer_200m_000_2019
#    CLUSTER ON temp_departement_buffer_200m_000_2019_geom_gist;
#"

# C] Tache Urbaine BATI BDTOPO 2019
# C.1] Création de la table temporaire d'addition des départements
psql -p 5432 ceremabase -c "DROP TABLE IF EXISTS tache_urbaine_2019.temp_enveloppe_bati_bdtopo_2019;"
psql -p 5432 ceremabase -c "CREATE TABLE tache_urbaine_2019.temp_enveloppe_bati_bdtopo_2019
(
	geom geometry(Polygon,2154)
);"

#for dpt in '02a' '02b' 
for dpt in '02b' '02a' '001' '002' '003' '004' '005' '006' '007' '008' '009' '010' '011' '012' '013' '014' '015' '016' '017' '018' '019'  '021' '022' '023' '024' '025' '026' '027' '028' '029' '030' '031' '032' '033' '034' '035' '036' '037' '038' '039' '040' '041' '042' '043' '044' '045' '046' '047' '048' '049' '050' '051' '052' '053' '054' '055' '056' '057' '058' '059' '060' '061' '062' '063' '064' '065' '066' '067' '068' '069' '070' '071' '072' '073' '074' '075' '076' '077' '078' '079' '080' '081' '082' '083' '084' '085' '086' '087' '088' '089' '090' '091' '092' '093' '094' '095'

do

# Buffer de +50m / Aggregation / Erosion de 40m pour chaque département
echo $dpt
psql -p 5432 ceremabase -c "
INSERT INTO tache_urbaine_2019.temp_enveloppe_bati_bdtopo_2019 (geom)
	SELECT (ST_Dump(ST_Buffer(ST_Union(ST_Buffer(t1.geom,50)),-40))).geom::geometry(Polygon,2154) as geom
	FROM r_bdtopo_2019.n_batiment_bdt_000 as t1
	JOIN tache_urbaine_2019.temp_departement_buffer_200m_000_2019 as t2
	ON ST_Intersects (t1.geom, t2.geom)
	WHERE t2.code_dep = upper('"$dpt"');
"

done

