echo "====================================================================================="
echo "	CREATION BATI AGREGE"
echo ""
echo "             CEREMABASE"
echo ""
echo "	Version 1 du 15 mai 2020"
echo "	c.badol"
echo "	Finalisé oui|X| / non | |"
echo "====================================================================================="
echo " Rq : ---- pour les commentaires / -- pour les commandes optionnelles, debuger"

CREATE SCHEMA p_enveloppe_bati_2019 AUTHORIZATION postgres;
COMMENT ON SCHEMA p_enveloppe_bati_2019 IS 'Enveloppe du bâti de 2019.
Création par fusion d un buffer de 50 m autour des batiments et érosion de -40 m.

Le référentiel utilisé est dans le nom de la table :
- pepci : Parcellaire Express de l IGN,
- etalab : cadastre disponible sur le site data.gouv.fr,
- bdtopo : batiments de la BDTOPO de l IGN.

Les scripts de ce travail sont disponibles ici : https://gitlab.com/christophe.badol/enveloppe_urbaine .';

echo "--------------------------------
echo "A1] Bâti agrégé 2019
echo "--------------------------------

#ARETROUVER

echo "------------------------------------------------------------"
echo "A.2] Partition "
echo "------------------------------------------------------------"
echo "A.2.1] Partition Nationale "
$commande "
select w_adl_delegue.set_partition_attach_metropole_et_regions('tache_urbaine_2019.l_bati_agrege_pepci','_2019');
--> /ERREUR: la contrainte de partition est violée par une ligne

update tache_urbaine_2019.l_bati_agrege_pepci_02A_2019 set code_dep = '02A';
--> Updated Rows	87377
update tache_urbaine_2019.l_bati_agrege_pepci_02B_2019 set code_dep = '02B';
--> Updated Rows	93125

select w_adl_delegue.set_partition_attach_metropole_et_regions('tache_urbaine_2019.l_bati_agrege_pepci','_2019');
--> 2020-05-21 - tache_urbaine_2019.l_bati_agrege_pepci_000_2019 reste visible

echo "A.2.2] Commentaires "
$commande "
DO $$
DECLARE
liste_valeur 				CHARACTER VARYING[];
req 						text;
BEGIN
liste_valeur := ARRAY  ['000','r11','r24','r27','r28','r32','r44','r52','r53','r75','r76','r84','r93','r94'];

FOR i_table IN 1..array_length(liste_valeur, 1) LOOP
		req := '
				COMMENT ON TABLE tache_urbaine_2019.l_bati_agrege_pepci_'|| liste_valeur[i_table] ||'_2019
				  IS ''Bâti agrégé de 2018.
				
				Il s’agit de fusionner tous les bâtiments du parcellaire express qui sont séparés de moins de 1 cm par un Buffer +0,01, ST_Union, Buffer -0,01, ST_Dump.'';
				COMMENT ON COLUMN tache_urbaine_2019.l_bati_agrege_pepci_'|| liste_valeur[i_table] ||'_2019.id
				IS ''Identifiant unique selon méthode COVADIS : n°département.000.000 + id.'';
				COMMENT ON COLUMN tache_urbaine_2019.l_bati_agrege_pepci_'|| liste_valeur[i_table] ||'_2019.nbatidur
				IS ''Nombre de bâtiments qualifiés de dur par la DGFiP intégrés dans cette agrégation de bâtiments.'';
				COMMENT ON COLUMN tache_urbaine_2019.l_bati_agrege_pepci_'|| liste_valeur[i_table] ||'_2019.nbatileg
				IS ''Nombre de bâtiments qualifiés de legers par la DGFiP intégrés dans cette agrégation de bâtiments.'';
				COMMENT ON COLUMN tache_urbaine_2019.l_bati_agrege_pepci_'|| liste_valeur[i_table] ||'_2019.code_dep
				IS ''Numéro INSEE du département sur 3 charactères.'';
				COMMENT ON COLUMN tache_urbaine_2019.l_bati_agrege_pepci_'|| liste_valeur[i_table] ||'_2019.geom
				IS ''Champs géométrique : Monopolygones en Lambert93 (EPSG:2154).''; 
			'; 
		RAISE NOTICE '%', req;
		EXECUTE(req);
		COMMIT;
   END LOOP;
END $$;
"
--> Updated Rows	0

---- 27 juin 2020
---- Ajout nouvelle contraintes Géométriques :
DO $$
DECLARE
liste_valeur 				CHARACTER VARYING[];
req 						text;
BEGIN
liste_valeur := ARRAY  [--'000',
						--'r11','r24','r27','r28','r32','r44','r52','r53','r75','r76','r84','r93','r94',
						'001','002','003','004','005','006','007','008','009','010',
						'011','012','013','014','015','016','017','018','019','02a','02b',
						'021','022','023','024','025','026','027','028','029','030',
						'031','032','033','034','035','036','037','038','039','040',
						'041','042','043','044','045','046','047','048','049','050',
						'051','052','053','054','055','056','057','058','059','060',
						'061','062','063','064','065','066','067','068','069','070',
						'071','072','073','074','075','076','077','078','079','080',
						'081','082','083','084','085','086','087','088','089','090',
						'091','092','093','094','095'--,'971','972','973','974','976'
						];
FOR i_table IN 1..array_length(liste_valeur, 1) LOOP
		req := '
				ALTER TABLE tache_urbaine_2019.l_bati_agrege_pepci_'|| liste_valeur[i_table] ||'_2019 ADD CONSTRAINT enforce_validite_geom CHECK (ST_IsValid(geom));
				ALTER TABLE tache_urbaine_2019.l_bati_agrege_pepci_'|| liste_valeur[i_table] ||'_2019 ADD CONSTRAINT enforce_no_self_intersection_geom CHECK (ST_IsSimple(geom));
				ALTER TABLE tache_urbaine_2019.l_bati_agrege_pepci_'|| liste_valeur[i_table] ||'_2019 ADD CONSTRAINT enforce_no_zero_area_geom CHECK (ST_Area(geom) > 0);
				';
		RAISE NOTICE '%', req;
		EXECUTE(req);
		COMMIT;
END LOOP;
END $$;

---- 25/03/2023
---- Mise aux normes 2023
ALTER SCHEMA tache_urbaine_2019 RENAME TO p_enveloppe_bati_2019;
--> Updated Rows	0

---- https://gitlab.com/christophe.badol/admin_ceremabase/-/blob/master/fonctions/synthese_geometrie_schema.sql
CREATE table p_enveloppe_bati_2019.synthese_geometrie_schema AS
SELECT * FROM w_fonctions.synthese_geometrie_schema('p_enveloppe_bati_2019');
--> Updated Rows	207
