echo "====================================================================================="
echo "	CREATION ENVELOPPE BATI"
echo ""
echo "             CEREMABASE"
echo ""
echo "	Version 1 du 14/04/2020"
echo "	c.badol"
echo "	Finalisé oui|X| / non | |"
echo "====================================================================================="
echo " Rq : ---- pour les commentaires / -- pour les commandes optionnelles, debuger"

echo "--------------------------------
echo "A] Enveloppe Urbaine 2019
echo "--------------------------------
#!/bin/bash
# A0 Préparation à Enveloppe Urbaine

# Paramètres
commande="psql -d ceremabase -c "
millesime="2019"
trigramme="pepci"
schematravail="tache_urbaine_"$millesime
schemacadastre="r_parcellaire_express"

# Important :
# Pour tout logger taper :
# time sh /home/administrateur/Documents/A0_enveloppe_urbaine_2019.sh 1>/mnt/data/A0_enveloppe_urbaine_2019.log 2>&1

$commande "
CREATE SCHEMA $schematravail;
"
$commande "
CREATE TABLE "$schematravail".temp_departement_buffer_200m_000_"$millesime" AS
	SELECT code_dep, ST_Multi(ST_Buffer(ST_Union(geom),200))::geometry(MultiPolygon,2154) as geom
	FROM "$schemacadastre".n_commune_"$trigramme"_000
	GROUP BY code_dep;

CREATE INDEX temp_departement_buffer_200m_000_"$millesime"_code_dep_idx
    ON "$schematravail".temp_departement_buffer_200m_000_"$millesime" USING brin
    (code_dep)
    TABLESPACE index;

CREATE INDEX temp_departement_buffer_200m_000_"$millesime"_geom_gist
    ON "$schematravail".temp_departement_buffer_200m_000_"$millesime" USING gist
    (geom)
    TABLESPACE index;

ALTER TABLE "$schematravail".temp_departement_buffer_200m_000_"$millesime"
    CLUSTER ON temp_departement_buffer_200m_000_"$millesime"_geom_gist;
"
## time sh /home/administrateur/Documents/A0_enveloppe_urbaine_2019.sh 1>/mnt/data/A0_enveloppe_urbaine_2019.log 2>&1
# real	170m5,906s
# user	0m0,087s
# sys	0m0,031s


echo "A.1] # Enveloppe Urbaine des 96 départements Métropolitains"
#!/bin/bash
# A1 Enveloppe Urbaine des 96 départements Métropolitains

# Paramètres
commande="psql -d ceremabase -c "
millesime="2019"
trigramme="pepci"
schematravail="tache_urbaine_"$millesime
schemacadastre="r_parcellaire_express"

# Important :
# Pour tout logger taper :
# time sh /home/administrateur/Documents/A1_enveloppe_urbaine_2019.sh 1>/mnt/data/02_enveloppe_urbaine_2019_A1.log 2>&1

#DEBUG
#for dpt in '090'
#for dpt in '02a' '02b'
for dpt in '001' '002' '003' '004' '005' '006' '007' '008' '009' '010' '011' '012' '013' '014' '015' '016' '017' '018' '019' '02b' '02a' '021' '022' '023' '024' '025' '026' '027' '028' '029' '030' '031' '032' '033' '034' '035' '036' '037' '038' '039' '040' '041' '042' '043' '044' '045' '046' '047' '048' '049' '050' '051' '052' '053' '054' '055' '056' '057' '058' '060' '061' '062' '063' '064' '065' '066' '067' '068' '069' '070' '071' '072' '073' '074' '075' '076' '077' '078' '079' '080' '081' '082' '083' '084' '085' '086' '087' '088' '089' '090' '091' '092' '093' '094' '095'

do

tabletravail="l_enveloppe_bati_"$trigramme"_"$dpt"_"$millesime	
now=$(date +"%T")
echo "--------------------"
echo "Début à $now"
echo "Département $dpt"
echo "--------------------"
echo "A.1.1] Création de la table à partir d’une dilatation de 50 m / fusion / érosion de 40 m"
$commande "
DROP TABLE IF EXISTS "$schematravail"."$tabletravail";
CREATE TABLE "$schematravail"."$tabletravail"
(
	id serial,
	nbatidur integer,
	nbatileg integer,
	code_dep character(3) DEFAULT '"$dpt"',  
	geom geometry(Polygon,2154)
);
"
#DEBUG
# Obligation de séparer ces trois commandes car si 2a 2b l’ALTER SEQUENCE echoue et annule les 4 commandes
$commande "
ALTER SEQUENCE "$schematravail"."$tabletravail"_id_seq
	RESTART WITH "$dpt"000001;
"
#DEBUG
# Obligation de séparer ces trois commandes car si 2a 2b l’ALTER SEQUENCE echoue et annule les 4 commandes
$commande "
INSERT INTO "$schematravail"."$tabletravail" (geom)
	SELECT (ST_Dump(ST_Buffer(ST_Union(ST_Buffer(t1.geom,50)),-40))).geom::geometry(Polygon,2154) as geom
	FROM "$schemacadastre".n_batiment_"$trigramme"_000_"$millesime" as t1
	JOIN "$schematravail".temp_departement_buffer_200m_000_"$millesime" as t2
	ON ST_Intersects (t1.geom, t2.geom)
	WHERE t2.code_dep = upper('"$dpt"');
"
echo "A.1.2] Clé + Index + Cluster + Optimisation"
$commande "
ALTER TABLE "$schematravail"."$tabletravail"
    ADD CONSTRAINT "$tabletravail"_pk PRIMARY KEY (id)
    USING INDEX TABLESPACE index;

ALTER TABLE "$schematravail"."$tabletravail"
    ADD CONSTRAINT enforce_dims_geom CHECK (ST_ndims(geom) = 2);

ALTER TABLE "$schematravail"."$tabletravail"
	ADD CONSTRAINT enforce_srid_geom CHECK (ST_Srid(geom)= 2154);

ALTER TABLE "$schematravail"."$tabletravail"
    ADD CONSTRAINT enforce_geotype_geom CHECK (geometrytype(geom) = 'POLYGON'::text);

CREATE INDEX l_enveloppe_bati_"$trigramme"_"$dpt"_"$millesime"_code_dep_idx ON "$schematravail"."$tabletravail"
	USING brin (code_dep) TABLESPACE index;

CREATE INDEX l_enveloppe_bati_"$trigramme"_"$dpt"_"$millesime"_geom ON "$schematravail"."$tabletravail"
	USING gist (geom) TABLESPACE index;
ALTER TABLE "$schematravail"."$tabletravail" CLUSTER ON "$tabletravail"_geom;
"

echo "A.1.3] Peuplement du champs nbatidur + index"
$commande "
UPDATE "$schematravail"."$tabletravail" as t1 SET nbatidur = (
	SELECT count(*)
	FROM "$schemacadastre".n_batiment_"$trigramme"_000_"$millesime" as t2
	-- Option sans partition	
	-- FROM "$schemacadastre".n_bati_"$trigramme"_"$dpt"_"$millesime" as t2
	WHERE 	t1.code_dep = '"$dpt"'
		and (t2.type='Bâtiment en dur' or t2.type='Bâti dur' or t2.type='Bati dur' or t2.type='01')
		and ST_Intersects(t1.geom, t2.geom))
	WHERE 	t1.code_dep = '"$dpt"';
"
$commande "
CREATE INDEX l_enveloppe_bati_"$trigramme"_"$dpt"_"$millesime"_nbatidur_idx ON "$schematravail"."$tabletravail"
	USING brin (nbatidur) TABLESPACE index;
"

echo "A.1.4] Peuplement du champs nbatileg + index"
$commande "
UPDATE "$schematravail"."$tabletravail" as t1 SET nbatileg = (
	SELECT count(*)
	FROM "$schemacadastre".n_batiment_"$trigramme"_000_"$millesime" as t2
	-- Option sans partition	
	-- FROM "$schemacadastre".n_bati_"$trigramme"_"$dpt"_"$millesime" as t2
	WHERE 	t1.code_dep = '"$dpt"'
		and (type='Construction légère' or type='Bâti léger' or type='Bati leger' or type='02')
		and ST_Intersects(t1.geom, t2.geom))
	WHERE 	t1.code_dep = '"$dpt"';
"
$commande "
CREATE INDEX l_enveloppe_bati_"$trigramme"_"$dpt"_"$millesime"_nbatileg_idx ON "$schematravail"."$tabletravail"
	USING brin (nbatileg) TABLESPACE index;
"

echo "A.1.5] Commentaires"
$commande "
COMMENT ON TABLE "$schematravail"."$tabletravail"
  IS 'Enveloppe du bati au 1er janvier 2019 pour le département ''"$dpt"''.

Il s’agit de fusionner tous les bâtiments issu du cadastre "$trigramme" par une dilatation de 50 m, puis une fusion et ensuite une érosion de 40 m';

COMMENT ON COLUMN "$schematravail"."$tabletravail".id
    IS 'Identifiant unique selon méthode COVADIS : n°département.000.000 + id.';

COMMENT ON COLUMN "$schematravail"."$tabletravail".nbatidur
    IS 'Nombre de bâtiments qualifiés de dur par la DGFiP intégrés dans cette enveloppe du bâti.';

COMMENT ON COLUMN "$schematravail"."$tabletravail".nbatileg
    IS 'Nombre de bâtiments qualifiés de legers par la DGFiP intégrés dans cette enveloppe du bâti.';

COMMENT ON COLUMN "$schematravail"."$tabletravail".code_dep
    IS 'Numéro INSEE du département sur 3 charactères.';

COMMENT ON COLUMN "$schematravail"."$tabletravail".geom
    IS 'Champs géométrique : Monopolygones en Lambert93 (EPSG:2154).';
"

done

# time sh /home/administrateur/Documents/A1_enveloppe_urbaine_2019.sh 1>/mnt/data/A1_enveloppe_urbaine_2019.log 2>&1
# real	1698m37,738s
# user	0m41,967s
# sys	0m11,033s

echo "A.1.59] Spécificité pour le 59/Nord - Trop Gros département"
$commande "
DO $$
DECLARE
liste_valeur 				CHARACTER VARYING[];
req 						text;
BEGIN
liste_valeur := ARRAY  ['059'];
FOR i_table IN 1..array_length(liste_valeur, 1) LOOP
		req := '
			DROP TABLE IF EXISTS tache_urbaine_2019.l_enveloppe_bati_pepci_'|| liste_valeur[i_table] ||'_2019;
			CREATE TABLE tache_urbaine_2019.l_enveloppe_bati_pepci_'|| liste_valeur[i_table] ||'_2019
				(
					id serial,
					nb_batidur integer,
					nb_batileg integer,
					surf_batidur integer,
					surf_batileg integer,
					code_dep character(3) DEFAULT '''||liste_valeur[i_table]||''',  
					geom geometry(Polygon,2154)
				);
		';
		req := '
			ALTER SEQUENCE tache_urbaine_2019.l_enveloppe_bati_pepci_'|| liste_valeur[i_table] ||'_2019_id_seq
				RESTART WITH '|| liste_valeur[i_table] ||'000001;
				';
		RAISE NOTICE '%', req;
		EXECUTE(req);
		COMMIT;
		req := '
				INSERT INTO tache_urbaine_2019.l_enveloppe_bati_pepci_'|| liste_valeur[i_table] ||'_2019 (geom)
					SELECT (ST_Dump(ST_Buffer(ST_Union(ST_Buffer(t1.geom,50)),-40))).geom::geometry(Polygon,2154) as geom
					FROM tache_urbaine_2019.l_bati_agrege_pepci_'|| liste_valeur[i_table] ||'_2019 as t1
					JOIN tache_urbaine_2019.temp_departement_buffer_200m_000_2019 as t2
					ON ST_Intersects (t1.geom, ST_Transform(t2.geom,2154))
					WHERE t2.code_dep = upper('''|| liste_valeur[i_table] ||''');
				';
		RAISE NOTICE '%', req;
		EXECUTE(req);
		COMMIT;
		req := '
				DROP INDEX IF EXISTS tache_urbaine_2019.l_enveloppe_bati_pepci_'|| liste_valeur[i_table] ||'_2019_nb_batidur_idx;
				UPDATE tache_urbaine_2019.l_enveloppe_bati_pepci_'|| liste_valeur[i_table] ||'_2019 as t1 SET nb_batidur = (
					SELECT count(t2.*)
					FROM r_parcellaire_express.n_batiment_pepci_000_2019 as t2
					WHERE 	
						ST_Intersects(t1.geom, t2.geom)
						AND 
						(t2.type=''01'' or t2.type=''Bâtiment en dur'' or t2.type=''Bâti dur'' or t2.type=''Bati dur'')
					);
				';
		RAISE NOTICE '%', req;
		EXECUTE(req);
		COMMIT;
		req := '
				CREATE INDEX l_enveloppe_bati_pepci_'|| liste_valeur[i_table] ||'_2019_nb_batidur_idx
					ON tache_urbaine_2019.l_enveloppe_bati_pepci_'|| liste_valeur[i_table] ||'_2019
						USING brin (nb_batidur) TABLESPACE index;
				';
		RAISE NOTICE '%', req;
		EXECUTE(req);
		COMMIT;
			req := '
				DROP INDEX IF EXISTS tache_urbaine_2019.l_enveloppe_bati_pepci_'|| liste_valeur[i_table] ||'_2019_nb_batileg_idx;
				UPDATE tache_urbaine_2019.l_enveloppe_bati_pepci_'|| liste_valeur[i_table] ||'_2019 as t1 SET nb_batileg = (
					SELECT count(t2.*)
					FROM r_parcellaire_express.n_batiment_pepci_000_2019 as t2
					WHERE 	
						ST_Intersects(t1.geom, t2.geom)
						AND
						(t2.type=''02'' or t2.type=''Construction légère'' or t2.type=''Bâti léger'' or t2.type=''Bati leger'')
					);
				';
		RAISE NOTICE '%', req;
		EXECUTE(req);
		COMMIT;
		req := '
				CREATE INDEX l_enveloppe_bati_pepci_'|| liste_valeur[i_table] ||'_2019_nb_batileg_idx
					ON tache_urbaine_2019.l_enveloppe_bati_pepci_'|| liste_valeur[i_table] ||'_2019
						USING brin (nb_batileg) TABLESPACE index;
				';
		RAISE NOTICE '%', req;
		EXECUTE(req);
		COMMIT;
		req='
				DROP INDEX IF EXISTS tache_urbaine_2019.l_enveloppe_bati_pepci_'|| liste_valeur[i_table] ||'_2019_surf_batidur_idx;
				UPDATE tache_urbaine_2019.l_enveloppe_bati_pepci_'|| liste_valeur[i_table] ||'_2019 as t1 SET surf_batidur = (
					SELECT sum(ST_Area(ST_Transform(t2.geom,2154)))
					FROM r_parcellaire_express.n_batiment_pepci_000_2019 as t2
					WHERE 	
						ST_Intersects(t1.geom, t2.geom)
						AND 
						(t2.type=''01'' or t2.type=''Bâtiment en dur'' or t2.type=''Bâti dur'' or t2.type=''Bati dur'')
					);
				';
		RAISE NOTICE '%', req;
		EXECUTE(req);
		COMMIT;
		req := '
				CREATE INDEX l_enveloppe_bati_pepci_'|| liste_valeur[i_table] ||'_2019_surf_batidur_idx
					ON tache_urbaine_2019.l_enveloppe_bati_pepci_'|| liste_valeur[i_table] ||'_2019
						USING brin (surf_batidur) TABLESPACE index;
				';
		RAISE NOTICE '%', req;
		EXECUTE(req);
		COMMIT;
		req='
				DROP INDEX IF EXISTS tache_urbaine_2019.l_enveloppe_bati_pepci_'|| liste_valeur[i_table] ||'_2019_surf_batileg_idx;
				UPDATE tache_urbaine_2019.l_enveloppe_bati_pepci_'|| liste_valeur[i_table] ||'_2019 as t1 SET surf_batileg = (
					SELECT sum(ST_Area(ST_Transform(t2.geom,2154)))
					FROM r_parcellaire_express.n_batiment_pepci_000_2019 as t2
					WHERE 	
						ST_Intersects(t1.geom, t2.geom)
						AND 
						(t2.type=''02'' or t2.type=''Construction légère'' or t2.type=''Bâti léger'' or t2.type=''Bati leger'')
					);
				';
		RAISE NOTICE '%', req;
		EXECUTE(req);
		COMMIT;
		req := '
				CREATE INDEX l_enveloppe_bati_pepci_'|| liste_valeur[i_table] ||'_2019_surf_batileg_idx
					ON tache_urbaine_2019.l_enveloppe_bati_pepci_'|| liste_valeur[i_table] ||'_2019
						USING brin (surf_batileg) TABLESPACE index;
				';
		RAISE NOTICE '%', req;
		EXECUTE(req);
		COMMIT;
		req := '
				COMMENT ON TABLE tache_urbaine_2019.l_enveloppe_bati_pepci_'|| liste_valeur[i_table] ||'_2019
				  IS ''Enveloppe du bati au 1er janvier 2019 pour le département '|| liste_valeur[i_table] ||'.

				Il s’agit de fusionner tous les bâtiments issu du Parcellaire Express par une dilatation de 50 m, puis une fusion et ensuite une érosion de 40 m'';

				COMMENT ON COLUMN tache_urbaine_2019.l_enveloppe_bati_pepci_'|| liste_valeur[i_table] ||'_2019.id
				    IS ''Identifiant unique selon méthode COVADIS : n°département.000.000 + id.'';
				COMMENT ON COLUMN tache_urbaine_2019.l_enveloppe_bati_pepci_'|| liste_valeur[i_table] ||'_2019.nb_batidur
				    IS ''Nombre de bâtiments qualifiés de dur par la DGFiP intégrés dans cette enveloppe du bâti.'';
				COMMENT ON COLUMN tache_urbaine_2019.l_enveloppe_bati_pepci_'|| liste_valeur[i_table] ||'_2019.nb_batileg
				    IS ''Nombre de bâtiments qualifiés de legers par la DGFiP intégrés dans cette enveloppe du bâti.'';
				COMMENT ON COLUMN tache_urbaine_2019.l_enveloppe_bati_pepci_'|| liste_valeur[i_table] ||'_2019.surf_batidur
				    IS ''Surface totale en m2 de tous les bâtiments qualifiés de dur par la DGFiP et présents dans cette enveloppe du bâti.'';
				COMMENT ON COLUMN tache_urbaine_2019.l_enveloppe_bati_pepci_'|| liste_valeur[i_table] ||'_2019.surf_batileg
				    IS ''Surface totale en m2 de tous les bâtiments qualifiés de leger par la DGFiP et présents dans cette enveloppe du bâti.'';
				COMMENT ON COLUMN tache_urbaine_2019.l_enveloppe_bati_pepci_'|| liste_valeur[i_table] ||'_2019.code_dep
				    IS ''Numéro INSEE du département sur 3 charactères.'';
				COMMENT ON COLUMN tache_urbaine_2019.l_enveloppe_bati_pepci_'|| liste_valeur[i_table] ||'_2019.geom
				    IS ''Champs géométrique : Monopolygones en Lambert93 (EPSG:2154).'';
		';
		RAISE NOTICE '%', req;
		EXECUTE(req);
		COMMIT;
END LOOP;
END $$;
"
-->> 2.688.022 ms

echo "-----------------------------------------------------"
echo "A.2] Travail sur la table nationale Enveloppe Urbaine"
echo "-----------------------------------------------------"
echo "A.2.0.1] Création des Index dans la table nationale des bati du cadastre 2019"
$commande "
DO $$
DECLARE
nomdelatable				character varying;
nomduschema					character varying;
liste_valeur 				CHARACTER VARYING[];
attribut 					character varying;
req 						text;
BEGIN
nomdelatable := 'n_batiment_pepci_000_2019';
nomduschema := 'r_parcellaire_express';
	FOR attribut IN
			SELECT COLUMN_NAME
				FROM INFORMATION_SCHEMA.COLUMNS
				WHERE TABLE_NAME = nomdelatable
				AND COLUMN_NAME != 'geom' AND COLUMN_NAME != 'the_geom'
		LOOP
				req := '
					DROP INDEX IF EXISTS ' || nomduschema || '.' || nomdelatable || '_' || attribut || '_idx;
					CREATE INDEX ' || nomdelatable || '_' || attribut || '_idx ON ' || nomduschema || '.' || nomdelatable || '
						USING BRIN (' || attribut || ') TABLESPACE index;
				';
				RAISE NOTICE '%', req;
				EXECUTE(req);
				COMMIT;
		END LOOP;
req := '
CREATE INDEX ' || nomdelatable || '_geom ON  ' || nomduschema || '.' || nomdelatable || '
	USING gist (geom) TABLESPACE index;
';
RAISE NOTICE '%', req;
EXECUTE(req);
COMMIT;
END $$;
"
--> ok

echo "A.2.0.2] Création des Index dans la table nationale des parcelles du cadastre 2019"
$commande "
DO $$
DECLARE
nomdelatable				character varying;
nomduschema					character varying;
liste_valeur 				CHARACTER VARYING[];
attribut 					character varying;
req 						text;
BEGIN
nomdelatable := 'n_parcelle_pepci_000_2019';
nomduschema := 'r_parcellaire_express';
	FOR attribut IN
			SELECT COLUMN_NAME
				FROM INFORMATION_SCHEMA.COLUMNS
				WHERE TABLE_NAME = nomdelatable
				AND COLUMN_NAME != 'geom' AND COLUMN_NAME != 'the_geom'
		LOOP
				req := '
					DROP INDEX IF EXISTS ' || nomduschema || '.' || nomdelatable || '_' || attribut || '_idx;
					CREATE INDEX ' || nomdelatable || '_' || attribut || '_idx ON ' || nomduschema || '.' || nomdelatable || '
						USING BRIN (' || attribut || ') TABLESPACE index;
				';
				RAISE NOTICE '%', req;
				EXECUTE(req);
				COMMIT;
		END LOOP;
req := '
CREATE INDEX ' || nomdelatable || '_geom ON  '|| nomduschema || '.' || nomdelatable || '
	USING gist (geom) TABLESPACE index;
';
RAISE NOTICE '%', req;
EXECUTE(req);
COMMIT;
END $$;
"
--> ok

echo "A.2] Travail sur la table nationale Enveloppe Urbaine"
#!/bin/bash
# "A.2] Travail sur la table nationale Enveloppe Urbaine
commande="psql -d ceremabase -c "

# Important :
# Pour tout logger taper :
# time sh /home/administrateur/Documents/A2_enveloppe_urbaine_2020.sh 1>/mnt/data/02_enveloppe_urbaine_2020_A2.log 2>&1

echo "A.2.1] Création de la table temporaire d addition des départements"
now=$(date +"%T")
echo "Début à $now"
echo "-------------------------------------------------------------------"
$commande "
DROP TABLE IF EXISTS tache_urbaine_2020.temp_enveloppe_bati_etalab_000_2020
"
$commande "
CREATE TABLE tache_urbaine_2020.temp_enveloppe_bati_etalab_000_2020
(
	code_dep character(3),
	geom geometry(Polygon,2154)
);
"

echo "A.2.2] Ajout des départements dans la table nationale temporaire"
#DEBUG
#for dpt in '001' '038' '069'
for dpt in '001' '002' '003' '004' '005' '006' '007' '008' '009' '010' '011' '012' '013' '014' '015' '016' '017' '018' '019' '02b' '02a' '021' '022' '023' '024' '025' '026' '027' '028' '029' '030' '031' '032' '033' '034' '035' '036' '037' '038' '039' '040' '041' '042' '043' '044' '045' '046' '047' '048' '049' '050' '051' '052' '053' '054' '055' '056' '057' '058' '059' '060' '061' '062' '063' '064' '065' '066' '067' '068' '069' '070' '071' '072' '073' '074' '075' '076' '077' '078' '079' '080' '081' '082' '083' '084' '085' '086' '087' '088' '089' '090' '091' '092' '093' '094' '095'

do

echo "Département $dpt"
$commande "
INSERT INTO tache_urbaine_2020.temp_enveloppe_bati_etalab_000_2020 (code_dep, geom)
	SELECT code_dep, geom
	FROM tache_urbaine_2020.l_enveloppe_bati_pepci_"$dpt"_2020;
"

done

echo "A.2.3] Préparation de la phase A.2.4"
now=$(date +"%T")
echo "Début à $now"
echo "-------------------------------------"
echo "A.2.3.1] Index Géomérique"

$commande "
CREATE INDEX temp_enveloppe_bati_etalab_000_2020_geom_gist ON tache_urbaine_2020.temp_enveloppe_bati_etalab_000_2020
	USING gist (geom) TABLESPACE index;
"
echo "A.2.3.2] Identifiant pour la phase A.3"
$commande "
ALTER TABLE tache_urbaine_2020.temp_enveloppe_bati_etalab_000_2020
	ADD COLUMN id serial;
"
echo "A.2.3.3] Sauvegarde en cas d'echec de la A.3"
$commande "
DROP TABLE IF EXISTS tache_urbaine_2020.sauve_temp_enveloppe_bati_etalab_000_2020;
CREATE TABLE tache_urbaine_2020.sauve_temp_enveloppe_bati_etalab_000_2020 AS
	SELECT * FROM tache_urbaine_2020.temp_enveloppe_bati_etalab_000_2020;
"


echo "A.2.4] Création de la table nationale"
now=$(date +"%T")
echo "Début à $now"
echo "------------------------------------"
$commande "
DROP TABLE IF EXISTS tache_urbaine_2020.l_enveloppe_bati_pepci_000_2020;
CREATE TABLE tache_urbaine_2020.l_enveloppe_bati_pepci_000_2020
(
    id_enveloppe serial,
	nb_batidur int4,
	nb_batileg int4,
	surf_batidur int4,
	surf_batileg int4,
	list_code_dep varchar,
    geom geometry(Polygon,2154)
);
"

echo "A.2.4.1] Sélection des enveloppes qui s'intersectent"
now=$(date +"%T")
echo "Début à $now"
echo "------------------------------------"
$commande "
DROP TABLE IF EXISTS tache_urbaine_2020.stintersects_enveloppe_bati_etalab_000_2020;
CREATE TABLE tache_urbaine_2020.stintersects_enveloppe_bati_etalab_000_2020 AS
	SELECT ST_Union(t1.geom,t2.geom)::geometry(Polygon,2154) as geometrie_union
		FROM tache_urbaine_2020.temp_enveloppe_bati_etalab_000_2020 as t1
		JOIN tache_urbaine_2020.temp_enveloppe_bati_etalab_000_2020 as t2
		ON ST_Intersects(t1.geom, t2.geom)
		WHERE t1.id < t2.id;
-->
" 
$commande "
CREATE INDEX stintersects_enveloppe_bati_etalab_000_2020_geom_gist
	ON tache_urbaine_2020.stintersects_enveloppe_bati_etalab_000_2020
	USING gist (geometrie_union) TABLESPACE index;
"

echo "A.2.4.2] Peuplement par agregation des zones de chevauchement interdépartementales"
now=$(date +"%T")
echo "Début à $now"
echo "------------------------------------"
$commande "
INSERT INTO tache_urbaine_2020.l_enveloppe_bati_pepci_000_2020 (geom)
SELECT (ST_Dump(ST_Union(geometrie_union))).geom::geometry(Polygon,2154) as geom
		FROM tache_urbaine_2020.stintersects_enveloppe_bati_etalab_000_2020;
--> INSERT 0 25040 / Requête exécutée avec succès en 43 hr 58 min.
"

echo "A.2.4.3] Suppression des zones de des zones de"
echo "         chevauchement interdépartementales que l'on vient d'inserer"
now=$(date +"%T")
echo "Début à $now"
echo "---------------------------------------------------------------------"
$commande "
DELETE FROM tache_urbaine_2020.temp_enveloppe_bati_etalab_000_2020 WHERE id IN (
		(SELECT t1.id
		FROM tache_urbaine_2020.temp_enveloppe_bati_etalab_000_2020 as t1
		JOIN tache_urbaine_2020.temp_enveloppe_bati_etalab_000_2020 as t2
		ON ST_Intersects(t1.geom, t2.geom)
		WHERE t1.id < t2.id)
	UNION ALL
		(SELECT t2.id
		FROM tache_urbaine_2020.temp_enveloppe_bati_etalab_000_2020 as t1
		JOIN tache_urbaine_2020.temp_enveloppe_bati_etalab_000_2020 as t2
		ON ST_Intersects(t1.geom, t2.geom)
		WHERE t1.id < t2.id)
);
--> 
"

echo "---------------------------------------------------------"
echo "A.2.4.4] Insersion du reste : ce qui ne se supperpose pas :"
echo "         On vérifie qu'il n'y a rien qui s'intersecte"
echo "---------------------------------------------------------"
$commande "
CREATE INDEX l_enveloppe_bati_pepci_000_2020_geom_gist
	ON tache_urbaine_2020.l_enveloppe_bati_pepci_000_2020
	USING gist (geom) TABLESPACE index;

--SELECT *
--FROM tache_urbaine_2020.temp_enveloppe_bati_etalab_000_2020 as t1
--JOIN tache_urbaine_2020.l_enveloppe_bati_pepci_000_2020 as t2
--ON ST_Intersects(t1.geom, t2.geom);
-->
"
echo "---------------------------------------------------------"
$commande "
DROP INDEX tache_urbaine_2020.l_enveloppe_bati_pepci_000_2020_geom_gist;
"

echo "A.2.4.5] On insere !"
$commande "
INSERT INTO tache_urbaine_2020.l_enveloppe_bati_pepci_000_2020 (geom)
	SELECT geom::geometry(Polygon,2154) as geom
	FROM tache_urbaine_2020.temp_enveloppe_bati_etalab_000_2020;
--> INSERT 0 1535920 / Requête exécutée avec succès en 2 min 29 secs.
"

echo "A.2.4.6] Index Géomérique + Cluster"
$commande "
CREATE INDEX l_enveloppe_bati_pepci_000_2020_geom_gist ON tache_urbaine_2020.l_enveloppe_bati_pepci_000_2020
	USING gist (geom) TABLESPACE index;
ALTER TABLE tache_urbaine_2020.l_enveloppe_bati_pepci_000_2020
	CLUSTER ON l_enveloppe_bati_pepci_000_2020_geom_gist;
"
$commande "
CREATE INDEX sauve_temp_enveloppe_bati_etalab_000_2020_geom_gist ON tache_urbaine_2020.sauve_temp_enveloppe_bati_etalab_000_2020
	USING gist (geom) TABLESPACE index;
ALTER TABLE tache_urbaine_2020.sauve_temp_enveloppe_bati_etalab_000_2020
	CLUSTER ON sauve_temp_enveloppe_bati_etalab_000_2020_geom_gist;
"
#DEBUG : Pas fait sur le referentiel cadastre ETALAB 2020 ! 
#$commande "
#CREATE INDEX n_bati_etalab_000_2020_type_idx
#    ON r_cadastre_etalab_2020.n_bati_etalab_000_2020 USING brin
#    (type)
#    TABLESPACE index;
#"

echo "A.2.5] Mise à jour des champs"
echo "A.2.5.1] Mise à jour des champs nb_batidur"
now=$(date +"%T")
echo "Début à $now"
echo "------------------------------------"
$commande "
UPDATE tache_urbaine_2020.l_enveloppe_bati_pepci_000_2020 as t1 SET nb_batidur = (
	SELECT count(*)
	FROM r_parcellaire_express.n_batiment_pepci_000_2019 as t2
	WHERE	ST_Intersects(t1.geom, t2.geom)
	AND (t2.type='Bâtiment en dur' or t2.type='Bâti dur' or t2.type='Bati dur' or t2.type='01')
	);
--> 
"

echo "A.2.5.2] Mise à jour des champs nb_batileg"
now=$(date +"%T")
echo "Début à $now"
echo "------------------------------------"
$commande "
UPDATE tache_urbaine_2020.l_enveloppe_bati_pepci_000_2020 as t1 SET nb_batileg = (
	SELECT count(*)
	FROM r_parcellaire_express.n_batiment_pepci_000_2019 as t2
	WHERE	ST_Intersects(t1.geom, t2.geom)
	AND	(t2.type='Construction légère' or t2.type='Bâti léger' or t2.type='Bati leger' or t2.type='02')
	);
--> Requête exécutée avec succès en 2 min 8 secs.
"
echo "A.2.5.3] Mise à jour des champs surf_batidur"
now=$(date +"%T")
echo "Début à $now"
echo "------------------------------------"
$commande "
UPDATE tache_urbaine_2019.l_enveloppe_bati_pepci_000_2019 as t1 SET surf_batidur = (
	SELECT sum(ST_Area(t2.geom))
	FROM r_parcellaire_express.n_batiment_pepci_000_2019 as t2
	WHERE 	
		ST_Intersects(t1.geom, t2.geom)
		AND 
		(t2.type=''01'' or t2.type=''Bâtiment en dur'' or t2.type=''Bâti dur'' or t2.type=''Bati dur'')
	);
"
echo "A.2.5.4] Mise à jour des champs surf_batileg"
now=$(date +"%T")
echo "Début à $now"
echo "------------------------------------"
$commande "
UPDATE tache_urbaine_2019.l_enveloppe_bati_pepci_000_2019 as t1 SET surf_batileg = (
	SELECT sum(ST_Area(t2.geom))
	FROM r_parcellaire_express.n_batiment_pepci_000_2019 as t2
	WHERE 	
		ST_Intersects(t1.geom, t2.geom)
		AND
		(t2.type=''02'' or t2.type=''Construction légère'' or t2.type=''Bâti léger'' or t2.type=''Bati leger'')
	);	
"
echo "A.2.5.5] Mise à jour du champs list_code_dep"
now=$(date +"%T")
echo "Début à $now"
echo "------------------------------------"
#DEBUG : si plusieurs taches fusionnées d une même département on a un doublon
#$commande "
#UPDATE tache_urbaine_2020.l_enveloppe_bati_pepci_000_2020 as t1 SET list_code_dep = (
#	SELECT array_agg(code_dep)::varchar
#	FROM tache_urbaine_2020.sauve_temp_enveloppe_bati_etalab_000_2020 as t2
#	WHERE	ST_Intersects(t1.geom, t2.geom)
#	);
#--> UPDATE 38360 / Requête exécutée avec succès en 1 min 25 secs.
#"

#DEBUG : Enleve les doublons de départements, mais on perds l information de la fusion de plusieurs tache dans le même département
$commande "
UPDATE tache_urbaine_2020.l_enveloppe_bati_pepci_000_2020 as t1 SET list_code_dep = (
	WITH resultat1 AS 
		(SELECT DISTINCT t2.code_dep
		FROM tache_urbaine_2020.sauve_temp_enveloppe_bati_etalab_000_2020 as t2
		WHERE ST_Intersects(t1.geom, t2.geom)
		ORDER BY t2.code_dep)
	SELECT array_agg(code_dep)::varchar FROM resultat1
	);
--> 
"

echo "A.2.6] Optimisations"
now=$(date +"%T")
echo "Début à $now"
echo "------------------------------------"
echo "A.2.6.1] Clé primaire"
$commande "
ALTER TABLE tache_urbaine_2020.l_enveloppe_bati_pepci_000_2020
    ADD CONSTRAINT l_enveloppe_bati_pepci_000_2020_id_enveloppe_pk PRIMARY KEY (id_enveloppe)
    USING INDEX TABLESPACE index;
ALTER TABLE tache_urbaine_2020.l_enveloppe_bati_pepci_000_2020
    ADD CONSTRAINT enforce_dims_geom CHECK (ST_ndims(geom) = 2);
ALTER TABLE tache_urbaine_2020.l_enveloppe_bati_pepci_000_2020
	ADD CONSTRAINT enforce_srid_geom CHECK (ST_Srid(geom)= 2154);
ALTER TABLE tache_urbaine_2020.l_enveloppe_bati_pepci_000_2020
    ADD CONSTRAINT enforce_geotype_geom CHECK (geometrytype(geom) = 'POLYGON'::text);
"
echo "A.2.6.2] Index attributaires"
$commande "
CREATE INDEX l_enveloppe_bati_pepci_000_2020_nbatidur_idx ON tache_urbaine_2020.l_enveloppe_bati_pepci_000_2020
	USING brin (nbatidur) TABLESPACE index;
CREATE INDEX l_enveloppe_bati_pepci_000_2020_nbatileg_idx ON tache_urbaine_2020.l_enveloppe_bati_pepci_000_2020
	USING brin (nbatileg) TABLESPACE index;
CREATE INDEX l_enveloppe_bati_pepci_000_2020_surf_batidur_idx ON tache_urbaine_2020.l_enveloppe_bati_pepci_000_2020
	USING brin (surf_batidur) TABLESPACE index;
CREATE INDEX l_enveloppe_bati_pepci_000_2020_surf_batileg_idx ON tache_urbaine_2020.l_enveloppe_bati_pepci_000_2020
	USING brin (surf_batileg) TABLESPACE index;
CREATE INDEX l_enveloppe_bati_pepci_000_2020_list_code_dep_idx ON tache_urbaine_2020.l_enveloppe_bati_pepci_000_2020
	USING brin (list_code_dep) TABLESPACE index;
"

echo "A.2.6.3] Index Géomérique + Cluster"
#DEBUG Déjà fait en partie # B.2.1]

echo "A.2.6.4] Commentaires"
$commande "
COMMENT ON TABLE tache_urbaine_2020.l_enveloppe_bati_pepci_000_2020
  IS 'Enveloppe nationale du bâti de 2020.

Il s’agit de fusionner tous les bâtiments issu du cadastre ETALAB qui sont séparés de moins de 50 m par un Buffer +50, ST_Union, Erosion de 40m';
"
$commande "
COMMENT ON COLUMN tache_urbaine_2020.l_enveloppe_bati_pepci_000_2020.id_enveloppe
    IS 'Identifiant national unique pour une même enveloppe du bati';
COMMENT ON COLUMN tache_urbaine_2020.l_enveloppe_bati_pepci_000_2020.nbatidur
    IS 'Nombre de bâtiments qualifiés de dur par la DGFiP intégrés dans cette enveloppe du bati.';
COMMENT ON COLUMN tache_urbaine_2020.l_enveloppe_bati_pepci_000_2020.nbatileg
    IS 'Nombre de bâtiments qualifiés de legers par la DGFiP intégrés dans cette enveloppe du bati.';
COMMENT ON COLUMN tache_urbaine_2019.l_enveloppe_bati_pepci_000_2019.surf_batidur
    IS ''Surface totale en m2 de tous les bâtiments qualifiés de dur par la DGFiP et présents dans cette enveloppe du bâti.'';
COMMENT ON COLUMN tache_urbaine_2019.l_enveloppe_bati_pepci_000_2019.surf_batileg
    IS ''Surface totale en m2 de tous les bâtiments qualifiés de leger par la DGFiP et présents dans cette enveloppe du bâti.'';	
COMMENT ON COLUMN tache_urbaine_2020.l_enveloppe_bati_pepci_000_2020.list_code_dep
    IS 'Liste des départements à l³origine de cette enveloppe du bati.';	
COMMENT ON COLUMN tache_urbaine_2020.l_enveloppe_bati_pepci_000_2020.geom
    IS 'Champs géométrique : Monopolygone en Lambert93 (EPSG:2154).';
"

echo "A.2.7] Netoyage des tables temporaires"
now=$(date +"%T")
echo "Début à $now"
echo "------------------------------------"
$commande "
DROP TABLE IF EXISTS tache_urbaine_2020.temp_departement_buffer_200m_000_2020;
----#DEBUG : on garde pour vérifier 
--DROP TABLE IF EXISTS tache_urbaine_2020.temp_enveloppe_bati_etalab_000_2020;
DROP TABLE IF EXISTS tache_urbaine_2020.stintersects_enveloppe_bati_etalab_000_2020;
"

#Execution :
# time sh /home/administrateur/Documents/A2_enveloppe_urbaine_2020.sh 1>/mnt/data/02_enveloppe_urbaine_2020_A2.log 2>&1
# real	
# user	
# sys	
# Echec et fini manuellement à partir du A.2.5.3] Mise à jour du champs list_code_dep

# 18 mai 2020
# Mise aux normes des tables départementales :
DO $$
DECLARE
liste_valeur 				CHARACTER VARYING[];
req 						text;
BEGIN
--liste_valeur := ARRAY  ['090'];
liste_valeur := ARRAY  [
						'001','002','003','004','005','006','007','008','009','010',
						'011','012','013','014','015','016','017','018','019','02a','02b',
						'021','022','023','024','025','026','027','028','029','030',
						'031','032','033','034','035','036','037','038','039','040',
						'041','042','043','044','045','046','047','048','049','050',
						'051','052','053','054','055','056','057','058','059','060',
						'061','062','063','064','065','066','067','068','069','070',
						'071','072','073','074','075','076','077','078','079','080',
						'081','082','083','084','085','086','087','088','089',
						'091','092','093','094','095'
						];
FOR i_table IN 1..array_length(liste_valeur, 1)
LOOP
		req := '
				ALTER TABLE tache_urbaine_2019.l_enveloppe_bati_pepci_'|| liste_valeur[i_table] ||'_2019 RENAME COLUMN nbatidur TO nb_batidur;
				ALTER TABLE tache_urbaine_2019.l_enveloppe_bati_pepci_'|| liste_valeur[i_table] ||'_2019 RENAME COLUMN nbatileg TO nb_batileg;
				ALTER TABLE tache_urbaine_2019.l_enveloppe_bati_pepci_'|| liste_valeur[i_table] ||'_2019
							ADD column surf_batidur integer,
							ADD column surf_batileg integer;
				
				COMMENT ON COLUMN tache_urbaine_2019.l_enveloppe_bati_pepci_'|| liste_valeur[i_table] ||'_2019.surf_batidur
				    IS ''Surface totale en m2 de tous les bâtiments qualifiés de dur par la DGFiP et présents dans cette enveloppe du bâti.'';
				COMMENT ON COLUMN tache_urbaine_2019.l_enveloppe_bati_pepci_'|| liste_valeur[i_table] ||'_2019.surf_batileg
				    IS ''Surface totale en m2 de tous les bâtiments qualifiés de leger par la DGFiP et présents dans cette enveloppe du bâti.'';	

				UPDATE tache_urbaine_2019.l_enveloppe_bati_pepci_'|| liste_valeur[i_table] ||'_2019 as t1 SET surf_batidur = (
					SELECT sum(ST_Area(t2.geom))
					FROM r_parcellaire_express.n_batiment_pepci_000_2019 as t2
					WHERE 	
						ST_Intersects(t1.geom, t2.geom)
						AND 
						(t2.type=''01'' or t2.type=''Bâtiment en dur'' or t2.type=''Bâti dur'' or t2.type=''Bati dur'')
					);

				UPDATE tache_urbaine_2019.l_enveloppe_bati_pepci_'|| liste_valeur[i_table] ||'_2019 as t1 SET surf_batileg = (
					SELECT sum(ST_Area(t2.geom))
					FROM r_parcellaire_express.n_batiment_pepci_000_2019 as t2
					WHERE 	
						ST_Intersects(t1.geom, t2.geom)
						AND
						(t2.type=''02'' or t2.type=''Construction légère'' or t2.type=''Bâti léger'' or t2.type=''Bati leger'')
					);				

				CREATE INDEX l_enveloppe_bati_pepci_'|| liste_valeur[i_table] ||'_2019_surf_batidur_idx
						ON tache_urbaine_2019.l_enveloppe_bati_pepci_'|| liste_valeur[i_table] ||'_2019 USING brin (surf_batidur);
				CREATE INDEX l_enveloppe_bati_pepci_'|| liste_valeur[i_table] ||'_2019_surf_batileg_idx
						ON tache_urbaine_2019.l_enveloppe_bati_pepci_'|| liste_valeur[i_table] ||'_2019 USING brin (surf_batileg);
				';
		RAISE NOTICE '%', req;
		EXECUTE(req);
		COMMIT;
END LOOP;
END $$;
# Requête exécutée avec succès en 3 h 30 min.













/*
# A.3] Sauvegarde
# A.3.1] Sauvegarde du schéma complet
---- Sauvegarde :
time pg_dump --port=5432 --dbname="ceremabase" --schema="tache_urbaine_2019" --username "postgres" --role "postgres" --format=plain --encoding UTF8 --inserts --column-inserts --verbose --quote-all-identifiers --file="/mnt/data/sauve_tache_urbaine_2019.sql"
#real	3m25,890s
#user	0m33,239s
#sys	0m28,120s
# A.3.2] Sauvegarde de la table finale
time pg_dump --port=5432 --dbname="ceremabase" --table="tache_urbaine_2019.l_enveloppe_bati_pepci_000_2019" --username "postgres" --role "postgres" --format=plain --encoding UTF8 --inserts --column-inserts --verbose --quote-all-identifiers --file="/mnt/data/sauve_tache_urbaine_2019.l_enveloppe_bati_pepci_000_2019.sql"
#real	1m14,090s
#user	0m16,494s
#sys	0m13,609s



