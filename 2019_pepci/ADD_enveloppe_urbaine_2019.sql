# A] Bati Agrege
#!/bin/bash
# Tache Urbaine
#for dpt in '090' '039' '025' '001' '002' '003' '004' '005' '006' '007' '008' '009' '010' '011' '012' '013' '014' '015' '016' '017' '018' '019' '021' '022' '023' '024' '026' '027' '028' '029' '030' '031' '032' '033' '034' '035' '036' '037' '038' '040' '041' '042' '043' '044' '045' '046' '047' '048' '049' '050' '051' '052' '053' '054' '055' '056' '057' '058' '059' '060' '061' '062' '063' '064' '065' '066' '067' '068' '069' '070' '071' '072' '073' '074' '075' '076' '077' '078' '079' '080' '081' '082' '083' '084' '085' '086' '087' '088' '089' '091' '092' '093' '094' '095'
#for dpt in '02b' '02a'

do

# A.1] Création des tables Départementales
psql -p 5444 -d ceremabase -c "DROP TABLE IF EXISTS tache_urbaine_2019.l_bati_agrege_pepci_"$dpt"_2019;"
psql -p 5444 -d ceremabase -c "
CREATE TABLE tache_urbaine_2019.l_bati_agrege_pepci_"$dpt"_2019
	(
	id serial,
	nbatidur integer,
	nbatileg integer,
	code_dep character(3) DEFAULT '"$dpt"',  
	geom geometry(Polygon,2154)
	);
"

psql -p 5444 -d ceremabase -c "
ALTER SEQUENCE tache_urbaine_2019.l_bati_agrege_pepci_"$dpt"_2019_id_seq
	RESTART WITH "$dpt"000001;
"

# A.2] Buffer +0,01, ST_Union, Buffer -0,01, ST_Dump
psql -p 5444 -d ceremabase -c "
INSERT INTO tache_urbaine_2019.l_bati_agrege_pepci_"$dpt"_2019 (geom)
	SELECT (ST_Dump(ST_Buffer(ST_Union(ST_Buffer(geom,0,01)),-0,01))).geom::geometry(Polygon,2154) as geom
	FROM r_parcellaire_express.n_batiment_pepci_"$dpt"_2019;
"

# A.3] Mise à jour des champs nbatidur & nbatileg
psql -p 5444 -d ceremabase -c "
UPDATE tache_urbaine_2019.l_bati_agrege_pepci_"$dpt"_2019 as t1 SET nbatidur = (
	SELECT count(*)
	FROM r_parcellaire_express.n_batiment_pepci_"$dpt"_2019 as t2
	WHERE 	t1.code_dep = '"$dpt"'
		and (t2.type='Bâtiment en dur' or t2.type='Bâti dur' or t2.type='Bati dur' or t2.type='01')
		and ST_Intersects(t1.geom, t2.geom))
	WHERE 	t1.code_dep = '"$dpt"';
"

psql -p 5444 -d ceremabase -c "
UPDATE tache_urbaine_2019.l_bati_agrege_pepci_"$dpt"_2019 as t1 SET nbatileg = (
	SELECT count(*)
	FROM r_parcellaire_express.n_batiment_pepci_"$dpt"_2019 as t2
	WHERE 	t1.code_dep = '"$dpt"'
		and (type='Construction légère' or type='Bâti léger' or type='Bati leger' or type='02')
		and ST_Intersects(t1.geom, t2.geom))
	WHERE 	t1.code_dep = '"$dpt"';
"

# A.4] Optimisation 
# Clé primaire 
psql -p 5444 -d ceremabase -c "
ALTER TABLE tache_urbaine_2019.l_bati_agrege_pepci_"$dpt"_2019
    ADD CONSTRAINT l_bati_agrege_pepci_"$dpt"_2019_pk PRIMARY KEY (id)
    USING INDEX TABLESPACE index;
"
# Contraintes Géométriques
psql -p 5444 -d ceremabase -c "
ALTER TABLE tache_urbaine_2019.l_bati_agrege_pepci_"$dpt"_2019
    ADD CONSTRAINT enforce_dims_geom CHECK (ST_ndims(geom) = 2);
"
psql -p 5444 -d ceremabase -c "
ALTER TABLE tache_urbaine_2019.l_bati_agrege_pepci_"$dpt"_2019
	ADD CONSTRAINT enforce_srid_geom CHECK (ST_Srid(geom)= 2154);
"
psql -p 5444 -d ceremabase -c "
ALTER TABLE tache_urbaine_2019.l_bati_agrege_pepci_"$dpt"_2019
    ADD CONSTRAINT enforce_geotype_geom CHECK (geometrytype(geom) = 'POLYGON'::text);
"
# Index attributaires
psql -p 5444 -d ceremabase -c "
CREATE INDEX l_bati_agrege_pepci_"$dpt"_2019_nbatidur_idx ON tache_urbaine_2019.l_bati_agrege_pepci_"$dpt"_2019
	USING brin (nbatidur) TABLESPACE index;
"
psql -p 5444 -d ceremabase -c "
CREATE INDEX l_bati_agrege_pepci_"$dpt"_2019_nbatileg_idx ON tache_urbaine_2019.l_bati_agrege_pepci_"$dpt"_2019
	USING brin (nbatileg) TABLESPACE index;
"
psql -p 5444 -d ceremabase -c "
CREATE INDEX l_bati_agrege_pepci_"$dpt"_2019_code_dep_idx ON tache_urbaine_2019.l_bati_agrege_pepci_"$dpt"_2019
	USING brin (code_dep) TABLESPACE index;
"
# Index Géomérique + Cluster
psql -p 5444 -d ceremabase -c "
CREATE INDEX l_bati_agrege_pepci_"$dpt"_2019_geom_gist ON tache_urbaine_2019.l_bati_agrege_pepci_"$dpt"_2019
	USING gist (geom) TABLESPACE index;
"
psql -p 5444 -d ceremabase -c "
ALTER TABLE tache_urbaine_2019.l_bati_agrege_pepci_"$dpt"_2019
	CLUSTER ON l_bati_agrege_pepci_"$dpt"_2019_geom_gist;
"

# A.5] Commentaires
# Table
psql -p 5444 -d ceremabase -c "
COMMENT ON TABLE tache_urbaine_2019.l_bati_agrege_pepci_"$dpt"_2019
  IS 'Bâti agrégé de 2019 pour le département ''"$dpt"''.

Il s’agit de fusionner tous les bâtiments du parcellaire express qui sont séparés de moins de 1 cm par un Buffer +0,01, ST_Union, Buffer -0,01, ST_Dump';
"
# Champs
psql -p 5444 -d ceremabase -c "
COMMENT ON COLUMN tache_urbaine_2019.l_bati_agrege_pepci_"$dpt"_2019.id
    IS 'Identifiant unique selon méthode COVADIS : n°département.000.000 + id.';
"
psql -p 5444 -d ceremabase -c "
COMMENT ON COLUMN tache_urbaine_2019.l_bati_agrege_pepci_"$dpt"_2019.nbatidur
    IS 'Nombre de bâtiments qualifiés de dur par la DGFiP intégrés dans cette agrégation de bâtiments.';
"
psql -p 5444 -d ceremabase -c "
COMMENT ON COLUMN tache_urbaine_2019.l_bati_agrege_pepci_"$dpt"_2019.nbatileg
    IS 'Nombre de bâtiments qualifiés de legers par la DGFiP intégrés dans cette agrégation de bâtiments.';
"
psql -p 5444 -d ceremabase -c "
COMMENT ON COLUMN tache_urbaine_2019.l_bati_agrege_pepci_"$dpt"_2019.code_dep
    IS 'Numéro INSEE du département sur 3 charactères.';
"
psql -p 5444 -d ceremabase -c "
COMMENT ON COLUMN tache_urbaine_2019.l_bati_agrege_pepci_"$dpt"_2019.geom
    IS 'Champs géométrique : Monopolygones en Lambert93 (EPSG:2154).';
"

done


# B] Tache Urbaine
#!/bin/bash
# Tache Urbaine
#psql ceremabase -c "
#CREATE TABLE tache_urbaine_2019.temp_departement_buffer_200m_000_2019 AS
#	SELECT code_dep, ST_Multi(ST_Buffer(ST_Union(geom),200))::geometry(MultiPolygon,2154) as geom
#	FROM r_parcellaire_express.n_commune_pepci_000
#	GROUP BY code_dep;

#CREATE INDEX temp_departement_buffer_200m_000_2019_code_dep_idx
#    ON tache_urbaine_2019.temp_departement_buffer_200m_000_2019 USING brin
#    (code_dep)
#    TABLESPACE index;

#CREATE INDEX temp_departement_buffer_200m_000_2019_geom_gist
#    ON tache_urbaine_2019.temp_departement_buffer_200m_000_2019 USING gist
#    (geom)
#    TABLESPACE index;

#ALTER TABLE tache_urbaine_2019.temp_departement_buffer_200m_000_2019
#    CLUSTER ON temp_departement_buffer_200m_000_2019_geom_gist;
#"

# B] Tache Urbaine PARCELLAIRE EXPRESS 2019
# B.1] Création de la table temporaire d'addition des départements
psql ceremabase -c "DROP TABLE IF EXISTS tache_urbaine_2019.temp_enveloppe_bati_pepci_2019;"
psql ceremabase -c "CREATE TABLE tache_urbaine_2019.temp_enveloppe_bati_pepci_2019
(
	geom geometry(Polygon,2154)
);"

for dpt in '001' '002' '003' '004' '005' '006' '007' '008' '009' '010' '011' '012' '013' '014' '015' '016' '017' '018' '019' '02b' '02a' '021' '022' '023' '024' '025' '026' '027' '028' '029' '030' '031' '032' '033' '034' '035' '036' '037' '038' '039' '040' '041' '042' '043' '044' '045' '046' '047' '048' '049' '050' '051' '052' '053' '054' '055' '056' '057' '058' '060' '061' '062' '063' '064' '065' '066' '067' '068' '069' '070' '071' '072' '073' '074' '075' '076' '077' '078' '079' '080' '081' '082' '083' '084' '085' '086' '087' '088' '089' '090' '091' '092' '093' '094' '095'

do

# Buffer de +50m / Aggregation / Erosion de 40m pour chaque département
psql ceremabase -c "
INSERT INTO tache_urbaine_2019.temp_enveloppe_bati_pepci_2019 (geom)
	SELECT (ST_Dump(ST_Buffer(ST_Union(ST_Buffer(t1.geom,50)),-40))).geom::geometry(Polygon,2154) as geom
	FROM r_parcellaire_express.n_batiment_pepci_000 as t1
	JOIN tache_urbaine_2019.temp_departement_buffer_200m_000_2019 as t2
	ON ST_Intersects (t1.geom, t2.geom)
	WHERE t2.code_dep = upper('"$dpt"');
"

done

# Spécificité pour le 59/Nord - Trop Gros département
psql ceremabase -c "
CREATE TABLE tache_urbaine_2019.temp_commune_buffer_100m_059_2019 AS
	SELECT code_insee, ST_Multi(ST_Buffer(geom,100))::geometry(MultiPolygon,2154) as geom
	FROM r_parcellaire_express.n_commune_pepci_059_2019;
--> SELECT 651

CREATE INDEX temp_commune_buffer_100m_059_2019_geom_gist
    ON tache_urbaine_2019.temp_commune_buffer_100m_059_2019 USING gist
    (geom)
    TABLESPACE index;
--> CREATE INDEX / Requête exécutée avec succès en 271 msec.


CREATE TABLE tache_urbaine_2019.temp1_buffer_059_2019 AS 
SELECT t2.code_insee, ST_multi(ST_Buffer(t1.geom,50))::geometry(MultiPolygon,2154) as geom	
	FROM r_parcellaire_express.n_batiment_pepci_000 as t1
	JOIN tache_urbaine_2019.temp_commune_buffer_100m_059_2019 as t2
	ON ST_Intersects (t1.geom, t2.geom)
	ORDER BY t2.code_insee;
--> SELECT 1936961 / Requête exécutée avec succès en 5 min 38 secs. 

Create table tache_urbaine_2019.temp2_union_059_2019 as
SELECT ST_multi(ST_Union(geom))::geometry(MultiPolygon,2154) as geom
FROM tache_urbaine_2019.temp1_buffer_059_2019
GROUP BY code_insee;
--> SELECT 648 / Requête exécutée avec succès en 9 min 47 secs.

INSERT INTO tache_urbaine_2019.temp_enveloppe_bati_pepci_2019 (geom)
SELECT (ST_Dump(ST_Buffer(ST_Union(geom),-40))).geom::geometry(Polygon,2154) as geom
FROM tache_urbaine_2019.temp2_union_059_2019
--> INSERT 0 17848 / Requête exécutée avec succès en 40 min 4 secs.

DROP TABLE IF EXISTS tache_urbaine_2019.temp_commune_buffer_100m_059_2019;
DROP TABLE IF EXISTS tache_urbaine_2019.temp1_buffer_059_2019;
DROP TABLE IF EXISTS tache_urbaine_2019.temp2_union_059_2019;
--> DROP TABLE
"

# Index Géomérique
psql ceremabase -c "
CREATE INDEX temp_enveloppe_bati_pepci_2019_geom_gist ON tache_urbaine_2019.temp_enveloppe_bati_pepci_2019
	USING gist (geom) TABLESPACE index;
"
# Identifiant pour la phase B.2
psql ceremabase -c "
ALTER TABLE tache_urbaine_2019.temp_enveloppe_bati_pepci_2019
	ADD COLUMN id serial;
"
# Sauvegarde en cas d'echec de la B.2
psql ceremabase -c "
DROP TABLE IF EXISTS tache_urbaine_2019.sauve_temp_enveloppe_bati_pepci_2019;
CREATE TABLE tache_urbaine_2019.sauve_temp_enveloppe_bati_pepci_2019 AS
	SELECT * FROM tache_urbaine_2019.temp_enveloppe_bati_pepci_2019;
"

# B.2] Création de la table nationale
psql ceremabase -c "
CREATE TABLE tache_urbaine_2019.l_enveloppe_bati_pepci_2019
(
    id_enveloppe serial,
    nbatidur integer,
    nbatileg integer,
    geom geometry(Polygon,2154)
);
"
# B.2.1] Sélection des enveloppes qui s'intersectent
psql ceremabase -c "
CREATE TABLE tache_urbaine_2019.stintersects_enveloppe_bati_pepci_2019 AS
	SELECT ST_Union(t1.geom,t2.geom)::geometry(Polygon,2154) as geometrie_union
		FROM tache_urbaine_2019.temp_enveloppe_bati_pepci_2019 as t1
		JOIN tache_urbaine_2019.temp_enveloppe_bati_pepci_2019 as t2
		ON ST_Intersects(t1.geom, t2.geom)
		WHERE t1.id < t2.id;

CREATE INDEX stintersects_enveloppe_bati_pepci_2019_geom_gist
	ON tache_urbaine_2019.stintersects_enveloppe_bati_pepci_2019
	USING gist (geometrie_union) TABLESPACE index;
"


# B.2.3] Peuplement par agregation des zones de chevauchement interdépartementales
psql ceremabase -c "
INSERT INTO tache_urbaine_2019.l_enveloppe_bati_pepci_2019 (geom)
SELECT (ST_Dump(ST_Union(geometrie_union))).geom::geometry(Polygon,2154) as geom
		FROM tache_urbaine_2019.stintersects_enveloppe_bati_pepci_2019;
--> INSERT 0 25040 / Requête exécutée avec succès en 43 hr 58 min.
"

# Suppression des zones de des zones de chevauchement interdépartementales que l'on vient d'inserer
psql ceremabase -c "
DELETE FROM tache_urbaine_2019.temp_enveloppe_bati_pepci_2019 WHERE id IN (
		(SELECT t1.id
		FROM tache_urbaine_2019.temp_enveloppe_bati_pepci_2019 as t1
		JOIN tache_urbaine_2019.temp_enveloppe_bati_pepci_2019 as t2
		ON ST_Intersects(t1.geom, t2.geom)
		WHERE t1.id < t2.id)
	UNION ALL
		(SELECT t2.id
		FROM tache_urbaine_2019.temp_enveloppe_bati_pepci_2019 as t1
		JOIN tache_urbaine_2019.temp_enveloppe_bati_pepci_2019 as t2
		ON ST_Intersects(t1.geom, t2.geom)
		WHERE t1.id < t2.id)
);
--> DELETE 51457 / Requête exécutée avec succès en 11 min 12 secs.
"

# B.2.4] Insersion du reste : ce qui ne se supperpose pas :
# B.2.4.1] On vérifie qu'il n'y a rien qui s'intersecte
psql ceremabase -c "
CREATE INDEX l_enveloppe_bati_pepci_2019_geom_gist
	ON tache_urbaine_2019.l_enveloppe_bati_pepci_2019
	USING gist (geom) TABLESPACE index;

SELECT *
FROM tache_urbaine_2019.temp_enveloppe_bati_pepci_2019 as t1
JOIN tache_urbaine_2019.l_enveloppe_bati_pepci_2019 as t2
ON ST_Intersects(t1.geom, t2.geom);
--> Exécution réussie. Temps total :  3 min 35 secs / 0 lignes affectées.

DROP INDEX tache_urbaine_2019.l_enveloppe_bati_pepci_2019_geom_gist;
"
# B.2.4.2] On insere !
psql ceremabase -c "
INSERT INTO tache_urbaine_2019.l_enveloppe_bati_pepci_2019 (geom)
	SELECT geom::geometry(Polygon,2154) as geom
	FROM tache_urbaine_2019.temp_enveloppe_bati_pepci_2019;
--> INSERT 0 1535920 / Requête exécutée avec succès en 2 min 29 secs.
"

# Spécificité pour les '02a' et '02b' --> résolu l'année prochaine avec un upper sur le champs dpt
/*
DROP TABLE IF EXISTS tache_urbaine_2019.temp_enveloppe_bati_pepci_corse;
CREATE TABLE tache_urbaine_2019.temp_enveloppe_bati_pepci_corse
(
	geom geometry(Polygon,2154)
);

INSERT INTO tache_urbaine_2019.temp_enveloppe_bati_pepci_corse (geom)
	SELECT (ST_Dump(ST_Buffer(ST_Union(ST_Buffer(t1.geom,50)),-40))).geom::geometry(Polygon,2154) as geom
	FROM r_parcellaire_express.n_batiment_pepci_000 as t1
	JOIN tache_urbaine_2019.temp_departement_buffer_200m_000_2019 as t2
	ON ST_Intersects (t1.geom, t2.geom)
	WHERE t2.code_dep = upper('02a');
--> INSERT 0 9876
INSERT INTO tache_urbaine_2019.temp_enveloppe_bati_pepci_corse (geom)
	SELECT (ST_Dump(ST_Buffer(ST_Union(ST_Buffer(t1.geom,50)),-40))).geom::geometry(Polygon,2154) as geom
	FROM r_parcellaire_express.n_batiment_pepci_000 as t1
	JOIN tache_urbaine_2019.temp_departement_buffer_200m_000_2019 as t2
	ON ST_Intersects (t1.geom, t2.geom)
	WHERE t2.code_dep = upper('02b');
--> INSERT 0 11432

ALTER TABLE tache_urbaine_2019.temp_enveloppe_bati_pepci_corse
	ADD COLUMN id serial;
CREATE TABLE tache_urbaine_2019.stintersects_enveloppe_bati_pepci_corse AS
	SELECT ST_Union(t1.geom,t2.geom)::geometry(Polygon,2154) as geometrie_union
		FROM tache_urbaine_2019.temp_enveloppe_bati_pepci_corse as t1
		JOIN tache_urbaine_2019.temp_enveloppe_bati_pepci_corse as t2
		ON ST_Intersects(t1.geom, t2.geom)
		WHERE t1.id < t2.id;
--> SELECT 16

INSERT INTO tache_urbaine_2019.l_enveloppe_bati_pepci_2019 (geom)
SELECT (ST_Dump(ST_Union(geometrie_union))).geom::geometry(Polygon,2154) as geom
		FROM tache_urbaine_2019.stintersects_enveloppe_bati_pepci_corse;
--> INSERT 0 15

DELETE FROM tache_urbaine_2019.temp_enveloppe_bati_pepci_corse WHERE id IN (
		(SELECT t1.id
		FROM tache_urbaine_2019.temp_enveloppe_bati_pepci_corse as t1
		JOIN tache_urbaine_2019.temp_enveloppe_bati_pepci_corse as t2
		ON ST_Intersects(t1.geom, t2.geom)
		WHERE t1.id < t2.id)
	UNION ALL
		(SELECT t2.id
		FROM tache_urbaine_2019.temp_enveloppe_bati_pepci_corse as t1
		JOIN tache_urbaine_2019.temp_enveloppe_bati_pepci_corse as t2
		ON ST_Intersects(t1.geom, t2.geom)
		WHERE t1.id < t2.id));
--> DELETE 31

SELECT *
FROM tache_urbaine_2019.temp_enveloppe_bati_pepci_corse as t1
JOIN tache_urbaine_2019.l_enveloppe_bati_pepci_2019 as t2
ON ST_Intersects(t1.geom, t2.geom);
--> 0

INSERT INTO tache_urbaine_2019.l_enveloppe_bati_pepci_2019 (geom)
	SELECT geom::geometry(Polygon,2154) as geom
	FROM tache_urbaine_2019.temp_enveloppe_bati_pepci_corse;
--> INSERT 0 21277

DROP TABLE IF EXISTS tache_urbaine_2019.temp_enveloppe_bati_pepci_corse;
DROP TABLE IF EXISTS tache_urbaine_2019.stintersects_enveloppe_bati_pepci_corse;
--> DROP TABLE
*/


# Index Géomérique + Cluster
psql ceremabase -c "
CREATE INDEX l_enveloppe_bati_pepci_2019_geom_gist ON tache_urbaine_2019.l_enveloppe_bati_pepci_2019
	USING gist (geom) TABLESPACE index;
ALTER TABLE tache_urbaine_2019.l_enveloppe_bati_pepci_2019
	CLUSTER ON l_enveloppe_bati_pepci_2019_geom_gist;
"

# B.2.5] Mise à jour des champs nbatidur & nbatileg
psql ceremabase -c "
UPDATE tache_urbaine_2019.l_enveloppe_bati_pepci_2019 as t1 SET nbatidur = (
	SELECT count(*)
	FROM r_parcellaire_express.n_batiment_pepci_000 as t2
	WHERE	(t2.type='Bâtiment en dur' or t2.type='Bâti dur' or t2.type='Bati dur' or t2.type='01')
			and ST_Intersects(t1.geom, t2.geom));
--> UPDATE 1582252 / Requête exécutée avec succès en 4 hr 4 min.
"

psql ceremabase -c "
UPDATE tache_urbaine_2019.l_enveloppe_bati_pepci_2019 as t1 SET nbatileg = (
	SELECT count(*)
	FROM r_parcellaire_express.n_batiment_pepci_000 as t2
	WHERE 	(t2.type='Construction légère' or t2.type='Bâti léger' or t2.type='Bati leger' or t2.type='02')
		and ST_Intersects(t1.geom, t2.geom));
--> UPDATE 1582252
"

# B.2.3] Optimisations
# Clé primaire 
psql ceremabase -c "
ALTER TABLE tache_urbaine_2019.l_enveloppe_bati_pepci_2019
    ADD CONSTRAINT l_enveloppe_bati_pepci_2019_id_enveloppe_pk PRIMARY KEY (id_enveloppe)
    USING INDEX TABLESPACE index;
"
# Contraintes Géométriques
psql ceremabase -c "
ALTER TABLE tache_urbaine_2019.l_enveloppe_bati_pepci_2019
    ADD CONSTRAINT enforce_dims_geom CHECK (ST_ndims(geom) = 2);
"
psql ceremabase -c "
ALTER TABLE tache_urbaine_2019.l_enveloppe_bati_pepci_2019
	ADD CONSTRAINT enforce_srid_geom CHECK (ST_Srid(geom)= 2154);
"
psql ceremabase -c "
ALTER TABLE tache_urbaine_2019.l_enveloppe_bati_pepci_2019
    ADD CONSTRAINT enforce_geotype_geom CHECK (geometrytype(geom) = 'POLYGON'::text);
"
# Index attributaires
psql ceremabase -c "
CREATE INDEX l_enveloppe_bati_pepci_2019_nbatidur_idx ON tache_urbaine_2019.l_enveloppe_bati_pepci_2019
	USING brin (nbatidur) TABLESPACE index;
"
psql ceremabase -c "
CREATE INDEX l_enveloppe_bati_pepci_2019_nbatileg_idx ON tache_urbaine_2019.l_enveloppe_bati_pepci_2019
	USING brin (nbatileg) TABLESPACE index;
"

# Index Géomérique + Cluster
# Déjà fait en partie # B.2.1]

# B.2.4] Commentaires
# Table
psql ceremabase -c "
COMMENT ON TABLE tache_urbaine_2019.l_enveloppe_bati_pepci_2019
  IS 'Enveloppe nationale du bâti de 2019.

Il s’agit de fusionner tous les bâtiments du parcellaire express qui sont séparés de moins de 50 m par un Buffer +50, ST_Union, Erosion de 40m';
"
# Champs
psql ceremabase -c "
COMMENT ON COLUMN tache_urbaine_2019.l_enveloppe_bati_pepci_2019.id_enveloppe
    IS 'Identifiant national unique pour une même enveloppe du bati';
"
psql ceremabase -c "
COMMENT ON COLUMN tache_urbaine_2019.l_enveloppe_bati_pepci_2019.nbatidur
    IS 'Nombre de bâtiments qualifiés de dur par la DGFiP intégrés dans cette enveloppe du bati.';
COMMENT ON COLUMN tache_urbaine_2019.l_enveloppe_bati_pepci_2019.nbatileg
    IS 'Nombre de bâtiments qualifiés de legers par la DGFiP intégrés dans cette enveloppe du bati.';
COMMENT ON COLUMN tache_urbaine_2019.l_enveloppe_bati_pepci_2019.geom
    IS 'Champs géométrique : Monopolygones en Lambert93 (EPSG:2154).';
";
#psql ceremabase -c "DROP TABLE IF EXISTS tache_urbaine_2019.temp_departement_buffer_200m_000_2019;"
#psql ceremabase -c "DROP TABLE IF EXISTS tache_urbaine_2019.temp_enveloppe_bati_pepci_2019;"
#psql ceremabase -c "DROP TABLE IF EXISTS tache_urbaine_2019.stintersects_enveloppe_bati_pepci_2019;"

/*
---- Spécificité pour le Nord - Trop Gros département
create table tache_urbaine_2019.temp1_buffer_059_2019 as
	SELECT ST_multi(ST_Buffer(t1.geom,50))::geometry(MultiPolygon,2154) as geom
	FROM r_parcellaire_express.n_batiment_pepci_000 as t1
	JOIN tache_urbaine_2019.temp_departement_buffer_200m_000_2019 as t2
	ON ST_Intersects (t1.geom, t2.geom)
	WHERE t2.code_dep = '059';
--> SELECT 1751158 / Requête exécutée avec succès en 12 min 45 secs.

Create table tache_urbaine_2019.temp2_union_059_2019 as
SELECT ST_Union(geom)::geometry(MultiPolygon,2154) as geom
FROM tache_urbaine_2019.temp2_buffer_059_2019
WHERE ;
--> ERROR:  array size exceeds the maximum allowed (1073741823)

ALTER table tache_urbaine_2019.temp2_buffer_059_2019
ADD column id serial;
--> ALTER TABLE / Requête exécutée avec succès en 48 secs 477 msec.

CREATE INDEX temp1_gist
    ON tache_urbaine_2019.temp1_buffer_059_2019 USING gist
    (geom)
    TABLESPACE index;
--> CREATE

SELECT ST_Union(t1.geom)::geometry(MultiPolygon,2154) as geom
FROM tache_urbaine_2019.temp2_buffer_059_2019 as t1
JOIN tache_urbaine_2019.temp2_buffer_059_2019 as t2
ON ST_Intersects (t1.geom,t2.geom)
WHERE t1.id > t2.id ;
*/

# C] Tache Urbaine BATI BDTOPO 2019
# C.1] Création de la table temporaire d addition des départements
psql -p 5432 ceremabase -c "DROP TABLE IF EXISTS tache_urbaine_2019.temp_enveloppe_bati_bdtopo_2019;"
psql -p 5432 ceremabase -c "CREATE TABLE tache_urbaine_2019.temp_enveloppe_bati_bdtopo_2019
(
	geom geometry(Polygon,2154)
);"

#for dpt in '02a' '02b' 
for dpt in '02b' '02a' '001' '002' '003' '004' '005' '006' '007' '008' '009' '010' '011' '012' '013' '014' '015' '016' '017' '018' '019'  '021' '022' '023' '024' '025' '026' '027' '028' '029' '030' '031' '032' '033' '034' '035' '036' '037' '038' '039' '040' '041' '042' '043' '044' '045' '046' '047' '048' '049' '050' '051' '052' '053' '054' '055' '056' '057' '058' '059' '060' '061' '062' '063' '064' '065' '066' '067' '068' '069' '070' '071' '072' '073' '074' '075' '076' '077' '078' '079' '080' '081' '082' '083' '084' '085' '086' '087' '088' '089' '090' '091' '092' '093' '094' '095'

do

# Buffer de +50m / Aggregation / Erosion de 40m pour chaque département
echo $dpt
psql -p 5432 ceremabase -c "
INSERT INTO tache_urbaine_2019.temp_enveloppe_bati_bdtopo_2019 (geom)
	SELECT (ST_Dump(ST_Buffer(ST_Union(ST_Buffer(t1.geom,50)),-40))).geom::geometry(Polygon,2154) as geom
	FROM r_bdtopo_2019.n_batiment_bdt_000 as t1
	JOIN tache_urbaine_2019.temp_departement_buffer_200m_000_2019 as t2
	ON ST_Intersects (t1.geom, t2.geom)
	WHERE t2.code_dep = upper('"$dpt"');
"

done


# Index Géomérique
psql -p 5432 ceremabase -c "
CREATE INDEX temp_enveloppe_bati_bdtopo_2019_geom_gist ON tache_urbaine_2019.temp_enveloppe_bati_bdtopo_2019
	USING gist (geom) TABLESPACE index;
"
# Identifiant pour la phase B.2
psql -p 5432 ceremabase -c "
ALTER TABLE tache_urbaine_2019.temp_enveloppe_bati_bdtopo_2019
	ADD COLUMN id serial;
"
# Sauvegarde en cas d echec de la B.2
psql -p 5432 ceremabase -c "
DROP TABLE IF EXISTS tache_urbaine_2019.sauve_temp_enveloppe_bati_bdtopo_2019;
CREATE TABLE tache_urbaine_2019.sauve_temp_enveloppe_bati_bdtopo_2019 AS
	SELECT * FROM tache_urbaine_2019.temp_enveloppe_bati_bdtopo_2019;
"
--> SELECT 1665359


# B.2] Création de la table nationale
psql ceremabase -c "
CREATE TABLE tache_urbaine_2019.l_enveloppe_bati_bdtopo_2019
(
    id_enveloppe serial,
    nbatidur integer,
    nbatileg integer,
    geom geometry(Polygon,2154)
);
"

# B.2.1] Sélection des enveloppes qui s intersectent
psql ceremabase -c "
CREATE TABLE tache_urbaine_2019.stintersects_enveloppe_bati_bdtopo_2019 AS
	SELECT ST_Union(t1.geom,t2.geom)::geometry(Polygon,2154) as geometrie_union
		FROM tache_urbaine_2019.temp_enveloppe_bati_bdtopo_2019 as t1
		JOIN tache_urbaine_2019.temp_enveloppe_bati_bdtopo_2019 as t2
		ON ST_Intersects(t1.geom, t2.geom)
		WHERE t1.id < t2.id;
--> SELECT 27757
CREATE INDEX stintersects_enveloppe_bati_bdtopo_2019_geom_gist
	ON tache_urbaine_2019.stintersects_enveloppe_bati_bdtopo_2019
	USING gist (geometrie_union) TABLESPACE index;
"

# B.2.3] Peuplement par agregation des zones de chevauchement interdépartementales
psql ceremabase -c "
INSERT INTO tache_urbaine_2019.l_enveloppe_bati_bdtopo_2019 (geom)
SELECT (ST_Dump(ST_Union(geometrie_union))).geom::geometry(Polygon,2154) as geom
		FROM tache_urbaine_2019.stintersects_enveloppe_bati_bdtopo_2019;
--> ERREUR:  GEOSUnaryUnion: TopologyException: found non-noded intersection between LINESTRING (652632 6.84881e+06, 652630 6.84881e+06) and LINESTRING (652632 6.84881e+06, 652626 6.84881e+06) at 652630.90817867685 6848809.4792262372

----> Erreur ?
UPDATE tache_urbaine_2019.stintersects_enveloppe_bati_bdtopo_2019 SET geometrie_union =
	CASE 
		WHEN GeometryType(geometrie_union) = 'POLYGON' 		OR GeometryType(geometrie_union) = 'MULTIPOLYGON' THEN
				ST_Multi(ST_Simplify(ST_Multi(ST_CollectionExtract(ST_ForceCollection(ST_MakeValid(geometrie_union)),3)),0))
		WHEN GeometryType(geometrie_union) = 'LINESTRING' 	OR GeometryType(geometrie_union) = 'MULTILINESTRING' THEN
				ST_Multi(ST_Simplify(ST_Multi(ST_CollectionExtract(ST_ForceCollection(ST_MakeValid(geometrie_union)),2)),0))
		WHEN GeometryType(geometrie_union) = 'POINT' 		OR GeometryType(geometrie_union) = 'MULTIPOINT' THEN
				ST_Multi(ST_Simplify(ST_Multi(ST_CollectionExtract(ST_ForceCollection(ST_MakeValid(geometrie_union)),1)),0))
		ELSE ST_MakeValid(geometrie_union)
	END
WHERE NOT ST_Isvalid(geometrie_union);

SELECT GeometryType(geometrie_union) FROM tache_urbaine_2019.stintersects_enveloppe_bati_bdtopo_2019 WHERE GeometryType(geometrie_union) != 'POLYGON';
--> (0 ligne)

CREATE TABLE tache_urbaine_2019.stunion_enveloppe_bati_bdtopo_2019 AS
SELECT ST_Union(geometrie_union)::geometry(Polygon,2154) as geom
		FROM tache_urbaine_2019.stintersects_enveloppe_bati_bdtopo_2019;


/*INSERT 0 25040 / Requête exécutée avec succès en 43 hr 58 min.
"

# Suppression des zones de des zones de chevauchement interdépartementales que l'on vient d'inserer
psql ceremabase -c "
DELETE FROM tache_urbaine_2019.temp_enveloppe_bati_pepci_2019 WHERE id IN (
		(SELECT t1.id
		FROM tache_urbaine_2019.temp_enveloppe_bati_pepci_2019 as t1
		JOIN tache_urbaine_2019.temp_enveloppe_bati_pepci_2019 as t2
		ON ST_Intersects(t1.geom, t2.geom)
		WHERE t1.id < t2.id)
	UNION ALL
		(SELECT t2.id
		FROM tache_urbaine_2019.temp_enveloppe_bati_pepci_2019 as t1
		JOIN tache_urbaine_2019.temp_enveloppe_bati_pepci_2019 as t2
		ON ST_Intersects(t1.geom, t2.geom)
		WHERE t1.id < t2.id)
);
--> DELETE 51457 / Requête exécutée avec succès en 11 min 12 secs.
"

# B.2.4] Insersion du reste : ce qui ne se supperpose pas :
# B.2.4.1] On vérifie qu'il n'y a rien qui s'intersecte
psql ceremabase -c "
CREATE INDEX l_enveloppe_bati_pepci_2019_geom_gist
	ON tache_urbaine_2019.l_enveloppe_bati_pepci_2019
	USING gist (geom) TABLESPACE index;

SELECT *
FROM tache_urbaine_2019.temp_enveloppe_bati_pepci_2019 as t1
JOIN tache_urbaine_2019.l_enveloppe_bati_pepci_2019 as t2
ON ST_Intersects(t1.geom, t2.geom);
--> Exécution réussie. Temps total :  3 min 35 secs / 0 lignes affectées.

DROP INDEX tache_urbaine_2019.l_enveloppe_bati_pepci_2019_geom_gist;
"
# B.2.4.2] On insere !
psql ceremabase -c "
INSERT INTO tache_urbaine_2019.l_enveloppe_bati_pepci_2019 (geom)
	SELECT geom::geometry(Polygon,2154) as geom
	FROM tache_urbaine_2019.temp_enveloppe_bati_pepci_2019;
--> INSERT 0 1535920 / Requête exécutée avec succès en 2 min 29 secs.
"

# Spécificité pour les '02a' et '02b' --> résolu l'année prochaine avec un upper sur le champs dpt


# Index Géomérique + Cluster
psql ceremabase -c "
CREATE INDEX l_enveloppe_bati_pepci_2019_geom_gist ON tache_urbaine_2019.l_enveloppe_bati_pepci_2019
	USING gist (geom) TABLESPACE index;
ALTER TABLE tache_urbaine_2019.l_enveloppe_bati_pepci_2019
	CLUSTER ON l_enveloppe_bati_pepci_2019_geom_gist;
"

# B.2.5] Mise à jour des champs nbatidur & nbatileg
psql ceremabase -c "
UPDATE tache_urbaine_2019.l_enveloppe_bati_pepci_2019 as t1 SET nbatidur = (
	SELECT count(*)
	FROM r_parcellaire_express.n_batiment_pepci_000 as t2
	WHERE	(t2.type='Bâtiment en dur' or t2.type='Bâti dur' or t2.type='Bati dur' or t2.type='01')
			and ST_Intersects(t1.geom, t2.geom));
--> UPDATE 1582252 / Requête exécutée avec succès en 4 hr 4 min.
"

psql ceremabase -c "
UPDATE tache_urbaine_2019.l_enveloppe_bati_pepci_2019 as t1 SET nbatileg = (
	SELECT count(*)
	FROM r_parcellaire_express.n_batiment_pepci_000 as t2
	WHERE 	(t2.type='Construction légère' or t2.type='Bâti léger' or t2.type='Bati leger' or t2.type='02')
		and ST_Intersects(t1.geom, t2.geom));
--> UPDATE 1582252
"

# B.2.3] Optimisations
# Clé primaire 
psql ceremabase -c "
ALTER TABLE tache_urbaine_2019.l_enveloppe_bati_pepci_2019
    ADD CONSTRAINT l_enveloppe_bati_pepci_2019_id_enveloppe_pk PRIMARY KEY (id_enveloppe)
    USING INDEX TABLESPACE index;
"
# Contraintes Géométriques
psql ceremabase -c "
ALTER TABLE tache_urbaine_2019.l_enveloppe_bati_pepci_2019
    ADD CONSTRAINT enforce_dims_geom CHECK (ST_ndims(geom) = 2);
"
psql ceremabase -c "
ALTER TABLE tache_urbaine_2019.l_enveloppe_bati_pepci_2019
	ADD CONSTRAINT enforce_srid_geom CHECK (ST_Srid(geom)= 2154);
"
psql ceremabase -c "
ALTER TABLE tache_urbaine_2019.l_enveloppe_bati_pepci_2019
    ADD CONSTRAINT enforce_geotype_geom CHECK (geometrytype(geom) = 'POLYGON'::text);
"
# Index attributaires
psql ceremabase -c "
CREATE INDEX l_enveloppe_bati_pepci_2019_nbatidur_idx ON tache_urbaine_2019.l_enveloppe_bati_pepci_2019
	USING brin (nbatidur) TABLESPACE index;
"
psql ceremabase -c "
CREATE INDEX l_enveloppe_bati_pepci_2019_nbatileg_idx ON tache_urbaine_2019.l_enveloppe_bati_pepci_2019
	USING brin (nbatileg) TABLESPACE index;
"

# Index Géomérique + Cluster
# Déjà fait en partie # B.2.1]

# B.2.4] Commentaires
# Table
psql ceremabase -c "
COMMENT ON TABLE tache_urbaine_2019.l_enveloppe_bati_pepci_2019
  IS 'Enveloppe nationale du bâti de 2019.

Il s’agit de fusionner tous les bâtiments du parcellaire express qui sont séparés de moins de 50 m par un Buffer +50, ST_Union, Erosion de 40m';
"
# Champs
psql ceremabase -c "
COMMENT ON COLUMN tache_urbaine_2019.l_enveloppe_bati_pepci_2019.id_enveloppe
    IS 'Identifiant national unique pour une même enveloppe du bati';
"
psql ceremabase -c "
COMMENT ON COLUMN tache_urbaine_2019.l_enveloppe_bati_pepci_2019.nbatidur
    IS 'Nombre de bâtiments qualifiés de dur par la DGFiP intégrés dans cette enveloppe du bati.';
COMMENT ON COLUMN tache_urbaine_2019.l_enveloppe_bati_pepci_2019.nbatileg
    IS 'Nombre de bâtiments qualifiés de legers par la DGFiP intégrés dans cette enveloppe du bati.';
COMMENT ON COLUMN tache_urbaine_2019.l_enveloppe_bati_pepci_2019.geom
    IS 'Champs géométrique : Monopolygones en Lambert93 (EPSG:2154).';
";
#psql ceremabase -c "DROP TABLE IF EXISTS tache_urbaine_2019.temp_departement_buffer_200m_000_2019;"
#psql ceremabase -c "DROP TABLE IF EXISTS tache_urbaine_2019.temp_enveloppe_bati_pepci_2019;"
#psql ceremabase -c "DROP TABLE IF EXISTS tache_urbaine_2019.stintersects_enveloppe_bati_pepci_2019;"

---- 27 juin 2020
---- Ajout nouvelle contraintes Géométriques :
DO $$
DECLARE
liste_valeur 				CHARACTER VARYING[];
req 						text;
BEGIN
liste_valeur := ARRAY  [--'000',
						--'r11','r24','r27','r28','r32','r44','r52','r53','r75','r76','r84','r93','r94',
						'001','002','003','004','005','006','007','008','009','010',
						'011','012','013','014','015','016','017','018','019','02a','02b',
						'021','022','023','024','025','026','027','028','029','030',
						'031','032','033','034','035','036','037','038','039','040',
						'041','042','043','044','045','046','047','048','049','050',
						'051','052','053','054','055','056','057','058','059','060',
						'061','062','063','064','065','066','067','068','069','070',
						'071','072','073','074','075','076','077','078','079','080',
						'081','082','083','084','085','086','087','088','089','090',
						'091','092','093','094','095'--,'971','972','973','974','976'
						];
FOR i_table IN 1..array_length(liste_valeur, 1) LOOP
		req := '
				ALTER TABLE tache_urbaine_2019.l_enveloppe_bati_pepci_'|| liste_valeur[i_table] ||'_2019 ADD CONSTRAINT enforce_validite_geom CHECK (ST_IsValid(geom));
				ALTER TABLE tache_urbaine_2019.l_enveloppe_bati_pepci_'|| liste_valeur[i_table] ||'_2019 ADD CONSTRAINT enforce_no_self_intersection_geom CHECK (ST_IsSimple(geom));
				ALTER TABLE tache_urbaine_2019.l_enveloppe_bati_pepci_'|| liste_valeur[i_table] ||'_2019 ADD CONSTRAINT enforce_no_zero_area_geom CHECK (ST_Area(geom) > 0);
				';
		RAISE NOTICE '%', req;
		EXECUTE(req);
		COMMIT;
END LOOP;
END $$;

---- 08/05/2021 - Synthèse
---- https://gitlab.cerema.fr/centre-est/admin_ceremabase/-/blob/master/fonctions/synthese_geometrie_schema.sql
CREATE table tache_urbaine_2019.synthese_geometrie_schema AS
SELECT * FROM w_fonctions.synthese_geometrie_schema('tache_urbaine_2019');
--> Updated Rows	210