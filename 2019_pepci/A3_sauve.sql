# 20/10/2020
# A] Sauvegardes

# A.1] Sauvegarde Schéma :
time pg_dump --port=5432 --dbname="ceremabase" --schema="tache_urbaine_2019" --username "postgres" --role "postgres" --format=plain --encoding UTF8 --inserts --column-inserts --verbose --quote-all-identifiers --file="/mnt/data/sauve_tache_urbaine_2019.sql" 1>/mnt/data/A3_sauve_enveloppe_urbaine_2019.log 2>&1
#real	14m0,387s
#user	2m19,686s
#sys	1m33,520s


# A.2] Sauvegarde de la table finale
time pg_dump --port=5432 --dbname="ceremabase" --table="tache_urbaine_2019.l_enveloppe_bati_pepci_000_2019" --username "postgres" --role "postgres" --format=plain --encoding UTF8 --inserts --column-inserts --verbose --quote-all-identifiers --file="/mnt/data/sauve_tache_urbaine_2019.l_enveloppe_bati_pepci_000_2019.sql" 1>/mnt/data/sauve_tache_urbaine_2019.l_enveloppe_bati_pepci_000_2019.log 2>&1
#real	2m36,098s
#user	0m19,293s
#sys	0m15,555s



