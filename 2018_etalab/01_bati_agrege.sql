echo "====================================================================================="
echo "	CREATION BATI AGREGE"
echo ""
echo "             CEREMABASE"
echo ""
echo "	Version 1 du 15 mai 2020"
echo "	c.badol"
echo "	Finalisé oui|X| / non | |"
echo "====================================================================================="
echo " Rq : ---- pour les commentaires / -- pour les commandes optionnelles, debuger"

CREATE SCHEMA p_enveloppe_bati_2018 AUTHORIZATION postgres;
COMMENT ON SCHEMA p_enveloppe_bati_2018 IS 'Enveloppe du bâti de 2018.
Création par fusion d un buffer de 50 m autour des batiments et érosion de -40 m.

Le référentiel utilisé est dans le nom de la table :
- pepci : Parcellaire Express de l IGN,
- etalab : cadastre disponible sur le site data.gouv.fr,
- bdtopo : batiments de la BDTOPO de l IGN.

Les scripts de ce travail sont disponibles ici : https://gitlab.com/christophe.badol/enveloppe_urbaine .';

echo "--------------------------------
echo "A] Bâti agrégé 2018
echo "--------------------------------
#!/bin/bash
# Tache Urbaine

# Important :
# Pour tout logger taper :
# time sh /home/administrateur/Documents/01_bati_agrege.sh 1>/mnt/data/01_bati_agrege_2018_10.log 2>&1

commande="psql -d ceremabase -c "

#log_corse for dpt in '02b' '02a'
# for dpt in '090'
# log1 : '02b' '02a'
# log1 : for dpt in '001' '002' '003' '004' '006' '005' '007' '008' '009' '010' 
# log2 : for dpt in '011' '012' '013' '014' '015' '016' '017'
# log3 : for dpt '018' '019' '021'
# log5 : for dpt in '022' '023' '024' '025' '026' '029' '030'
# log6 : for dpt in '031' '032' '033' '034' '035' '036' '037' '038' '039' '040'
# log7 : for dpt in '041' '042' '043' 
# log8 : for dpt in '044' '045' '046' '047' '048' '049' '050'
# log9 : for dpt in '051' '052' '053' '054' '055' '056' '057' '058'
# '059' à faire autrement
for dpt in '060' '061' '062' '063' '064' '065' '066' '067' '068' '069' '070' '071' '072' '073' '074' '075' '076' '077' '078' '079' '080' '081' '082' '083' '084' '085' '086' '087' '088' '089' '090' '091' '092' '093' '094' '095'

do
schemaorigine="r_cadastre_etalab_2018"
fichierorigine="n_bati_etalab_"$dpt"_2018"
schemabatiagrege="tache_urbaine_2018"
fichierbatiagrege="l_bati_agrege_pci_"$dpt"_2018"

emplacementorigine=$schemaorigine"."$fichierorigine
emplacementbatiagrege=$schemabatiagrege"."$fichierbatiagrege
#echo $emplacementorigine
#echo $emplacementbatiagrege

echo ---------------------------------------------
echo $fichierbatiagrege
now=$(date)
echo $now
echo ---------------------------------------------

echo A.0] Corrections des erreurs d origine
$commande "
UPDATE "$emplacementorigine" SET geom =
	CASE 
		WHEN GeometryType(geom) = 'POLYGON' 		OR GeometryType(geom) = 'MULTIPOLYGON' THEN
				ST_Multi(ST_Simplify(ST_Multi(ST_CollectionExtract(ST_ForceCollection(ST_MakeValid(geom)),3)),0))
		WHEN GeometryType(geom) = 'LINESTRING' 	OR GeometryType(geom) = 'MULTILINESTRING' THEN
				ST_Multi(ST_Simplify(ST_Multi(ST_CollectionExtract(ST_ForceCollection(ST_MakeValid(geom)),2)),0))
		WHEN GeometryType(geom) = 'POINT' 		OR GeometryType(geom) = 'MULTIPOINT' THEN
				ST_Multi(ST_Simplify(ST_Multi(ST_CollectionExtract(ST_ForceCollection(ST_MakeValid(geom)),1)),0))
		ELSE ST_MakeValid(geom)
	END
WHERE NOT ST_Isvalid(geom);
"

echo A.1] Création des tables Départementales
now=$(date +"%T")
echo "Début à $now"
echo "----------------"
$commande "DROP TABLE IF EXISTS "$emplacementbatiagrege";"
$commande "
CREATE TABLE "$emplacementbatiagrege"
	(
	id serial,
	nbatidur integer,
	nbatileg integer,
	code_dep character(3) DEFAULT '"$dpt"',  
	geom geometry(Polygon,4326)
	);
"

$commande "
ALTER SEQUENCE "$emplacementbatiagrege"_id_seq
	RESTART WITH "$dpt"000001;
"

echo A.2] Buffer +0,01, ST_Union, Buffer -0,01, ST_Dump
now=$(date +"%T")
echo "Début à $now"
echo "----------------"
$commande "
INSERT INTO "$emplacementbatiagrege" (geom)
	SELECT (ST_Dump(ST_Buffer(ST_Union(ST_Buffer(geom,0,01)),-0,01))).geom::geometry(Polygon,4326) as geom
	FROM "$emplacementorigine";
"

echo A.2.1] Corrections des erreurs si existantes
now=$(date +"%T")
echo "Début à $now"
$commande "
UPDATE "$emplacementbatiagrege" SET geom =
	CASE 
		WHEN GeometryType(geom) = 'POLYGON' 		OR GeometryType(geom) = 'MULTIPOLYGON' THEN
				ST_Multi(ST_Simplify(ST_Multi(ST_CollectionExtract(ST_ForceCollection(ST_MakeValid(geom)),3)),0))
		WHEN GeometryType(geom) = 'LINESTRING' 	OR GeometryType(geom) = 'MULTILINESTRING' THEN
				ST_Multi(ST_Simplify(ST_Multi(ST_CollectionExtract(ST_ForceCollection(ST_MakeValid(geom)),2)),0))
		WHEN GeometryType(geom) = 'POINT' 		OR GeometryType(geom) = 'MULTIPOINT' THEN
				ST_Multi(ST_Simplify(ST_Multi(ST_CollectionExtract(ST_ForceCollection(ST_MakeValid(geom)),1)),0))
		ELSE ST_MakeValid(geom)
	END
WHERE NOT ST_Isvalid(geom);
"

echo A.2.2] Index Géomérique + Cluster
now=$(date +"%T")
echo "Début à $now"
$commande "
CREATE INDEX "$fichierbatiagrege"_geom_gist ON "$emplacementbatiagrege"
	USING gist (geom) TABLESPACE index;
"
$commande "
ALTER TABLE "$emplacementbatiagrege"
	CLUSTER ON "$fichierbatiagrege"_geom_gist;
"

echo A.3.1] Mise à jour du champ nbatidur
now=$(date +"%T")
echo "Début à $now"
echo ----------------
$commande "
UPDATE "$emplacementbatiagrege" as t1 SET nbatidur = (
	SELECT count(*)
	FROM "$emplacementorigine" as t2
	WHERE 	t1.code_dep = '"$dpt"'
		and (t2.type='Bâtiment en dur' or t2.type='Bâti dur' or t2.type='Bati dur' or t2.type='01')
		and ST_Intersects(t1.geom, t2.geom))
	WHERE 	t1.code_dep = '"$dpt"';
"

echo A.3.2] Mise à jour du champ nbatileg
now=$(date +"%T")
echo "Début à $now"
echo "----------------"
$commande "
UPDATE "$emplacementbatiagrege" as t1 SET nbatileg = (
	SELECT count(*)
	FROM "$emplacementorigine" as t2
	WHERE 	t1.code_dep = '"$dpt"'
		and (type='Construction légère' or type='Bâti léger' or type='Bati leger' or type='02')
		and ST_Intersects(t1.geom, t2.geom))
	WHERE 	t1.code_dep = '"$dpt"';
"

echo A.4] Passage en Lambert93
now=$(date +"%T")
echo "Début à $now"
echo "----------------"
$commande "
ALTER TABLE "$emplacementbatiagrege"
RENAME COLUMN geom TO geom_old;
"
$commande "
ALTER TABLE "$emplacementbatiagrege"
ADD COLUMN geom geometry(Polygon,2154);
"
$commande "
UPDATE "$emplacementbatiagrege"  SET geom = ST_Transform(geom_old,2154);
"
$commande "
ALTER TABLE "$emplacementbatiagrege"
DROP COLUMN geom_old;
"

echo A.5] Optimisations
now=$(date +"%T")
echo "Début à $now"
# Clé primaire 
$commande "
CREATE UNIQUE INDEX "$fichierbatiagrege"_id_idx ON "$emplacementbatiagrege"
	USING btree (id) TABLESPACE index;
"
$commande "
ALTER TABLE "$emplacementbatiagrege" 
    ADD CONSTRAINT "$fichierbatiagrege"_pk PRIMARY KEY
	USING INDEX "$fichierbatiagrege"_id_idx;
"


# Contraintes Géométriques
$commande "
ALTER TABLE "$emplacementbatiagrege"
    ADD CONSTRAINT enforce_dims_geom CHECK (ST_ndims(geom) = 2);
"
$commande "
ALTER TABLE "$emplacementbatiagrege"
	ADD CONSTRAINT enforce_srid_geom CHECK (ST_Srid(geom)= 2154);
"
$commande "
ALTER TABLE "$emplacementbatiagrege"
    ADD CONSTRAINT enforce_geotype_geom CHECK (geometrytype(geom) = 'POLYGON'::text);
"
# Index attributaires
$commande "
CREATE INDEX "$fichierbatiagrege"_nbatidur_idx ON "$emplacementbatiagrege"
	USING brin (nbatidur) TABLESPACE index;
"
$commande "
CREATE INDEX "$fichierbatiagrege"_nbatileg_idx ON "$emplacementbatiagrege"
	USING brin (nbatileg) TABLESPACE index;
"
$commande "
CREATE INDEX "$fichierbatiagrege"_code_dep_idx ON "$emplacementbatiagrege"
	USING brin (code_dep) TABLESPACE index;
"
# Index geométrique + Cluster
$commande "
CREATE INDEX "$fichierbatiagrege"_geom_gist ON "$emplacementbatiagrege"
	USING gist (geom) TABLESPACE index;
"
$commande "
ALTER TABLE "$emplacementbatiagrege"
	CLUSTER ON "$fichierbatiagrege"_geom_gist;
"

echo A.6] Commentaires
now=$(date +"%T")
echo "Début à $now"
# Table
$commande "
COMMENT ON TABLE "$emplacementbatiagrege"
  IS 'Bâti agrégé de 2018 pour le département ''"$dpt"''.

Il s’agit de fusionner tous les bâtiments du parcellaire express qui sont séparés de moins de 1 cm par un Buffer +0,01, ST_Union, Buffer -0,01, ST_Dump';
"
# Champs
$commande "
COMMENT ON COLUMN "$emplacementbatiagrege".id
    IS 'Identifiant unique selon méthode COVADIS : n°département.000.000 + id.';
"
$commande "
COMMENT ON COLUMN "$emplacementbatiagrege".nbatidur
    IS 'Nombre de bâtiments qualifiés de dur par la DGFiP intégrés dans cette agrégation de bâtiments.';
"
$commande "
COMMENT ON COLUMN "$emplacementbatiagrege".nbatileg
    IS 'Nombre de bâtiments qualifiés de legers par la DGFiP intégrés dans cette agrégation de bâtiments.';
"
$commande "
COMMENT ON COLUMN "$emplacementbatiagrege".code_dep
    IS 'Numéro INSEE du département sur 3 charactères.';
"
$commande "
COMMENT ON COLUMN "$emplacementbatiagrege".geom
    IS 'Champs géométrique : Monopolygones en Lambert93 (EPSG:2154).';
"

done

#log_corse for dpt in '02b' '02a'
# for dpt in '090'
# log1 : '02b' '02a'
# log1 : for dpt in '001' '002' '003' '004' '006' '005' '007' '008' '009' '010' 
# log2 : for dpt in '011' '012' '013' '014' '015' '016' '017'
# log3 : for dpt '018' '019' '021'
# log5 : for dpt in '022' '023' '024' '025' '026' '029' '030'
# log6 : for dpt in '031' '032' '033' '034' '035' '036' '037' '038' '039' '040'
# log7 : for dpt in '041' '042' '043' 
# log8 : for dpt in '044' '045' '046' '047' '048' '049' '050'
# log9 : for dpt in '051' '052' '053' '054' '055' '056' '057' '058'
# '059' à faire autrement
# log10 : for dpt in '060' '061' '062' '063' '064' '065' '066' '067' '068' '069' '070' '071' '072' '073' '074' '075' '076' '077' '078' '079' '080' '081' '082' '083' '084' '085' '086' '087' '088' '089' '090' '091' '092' '093' '094' '095'
#real	8528m24,092s
#user	0m47,908s
#sys	0m11,633s


/*
schemaorigine="r_cadastre_etalab_2018"
fichierorigine="n_bati_etalab_"$dpt"_2018"
schemabatiagrege="tache_urbaine_2018"
fichierbatiagrege="l_bati_agrege_pci_"$dpt"_2018"

emplacementorigine=$schemaorigine"."$fichierorigine
emplacementbatiagrege=$schemabatiagrege"."$fichierbatiagrege
#echo $emplacementorigine
#echo $emplacementbatiagrege
*/

echo "------------------------------------------------------------
echo "A.1.59] Spécificité pour le 59/Nord - Trop Gros département"
echo "------------------------------------------------------------
echo "
$commande "
UPDATE tache_urbaine_2018.temp_commune_buffer_100m_059_2018 SET geom =
	CASE 
		WHEN GeometryType(geom) = 'POLYGON' 		OR GeometryType(geom) = 'MULTIPOLYGON' THEN
				ST_Multi(ST_Simplify(ST_Multi(ST_CollectionExtract(ST_ForceCollection(ST_MakeValid(geom)),3)),0))
		WHEN GeometryType(geom) = 'LINESTRING' 	OR GeometryType(geom) = 'MULTILINESTRING' THEN
				ST_Multi(ST_Simplify(ST_Multi(ST_CollectionExtract(ST_ForceCollection(ST_MakeValid(geom)),2)),0))
		WHEN GeometryType(geom) = 'POINT' 		OR GeometryType(geom) = 'MULTIPOINT' THEN
				ST_Multi(ST_Simplify(ST_Multi(ST_CollectionExtract(ST_ForceCollection(ST_MakeValid(geom)),1)),0))
		ELSE ST_MakeValid(geom)
	END
WHERE NOT ST_Isvalid(geom);
--> UPDATE 0 / Requête exécutée avec succès en 927 msec.
"
$commande "
CREATE TABLE tache_urbaine_2018.temp_commune_buffer_100m_059_2018 AS
	SELECT id as code_insee, ST_Multi(ST_Buffer(geom,0.0031))::geometry(MultiPolygon,4326) as geom
	FROM r_cadastre_etalab_2018.n_commune_etalab_059_2018;
--> SELECT 647 / Requête exécutée avec succès en 6 secs 357 msec.
"
$commande "
CREATE INDEX temp_commune_buffer_100m_059_2018_geom_gist
    ON tache_urbaine_2018.temp_commune_buffer_100m_059_2018 USING gist
    (geom)
    TABLESPACE index;
--> CREATE INDEX / Requête exécutée avec succès en 271 msec.
"
$commande "
DROP TABLE if exists tache_urbaine_2018.temp1_buffer_059_2018;
CREATE TABLE tache_urbaine_2018.temp1_buffer_059_2018 AS 
SELECT t2.code_insee, ST_multi(ST_Buffer(t1.geom,0,01))::geometry(MultiPolygon,4326) as geom	
	FROM r_cadastre_etalab_2018.n_bati_etalab_000_2018 as t1
	JOIN tache_urbaine_2018.temp_commune_buffer_100m_059_2018 as t2
	ON ST_Intersects (t1.geom, t2.geom);
--> Updated Rows	2266250
--> Total time (ms)	194545
CREATE INDEX temp1_buffer_059_2018_geom_gist ON tache_urbaine_2018.temp1_buffer_059_2018
	USING gist (geom) TABLESPACE index;
CREATE INDEX temp1_buffer_059_2018_code_insee_idx ON tache_urbaine_2018.temp1_buffer_059_2018
	USING brin (code_insee) TABLESPACE index;
--> CREATE INDEX / Requête exécutée avec succès en 40 secs 943 msec.
"
$commande "
Create table tache_urbaine_2018.temp2_union_059_2018 as
	SELECT ST_multi(ST_Union(geom))::geometry(MultiPolygon,4326) as geom
	FROM tache_urbaine_2018.temp1_buffer_059_2018
	GROUP BY code_insee;
--> Updated Rows	648
CREATE INDEX temp2_union_059_2018_geom_gist ON tache_urbaine_2018.temp2_union_059_2018
	USING gist (geom) TABLESPACE index;
--> CREATE INDEX / Requête exécutée avec succès en 127 msec.
"
$commande "
DROP TABLE IF EXISTS tache_urbaine_2018.l_enveloppe_bati_etalab_059_2018;
CREATE TABLE tache_urbaine_2018.l_enveloppe_bati_etalab_059_2018
(
	id serial,
	nbatidur integer,
	nbatileg integer,
	code_dep character(3) DEFAULT '059',  
	geom geometry(Polygon,4326)
);
ALTER SEQUENCE tache_urbaine_2018.l_enveloppe_bati_etalab_059_2018_id_seq
	RESTART WITH 59000001;
INSERT INTO tache_urbaine_2018.l_enveloppe_bati_etalab_059_2018 (geom)
	SELECT (ST_Dump(ST_Buffer(ST_Union(geom),-0,01))).geom::geometry(Polygon,4326) as geom
FROM tache_urbaine_2018.temp2_union_059_2018;
--> Updated Rows	719025
ALTER TABLE tache_urbaine_2018.l_enveloppe_bati_etalab_059_2018 RENAME TO l_bati_agrege_pci_059_2018;
"
echo A.2.1] Corrections des erreurs si existantes
$commande "
UPDATE tache_urbaine_2018.l_bati_agrege_pci_059_2018 SET geom =
	CASE 
		WHEN GeometryType(geom) = 'POLYGON' 		OR GeometryType(geom) = 'MULTIPOLYGON' THEN
				ST_Multi(ST_Simplify(ST_Multi(ST_CollectionExtract(ST_ForceCollection(ST_MakeValid(geom)),3)),0))
		WHEN GeometryType(geom) = 'LINESTRING' 	OR GeometryType(geom) = 'MULTILINESTRING' THEN
				ST_Multi(ST_Simplify(ST_Multi(ST_CollectionExtract(ST_ForceCollection(ST_MakeValid(geom)),2)),0))
		WHEN GeometryType(geom) = 'POINT' 		OR GeometryType(geom) = 'MULTIPOINT' THEN
				ST_Multi(ST_Simplify(ST_Multi(ST_CollectionExtract(ST_ForceCollection(ST_MakeValid(geom)),1)),0))
		ELSE ST_MakeValid(geom)
	END
WHERE NOT ST_Isvalid(geom);
"
echo A.2.2] Index Géomérique + Cluster
$commande "
CREATE INDEX l_bati_agrege_pci_059_2018_geom_gist ON tache_urbaine_2018.l_bati_agrege_pci_059_2018
	USING gist (geom) TABLESPACE index;
"
$commande "
ALTER TABLE tache_urbaine_2018.l_bati_agrege_pci_059_2018
	CLUSTER ON l_bati_agrege_pci_059_2018_geom_gist;
"
echo A.3.1] Mise à jour du champ nbatidur
$commande "
UPDATE tache_urbaine_2018.l_bati_agrege_pci_059_2018 as t1 SET nbatidur = (
	SELECT count(*)
	FROM r_cadastre_etalab_2018.n_bati_etalab_059_2018 as t2
	WHERE 	t1.code_dep = '059'
		and (t2.type='Bâtiment en dur' or t2.type='Bâti dur' or t2.type='Bati dur' or t2.type='01')
		and ST_Intersects(t1.geom, t2.geom))
	WHERE 	t1.code_dep = '059';
--> Updated Rows	719025
"
echo A.3.2] Mise à jour du champ nbatileg
$commande "
UPDATE tache_urbaine_2018.l_bati_agrege_pci_059_2018 as t1 SET nbatileg = (
	SELECT count(*)
	FROM r_cadastre_etalab_2018.n_bati_etalab_059_2018 as t2
	WHERE 	t1.code_dep = '059'
		and (type='Construction légère' or type='Bâti léger' or type='Bati leger' or type='02')
		and ST_Intersects(t1.geom, t2.geom))
	WHERE 	t1.code_dep = '059';
--> Updated Rows	719025
"
echo A.4] Passage en Lambert93
$commande "
ALTER TABLE tache_urbaine_2018.l_bati_agrege_pci_059_2018
RENAME COLUMN geom TO geom_old;
"
$commande "
ALTER TABLE tache_urbaine_2018.l_bati_agrege_pci_059_2018
ADD COLUMN geom geometry(Polygon,2154);
"
$commande "
UPDATE tache_urbaine_2018.l_bati_agrege_pci_059_2018  SET geom = ST_Transform(geom_old,2154);
--> Updated Rows	719025
"
$commande "
ALTER TABLE tache_urbaine_2018.l_bati_agrege_pci_059_2018
DROP COLUMN geom_old;
"
echo A.5] Optimisations
# Clé primaire 
$commande "
CREATE UNIQUE INDEX l_bati_agrege_pci_059_2018_id_idx ON tache_urbaine_2018.l_bati_agrege_pci_059_2018
	USING btree (id) TABLESPACE index;
"
$commande "
ALTER TABLE tache_urbaine_2018.l_bati_agrege_pci_059_2018
    ADD CONSTRAINT l_bati_agrege_pci_059_2018_pk PRIMARY KEY
	USING INDEX l_bati_agrege_pci_059_2018_id_idx;
"
# Contraintes Géométriques
$commande "
ALTER TABLE tache_urbaine_2018.l_bati_agrege_pci_059_2018
    ADD CONSTRAINT enforce_dims_geom CHECK (ST_ndims(geom) = 2);
"
$commande "
ALTER TABLE tache_urbaine_2018.l_bati_agrege_pci_059_2018
	ADD CONSTRAINT enforce_srid_geom CHECK (ST_Srid(geom)= 2154);
"
$commande "
ALTER TABLE tache_urbaine_2018.l_bati_agrege_pci_059_2018
    ADD CONSTRAINT enforce_geotype_geom CHECK (geometrytype(geom) = 'POLYGON'::text);
"
# Index attributaires
$commande "
CREATE INDEX l_bati_agrege_pci_059_2018_nbatidur_idx ON tache_urbaine_2018.l_bati_agrege_pci_059_2018
	USING brin (nbatidur) TABLESPACE index;
"
$commande "
CREATE INDEX l_bati_agrege_pci_059_2018_nbatileg_idx ON tache_urbaine_2018.l_bati_agrege_pci_059_2018
	USING brin (nbatileg) TABLESPACE index;
"
$commande "
CREATE INDEX l_bati_agrege_pci_059_2018_code_dep_idx ON tache_urbaine_2018.l_bati_agrege_pci_059_2018
	USING brin (code_dep) TABLESPACE index;
"
# Index geométrique + Cluster
$commande "
CREATE INDEX l_bati_agrege_pci_059_2018_geom_gist ON tache_urbaine_2018.l_bati_agrege_pci_059_2018
	USING gist (geom) TABLESPACE index;
"
$commande "
ALTER TABLE tache_urbaine_2018.l_bati_agrege_pci_059_2018
	CLUSTER ON l_bati_agrege_pci_059_2018_geom_gist;
"
echo A.6] Commentaires
# Table
$commande "
COMMENT ON TABLE tache_urbaine_2018.l_bati_agrege_pci_059_2018
  IS 'Bâti agrégé de 2018 pour le département 059.

Il s’agit de fusionner tous les bâtiments du parcellaire express qui sont séparés de moins de 1 cm par un Buffer +0,01, ST_Union, Buffer -0,01, ST_Dump';
"
# Champs
$commande "
COMMENT ON COLUMN tache_urbaine_2018.l_bati_agrege_pci_059_2018.id
    IS 'Identifiant unique selon méthode COVADIS : n°département.000.000 + id.';
"
$commande "
COMMENT ON COLUMN tache_urbaine_2018.l_bati_agrege_pci_059_2018.nbatidur
    IS 'Nombre de bâtiments qualifiés de dur par la DGFiP intégrés dans cette agrégation de bâtiments.';
"
$commande "
COMMENT ON COLUMN tache_urbaine_2018.l_bati_agrege_pci_059_2018.nbatileg
    IS 'Nombre de bâtiments qualifiés de legers par la DGFiP intégrés dans cette agrégation de bâtiments.';
"
$commande "
COMMENT ON COLUMN tache_urbaine_2018.l_bati_agrege_pci_059_2018.code_dep
    IS 'Numéro INSEE du département sur 3 charactères.';
"
$commande "
COMMENT ON COLUMN tache_urbaine_2018.l_bati_agrege_pci_059_2018.geom
    IS 'Champs géométrique : Monopolygones en Lambert93 (EPSG:2154).';
"

echo "------------------------------------------------------------"
echo "A.2] Partition "
echo "------------------------------------------------------------"
echo "A.2.1] Partition Nationale "
$commande "
select w_adl_delegue.set_partition_attach_metropole_et_regions('tache_urbaine_2018.l_bati_agrege_pci','_2018');
/*
/ERREUR: la contrainte de partition est violée par une ligne
  Où : instruction SQL « 
					ALTER TABLE IF EXISTS tache_urbaine_2018.l_bati_agrege_pci_r94_2018 ATTACH PARTITION tache_urbaine_2018.l_bati_agrege_pci_02A_2018 FOR VALUES IN ('02A');
			 »
fonction PL/pgsql w_adl_delegue.set_partition_attach_metropole_et_regions(character varying,character varying,boolean)
*/
update tache_urbaine_2018.l_bati_agrege_pci_02A_2018 set code_dep = '02A';
--> Updated Rows	85181
update tache_urbaine_2018.l_bati_agrege_pci_02B_2018 set code_dep = '02B';
--> Updated Rows	91508
"
$commande "
select w_adl_delegue.set_partition_attach_metropole_et_regions('tache_urbaine_2018.l_bati_agrege_pci','_2018');
/*
set_partition_attach_metropole_et_regions                               |
------------------------------------------------------------------------|
2020-05-15 - tache_urbaine_2018.l_bati_agrege_pci_000_2018 reste visible|
*/

echo "A.2.2] Commentaires "
$commande "
DO $$
DECLARE
liste_valeur 				CHARACTER VARYING[];
req 						text;
BEGIN
liste_valeur := ARRAY  ['000','r11','r24','r27','r28','r32','r44','r52','r53','r75','r76','r84','r93','r94'];

FOR i_table IN 1..array_length(liste_valeur, 1) LOOP
		req := '
				COMMENT ON TABLE tache_urbaine_2018.l_bati_agrege_pci_'|| liste_valeur[i_table] ||'_2018
				  IS ''Bâti agrégé de 2018.
				
				Il s’agit de fusionner tous les bâtiments du parcellaire express qui sont séparés de moins de 1 cm par un Buffer +0,01, ST_Union, Buffer -0,01, ST_Dump.'';
				COMMENT ON COLUMN tache_urbaine_2018.l_bati_agrege_pci_'|| liste_valeur[i_table] ||'_2018.id
				IS ''Identifiant unique selon méthode COVADIS : n°département.000.000 + id.'';
				COMMENT ON COLUMN tache_urbaine_2018.l_bati_agrege_pci_'|| liste_valeur[i_table] ||'_2018.nbatidur
				IS ''Nombre de bâtiments qualifiés de dur par la DGFiP intégrés dans cette agrégation de bâtiments.'';
				COMMENT ON COLUMN tache_urbaine_2018.l_bati_agrege_pci_'|| liste_valeur[i_table] ||'_2018.nbatileg
				IS ''Nombre de bâtiments qualifiés de legers par la DGFiP intégrés dans cette agrégation de bâtiments.'';
				COMMENT ON COLUMN tache_urbaine_2018.l_bati_agrege_pci_'|| liste_valeur[i_table] ||'_2018.code_dep
				IS ''Numéro INSEE du département sur 3 charactères.'';
				COMMENT ON COLUMN tache_urbaine_2018.l_bati_agrege_pci_'|| liste_valeur[i_table] ||'_2018.geom
				IS ''Champs géométrique : Monopolygones en Lambert93 (EPSG:2154).''; 
			'; 
		RAISE NOTICE '%', req;
		EXECUTE(req);
		COMMIT;
   END LOOP;
END $$;
"

---- 27 juin 2020
---- Correction des erreurs pour MAJ des contraintes
SELECT count(*) FROM tache_urbaine_2018.l_bati_agrege_pci_000_2018 WHERE NOT ST_Isvalid(geom);
/*
count|
-----|
 8848|
*/
---- Trop d'erreur : tout réfaire !

---- Remise en place :
#!/bin/bash
# Tache Urbaine
commande="psql -d ceremabase -c "
millesime="2018"
cadastre="etalab"

# Important :
# Pour tout logger taper :
# time sh /home/administrateur/gitlab/enveloppe_urbaine/2018_etalab/A1_l_bati_agrege_etalab_2018.sh 1>/mnt/data/A1_l_bati_agrege_etalab_2018.log 2>&1

# A] Bati Agrege

#for dpt in '090'
for dpt in '001' '002' '003' '004' '005' '006' '007' '008' '009' '010' '011' '012' '013' '014' '015' '016' '017' '018' '019' '021' '022' '023' '024' '025' '026' '027' '028' '029' '030' '031' '032' '033' '034' '035' '036' '037' '038' '039' '040' '041' '042' '043' '044' '045' '046' '047' '048' '049' '050' '051' '052' '053' '054' '055' '056' '057' '058' '059' '060' '061' '062' '063' '064' '065' '066' '067' '068' '069' '070' '071' '072' '073' '074' '075' '076' '077' '078' '079' '080' '081' '082' '083' '084' '085' '086' '087' '088' '089' '090' '091' '092' '093' '094' '095'
#for dpt in '02b' '02a'

do
table_en_traitement="tache_urbaine_"$millesime".l_bati_agrege_"$cadastre"_"$dpt"_"$millesime
couchebaticadastre="r_cadastre_"$cadastre"_"$millesime".n_bati_"$cadastre"_"$dpt"_"$millesime

echo $table_en_traitement
echo '--------------------------------------------'

# A.1] Création des tables Départementales
$commande "
DROP TABLE IF EXISTS "$table_en_traitement";
CREATE TABLE "$table_en_traitement"
	(
	id serial,
	nb_batidur integer,
	nb_batileg integer,
	code_dep character(3) DEFAULT '"$dpt"',  
	geom geometry(Polygon,2154)
	);
"
# A.2] Buffer +0,01, ST_Union, Buffer -0,01, ST_Dump
$commande "
ALTER SEQUENCE "$table_en_traitement"_id_seq
	RESTART WITH "$dpt"000001;
INSERT INTO "$table_en_traitement" (geom)
	SELECT (ST_Dump(ST_Buffer(ST_Union(ST_Buffer(ST_Transform(geom,2154),0,01)),-0,01))).geom::geometry(Polygon,2154) as geom
	FROM "$couchebaticadastre";
"
# A.3] Mise à jour des champs nb_batidur & nb_batileg
$commande "
UPDATE "$table_en_traitement" as t1 SET nb_batidur = (
	SELECT count(*)
	FROM "$couchebaticadastre" as t2
	WHERE 	t1.code_dep = '"$dpt"'
		and (t2.type='Bâtiment en dur' or t2.type='Bâti dur' or t2.type='Bati dur' or t2.type='01')
		and ST_Intersects(t1.geom, t2.geom))
	WHERE 	t1.code_dep = '"$dpt"';
"

$commande "
UPDATE "$table_en_traitement" as t1 SET nb_batileg = (
	SELECT count(*)
	FROM "$couchebaticadastre" as t2
	WHERE 	t1.code_dep = '"$dpt"'
		and (type='Construction légère' or type='Bâti léger' or type='Bati leger' or type='02')
		and ST_Intersects(t1.geom, t2.geom))
	WHERE 	t1.code_dep = '"$dpt"';
"
# A.4] Optimisation 
# Clé primaire 
$commande "
ALTER TABLE "$table_en_traitement"
    ADD CONSTRAINT l_bati_agrege_"$cadastre"_"$dpt"_"$millesime"_pk PRIMARY KEY (id)
    USING INDEX TABLESPACE index;
--- # Contraintes Géométriques
ALTER TABLE "$table_en_traitement"
    ADD CONSTRAINT enforce_dims_geom CHECK (ST_ndims(geom) = 2);
ALTER TABLE "$table_en_traitement"
	ADD CONSTRAINT enforce_srid_geom CHECK (ST_Srid(geom)= 2154);
ALTER TABLE "$table_en_traitement"
    ADD CONSTRAINT enforce_geotype_geom CHECK (geometrytype(geom) = 'POLYGON'::text);
ALTER TABLE "$table_en_traitement"
	ADD CONSTRAINT enforce_validite_geom CHECK (ST_IsValid(geom));
ALTER TABLE "$table_en_traitement"
	ADD CONSTRAINT enforce_no_self_intersection_geom CHECK (ST_IsSimple(geom));
ALTER TABLE "$table_en_traitement"
	ADD CONSTRAINT enforce_no_zero_area_geom CHECK (ST_Area(geom) > 0);
----# Index attributaires
CREATE INDEX l_bati_agrege_"$cadastre"_"$dpt"_"$millesime"_nb_batidur_idx ON "$table_en_traitement"
	USING brin (nb_batidur) TABLESPACE index;
CREATE INDEX l_bati_agrege_"$cadastre"_"$dpt"_"$millesime"_nb_batileg_idx ON "$table_en_traitement"
	USING brin (nb_batileg) TABLESPACE index;
CREATE INDEX l_bati_agrege_"$cadastre"_"$dpt"_"$millesime"_code_dep_idx ON "$table_en_traitement"
	USING brin (code_dep) TABLESPACE index;
----# Index Géomérique + Cluster
CREATE INDEX l_bati_agrege_"$cadastre"_"$dpt"_"$millesime"_geom_gist ON "$table_en_traitement"
	USING gist (geom) TABLESPACE index;
ALTER TABLE "$table_en_traitement"
	CLUSTER ON l_bati_agrege_"$cadastre"_"$dpt"_"$millesime"_geom_gist;
"

# A.5] Commentaires
# Table
$commande "
COMMENT ON TABLE "$table_en_traitement"
  IS 'Bâti agrégé de "$millesime" pour le département ''"$dpt"''.

Il s’agit de fusionner tous les bâtiments du cadastre "$cadastre" qui sont séparés de moins de 1 cm par un Buffer +0,01, ST_Union, Buffer -0,01, ST_Dump';
----# Champs
COMMENT ON COLUMN "$table_en_traitement".id
    IS 'Identifiant unique selon méthode COVADIS : n°département.000.000 + id.';
COMMENT ON COLUMN "$table_en_traitement".nb_batidur
    IS 'Nombre de bâtiments qualifiés de dur par la DGFiP intégrés dans cette agrégation de bâtiments.';
COMMENT ON COLUMN "$table_en_traitement".nb_batileg
    IS 'Nombre de bâtiments qualifiés de legers par la DGFiP intégrés dans cette agrégation de bâtiments.';
COMMENT ON COLUMN "$table_en_traitement".code_dep
    IS 'Numéro INSEE du département sur 3 charactères.';
COMMENT ON COLUMN "$table_en_traitement".geom
    IS 'Champs géométrique : Monopolygones en Lambert93 (EPSG:2154).';
"

done

---- Execution :
time sh /home/administrateur/gitlab/enveloppe_urbaine/2018_etalab/A1_l_bati_agrege_etalab_2018.sh 1>/mnt/data/A1_l_bati_agrege_etalab_2018.log 2>&1
/*
real	26135m35,607s
user	0m29,558s
sys	0m6,459s
*/

echo "------------------------------------------------------------"
echo "B] Partition "
echo "------------------------------------------------------------"
echo "27/07/2020"
echo "B.1] Partition Nationale "
$commande "
select w_adl_delegue.set_partition_attach_metropole_et_regions('tache_urbaine_2018.l_bati_agrege_etalab','_2018');
--> /ERREUR: la contrainte de partition est violée par une ligne

update tache_urbaine_2018.l_bati_agrege_etalab_02A_2018 set code_dep = '02A';
--> Updated Rows	85184
update tache_urbaine_2018.l_bati_agrege_pepci_02B_2018 set code_dep = '02B';
--> Updated Rows	91516

select w_adl_delegue.set_partition_attach_metropole_et_regions('tache_urbaine_2018.l_bati_agrege_etalab','_2018');
-->  2020-08-03 - tache_urbaine_2018.l_bati_agrege_etalab_000_2018 reste visible

echo "A.2.2] Commentaires "
$commande "
DO $$
DECLARE
liste_valeur 				CHARACTER VARYING[];
millesime				CHARACTER VARYING(4);
req 					text;
BEGIN
------ Paramètres en entrée
liste_valeur := ARRAY  ['000','r11','r24','r27','r28','r32','r44','r52','r53','r75','r76','r84','r93','r94'];
millesime := '2018';
------

FOR i_table IN 1..array_length(liste_valeur, 1) LOOP
		req := '
				COMMENT ON TABLE tache_urbaine_' || millesime || '.l_bati_agrege_etalab_'|| liste_valeur[i_table] ||'_' || millesime || '
				  IS ''Bâti agrégé de 2018.
				
				Il s’agit de fusionner tous les bâtiments du parcellaire express qui sont séparés de moins de 1 cm par un Buffer +0,01, ST_Union, Buffer -0,01, ST_Dump.'';
				COMMENT ON COLUMN tache_urbaine_' || millesime || '.l_bati_agrege_etalab_'|| liste_valeur[i_table] ||'_' || millesime || '.id
				IS ''Identifiant unique selon méthode COVADIS : n°département.000.000 + id.'';
				COMMENT ON COLUMN tache_urbaine_' || millesime || '.l_bati_agrege_etalab_'|| liste_valeur[i_table] ||'_' || millesime || '.nb_batidur
				IS ''Nombre de bâtiments qualifiés de dur par la DGFiP intégrés dans cette agrégation de bâtiments.'';
				COMMENT ON COLUMN tache_urbaine_' || millesime || '.l_bati_agrege_etalab_'|| liste_valeur[i_table] ||'_' || millesime || '.nb_batileg
				IS ''Nombre de bâtiments qualifiés de legers par la DGFiP intégrés dans cette agrégation de bâtiments.'';
				COMMENT ON COLUMN tache_urbaine_' || millesime || '.l_bati_agrege_etalab_'|| liste_valeur[i_table] ||'_' || millesime || '.code_dep
				IS ''Numéro INSEE du département sur 3 charactères.'';
				COMMENT ON COLUMN tache_urbaine_' || millesime || '.l_bati_agrege_etalab_'|| liste_valeur[i_table] ||'_' || millesime || '.geom
				IS ''Champs géométrique : Monopolygones en Lambert93 (EPSG:2154).''; 
			'; 
		RAISE NOTICE '%', req;
		EXECUTE(req);
		COMMIT;
   END LOOP;
END $$;
"
--> Updated Rows	0


echo "------------------------------------------------------------"
echo "C] CORRECTIONS car oubli du ST_Transform"
echo "------------------------------------------------------------"
echo "27/07/2020"
create table tache_urbaine_2018.bati_2154_temp as
select type, st_transform(geom,2154)::Geometry('MULTIPOLYGON',2154) as geom
from r_cadastre_etalab_2018.n_bati_etalab_000_2018;
--> Updated Rows	50470742

CREATE INDEX bati_2154_temp_geom ON tache_urbaine_2018.bati_2154_temp USING gist (geom);

UPDATE tache_urbaine_2018.bati_2154_temp SET geom=
    CASE
        WHEN GeometryType(geom) = 'POLYGON'         OR GeometryType(geom) = 'MULTIPOLYGON' THEN
                ST_Multi(ST_Simplify(ST_Multi(ST_CollectionExtract(ST_ForceCollection(ST_MakeValid(geom)),3)),0))
        WHEN GeometryType(geom) = 'LINESTRING'     OR GeometryType(geom) = 'MULTILINESTRING' THEN
                ST_Multi(ST_Simplify(ST_Multi(ST_CollectionExtract(ST_ForceCollection(ST_MakeValid(geom)),2)),0))
        WHEN GeometryType(geom) = 'POINT'         OR GeometryType(geom) = 'MULTIPOINT' THEN
                ST_Multi(ST_Simplify(ST_Multi(ST_CollectionExtract(ST_ForceCollection(ST_MakeValid(geom)),1)),0))
        ELSE ST_MakeValid(geom)
    END
WHERE NOT ST_Isvalid(geom);
/*
Self-intersection at or near point 502910.10825609713 6491436.8947211942
Self-intersection at or near point 224085.32057809978 6853607.2579676434
Self-intersection at or near point 289290.44448147569 6780383.094452682
Too few points in geometry component at or near point 794323.93681298266 6291567.9025010793
 */

UPDATE tache_urbaine_2018.l_bati_agrege_etalab_000_2018 SET geom=
    CASE
        WHEN GeometryType(geom) = 'POLYGON'         OR GeometryType(geom) = 'MULTIPOLYGON' THEN
                ST_Multi(ST_Simplify(ST_Multi(ST_CollectionExtract(ST_ForceCollection(ST_MakeValid(geom)),3)),0))
        WHEN GeometryType(geom) = 'LINESTRING'     OR GeometryType(geom) = 'MULTILINESTRING' THEN
                ST_Multi(ST_Simplify(ST_Multi(ST_CollectionExtract(ST_ForceCollection(ST_MakeValid(geom)),2)),0))
        WHEN GeometryType(geom) = 'POINT'         OR GeometryType(geom) = 'MULTIPOINT' THEN
                ST_Multi(ST_Simplify(ST_Multi(ST_CollectionExtract(ST_ForceCollection(ST_MakeValid(geom)),1)),0))
        ELSE ST_MakeValid(geom)
    END
WHERE NOT ST_Isvalid(geom);


UPDATE tache_urbaine_2018.l_bati_agrege_etalab_000_2018 as t1 SET nb_batidur = (
	SELECT count(*)
	FROM tache_urbaine_2018.bati_2154_temp as t2
	WHERE ST_Intersects(t1.geom, t2.geom) and t2.type='01');
--> Updated Rows	27809140


UPDATE tache_urbaine_2018.l_bati_agrege_etalab_000_2018 as t1 SET nb_batileg = (
	SELECT count(*)
	FROM tache_urbaine_2018.bati_2154_temp as t2
	WHERE type='02'	and ST_Intersects(t1.geom, t2.geom));
--> Updated Rows	27809140

Drop table tache_urbaine_2018.bati_2154_temp;
--> Updated Rows	0

echo "------------------------"
echo "D] Mise à la norme 2023"
echo "27/07/2020"
echo "------------------------"
ALTER SCHEMA tache_urbaine_2018 RENAME TO p_enveloppe_bati_2018;
--> Updated Rows	0

---- https://gitlab.com/christophe.badol/admin_ceremabase/-/blob/master/fonctions/synthese_geometrie_schema.sql
CREATE table p_enveloppe_bati_2018.synthese_geometrie_schema AS
SELECT * FROM w_fonctions.synthese_geometrie_schema('p_enveloppe_bati_2018');
--> Updated Rows	207
