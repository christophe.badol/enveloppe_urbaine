# 26/10/2020
# A] Sauvegardes

# A.1] Sauvegarde Schéma :
time pg_dump --port=5432 --dbname="ceremabase" --schema="tache_urbaine_2018" --username "postgres" --role "postgres" --format=plain --encoding UTF8 --inserts --column-inserts --verbose --quote-all-identifiers --file="/mnt/data/sauve_tache_urbaine_2018.sql" 1>/mnt/data/A3_sauve_enveloppe_urbaine_2018.log 2>&1
#real	19m32,278s
#user	2m29,545s
#sys	2m8,537s




# A.2] Sauvegarde de la table finale
time pg_dump --port=5432 --dbname="ceremabase" --table="tache_urbaine_2018.l_enveloppe_bati_etalab_000_2018" --username "postgres" --role "postgres" --format=plain --encoding UTF8 --inserts --column-inserts --verbose --quote-all-identifiers --file="/mnt/data/sauve_tache_urbaine_2018.l_enveloppe_bati_etalab_000_2018.sql" 1>/mnt/data/sauve_tache_urbaine_2018.l_enveloppe_bati_etalab_000_2018.log 2>&1
#real	2m33,020s
#user	0m19,703s
#sys	0m18,525s





