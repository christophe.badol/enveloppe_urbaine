#!/bin/bash
# A0 Préparation à Enveloppe Urbaine
commande="psql -d ceremabase -c "

$commande "
CREATE SCHEMA tache_urbaine_2018;
"

#---- Combien font 200 mètres à l'aeroport de Gueret
#SELECT 2*(ST_Length(ST_Transform(ST_GeomFromText('LINESTRING(650621 6565713,650721 6565813)',2154),4326)));
#--> 0.00314826850940874

$commande "
DROP TABLE IF EXISTS tache_urbaine_2018.temp_departement_buffer_200m_000_2018;
CREATE TABLE tache_urbaine_2018.temp_departement_buffer_200m_000_2018 AS
	SELECT code_dep, ST_Multi(ST_Union(ST_Buffer(geom,0.00314827)))::geometry(MultiPolygon,4326) as geom ----200
	FROM r_cadastre_etalab_2018.n_commune_etalab_000_2018
	GROUP BY code_dep;

CREATE INDEX temp_departement_buffer_200m_000_2018_code_dep_idx
    ON tache_urbaine_2018.temp_departement_buffer_200m_000_2018 USING brin
    (code_dep)
    TABLESPACE index;

CREATE INDEX temp_departement_buffer_200m_000_2018_geom_gist
    ON tache_urbaine_2018.temp_departement_buffer_200m_000_2018 USING gist
    (geom)
    TABLESPACE index;

ALTER TABLE tache_urbaine_2018.temp_departement_buffer_200m_000_2018
    CLUSTER ON temp_departement_buffer_200m_000_2018_geom_gist;
"
