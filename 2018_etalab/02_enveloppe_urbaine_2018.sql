echo "====================================================================================="
echo "	CREATION ENVELOPPE BATI"
echo ""
echo "             CEREMABASE"
echo ""
echo "	Version 1 du 01/04/2018"
echo "	c.badol"
echo "	Finalisé oui|X| / non | |"
echo "====================================================================================="
echo " Rq : ---- pour les commentaires / -- pour les commandes optionnelles, debuger"

echo "--------------------------------
echo "A] Enveloppe Urbaine 2018
echo "--------------------------------
#!/bin/bash
# A0 Préparation à Enveloppe Urbaine
commande="psql -d ceremabase -c "

$commande "
CREATE SCHEMA tache_urbaine_2018;
"

#---- Combien font 200 mètres à l'aeroport de Gueret
#SELECT 2*(ST_Length(ST_Transform(ST_GeomFromText('LINESTRING(650621 6565713,650721 6565813)',2154),4326)));
#--> 0.00314826850940874

$commande "
DROP TABLE IF EXISTS tache_urbaine_2018.temp_departement_buffer_200m_000_2018;
CREATE TABLE tache_urbaine_2018.temp_departement_buffer_200m_000_2018 AS
	SELECT code_dep, ST_Multi(ST_Union(ST_Buffer(geom,0.00314827)))::geometry(MultiPolygon,4326) as geom ----200
	FROM r_cadastre_etalab_2018.n_commune_etalab_000_2018
	GROUP BY code_dep;

CREATE INDEX temp_departement_buffer_200m_000_2018_code_dep_idx
    ON tache_urbaine_2018.temp_departement_buffer_200m_000_2018 USING brin
    (code_dep)
    TABLESPACE index;

CREATE INDEX temp_departement_buffer_200m_000_2018_geom_gist
    ON tache_urbaine_2018.temp_departement_buffer_200m_000_2018 USING gist
    (geom)
    TABLESPACE index;

ALTER TABLE tache_urbaine_2018.temp_departement_buffer_200m_000_2018
    CLUSTER ON temp_departement_buffer_200m_000_2018_geom_gist;
"
## Execution le : 09/04/2020 
## time sh /home/administrateur/Documents/add_enveloppe_bati_etalab_2018/A0_enveloppe_urbaine_2018.sh 1>/mnt/data/02_enveloppe_urbaine_2018_A0.log 2>&1
# --> real	16m33,910s
# --> user	0m0,043s
# --> sys	0m0,028s

echo "A.0] # Enveloppe Urbaine des 96 départements Métropolitains"
#!/bin/bash
# A1 Enveloppe Urbaine des 96 départements Métropolitains
commande="psql -d ceremabase -c "
millesime="2018"
schemaorigine="tache_urbaine_"$millesime

# Important :
# Pour tout logger taper :
# time sh /home/administrateur/Documents/add_enveloppe_bati_etalab_2018/A1_enveloppe_urbaine_2018.sh 1>/mnt/data/A1_enveloppe_urbaine_2018.log 2>&1

#DEBUG
#for dpt in '090'
#for dpt in '02a' '02b'
for dpt in '001' '002' '003' '004' '005' '006' '007' '008' '009' '010' '011' '012' '013' '014' '015' '016' '017' '018' '019' '02b' '02a' '021' '022' '023' '024' '025' '026' '027' '028' '029' '030' '031' '032' '033' '034' '035' '036' '037' '038' '039' '040' '041' '042' '043' '044' '045' '046' '047' '048' '049' '050' '051' '052' '053' '054' '055' '056' '057' '058' '060' '061' '062' '063' '064' '065' '066' '067' '068' '069' '070' '071' '072' '073' '074' '075' '076' '077' '078' '079' '080' '081' '082' '083' '084' '085' '086' '087' '088' '089' '090' '091' '092' '093' '094' '095'

do
	
now=$(date +"%T")
echo "--------------------"
echo "Début à $now"
echo "Département $dpt"
echo "--------------------"

echo A.0] Corrections des erreurs d origine
$commande "
UPDATE r_cadastre_etalab_"$millesime".n_bati_etalab_"$dpt"_"$millesime" SET geom =
	CASE 
		WHEN GeometryType(geom) = 'POLYGON' 		OR GeometryType(geom) = 'MULTIPOLYGON' THEN
				ST_Multi(ST_Simplify(ST_Multi(ST_CollectionExtract(ST_ForceCollection(ST_MakeValid(geom)),3)),0))
		WHEN GeometryType(geom) = 'LINESTRING' 	OR GeometryType(geom) = 'MULTILINESTRING' THEN
				ST_Multi(ST_Simplify(ST_Multi(ST_CollectionExtract(ST_ForceCollection(ST_MakeValid(geom)),2)),0))
		WHEN GeometryType(geom) = 'POINT' 		OR GeometryType(geom) = 'MULTIPOINT' THEN
				ST_Multi(ST_Simplify(ST_Multi(ST_CollectionExtract(ST_ForceCollection(ST_MakeValid(geom)),1)),0))
		ELSE ST_MakeValid(geom)
	END
WHERE NOT ST_Isvalid(geom);
"


echo "A.1.1] Création de la table à partir d’une dilatation de 50 m / fusion / érosion de 40 m"


#Changement en WGS84
#---- Combien font 100 mètres à la Frontière Nord Franco/Belge
#SELECT ST_Length(ST_Transform(ST_GeomFromText('LINESTRING(668175 7110465,668275 7110565)',2154),4326));
#--> 0.00167867198734235 degrés
#---- Combien font 100 mètres à l'aeroport de Guerret
#SELECT ST_Length(ST_Transform(ST_GeomFromText('LINESTRING(650621 6565713,650721 6565813)',2154),4326));
#--> 0.00157413425470437 degrés
#---- Combien font 100 mètres au SUD d'Ajacio
#SELECT ST_Length(ST_Transform(ST_GeomFromText('LINESTRING(1218075 6049418,1218175 6049518)',2154),4326));
#--> 0.00152369366030852 degrés

$commande "
DROP TABLE IF EXISTS "$schemaorigine".l_enveloppe_bati_etalab_"$dpt"_"$millesime";
CREATE TABLE "$schemaorigine".l_enveloppe_bati_etalab_"$dpt"_"$millesime"
(
	id serial,
	nb_batidur integer,
	nb_batileg integer,
	surf_batidur integer,
	surf_batileg integer,
	code_dep character(3) DEFAULT '"$dpt"',  
	geom geometry(Polygon,2154)
);
"
#DEBUG
# Obligation de séparer ces trois commandes car si 2a 2b l’ALTER SEQUENCE echoue et annule les 4 commandes
$commande "
ALTER SEQUENCE "$schemaorigine".l_enveloppe_bati_etalab_"$dpt"_"$millesime"_id_seq
	RESTART WITH "$dpt"000001;
"
#DEBUG
# Obligation de séparer ces trois commandes car si 2a 2b l’ALTER SEQUENCE echoue et annule les 4 commandes
$commande "
INSERT INTO "$schemaorigine".l_enveloppe_bati_etalab_"$dpt"_"$millesime" (geom)
	SELECT (ST_Dump(ST_Buffer(ST_Union(ST_Buffer(ST_Transform(ST_Buffer(t1.geom,0),2154),50)),-40))).geom::geometry(Polygon,2154) as geom
	--DEBUG Buffer à 0 pour corriger certaines erreurs
	--SELECT (ST_Dump(ST_Buffer(ST_Union(ST_Buffer(ST_Transform(t1.geom,2154),50)),-40))).geom::geometry(Polygon,2154) as geom
FROM r_cadastre_etalab_"$millesime".n_bati_etalab_000_"$millesime" as t1
	JOIN "$schemaorigine".temp_departement_buffer_200m_000_"$millesime" as t2
	ON ST_Intersects (t1.geom, t2.geom)
	WHERE t2.code_dep = upper('"$dpt"');
"
echo "A.1.2] Clé + Index + Cluster + Optimisation"
$commande "
ALTER TABLE "$schemaorigine".l_enveloppe_bati_etalab_"$dpt"_"$millesime"
    ADD CONSTRAINT l_enveloppe_bati_etalab_"$dpt"_"$millesime"_pk PRIMARY KEY (id)
    USING INDEX TABLESPACE index;

ALTER TABLE "$schemaorigine".l_enveloppe_bati_etalab_"$dpt"_"$millesime"
    ADD CONSTRAINT enforce_dims_geom CHECK (ST_ndims(geom) = 2);

ALTER TABLE "$schemaorigine".l_enveloppe_bati_etalab_"$dpt"_"$millesime"
	ADD CONSTRAINT enforce_srid_geom CHECK (ST_Srid(geom)= 2154);

ALTER TABLE "$schemaorigine".l_enveloppe_bati_etalab_"$dpt"_"$millesime"
    ADD CONSTRAINT enforce_geotype_geom CHECK (geometrytype(geom) = 'POLYGON'::text);

CREATE INDEX l_enveloppe_bati_etalab_"$dpt"_"$millesime"_code_dep_idx ON "$schemaorigine".l_enveloppe_bati_etalab_"$dpt"_"$millesime"
	USING brin (code_dep) TABLESPACE index;

CREATE INDEX l_enveloppe_bati_etalab_"$dpt"_"$millesime"_geom ON "$schemaorigine".l_enveloppe_bati_etalab_"$dpt"_"$millesime"
	USING gist (geom) TABLESPACE index;
ALTER TABLE "$schemaorigine".l_enveloppe_bati_etalab_"$dpt"_"$millesime" CLUSTER ON l_enveloppe_bati_etalab_"$dpt"_"$millesime"_geom;
"

echo "A.1.3] Création d’un champs geom_4326"
$commande "
ALTER TABLE "$schemaorigine".l_enveloppe_bati_etalab_"$dpt"_"$millesime"
	ADD COLUMN geom_4326 geometry(Polygon,4326);
UPDATE "$schemaorigine".l_enveloppe_bati_etalab_"$dpt"_"$millesime" SET geom_4326 = ST_Transform(geom,4326);
"
$commande "
CREATE INDEX l_enveloppe_bati_etalab_"$dpt"_"$millesime"_geom_4326 ON "$schemaorigine".l_enveloppe_bati_etalab_"$dpt"_"$millesime"
	USING gist (geom_4326) TABLESPACE index;
"
#DEBUG
#echo "Création des Index dans la table nationale des bati du cadastre 2018"
#$commande "
#CREATE INDEX n_bati_etalab_000_"$millesime"_type_idx ON r_cadastre_etalab_"$millesime".n_bati_etalab_000_"$millesime"
#	USING brin (type) TABLESPACE index;
#CREATE INDEX n_bati_etalab_000_"$millesime"_code_dep_idx ON r_cadastre_etalab_"$millesime".n_bati_etalab_000_"$millesime"
#	USING brin (code_dep) TABLESPACE index;
#CREATE INDEX n_bati_etalab_000_"$millesime"_geom ON r_cadastre_etalab_"$millesime".n_bati_etalab_000_"$millesime"
#	USING gist (geom) TABLESPACE index;
#"

echo "A.1.4] Peuplement du champs nb_batidur + index"
$commande "
UPDATE "$schemaorigine".l_enveloppe_bati_etalab_"$dpt"_"$millesime" as t1 SET nb_batidur = (
	SELECT count(t2.*)
	FROM r_cadastre_etalab_"$millesime".n_bati_etalab_000_"$millesime" as t2
	-- Option sans partition	
	-- FROM r_cadastre_etalab_"$millesime".n_bati_etalab_"$dpt"_"$millesime" as t2
	WHERE 	
		ST_Intersects(t1.geom_4326, t2.geom)
		AND 
		(t2.type='01' or t2.type='Bâtiment en dur' or t2.type='Bâti dur' or t2.type='Bati dur')
	);
"
$commande "
CREATE INDEX l_enveloppe_bati_etalab_"$dpt"_"$millesime"_nb_batidur_idx ON "$schemaorigine".l_enveloppe_bati_etalab_"$dpt"_"$millesime"
	USING brin (nb_batidur) TABLESPACE index;
"

echo "A.1.5] Peuplement du champs nb_batileg + index"
$commande "
UPDATE "$schemaorigine".l_enveloppe_bati_etalab_"$dpt"_"$millesime" as t1 SET nb_batileg = (
	SELECT count(t2.*)
	FROM r_cadastre_etalab_"$millesime".n_bati_etalab_000_"$millesime" as t2
	-- Option sans partition	
	-- FROM r_cadastre_etalab_"$millesime".n_bati_etalab_"$dpt"_"$millesime" as t2
	WHERE 	
		ST_Intersects(t1.geom_4326, t2.geom)
		AND
		(t2.type='02' or t2.type='Construction légère' or t2.type='Bâti léger' or t2.type='Bati leger')
	);
"
$commande "
CREATE INDEX l_enveloppe_bati_etalab_"$dpt"_"$millesime"_nb_batileg_idx ON "$schemaorigine".l_enveloppe_bati_etalab_"$dpt"_"$millesime"
	USING brin (nb_batileg) TABLESPACE index;
"

echo "A.1.6] Peuplement du champs surf_batidur + index"
$commande "
UPDATE "$schemaorigine".l_enveloppe_bati_etalab_"$dpt"_"$millesime" as t1 SET surf_batidur = (
	SELECT sum(ST_Area(ST_Transform(t2.geom,2154)))
	FROM r_cadastre_etalab_"$millesime".n_bati_etalab_000_"$millesime" as t2
	-- Option sans partition	
	-- FROM r_cadastre_etalab_"$millesime".n_bati_etalab_"$dpt"_"$millesime" as t2
	WHERE 	
		ST_Intersects(t1.geom_4326, t2.geom)
		AND 
		(t2.type='01' or t2.type='Bâtiment en dur' or t2.type='Bâti dur' or t2.type='Bati dur')
	);
"
$commande "
CREATE INDEX l_enveloppe_bati_etalab_"$dpt"_"$millesime"_surf_batidur_idx ON "$schemaorigine".l_enveloppe_bati_etalab_"$dpt"_"$millesime"
	USING brin (surf_batidur) TABLESPACE index;
"

echo "A.1.7] Peuplement du champs surf_batileg + index"
$commande "
UPDATE "$schemaorigine".l_enveloppe_bati_etalab_"$dpt"_"$millesime" as t1 SET surf_batileg = (
	SELECT sum(ST_Area(ST_Transform(t2.geom,2154)))
	FROM r_cadastre_etalab_"$millesime".n_bati_etalab_000_"$millesime" as t2
	-- Option sans partition	
	-- FROM r_cadastre_etalab_"$millesime".n_bati_etalab_"$dpt"_"$millesime" as t2
	WHERE 	
		ST_Intersects(t1.geom_4326, t2.geom)
		AND
		(t2.type='02' or t2.type='Construction légère' or t2.type='Bâti léger' or t2.type='Bati leger')
	);
"
$commande "
CREATE INDEX l_enveloppe_bati_etalab_"$dpt"_"$millesime"_surf_batileg_idx ON "$schemaorigine".l_enveloppe_bati_etalab_"$dpt"_"$millesime"
	USING brin (surf_batileg) TABLESPACE index;
"

echo "A.1.8] Commentaires"
$commande "
COMMENT ON TABLE "$schemaorigine".l_enveloppe_bati_etalab_"$dpt"_"$millesime"
  IS 'Enveloppe du bati au 1er janvier "$millesime" pour le département ''"$dpt"''.

Il s’agit de fusionner tous les bâtiments issu du cadastre ETALAB par une dilatation de 50 m, puis une fusion et ensuite une érosion de 40 m';

COMMENT ON COLUMN "$schemaorigine".l_enveloppe_bati_etalab_"$dpt"_"$millesime".id
    IS 'Identifiant unique selon méthode COVADIS : n°département.000.000 + id.';

COMMENT ON COLUMN "$schemaorigine".l_enveloppe_bati_etalab_"$dpt"_"$millesime".nb_batidur
    IS 'Nombre de bâtiments qualifiés de dur par la DGFiP intégrés dans cette enveloppe du bâti.';

COMMENT ON COLUMN "$schemaorigine".l_enveloppe_bati_etalab_"$dpt"_"$millesime".nb_batileg
    IS 'Nombre de bâtiments qualifiés de leger par la DGFiP intégrés dans cette enveloppe du bâti.';

COMMENT ON COLUMN "$schemaorigine".l_enveloppe_bati_etalab_"$dpt"_"$millesime".surf_batidur
    IS 'Surface totale en m2 de tous les bâtiments qualifiés de dur par la DGFiP et présents dans cette enveloppe du bâti.';

COMMENT ON COLUMN "$schemaorigine".l_enveloppe_bati_etalab_"$dpt"_"$millesime".surf_batileg
    IS 'Surface totale en m2 de tous les bâtiments qualifiés de leger par la DGFiP et présents dans cette enveloppe du bâti.';

COMMENT ON COLUMN "$schemaorigine".l_enveloppe_bati_etalab_"$dpt"_"$millesime".code_dep
    IS 'Numéro INSEE du département sur 3 charactères.';

COMMENT ON COLUMN "$schemaorigine".l_enveloppe_bati_etalab_"$dpt"_"$millesime".geom
    IS 'Champs géométrique : Monopolygones en Lambert93 (EPSG:2154).';
"

echo "A.1.9] Suppression du champs geom_4326"
$commande "
ALTER TABLE "$schemaorigine".l_enveloppe_bati_etalab_"$dpt"_"$millesime"
	DROP COLUMN geom_4326;
"

done

# time sh /home/administrateur/Documents/add_enveloppe_bati_etalab_2018/A1_enveloppe_urbaine_2018.sh 1>/mnt/data/A1_enveloppe_urbaine_2018.log 2>&1
# real	1225m55,247s
# user	1m18,461s
# sys	0m20,425s

echo "A.1.10] Erreur : besoin de refaire de 69 à 76"
# time sh /home/administrateur/Documents/add_enveloppe_bati_etalab_2018/A1_enveloppe_urbaine_2018.sh 1>/mnt/data/A1_enveloppe_urbaine_2018_69_a_76.log 2>&1

echo "A.1.10] Erreur des département pour lesquels ca ne fonctionne pas"
# A.1.1] Création de la table à partir d’une dilatation de 50 m / fusion / érosion de 40 m
# ERREUR:  Geometry could not be converted to GEOS: IllegalArgumentException: Invalid number of points in LinearRing found 3 - must be 0 or >= 4
#
# 001, 005, 024, 026, 027, 031, 042, 061, 073, 077, 079 

echo "A.1.10.1] Essai de passer par la table du bati agrégé"
INSERT INTO tache_urbaine_2018.l_enveloppe_bati_etalab_005_2018 (geom)
	SELECT (ST_Dump(ST_Buffer(ST_Union(ST_Buffer(t1.geom,50)),-40))).geom::geometry(Polygon,2154) as geom
	FROM tache_urbaine_2018.l_bati_agrege_pci_005_2018 as t1
	JOIN tache_urbaine_2018.temp_departement_buffer_200m_000_2018 as t2
	ON ST_Intersects (t1.geom, ST_Transform(t2.geom,2154))
	WHERE t2.code_dep = upper('005');
--> Updated Rows	8340

echo "A.1.10.2] Mise en place pour les Départements manquants :"
$commande "
DROP TABLE IF EXISTS tache_urbaine_2018.temp_departement_buffer_200m_000_2018;
CREATE TABLE tache_urbaine_2018.temp_departement_buffer_200m_000_2018 AS
	SELECT code_dep, ST_Multi(ST_Union(ST_Buffer(geom,0.00314827)))::geometry(MultiPolygon,4326) as geom ----200
	FROM r_cadastre_etalab_2018.n_commune_etalab_000_2018
	GROUP BY code_dep;
--> Updated Rows	96
CREATE INDEX temp_departement_buffer_200m_000_2018_code_dep_idx
    ON tache_urbaine_2018.temp_departement_buffer_200m_000_2018 USING brin
    (code_dep)
    TABLESPACE index;

CREATE INDEX temp_departement_buffer_200m_000_2018_geom_gist
    ON tache_urbaine_2018.temp_departement_buffer_200m_000_2018 USING gist
    (geom)
    TABLESPACE index;

ALTER TABLE tache_urbaine_2018.temp_departement_buffer_200m_000_2018
    CLUSTER ON temp_departement_buffer_200m_000_2018_geom_gist;
"
$commande "
DO $$
DECLARE
liste_valeur 				CHARACTER VARYING[];
req 						text;
BEGIN
liste_valeur := ARRAY  ['001', '005', '024', '026', '027', '031', '042', '061', '073', '077', '079', '059'];
FOR i_table IN 1..array_length(liste_valeur, 1) LOOP
		req := '
				TRUNCATE TABLE tache_urbaine_2018.l_enveloppe_bati_etalab_'|| liste_valeur[i_table] ||'_2018 CONTINUE IDENTITY RESTRICT;
				';
		RAISE NOTICE '%', req;
		EXECUTE(req);
		COMMIT;
		req := '
				INSERT INTO tache_urbaine_2018.l_enveloppe_bati_etalab_'|| liste_valeur[i_table] ||'_2018 (geom)
					SELECT (ST_Dump(ST_Buffer(ST_Union(ST_Buffer(t1.geom,50)),-40))).geom::geometry(Polygon,2154) as geom
					FROM tache_urbaine_2018.l_bati_agrege_pci_'|| liste_valeur[i_table] ||'_2018 as t1
					JOIN tache_urbaine_2018.temp_departement_buffer_200m_000_2018 as t2
					ON ST_Intersects (t1.geom, ST_Transform(t2.geom,2154))
					WHERE t2.code_dep = upper('''|| liste_valeur[i_table] ||''');
				';
		RAISE NOTICE '%', req;
		EXECUTE(req);
		COMMIT;
		req := '
					ALTER TABLE tache_urbaine_2018.l_enveloppe_bati_etalab_'|| liste_valeur[i_table] ||'_2018
						ADD COLUMN geom_4326 geometry(Polygon,4326);
					UPDATE tache_urbaine_2018.l_enveloppe_bati_etalab_'|| liste_valeur[i_table] ||'_2018 SET geom_4326 = ST_Transform(geom,4326);
					CREATE INDEX l_enveloppe_bati_etalab_'|| liste_valeur[i_table] ||'_2018_geom_4326
						ON tache_urbaine_2018.l_enveloppe_bati_etalab_'|| liste_valeur[i_table] ||'_2018
						USING gist (geom_4326) TABLESPACE index;
				';
		RAISE NOTICE '%', req;
		EXECUTE(req);
		COMMIT;
	

		req := '
				DROP INDEX IF EXISTS tache_urbaine_2018.l_enveloppe_bati_etalab_'|| liste_valeur[i_table] ||'_2018_nb_batidur_idx;
				UPDATE tache_urbaine_2018.l_enveloppe_bati_etalab_'|| liste_valeur[i_table] ||'_2018 as t1 SET nb_batidur = (
					SELECT count(t2.*)
					FROM r_cadastre_etalab_2018.n_bati_etalab_000_2018 as t2
					WHERE 	
						ST_Intersects(t1.geom_4326, t2.geom)
						AND 
						(t2.type=''01'' or t2.type=''Bâtiment en dur'' or t2.type=''Bâti dur'' or t2.type=''Bati dur'')
					);
				';
		RAISE NOTICE '%', req;
		EXECUTE(req);
		COMMIT;
		req := '
				CREATE INDEX l_enveloppe_bati_etalab_'|| liste_valeur[i_table] ||'_2018_nb_batidur_idx
					ON tache_urbaine_2018.l_enveloppe_bati_etalab_'|| liste_valeur[i_table] ||'_2018
						USING brin (nb_batidur) TABLESPACE index;
				';
		RAISE NOTICE '%', req;
		EXECUTE(req);
		COMMIT;
			req := '
				DROP INDEX IF EXISTS tache_urbaine_2018.l_enveloppe_bati_etalab_'|| liste_valeur[i_table] ||'_2018_nb_batileg_idx;
				UPDATE tache_urbaine_2018.l_enveloppe_bati_etalab_'|| liste_valeur[i_table] ||'_2018 as t1 SET nb_batileg = (
					SELECT count(t2.*)
					FROM r_cadastre_etalab_2018.n_bati_etalab_000_2018 as t2
					WHERE 	
						ST_Intersects(t1.geom_4326, t2.geom)
						AND
						(t2.type=''02'' or t2.type=''Construction légère'' or t2.type=''Bâti léger'' or t2.type=''Bati leger'')
					);
				';
		RAISE NOTICE '%', req;
		EXECUTE(req);
		COMMIT;
		req := '
				CREATE INDEX l_enveloppe_bati_etalab_'|| liste_valeur[i_table] ||'_2018_nb_batileg_idx
					ON tache_urbaine_2018.l_enveloppe_bati_etalab_'|| liste_valeur[i_table] ||'_2018
						USING brin (nb_batileg) TABLESPACE index;
				';
		RAISE NOTICE '%', req;
		EXECUTE(req);
		COMMIT;
		req='
				DROP INDEX IF EXISTS tache_urbaine_2018.l_enveloppe_bati_etalab_'|| liste_valeur[i_table] ||'_2018_surf_batidur_idx;
				UPDATE tache_urbaine_2018.l_enveloppe_bati_etalab_'|| liste_valeur[i_table] ||'_2018 as t1 SET surf_batidur = (
					SELECT sum(ST_Area(ST_Transform(t2.geom,2154)))
					FROM r_cadastre_etalab_2018.n_bati_etalab_000_2018 as t2
					WHERE 	
						ST_Intersects(t1.geom_4326, t2.geom)
						AND 
						(t2.type=''01'' or t2.type=''Bâtiment en dur'' or t2.type=''Bâti dur'' or t2.type=''Bati dur'')
					);
				';
		RAISE NOTICE '%', req;
		EXECUTE(req);
		COMMIT;
		req := '
				CREATE INDEX l_enveloppe_bati_etalab_'|| liste_valeur[i_table] ||'_2018_surf_batidur_idx
					ON tache_urbaine_2018.l_enveloppe_bati_etalab_'|| liste_valeur[i_table] ||'_2018
						USING brin (surf_batidur) TABLESPACE index;
				';
		RAISE NOTICE '%', req;
		EXECUTE(req);
		COMMIT;
		req='
				DROP INDEX IF EXISTS tache_urbaine_2018.l_enveloppe_bati_etalab_'|| liste_valeur[i_table] ||'_2018_surf_batileg_idx;
				UPDATE tache_urbaine_2018.l_enveloppe_bati_etalab_'|| liste_valeur[i_table] ||'_2018 as t1 SET surf_batileg = (
					SELECT sum(ST_Area(ST_Transform(t2.geom,2154)))
					FROM r_cadastre_etalab_2018.n_bati_etalab_000_2018 as t2
					WHERE 	
						ST_Intersects(t1.geom_4326, t2.geom)
						AND 
						(t2.type=''02'' or t2.type=''Construction légère'' or t2.type=''Bâti léger'' or t2.type=''Bati leger'')
					);
				';
		RAISE NOTICE '%', req;
		EXECUTE(req);
		COMMIT;
		req := '
				CREATE INDEX l_enveloppe_bati_etalab_'|| liste_valeur[i_table] ||'_2018_surf_batileg_idx
					ON tache_urbaine_2018.l_enveloppe_bati_etalab_'|| liste_valeur[i_table] ||'_2018
						USING brin (surf_batileg) TABLESPACE index;
				';
		RAISE NOTICE '%', req;
		EXECUTE(req);
		COMMIT;
		req := '
				ALTER TABLE tache_urbaine_2018.l_enveloppe_bati_etalab_'|| liste_valeur[i_table] ||'_2018
					DROP COLUMN geom_4326;
			';
		RAISE NOTICE '%', req;
		EXECUTE(req);
		COMMIT;
END LOOP;
END $$;
--> Requête exécutée avec succès en 13 hr 30 min.
"
$commande "
COMMENT ON TABLE tache_urbaine_2018.l_enveloppe_bati_etalab_059_2018
  IS 'Enveloppe du bati au 1er janvier 2018 pour le département 059.

Il s’agit de fusionner tous les bâtiments issu du cadastre ETALAB par une dilatation de 50 m, puis une fusion et ensuite une érosion de 40 m';
COMMENT ON COLUMN tache_urbaine_2018.l_enveloppe_bati_etalab_059_2018.id
    IS 'Identifiant unique selon méthode COVADIS : n°département.000.000 + id.';
COMMENT ON COLUMN tache_urbaine_2018.l_enveloppe_bati_etalab_059_2018.nb_batidur
    IS 'Nombre de bâtiments qualifiés de dur par la DGFiP intégrés dans cette enveloppe du bâti.';
COMMENT ON COLUMN tache_urbaine_2018.l_enveloppe_bati_etalab_059_2018.nb_batileg
    IS 'Nombre de bâtiments qualifiés de legers par la DGFiP intégrés dans cette enveloppe du bâti.';
COMMENT ON COLUMN tache_urbaine_2018.l_enveloppe_bati_etalab_059_2018.surf_batidur
    IS 'Surface totale en m2 de tous les bâtiments qualifiés de dur par la DGFiP et présents dans cette enveloppe du bâti.';
COMMENT ON COLUMN tache_urbaine_2018.l_enveloppe_bati_etalab_059_2018.surf_batileg
    IS 'Surface totale en m2 de tous les bâtiments qualifiés de leger par la DGFiP et présents dans cette enveloppe du bâti.';
COMMENT ON COLUMN tache_urbaine_2018.l_enveloppe_bati_etalab_059_2018.code_dep
    IS 'Numéro INSEE du département sur 3 charactères.';
COMMENT ON COLUMN tache_urbaine_2018.l_enveloppe_bati_etalab_059_2018.geom
    IS 'Champs géométrique : Monopolygones en Lambert93 (EPSG:2154).';
"
--> Requête exécutée avec succès en 111 msec.

---- Département 062 vide 
$commande "
DO $$
DECLARE
liste_valeur 				CHARACTER VARYING[];
req 						text;
BEGIN
liste_valeur := ARRAY  ['062'];
FOR i_table IN 1..array_length(liste_valeur, 1) LOOP
		req := '
				TRUNCATE TABLE tache_urbaine_2018.l_enveloppe_bati_etalab_'|| liste_valeur[i_table] ||'_2018 CONTINUE IDENTITY RESTRICT;
				';
		RAISE NOTICE '%', req;
		EXECUTE(req);
		COMMIT;
		req := '
				INSERT INTO tache_urbaine_2018.l_enveloppe_bati_etalab_'|| liste_valeur[i_table] ||'_2018 (geom)
					SELECT (ST_Dump(ST_Buffer(ST_Union(ST_Buffer(t1.geom,50)),-40))).geom::geometry(Polygon,2154) as geom
					FROM tache_urbaine_2018.l_bati_agrege_pci_'|| liste_valeur[i_table] ||'_2018 as t1
					JOIN tache_urbaine_2018.temp_departement_buffer_200m_000_2018 as t2
					ON ST_Intersects (t1.geom, ST_Transform(t2.geom,2154))
					WHERE t2.code_dep = upper('''|| liste_valeur[i_table] ||''');
				';
		RAISE NOTICE '%', req;
		EXECUTE(req);
		COMMIT;
		req := '
					ALTER TABLE tache_urbaine_2018.l_enveloppe_bati_etalab_'|| liste_valeur[i_table] ||'_2018
						ADD COLUMN geom_4326 geometry(Polygon,4326);
					UPDATE tache_urbaine_2018.l_enveloppe_bati_etalab_'|| liste_valeur[i_table] ||'_2018 SET geom_4326 = ST_Transform(geom,4326);
					CREATE INDEX l_enveloppe_bati_etalab_'|| liste_valeur[i_table] ||'_2018_geom_4326
						ON tache_urbaine_2018.l_enveloppe_bati_etalab_'|| liste_valeur[i_table] ||'_2018
						USING gist (geom_4326) TABLESPACE index;
				';
		RAISE NOTICE '%', req;
		EXECUTE(req);
		COMMIT;
	

		req := '
				DROP INDEX IF EXISTS tache_urbaine_2018.l_enveloppe_bati_etalab_'|| liste_valeur[i_table] ||'_2018_nb_batidur_idx;
				UPDATE tache_urbaine_2018.l_enveloppe_bati_etalab_'|| liste_valeur[i_table] ||'_2018 as t1 SET nb_batidur = (
					SELECT count(t2.*)
					FROM r_cadastre_etalab_2018.n_bati_etalab_000_2018 as t2
					WHERE 	
						ST_Intersects(t1.geom_4326, t2.geom)
						AND 
						(t2.type=''01'' or t2.type=''Bâtiment en dur'' or t2.type=''Bâti dur'' or t2.type=''Bati dur'')
					);
				';
		RAISE NOTICE '%', req;
		EXECUTE(req);
		COMMIT;
		req := '
				CREATE INDEX l_enveloppe_bati_etalab_'|| liste_valeur[i_table] ||'_2018_nb_batidur_idx
					ON tache_urbaine_2018.l_enveloppe_bati_etalab_'|| liste_valeur[i_table] ||'_2018
						USING brin (nb_batidur) TABLESPACE index;
				';
		RAISE NOTICE '%', req;
		EXECUTE(req);
		COMMIT;
			req := '
				DROP INDEX IF EXISTS tache_urbaine_2018.l_enveloppe_bati_etalab_'|| liste_valeur[i_table] ||'_2018_nb_batileg_idx;
				UPDATE tache_urbaine_2018.l_enveloppe_bati_etalab_'|| liste_valeur[i_table] ||'_2018 as t1 SET nb_batileg = (
					SELECT count(t2.*)
					FROM r_cadastre_etalab_2018.n_bati_etalab_000_2018 as t2
					WHERE 	
						ST_Intersects(t1.geom_4326, t2.geom)
						AND
						(t2.type=''02'' or t2.type=''Construction légère'' or t2.type=''Bâti léger'' or t2.type=''Bati leger'')
					);
				';
		RAISE NOTICE '%', req;
		EXECUTE(req);
		COMMIT;
		req := '
				CREATE INDEX l_enveloppe_bati_etalab_'|| liste_valeur[i_table] ||'_2018_nb_batileg_idx
					ON tache_urbaine_2018.l_enveloppe_bati_etalab_'|| liste_valeur[i_table] ||'_2018
						USING brin (nb_batileg) TABLESPACE index;
				';
		RAISE NOTICE '%', req;
		EXECUTE(req);
		COMMIT;
		req='
				DROP INDEX IF EXISTS tache_urbaine_2018.l_enveloppe_bati_etalab_'|| liste_valeur[i_table] ||'_2018_surf_batidur_idx;
				UPDATE tache_urbaine_2018.l_enveloppe_bati_etalab_'|| liste_valeur[i_table] ||'_2018 as t1 SET surf_batidur = (
					SELECT sum(ST_Area(ST_Transform(t2.geom,2154)))
					FROM r_cadastre_etalab_2018.n_bati_etalab_000_2018 as t2
					WHERE 	
						ST_Intersects(t1.geom_4326, t2.geom)
						AND 
						(t2.type=''01'' or t2.type=''Bâtiment en dur'' or t2.type=''Bâti dur'' or t2.type=''Bati dur'')
					);
				';
		RAISE NOTICE '%', req;
		EXECUTE(req);
		COMMIT;
		req := '
				CREATE INDEX l_enveloppe_bati_etalab_'|| liste_valeur[i_table] ||'_2018_surf_batidur_idx
					ON tache_urbaine_2018.l_enveloppe_bati_etalab_'|| liste_valeur[i_table] ||'_2018
						USING brin (surf_batidur) TABLESPACE index;
				';
		RAISE NOTICE '%', req;
		EXECUTE(req);
		COMMIT;
		req='
				DROP INDEX IF EXISTS tache_urbaine_2018.l_enveloppe_bati_etalab_'|| liste_valeur[i_table] ||'_2018_surf_batileg_idx;
				UPDATE tache_urbaine_2018.l_enveloppe_bati_etalab_'|| liste_valeur[i_table] ||'_2018 as t1 SET surf_batileg = (
					SELECT sum(ST_Area(ST_Transform(t2.geom,2154)))
					FROM r_cadastre_etalab_2018.n_bati_etalab_000_2018 as t2
					WHERE 	
						ST_Intersects(t1.geom_4326, t2.geom)
						AND 
						(t2.type=''02'' or t2.type=''Construction légère'' or t2.type=''Bâti léger'' or t2.type=''Bati leger'')
					);
				';
		RAISE NOTICE '%', req;
		EXECUTE(req);
		COMMIT;
		req := '
				CREATE INDEX l_enveloppe_bati_etalab_'|| liste_valeur[i_table] ||'_2018_surf_batileg_idx
					ON tache_urbaine_2018.l_enveloppe_bati_etalab_'|| liste_valeur[i_table] ||'_2018
						USING brin (surf_batileg) TABLESPACE index;
				';
		RAISE NOTICE '%', req;
		EXECUTE(req);
		COMMIT;
		req := '
				ALTER TABLE tache_urbaine_2018.l_enveloppe_bati_etalab_'|| liste_valeur[i_table] ||'_2018
					DROP COLUMN geom_4326;
			';
		RAISE NOTICE '%', req;
		EXECUTE(req);
		COMMIT;
END LOOP;
END $$;
--> OK
"

--- 27 juin 2020
---- Ajout nouvelle contraintes Géométriques :
DO $$
DECLARE
liste_valeur 				CHARACTER VARYING[];
req 						text;
BEGIN
liste_valeur := ARRAY  [--'000',
						--'r11','r24','r27','r28','r32','r44','r52','r53','r75','r76','r84','r93','r94',
						'002','003','004','005','006','007','008','009','010',
						'011','012','013','014','015','016','017','018','019','02a','02b',
						'021','022','023','024','025','026','027','028','029','030',
						'031','032','033','034','035','036','037','038','039','040',
						'041','042','043','044','045','046','047','048','049','050',
						'051','052','053','054','055','056','057','058','059','060',
						'061','062','063','064','065','066','067','068','069','070',
						'071','072','073','074','075','076','077','078','079','080',
						'081','082','083','084','085','086','087','088','089','090',
						'091','092','093','094','095'--,'971','972','973','974','976'
						];
FOR i_table IN 1..array_length(liste_valeur, 1) LOOP
		req := '
				ALTER TABLE tache_urbaine_2018.l_enveloppe_bati_etalab_'|| liste_valeur[i_table] ||'_2018 ADD CONSTRAINT enforce_validite_geom CHECK (ST_IsValid(geom));
				ALTER TABLE tache_urbaine_2018.l_enveloppe_bati_etalab_'|| liste_valeur[i_table] ||'_2018 ADD CONSTRAINT enforce_no_self_intersection_geom CHECK (ST_IsSimple(geom));
				ALTER TABLE tache_urbaine_2018.l_enveloppe_bati_etalab_'|| liste_valeur[i_table] ||'_2018 ADD CONSTRAINT enforce_no_zero_area_geom CHECK (ST_Area(geom) > 0);
				';
		RAISE NOTICE '%', req;
		EXECUTE(req);
		COMMIT;
END LOOP;
END $$;


/*
# A.3] Sauvegarde
# A.3.1] Sauvegarde du schéma complet
---- Sauvegarde :
time pg_dump --port=5432 --dbname="ceremabase" --schema="tache_urbaine_2020" --username "postgres" --role "postgres" --format=plain --encoding UTF8 --inserts --column-inserts --verbose --quote-all-identifiers --file="/mnt/data/sauve_tache_urbaine_2020.sql"
#real	3m25,890s
#user	0m33,239s
#sys	0m28,120s
# A.3.2] Sauvegarde de la table finale
time pg_dump --port=5432 --dbname="ceremabase" --table="tache_urbaine_2020.l_enveloppe_bati_etalab_000_2020" --username "postgres" --role "postgres" --format=plain --encoding UTF8 --inserts --column-inserts --verbose --quote-all-identifiers --file="/mnt/data/sauve_tache_urbaine_2020.l_enveloppe_bati_etalab_000_2020.sql"
#real	1m14,090s
#user	0m16,494s
#sys	0m13,609s
*/

---- 08/05/2021 - Synthèse
---- https://gitlab.cerema.fr/centre-est/admin_ceremabase/-/blob/master/fonctions/synthese_geometrie_schema.sql
CREATE table tache_urbaine_2018.synthese_geometrie_schema AS
SELECT * FROM w_fonctions.synthese_geometrie_schema('tache_urbaine_2018');
--> Updated Rows	110
