#!/bin/bash
# Tache Urbaine

# Important :
# Pour tout logger taper :
# time sh /home/administrateur/Documents/01_bati_agrege.sh 1>/mnt/data/01_bati_agrege_2018_10.log 2>&1

commande="psql -d ceremabase -c "

#log_corse for dpt in '02b' '02a'
# for dpt in '090'
# log1 : '02b' '02a'
# log1 : for dpt in '001' '002' '003' '004' '006' '005' '007' '008' '009' '010' 
# log2 : for dpt in '011' '012' '013' '014' '015' '016' '017'
# log3 : for dpt '018' '019' '021'
# log5 : for dpt in '022' '023' '024' '025' '026' '029' '030'
# log6 : for dpt in '031' '032' '033' '034' '035' '036' '037' '038' '039' '040'
# log7 : for dpt in '041' '042' '043' 
# log8 : for dpt in '044' '045' '046' '047' '048' '049' '050'
# log9 : for dpt in '051' '052' '053' '054' '055' '056' '057' '058'
# '059' à faire autrement
for dpt in '060' '061' '062' '063' '064' '065' '066' '067' '068' '069' '070' '071' '072' '073' '074' '075' '076' '077' '078' '079' '080' '081' '082' '083' '084' '085' '086' '087' '088' '089' '090' '091' '092' '093' '094' '095'

do
schemaorigine="r_cadastre_etalab_2018"
fichierorigine="n_bati_etalab_"$dpt"_2018"
schemabatiagrege="tache_urbaine_2018"
fichierbatiagrege="l_bati_agrege_pci_"$dpt"_2018"

emplacementorigine=$schemaorigine"."$fichierorigine
emplacementbatiagrege=$schemabatiagrege"."$fichierbatiagrege
#echo $emplacementorigine
#echo $emplacementbatiagrege

echo ---------------------------------------------
echo $fichierbatiagrege
now=$(date)
echo $now
echo ---------------------------------------------

echo A.0] Corrections des erreurs d origine
$commande "
UPDATE "$emplacementorigine" SET geom =
	CASE 
		WHEN GeometryType(geom) = 'POLYGON' 		OR GeometryType(geom) = 'MULTIPOLYGON' THEN
				ST_Multi(ST_Simplify(ST_Multi(ST_CollectionExtract(ST_ForceCollection(ST_MakeValid(geom)),3)),0))
		WHEN GeometryType(geom) = 'LINESTRING' 	OR GeometryType(geom) = 'MULTILINESTRING' THEN
				ST_Multi(ST_Simplify(ST_Multi(ST_CollectionExtract(ST_ForceCollection(ST_MakeValid(geom)),2)),0))
		WHEN GeometryType(geom) = 'POINT' 		OR GeometryType(geom) = 'MULTIPOINT' THEN
				ST_Multi(ST_Simplify(ST_Multi(ST_CollectionExtract(ST_ForceCollection(ST_MakeValid(geom)),1)),0))
		ELSE ST_MakeValid(geom)
	END
WHERE NOT ST_Isvalid(geom);
"

echo A.1] Création des tables Départementales
now=$(date +"%T")
echo "Début à $now"
echo "----------------"
$commande "DROP TABLE IF EXISTS "$emplacementbatiagrege";"
$commande "
CREATE TABLE "$emplacementbatiagrege"
	(
	id serial,
	nbatidur integer,
	nbatileg integer,
	code_dep character(3) DEFAULT '"$dpt"',  
	geom geometry(Polygon,4326)
	);
"

$commande "
ALTER SEQUENCE "$emplacementbatiagrege"_id_seq
	RESTART WITH "$dpt"000001;
"

echo A.2] Buffer +0,01, ST_Union, Buffer -0,01, ST_Dump
now=$(date +"%T")
echo "Début à $now"
echo "----------------"
$commande "
INSERT INTO "$emplacementbatiagrege" (geom)
	SELECT (ST_Dump(ST_Buffer(ST_Union(ST_Buffer(geom,0,01)),-0,01))).geom::geometry(Polygon,4326) as geom
	FROM "$emplacementorigine";
"

echo A.2.1] Corrections des erreurs si existantes
now=$(date +"%T")
echo "Début à $now"
$commande "
UPDATE "$emplacementbatiagrege" SET geom =
	CASE 
		WHEN GeometryType(geom) = 'POLYGON' 		OR GeometryType(geom) = 'MULTIPOLYGON' THEN
				ST_Multi(ST_Simplify(ST_Multi(ST_CollectionExtract(ST_ForceCollection(ST_MakeValid(geom)),3)),0))
		WHEN GeometryType(geom) = 'LINESTRING' 	OR GeometryType(geom) = 'MULTILINESTRING' THEN
				ST_Multi(ST_Simplify(ST_Multi(ST_CollectionExtract(ST_ForceCollection(ST_MakeValid(geom)),2)),0))
		WHEN GeometryType(geom) = 'POINT' 		OR GeometryType(geom) = 'MULTIPOINT' THEN
				ST_Multi(ST_Simplify(ST_Multi(ST_CollectionExtract(ST_ForceCollection(ST_MakeValid(geom)),1)),0))
		ELSE ST_MakeValid(geom)
	END
WHERE NOT ST_Isvalid(geom);
"

echo A.2.2] Index Géomérique + Cluster
now=$(date +"%T")
echo "Début à $now"
$commande "
CREATE INDEX "$fichierbatiagrege"_geom_gist ON "$emplacementbatiagrege"
	USING gist (geom) TABLESPACE index;
"
$commande "
ALTER TABLE "$emplacementbatiagrege"
	CLUSTER ON "$fichierbatiagrege"_geom_gist;
"

echo A.3.1] Mise à jour du champ nbatidur
now=$(date +"%T")
echo "Début à $now"
echo ----------------
$commande "
UPDATE "$emplacementbatiagrege" as t1 SET nbatidur = (
	SELECT count(*)
	FROM "$emplacementorigine" as t2
	WHERE 	t1.code_dep = '"$dpt"'
		and (t2.type='Bâtiment en dur' or t2.type='Bâti dur' or t2.type='Bati dur' or t2.type='01')
		and ST_Intersects(t1.geom, t2.geom))
	WHERE 	t1.code_dep = '"$dpt"';
"

echo A.3.2] Mise à jour du champ nbatileg
now=$(date +"%T")
echo "Début à $now"
echo "----------------"
$commande "
UPDATE "$emplacementbatiagrege" as t1 SET nbatileg = (
	SELECT count(*)
	FROM "$emplacementorigine" as t2
	WHERE 	t1.code_dep = '"$dpt"'
		and (type='Construction légère' or type='Bâti léger' or type='Bati leger' or type='02')
		and ST_Intersects(t1.geom, t2.geom))
	WHERE 	t1.code_dep = '"$dpt"';
"

echo A.4] Passage en Lambert93
now=$(date +"%T")
echo "Début à $now"
echo "----------------"
$commande "
ALTER TABLE "$emplacementbatiagrege"
RENAME COLUMN geom TO geom_old;
"
$commande "
ALTER TABLE "$emplacementbatiagrege"
ADD COLUMN geom geometry(Polygon,2154);
"
$commande "
UPDATE "$emplacementbatiagrege"  SET geom = ST_Transform(geom_old,2154);
"
$commande "
ALTER TABLE "$emplacementbatiagrege"
DROP COLUMN geom_old;
"

echo A.5] Optimisations
now=$(date +"%T")
echo "Début à $now"
# Clé primaire 
$commande "
CREATE UNIQUE INDEX "$fichierbatiagrege"_id_idx ON "$emplacementbatiagrege"
	USING btree (id) TABLESPACE index;
"
$commande "
ALTER TABLE "$emplacementbatiagrege" 
    ADD CONSTRAINT "$fichierbatiagrege"_pk PRIMARY KEY
	USING INDEX "$fichierbatiagrege"_id_idx;
"


# Contraintes Géométriques
$commande "
ALTER TABLE "$emplacementbatiagrege"
    ADD CONSTRAINT enforce_dims_geom CHECK (ST_ndims(geom) = 2);
"
$commande "
ALTER TABLE "$emplacementbatiagrege"
	ADD CONSTRAINT enforce_srid_geom CHECK (ST_Srid(geom)= 2154);
"
$commande "
ALTER TABLE "$emplacementbatiagrege"
    ADD CONSTRAINT enforce_geotype_geom CHECK (geometrytype(geom) = 'POLYGON'::text);
"
# Index attributaires
$commande "
CREATE INDEX "$fichierbatiagrege"_nbatidur_idx ON "$emplacementbatiagrege"
	USING brin (nbatidur) TABLESPACE index;
"
$commande "
CREATE INDEX "$fichierbatiagrege"_nbatileg_idx ON "$emplacementbatiagrege"
	USING brin (nbatileg) TABLESPACE index;
"
$commande "
CREATE INDEX "$fichierbatiagrege"_code_dep_idx ON "$emplacementbatiagrege"
	USING brin (code_dep) TABLESPACE index;
"
# Index geométrique + Cluster
$commande "
CREATE INDEX "$fichierbatiagrege"_geom_gist ON "$emplacementbatiagrege"
	USING gist (geom) TABLESPACE index;
"
$commande "
ALTER TABLE "$emplacementbatiagrege"
	CLUSTER ON "$fichierbatiagrege"_geom_gist;
"

echo A.6] Commentaires
now=$(date +"%T")
echo "Début à $now"
# Table
$commande "
COMMENT ON TABLE "$emplacementbatiagrege"
  IS 'Bâti agrégé de 2018 pour le département ''"$dpt"''.

Il s’agit de fusionner tous les bâtiments du parcellaire express qui sont séparés de moins de 1 cm par un Buffer +0,01, ST_Union, Buffer -0,01, ST_Dump';
"
# Champs
$commande "
COMMENT ON COLUMN "$emplacementbatiagrege".id
    IS 'Identifiant unique selon méthode COVADIS : n°département.000.000 + id.';
"
$commande "
COMMENT ON COLUMN "$emplacementbatiagrege".nbatidur
    IS 'Nombre de bâtiments qualifiés de dur par la DGFiP intégrés dans cette agrégation de bâtiments.';
"
$commande "
COMMENT ON COLUMN "$emplacementbatiagrege".nbatileg
    IS 'Nombre de bâtiments qualifiés de legers par la DGFiP intégrés dans cette agrégation de bâtiments.';
"
$commande "
COMMENT ON COLUMN "$emplacementbatiagrege".code_dep
    IS 'Numéro INSEE du département sur 3 charactères.';
"
$commande "
COMMENT ON COLUMN "$emplacementbatiagrege".geom
    IS 'Champs géométrique : Monopolygones en Lambert93 (EPSG:2154).';
"

done
