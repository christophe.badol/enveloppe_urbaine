#!/bin/bash
# A1 Enveloppe Urbaine des 96 départements Métropolitains
commande="psql -d ceremabase -c "
millesime="2018"
schemaorigine="tache_urbaine_"$millesime

# Important :
# Pour tout logger taper :
# time sh 02_enveloppe_urbaine_2018.sh 1>/mnt/data/02_enveloppe_urbaine_2018_A1.log 2>&1

#DEBUG
#for dpt in '090'
#for dpt in '02a' '02b'
#for dpt in '001' '002' '003' '004' '005' '006' '007' '008' '009' '010' '011' '012' '013' '014' '015' '016' '017' '018' '019' '02b' '02a' '021' '022' '023' '024' '025' '026' '027' '028' '029' '030' '031' '032' '033' '034' '035' '036' '037' '038' '039' '040' '041' '042' '043' '044' '045' '046' '047' '048' '049' '050' '051' '052' '053' '054' '055' '056' '057' '058' '060' '061' '062' '063' '064' '065' '066' '067' '068' '069' '070' '071' '072' '073' '074' '075' '076' '077' '078' '079' '080' '081' '082' '083' '084' '085' '086' '087' '088' '089' '090' '091' '092' '093' '094' '095'

for dpt in '069' '070' '071' '072' '073' '074' '075' '076'

do
	
now=$(date +"%T")
echo "--------------------"
echo "Début à $now"
echo "Département $dpt"
echo "--------------------"

echo A.0] Corrections des erreurs d origine
$commande "
UPDATE r_cadastre_etalab_"$millesime".n_bati_etalab_"$dpt"_"$millesime" SET geom =
	CASE 
		WHEN GeometryType(geom) = 'POLYGON' 		OR GeometryType(geom) = 'MULTIPOLYGON' THEN
				ST_Multi(ST_Simplify(ST_Multi(ST_CollectionExtract(ST_ForceCollection(ST_MakeValid(geom)),3)),0))
		WHEN GeometryType(geom) = 'LINESTRING' 	OR GeometryType(geom) = 'MULTILINESTRING' THEN
				ST_Multi(ST_Simplify(ST_Multi(ST_CollectionExtract(ST_ForceCollection(ST_MakeValid(geom)),2)),0))
		WHEN GeometryType(geom) = 'POINT' 		OR GeometryType(geom) = 'MULTIPOINT' THEN
				ST_Multi(ST_Simplify(ST_Multi(ST_CollectionExtract(ST_ForceCollection(ST_MakeValid(geom)),1)),0))
		ELSE ST_MakeValid(geom)
	END
WHERE NOT ST_Isvalid(geom);
"


echo "A.1.1] Création de la table à partir d’une dilatation de 50 m / fusion / érosion de 40 m"


#Changement en WGS84
#---- Combien font 100 mètres à la Frontière Nord Franco/Belge
#SELECT ST_Length(ST_Transform(ST_GeomFromText('LINESTRING(668175 7110465,668275 7110565)',2154),4326));
#--> 0.00167867198734235 degrés
#---- Combien font 100 mètres à l'aeroport de Guerret
#SELECT ST_Length(ST_Transform(ST_GeomFromText('LINESTRING(650621 6565713,650721 6565813)',2154),4326));
#--> 0.00157413425470437 degrés
#---- Combien font 100 mètres au SUD d'Ajacio
#SELECT ST_Length(ST_Transform(ST_GeomFromText('LINESTRING(1218075 6049418,1218175 6049518)',2154),4326));
#--> 0.00152369366030852 degrés

$commande "
DROP TABLE IF EXISTS "$schemaorigine".l_enveloppe_bati_etalab_"$dpt"_"$millesime";
CREATE TABLE "$schemaorigine".l_enveloppe_bati_etalab_"$dpt"_"$millesime"
(
	id serial,
	nb_batidur integer,
	nb_batileg integer,
	surf_batidur integer,
	surf_batileg integer,
	code_dep character(3) DEFAULT '"$dpt"',  
	geom geometry(Polygon,2154)
);
"
#DEBUG
# Obligation de séparer ces trois commandes car si 2a 2b l’ALTER SEQUENCE echoue et annule les 4 commandes
$commande "
ALTER SEQUENCE "$schemaorigine".l_enveloppe_bati_etalab_"$dpt"_"$millesime"_id_seq
	RESTART WITH "$dpt"000001;
"
#DEBUG
# Obligation de séparer ces trois commandes car si 2a 2b l’ALTER SEQUENCE echoue et annule les 4 commandes
$commande "
INSERT INTO "$schemaorigine".l_enveloppe_bati_etalab_"$dpt"_"$millesime" (geom)
	SELECT (ST_Dump(ST_Buffer(ST_Union(ST_Buffer(ST_Transform(ST_Buffer(t1.geom,0),2154),50)),-40))).geom::geometry(Polygon,2154) as geom
	--DEBUG Buffer à 0 pour corriger certaines erreurs
	--SELECT (ST_Dump(ST_Buffer(ST_Union(ST_Buffer(ST_Transform(t1.geom,2154),50)),-40))).geom::geometry(Polygon,2154) as geom
FROM r_cadastre_etalab_"$millesime".n_bati_etalab_000_"$millesime" as t1
	JOIN "$schemaorigine".temp_departement_buffer_200m_000_"$millesime" as t2
	ON ST_Intersects (t1.geom, t2.geom)
	WHERE t2.code_dep = upper('"$dpt"');
"
echo "A.1.2] Clé + Index + Cluster + Optimisation"
$commande "
ALTER TABLE "$schemaorigine".l_enveloppe_bati_etalab_"$dpt"_"$millesime"
    ADD CONSTRAINT l_enveloppe_bati_etalab_"$dpt"_"$millesime"_pk PRIMARY KEY (id)
    USING INDEX TABLESPACE index;

ALTER TABLE "$schemaorigine".l_enveloppe_bati_etalab_"$dpt"_"$millesime"
    ADD CONSTRAINT enforce_dims_geom CHECK (ST_ndims(geom) = 2);

ALTER TABLE "$schemaorigine".l_enveloppe_bati_etalab_"$dpt"_"$millesime"
	ADD CONSTRAINT enforce_srid_geom CHECK (ST_Srid(geom)= 2154);

ALTER TABLE "$schemaorigine".l_enveloppe_bati_etalab_"$dpt"_"$millesime"
    ADD CONSTRAINT enforce_geotype_geom CHECK (geometrytype(geom) = 'POLYGON'::text);

CREATE INDEX l_enveloppe_bati_etalab_"$dpt"_"$millesime"_code_dep_idx ON "$schemaorigine".l_enveloppe_bati_etalab_"$dpt"_"$millesime"
	USING brin (code_dep) TABLESPACE index;

CREATE INDEX l_enveloppe_bati_etalab_"$dpt"_"$millesime"_geom ON "$schemaorigine".l_enveloppe_bati_etalab_"$dpt"_"$millesime"
	USING gist (geom) TABLESPACE index;
ALTER TABLE "$schemaorigine".l_enveloppe_bati_etalab_"$dpt"_"$millesime" CLUSTER ON l_enveloppe_bati_etalab_"$dpt"_"$millesime"_geom;
"

echo "A.1.3] Création d'un champs geom_4326"
$commande "
ALTER TABLE "$schemaorigine".l_enveloppe_bati_etalab_"$dpt"_"$millesime"
	ADD COLUMN geom_4326 geometry(Polygon,4326);
UPDATE "$schemaorigine".l_enveloppe_bati_etalab_"$dpt"_"$millesime" SET geom_4326 = ST_Transform(geom,4326);
"
$commande "
CREATE INDEX l_enveloppe_bati_etalab_"$dpt"_"$millesime"_geom_4326 ON "$schemaorigine".l_enveloppe_bati_etalab_"$dpt"_"$millesime"
	USING gist (geom_4326) TABLESPACE index;
"
#DEBUG
#echo "Création des Index dans la table nationale des bati du cadastre 2018"
#$commande "
#CREATE INDEX n_bati_etalab_000_"$millesime"_type_idx ON r_cadastre_etalab_"$millesime".n_bati_etalab_000_"$millesime"
#	USING brin (type) TABLESPACE index;
#CREATE INDEX n_bati_etalab_000_"$millesime"_code_dep_idx ON r_cadastre_etalab_"$millesime".n_bati_etalab_000_"$millesime"
#	USING brin (code_dep) TABLESPACE index;
#CREATE INDEX n_bati_etalab_000_"$millesime"_geom ON r_cadastre_etalab_"$millesime".n_bati_etalab_000_"$millesime"
#	USING gist (geom) TABLESPACE index;
#"

echo "A.1.4] Peuplement du champs nb_batidur + index"
$commande "
UPDATE "$schemaorigine".l_enveloppe_bati_etalab_"$dpt"_"$millesime" as t1 SET nb_batidur = (
	SELECT count(t2.*)
	FROM r_cadastre_etalab_"$millesime".n_bati_etalab_000_"$millesime" as t2
	-- Option sans partition	
	-- FROM r_cadastre_etalab_"$millesime".n_bati_etalab_"$dpt"_"$millesime" as t2
	WHERE 	
		ST_Intersects(t1.geom_4326, t2.geom)
		AND 
		(t2.type='01' or t2.type='Bâtiment en dur' or t2.type='Bâti dur' or t2.type='Bati dur')
	);
"
$commande "
CREATE INDEX l_enveloppe_bati_etalab_"$dpt"_"$millesime"_nb_batidur_idx ON "$schemaorigine".l_enveloppe_bati_etalab_"$dpt"_"$millesime"
	USING brin (nb_batidur) TABLESPACE index;
"

echo "A.1.5] Peuplement du champs nb_batileg + index"
$commande "
UPDATE "$schemaorigine".l_enveloppe_bati_etalab_"$dpt"_"$millesime" as t1 SET nb_batileg = (
	SELECT count(t2.*)
	FROM r_cadastre_etalab_"$millesime".n_bati_etalab_000_"$millesime" as t2
	-- Option sans partition	
	-- FROM r_cadastre_etalab_"$millesime".n_bati_etalab_"$dpt"_"$millesime" as t2
	WHERE 	
		ST_Intersects(t1.geom_4326, t2.geom)
		AND
		(t2.type='02' or t2.type='Construction légère' or t2.type='Bâti léger' or t2.type='Bati leger')
	);
"
$commande "
CREATE INDEX l_enveloppe_bati_etalab_"$dpt"_"$millesime"_nb_batileg_idx ON "$schemaorigine".l_enveloppe_bati_etalab_"$dpt"_"$millesime"
	USING brin (nb_batileg) TABLESPACE index;
"

echo "A.1.6] Peuplement du champs surf_batidur + index"
$commande "
UPDATE "$schemaorigine".l_enveloppe_bati_etalab_"$dpt"_"$millesime" as t1 SET surf_batidur = (
	SELECT sum(ST_Area(ST_Transform(t2.geom,2154)))
	FROM r_cadastre_etalab_"$millesime".n_bati_etalab_000_"$millesime" as t2
	-- Option sans partition	
	-- FROM r_cadastre_etalab_"$millesime".n_bati_etalab_"$dpt"_"$millesime" as t2
	WHERE 	
		ST_Intersects(t1.geom_4326, t2.geom)
		AND 
		(t2.type='01' or t2.type='Bâtiment en dur' or t2.type='Bâti dur' or t2.type='Bati dur')
	);
"
$commande "
CREATE INDEX l_enveloppe_bati_etalab_"$dpt"_"$millesime"_surf_batidur_idx ON "$schemaorigine".l_enveloppe_bati_etalab_"$dpt"_"$millesime"
	USING brin (surf_batidur) TABLESPACE index;
"

echo "A.1.7] Peuplement du champs surf_batileg + index"
$commande "
UPDATE "$schemaorigine".l_enveloppe_bati_etalab_"$dpt"_"$millesime" as t1 SET surf_batileg = (
	SELECT sum(ST_Area(ST_Transform(t2.geom,2154)))
	FROM r_cadastre_etalab_"$millesime".n_bati_etalab_000_"$millesime" as t2
	-- Option sans partition	
	-- FROM r_cadastre_etalab_"$millesime".n_bati_etalab_"$dpt"_"$millesime" as t2
	WHERE 	
		ST_Intersects(t1.geom_4326, t2.geom)
		AND
		(t2.type='02' or t2.type='Construction légère' or t2.type='Bâti léger' or t2.type='Bati leger')
	);
"
$commande "
CREATE INDEX l_enveloppe_bati_etalab_"$dpt"_"$millesime"_surf_batileg_idx ON "$schemaorigine".l_enveloppe_bati_etalab_"$dpt"_"$millesime"
	USING brin (surf_batileg) TABLESPACE index;
"

echo "A.1.8] Commentaires"
$commande "
COMMENT ON TABLE "$schemaorigine".l_enveloppe_bati_etalab_"$dpt"_"$millesime"
  IS 'Enveloppe du bati au 1er janvier "$millesime" pour le département ''"$dpt"''.

Il s’agit de fusionner tous les bâtiments issu du cadastre ETALAB par une dilatation de 50 m, puis une fusion et ensuite une érosion de 40 m';

COMMENT ON COLUMN "$schemaorigine".l_enveloppe_bati_etalab_"$dpt"_"$millesime".id
    IS 'Identifiant unique selon méthode COVADIS : n°département.000.000 + id.';

COMMENT ON COLUMN "$schemaorigine".l_enveloppe_bati_etalab_"$dpt"_"$millesime".nb_batidur
    IS 'Nombre de bâtiments qualifiés de dur par la DGFiP intégrés dans cette enveloppe du bâti.';

COMMENT ON COLUMN "$schemaorigine".l_enveloppe_bati_etalab_"$dpt"_"$millesime".nb_batileg
    IS 'Nombre de bâtiments qualifiés de leger par la DGFiP intégrés dans cette enveloppe du bâti.';

COMMENT ON COLUMN "$schemaorigine".l_enveloppe_bati_etalab_"$dpt"_"$millesime".surf_batidur
    IS 'Surface totale en m2 de tous les bâtiments qualifiés de dur par la DGFiP et présents dans cette enveloppe du bâti.';

COMMENT ON COLUMN "$schemaorigine".l_enveloppe_bati_etalab_"$dpt"_"$millesime".surf_batileg
    IS 'Surface totale en m2 de tous les bâtiments qualifiés de leger par la DGFiP et présents dans cette enveloppe du bâti.';

COMMENT ON COLUMN "$schemaorigine".l_enveloppe_bati_etalab_"$dpt"_"$millesime".code_dep
    IS 'Numéro INSEE du département sur 3 charactères.';

COMMENT ON COLUMN "$schemaorigine".l_enveloppe_bati_etalab_"$dpt"_"$millesime".geom
    IS 'Champs géométrique : Monopolygones en Lambert93 (EPSG:2154).';
"

echo "A.1.9] Suppression du champs geom_4326"
$commande "
ALTER TABLE "$schemaorigine".l_enveloppe_bati_etalab_"$dpt"_"$millesime"
	DROP COLUMN geom_4326;
"

done
