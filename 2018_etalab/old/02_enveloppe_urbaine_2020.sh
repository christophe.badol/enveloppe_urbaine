#!/bin/bash
# Enveloppe Urbaine
commande="psql -d ceremabase -c "

# Important :
# Pour tout logger taper :
# time sh 02_enveloppe_urbaine_2020.sh 1>/mnt/data/02_enveloppe_urbaine_2020_A1.log 2>&1

#DEBUG
#for dpt in '090'
for dpt in '02a' '02b'
#for dpt in '001' '002' '003' '004' '005' '006' '007' '008' '009' '010' '011' '012' '013' '014' '015' '016' '017' '018' '019' '02b' '02a' '021' '022' '023' '024' '025' '026' '027' '028' '029' '030' '031' '032' '033' '034' '035' '036' '037' '038' '039' '040' '041' '042' '043' '044' '045' '046' '047' '048' '049' '050' '051' '052' '053' '054' '055' '056' '057' '058' '060' '061' '062' '063' '064' '065' '066' '067' '068' '069' '070' '071' '072' '073' '074' '075' '076' '077' '078' '079' '080' '081' '082' '083' '084' '085' '086' '087' '088' '089' '090' '091' '092' '093' '094' '095'

do
	
now=$(date +"%T")
echo "--------------------"
echo "Début à $now"
echo "Département $dpt"
echo "--------------------"
echo "A.1.1] Création de la table à partir d’une dilatation de 50 m / fusion / érosion de 40 m"
$commande "
DROP TABLE IF EXISTS tache_urbaine_2020.l_enveloppe_bati_etalab_"$dpt"_2020;
CREATE TABLE tache_urbaine_2020.l_enveloppe_bati_etalab_"$dpt"_2020
(
	id serial,
	nbatidur integer,
	nbatileg integer,
	code_dep character(3) DEFAULT '"$dpt"',  
	geom geometry(Polygon,2154)
);
"
#DEBUG
# Obligation de séparer ces trois commandes car si 2a 2b l’ALTER SEQUENCE echoue et annule les 4 commandes
$commande "
ALTER SEQUENCE tache_urbaine_2020.l_enveloppe_bati_etalab_"$dpt"_2020_id_seq
	RESTART WITH "$dpt"000001;
"
#DEBUG
# Obligation de séparer ces trois commandes car si 2a 2b l’ALTER SEQUENCE echoue et annule les 4 commandes
$commande "
INSERT INTO tache_urbaine_2020.l_enveloppe_bati_etalab_"$dpt"_2020 (geom)
	SELECT (ST_Dump(ST_Buffer(ST_Union(ST_Buffer(t1.geom,50)),-40))).geom::geometry(Polygon,2154) as geom
	FROM r_cadastre_etalab_2020.n_bati_etalab_000_2020 as t1
	JOIN tache_urbaine_2020.temp_departement_buffer_200m_000_2020 as t2
	ON ST_Intersects (t1.geom, t2.geom)
	WHERE t2.code_dep = upper('"$dpt"');
"
echo "A.1.2] Clé + Index + Cluster + Optimisation"
$commande "
ALTER TABLE tache_urbaine_2020.l_enveloppe_bati_etalab_"$dpt"_2020
    ADD CONSTRAINT l_enveloppe_bati_etalab_"$dpt"_2020_pk PRIMARY KEY (id)
    USING INDEX TABLESPACE index;

ALTER TABLE tache_urbaine_2020.l_enveloppe_bati_etalab_"$dpt"_2020
    ADD CONSTRAINT enforce_dims_geom CHECK (ST_ndims(geom) = 2);

ALTER TABLE tache_urbaine_2020.l_enveloppe_bati_etalab_"$dpt"_2020
	ADD CONSTRAINT enforce_srid_geom CHECK (ST_Srid(geom)= 2154);

ALTER TABLE tache_urbaine_2020.l_enveloppe_bati_etalab_"$dpt"_2020
    ADD CONSTRAINT enforce_geotype_geom CHECK (geometrytype(geom) = 'POLYGON'::text);

CREATE INDEX l_enveloppe_bati_etalab_"$dpt"_2020_code_dep_idx ON tache_urbaine_2020.l_enveloppe_bati_etalab_"$dpt"_2020
	USING brin (code_dep) TABLESPACE index;

CREATE INDEX l_enveloppe_bati_etalab_"$dpt"_2020_geom ON tache_urbaine_2020.l_enveloppe_bati_etalab_"$dpt"_2020
	USING gist (geom) TABLESPACE index;
ALTER TABLE tache_urbaine_2020.l_enveloppe_bati_etalab_"$dpt"_2020 CLUSTER ON l_enveloppe_bati_etalab_"$dpt"_2020_geom;
"

echo "A.1.3] Peuplement du champs nbatidur + index"
$commande "
UPDATE tache_urbaine_2020.l_enveloppe_bati_etalab_"$dpt"_2020 as t1 SET nbatidur = (
	SELECT count(*)
	FROM r_cadastre_etalab_2020.n_bati_etalab_000_2020 as t2
	-- Option sans partition	
	-- FROM r_cadastre_etalab_2020.n_bati_etalab_"$dpt"_2020 as t2
	WHERE 	t1.code_dep = '"$dpt"'
		and (t2.type='Bâtiment en dur' or t2.type='Bâti dur' or t2.type='Bati dur' or t2.type='01')
		and ST_Intersects(t1.geom, t2.geom))
	WHERE 	t1.code_dep = '"$dpt"';
"
$commande "
CREATE INDEX l_enveloppe_bati_etalab_"$dpt"_2020_nbatidur_idx ON tache_urbaine_2020.l_enveloppe_bati_etalab_"$dpt"_2020
	USING brin (nbatidur) TABLESPACE index;
"

echo "A.1.4] Peuplement du champs nbatileg + index"
$commande "
UPDATE tache_urbaine_2020.l_enveloppe_bati_etalab_"$dpt"_2020 as t1 SET nbatileg = (
	SELECT count(*)
	FROM r_cadastre_etalab_2020.n_bati_etalab_000_2020 as t2
	-- Option sans partition	
	-- FROM r_cadastre_etalab_2020.n_bati_etalab_"$dpt"_2020 as t2
	WHERE 	t1.code_dep = '"$dpt"'
		and (type='Construction légère' or type='Bâti léger' or type='Bati leger' or type='02')
		and ST_Intersects(t1.geom, t2.geom))
	WHERE 	t1.code_dep = '"$dpt"';
"
$commande "
CREATE INDEX l_enveloppe_bati_etalab_"$dpt"_2020_nbatileg_idx ON tache_urbaine_2020.l_enveloppe_bati_etalab_"$dpt"_2020
	USING brin (nbatileg) TABLESPACE index;
"

echo "A.1.5] Commentaires"
$commande "
COMMENT ON TABLE tache_urbaine_2020.l_enveloppe_bati_etalab_"$dpt"_2020
  IS 'Enveloppe du bati au 1er janvier 2020 pour le département ''"$dpt"''.

Il s’agit de fusionner tous les bâtiments issu du cadastre ETALAB par une dilatation de 50 m, puis une fusion et ensuite une érosion de 40 m';

COMMENT ON COLUMN tache_urbaine_2020.l_enveloppe_bati_etalab_"$dpt"_2020.id
    IS 'Identifiant unique selon méthode COVADIS : n°département.000.000 + id.';

COMMENT ON COLUMN tache_urbaine_2020.l_enveloppe_bati_etalab_"$dpt"_2020.nbatidur
    IS 'Nombre de bâtiments qualifiés de dur par la DGFiP intégrés dans cette enveloppe du bâti.';

COMMENT ON COLUMN tache_urbaine_2020.l_enveloppe_bati_etalab_"$dpt"_2020.nbatileg
    IS 'Nombre de bâtiments qualifiés de legers par la DGFiP intégrés dans cette enveloppe du bâti.';

COMMENT ON COLUMN tache_urbaine_2020.l_enveloppe_bati_etalab_"$dpt"_2020.code_dep
    IS 'Numéro INSEE du département sur 3 charactères.';

COMMENT ON COLUMN tache_urbaine_2020.l_enveloppe_bati_etalab_"$dpt"_2020.geom
    IS 'Champs géométrique : Monopolygones en Lambert93 (EPSG:2154).';
"

done
