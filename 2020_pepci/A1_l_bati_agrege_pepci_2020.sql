------------------------------------------------
----	CREATION BATI AGREGE
----
----             CEREMABASE
----
----	Version 1 du 4 avril mai 2021
----	c.badol
----	Finalisé oui|X| / non | |
------------------------------------------------
---- Rq : ---- pour les commentaires / -- pour les commandes optionnelles, debuger"

CREATE SCHEMA p_enveloppe_bati_2020 AUTHORIZATION postgres;
COMMENT ON SCHEMA p_enveloppe_bati_2020 IS 'Enveloppe du bâti de 2020.
Création par fusion d un buffer de 50 m autour des batiments et érosion de -40 m.

Le référentiel utilisé est dans le nom de la table :
- pepci : Parcellaire Express de l IGN,
- etalab : cadastre disponible sur le site data.gouv.fr,
- bdtopo : batiments de la BDTOPO de l IGN.

Les scripts de ce travail sont disponibles ici : https://gitlab.com/christophe.badol/enveloppe_urbaine .';

------------------------------------
---- A] Bâti agrégé 2021
------------------------------------

---- A1] Script 
#!/bin/bash
# Bati Agrégé
commande="psql -p 5433 -d ceremabase -c "
millesime="2021"
cadastre="pepci"
couchebaticadastre="r_parcellaire_express_"$millesime".n_batiment_"$cadastre

#DEBUG
#echo $couchebaticadastre

# Important :
# Pour tout logger taper :
# time sh /home/administrateur/00_gitlab/prod/enveloppe_urbaine/2021_pepci/A1_l_bati_agrege_pepci_2021.sh 1>/mnt/data/A1_l_bati_agrege_pepci_2021.log 2>&1

# A] Bati Agrege
#DEBUG
#for dpt in '090'
for dpt in '001' '002' '003' '004' '005' '006' '007' '008' '009' '010' '011' '012' '013' '014' '015' '016' '017' '018' '019' '021' '022' '023' '024' '025' '026' '027' '028' '029' '030' '031' '032' '033' '034' '035' '036' '037' '038' '039' '040' '041' '042' '043' '044' '045' '046' '047' '048' '049' '050' '051' '052' '053' '054' '055' '056' '057' '058' '059' '060' '061' '062' '063' '064' '065' '066' '067' '068' '069' '070' '071' '072' '073' '074' '075' '076' '077' '078' '079' '080' '081' '082' '083' '084' '085' '086' '087' '088' '089' '090' '091' '092' '093' '094' '095' '02b' '02a'

do

echo $dpt
echo '----'

# A.1] Création des tables Départementales
$commande "
DROP TABLE IF EXISTS tache_urbaine_"$millesime".l_bati_agrege_"$cadastre"_"$dpt"_"$millesime";
"
$commande "
CREATE TABLE tache_urbaine_"$millesime".l_bati_agrege_"$cadastre"_"$dpt"_"$millesime"
	(
	id serial,
	nb_batidur integer,
	nb_batileg integer,
	code_dep character(3) DEFAULT UPPER('"$dpt"'),  
	geom geometry(Polygon,2154)
	);
"

$commande "
ALTER SEQUENCE tache_urbaine_"$millesime".l_bati_agrege_"$cadastre"_"$dpt"_"$millesime"_id_seq
	RESTART WITH "$dpt"000001;
"

# A.2] Buffer +0,01, ST_Union, Buffer -0,01, ST_Dump
$commande "
INSERT INTO tache_urbaine_"$millesime".l_bati_agrege_"$cadastre"_"$dpt"_"$millesime" (geom)
	SELECT (ST_Dump(ST_Buffer(ST_Union(ST_Buffer(geom,0,01)),-0,01))).geom::geometry(Polygon,2154) as geom
	FROM "$couchebaticadastre"_"$dpt"_"$millesime";
"

# A.3] Index Géomérique + Cluster
$commande "
CREATE INDEX l_bati_agrege_"$cadastre"_"$dpt"_"$millesime"_geom_gist ON tache_urbaine_"$millesime".l_bati_agrege_"$cadastre"_"$dpt"_"$millesime"
	USING gist (geom) TABLESPACE index;
"
$commande "
ALTER TABLE tache_urbaine_"$millesime".l_bati_agrege_"$cadastre"_"$dpt"_"$millesime"
	CLUSTER ON l_bati_agrege_"$cadastre"_"$dpt"_"$millesime"_geom_gist;
"

# A.4] Mise à jour des champs nb_batidur & nb_batileg
$commande "
UPDATE tache_urbaine_"$millesime".l_bati_agrege_"$cadastre"_"$dpt"_"$millesime" as t1 SET nb_batidur = (
	SELECT count(*)
	FROM "$couchebaticadastre"_"$dpt"_"$millesime" as t2
	WHERE 	t1.code_dep = '"$dpt"'
		and (t2.type='Bâtiment en dur' or t2.type='Bâti dur' or t2.type='Bati dur' or t2.type='01')
		and ST_Intersects(t1.geom, t2.geom))
	WHERE 	t1.code_dep = '"$dpt"';
"

$commande "
UPDATE tache_urbaine_"$millesime".l_bati_agrege_"$cadastre"_"$dpt"_"$millesime" as t1 SET nb_batileg = (
	SELECT count(*)
	FROM "$couchebaticadastre"_"$dpt"_"$millesime" as t2
	WHERE 	t1.code_dep = '"$dpt"'
		and (type='Construction légère' or type='Bâti léger' or type='Bati leger' or type='02')
		and ST_Intersects(t1.geom, t2.geom))
	WHERE 	t1.code_dep = '"$dpt"';
"

# A.5] Optimisation 
# Clé primaire 
$commande "
ALTER TABLE tache_urbaine_"$millesime".l_bati_agrege_"$cadastre"_"$dpt"_"$millesime"
    ADD CONSTRAINT l_bati_agrege_"$cadastre"_"$dpt"_"$millesime"_pk PRIMARY KEY (id)
    USING INDEX TABLESPACE index;
"
# Contraintes Géométriques
$commande "
ALTER TABLE tache_urbaine_"$millesime".l_bati_agrege_"$cadastre"_"$dpt"_"$millesime"
    ADD CONSTRAINT enforce_dims_geom CHECK (ST_ndims(geom) = 2);
"
$commande "
ALTER TABLE tache_urbaine_"$millesime".l_bati_agrege_"$cadastre"_"$dpt"_"$millesime"
	ADD CONSTRAINT enforce_srid_geom CHECK (ST_Srid(geom)= 2154);
"
$commande "
ALTER TABLE tache_urbaine_"$millesime".l_bati_agrege_"$cadastre"_"$dpt"_"$millesime"
    ADD CONSTRAINT enforce_geotype_geom CHECK (geometrytype(geom) = 'POLYGON'::text);
"
$commande "
ALTER TABLE tache_urbaine_"$millesime".l_bati_agrege_"$cadastre"_"$dpt"_"$millesime"
    ADD CONSTRAINT enforce_validite_geom CHECK (ST_IsValid(geom));
"
$commande "
ALTER TABLE tache_urbaine_"$millesime".l_bati_agrege_"$cadastre"_"$dpt"_"$millesime"
    ADD CONSTRAINT enforce_no_self_intersection_geom CHECK (ST_IsSimple(geom));
"
$commande "
ALTER TABLE tache_urbaine_"$millesime".l_bati_agrege_"$cadastre"_"$dpt"_"$millesime"
    ADD CONSTRAINT enforce_no_zero_area_geom CHECK (ST_Area(geom) > 0);
"

# Index attributaires
$commande "
CREATE INDEX l_bati_agrege_"$cadastre"_"$dpt"_"$millesime"_nb_batidur_idx ON tache_urbaine_"$millesime".l_bati_agrege_"$cadastre"_"$dpt"_"$millesime"
	USING brin (nb_batidur) TABLESPACE index;
"
$commande "
CREATE INDEX l_bati_agrege_"$cadastre"_"$dpt"_"$millesime"_nb_batileg_idx ON tache_urbaine_"$millesime".l_bati_agrege_"$cadastre"_"$dpt"_"$millesime"
	USING brin (nb_batileg) TABLESPACE index;
"
$commande "
CREATE INDEX l_bati_agrege_"$cadastre"_"$dpt"_"$millesime"_code_dep_idx ON tache_urbaine_"$millesime".l_bati_agrege_"$cadastre"_"$dpt"_"$millesime"
	USING brin (code_dep) TABLESPACE index;
"

# A.6] Commentaires
# Table
$commande "
COMMENT ON TABLE tache_urbaine_"$millesime".l_bati_agrege_"$cadastre"_"$dpt"_"$millesime"
  IS 'Bâti agrégé de "$millesime" pour le département ''"$dpt"''.

Il s’agit de fusionner tous les bâtiments du cadastre "$cadastre" qui sont séparés de moins de 1 cm par un Buffer +0,01, ST_Union, Buffer -0,01, ST_Dump';
"
# Champs
$commande "
COMMENT ON COLUMN tache_urbaine_"$millesime".l_bati_agrege_"$cadastre"_"$dpt"_"$millesime".id
    IS 'Identifiant unique selon méthode COVADIS : n°département.000.000 + id.';
"
$commande "
COMMENT ON COLUMN tache_urbaine_"$millesime".l_bati_agrege_"$cadastre"_"$dpt"_"$millesime".nb_batidur
    IS 'Nombre de bâtiments qualifiés de dur par la DGFiP intégrés dans cette agrégation de bâtiments.';
"
$commande "
COMMENT ON COLUMN tache_urbaine_"$millesime".l_bati_agrege_"$cadastre"_"$dpt"_"$millesime".nb_batileg
    IS 'Nombre de bâtiments qualifiés de legers par la DGFiP intégrés dans cette agrégation de bâtiments.';
"
$commande "
COMMENT ON COLUMN tache_urbaine_"$millesime".l_bati_agrege_"$cadastre"_"$dpt"_"$millesime".code_dep
    IS 'Numéro INSEE du département sur 3 charactères.';
"
$commande "
COMMENT ON COLUMN tache_urbaine_"$millesime".l_bati_agrege_"$cadastre"_"$dpt"_"$millesime".geom
    IS 'Champs géométrique : Monopolygones en Lambert93 (EPSG:2154).';
"
done

---- Execution :
---- 17/04/2021 : 
time sh /home/administrateur/00_gitlab/prod/enveloppe_urbaine/2021_pepci/A1_l_bati_agrege_pepci_2021.sh 1>/mnt/data/A1_l_bati_agrege_pepci_2021.log 2>&1
--> fait en 3 phases :
--> A1_l_bati_agrege_pepci_2021_1_a_23.log.log
--> A1_l_bati_agrege_pepci_2021_24_a_41.log.log
--> A1_l_bati_agrege_etalab_2021_41_a.log
/*
real	16902m27,424s
user	1m1,594s
sys	0m13,432s
*/

update tache_urbaine_2021.l_bati_agrege_pepci_02A_2021 set code_dep = '02A';
--> Updated Rows	90953
update tache_urbaine_2021.l_bati_agrege_pepci_02B_2021 set code_dep = '02B';
--> Updated Rows	93636

------------------------------------------------------------
---- B] Partitions
------------------------------------------------------------
---- 08/05/2021
---- B.1] Partition Nationale 
select w_adl_delegue.set_partition_attach_metropole_et_regions('tache_urbaine_2021.l_bati_agrege_pepci','_2021');
/*
set_partition_attach_metropole_et_regions                                 |
--------------------------------------------------------------------------+
2021-05-08 - tache_urbaine_2021.l_bati_agrege_pepci_000_2021 reste visible|
 */

---- B.2] Commentaires sur paritions
DO $$
DECLARE
liste_valeur 				CHARACTER VARYING[];
millesime				CHARACTER VARYING(4);
req 					text;
BEGIN
------ Paramètres en entrée
liste_valeur := ARRAY  ['000','r11','r24','r27','r28','r32','r44','r52','r53','r75','r76','r84','r93','r94'];
millesime := '2021';
------

FOR i_table IN 1..array_length(liste_valeur, 1) LOOP
		req := '
				COMMENT ON TABLE tache_urbaine_' || millesime || '.l_bati_agrege_pepci_'|| liste_valeur[i_table] ||'_' || millesime || '
				  IS ''Bâti agrégé de ' || millesime || '.
				
				Il s’agit de fusionner tous les bâtiments du parcellaire express qui sont séparés de moins de 1 cm par un Buffer +0,01, ST_Union, Buffer -0,01, ST_Dump.'';
				COMMENT ON COLUMN tache_urbaine_' || millesime || '.l_bati_agrege_pepci_'|| liste_valeur[i_table] ||'_' || millesime || '.id
				IS ''Identifiant unique selon méthode COVADIS : n°département.000.000 + id.'';
				COMMENT ON COLUMN tache_urbaine_' || millesime || '.l_bati_agrege_pepci_'|| liste_valeur[i_table] ||'_' || millesime || '.nb_batidur
				IS ''Nombre de bâtiments qualifiés de dur par la DGFiP intégrés dans cette agrégation de bâtiments.'';
				COMMENT ON COLUMN tache_urbaine_' || millesime || '.l_bati_agrege_pepci_'|| liste_valeur[i_table] ||'_' || millesime || '.nb_batileg
				IS ''Nombre de bâtiments qualifiés de legers par la DGFiP intégrés dans cette agrégation de bâtiments.'';
				COMMENT ON COLUMN tache_urbaine_' || millesime || '.l_bati_agrege_pepci_'|| liste_valeur[i_table] ||'_' || millesime || '.code_dep
				IS ''Numéro INSEE du département sur 3 charactères.'';
				COMMENT ON COLUMN tache_urbaine_' || millesime || '.l_bati_agrege_pepci_'|| liste_valeur[i_table] ||'_' || millesime || '.geom
				IS ''Champs géométrique : Monopolygones en Lambert93 (EPSG:2154).''; 
			'; 
		RAISE NOTICE '%', req;
		EXECUTE(req);
		COMMIT;
   END LOOP;
END $$;
--> Updated Rows	0

------------------------------------------------------------
---- C] Synthèse
------------------------------------------------------------
---- 08/05/2021
---- https://gitlab.cerema.fr/centre-est/admin_ceremabase/-/blob/master/fonctions/synthese_geometrie_schema.sql
CREATE table tache_urbaine_2021.synthese_geometrie_schema AS
SELECT * FROM w_fonctions.synthese_geometrie_schema('tache_urbaine_2021');
--> Updated Rows	110
