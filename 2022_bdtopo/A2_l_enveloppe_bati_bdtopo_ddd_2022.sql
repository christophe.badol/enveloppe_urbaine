------------------------------------------------
----	CREATION ENVELOPPE BATI PAR DEPARTEMENT
----
----             CEREMABASE
----
----	Version 1 du 01/02/2023
----	c.badol
----	Finalisé oui| | / non |X|
------------------------------------------------
---- Rq : ---- pour les commentaires / -- pour les commandes optionnelles, debuger

-----------------------------------
---- A] Enveloppe Urbaine 2021
-----------------------------------
#!/bin/bash
# 19/06/2022
echo "-----------------------------------------------------"
echo "A.2] Travail sur la table nationale Enveloppe Urbaine"
echo "-----------------------------------------------------"
# Paramètres
export PGPASSWORD='tunelesaurapas!'
commande="psql -h hp-geomatique -p 5432 -U postgres -d ceremabase -c "
millesime="2022"

##############################################################
# Option 1.1 : Depuis le parcellaire express ==> pepci
#trigramme="pepci"
# Option 1.2 : Depuis le cadastre de data.gouv.fr ==> etalab
#trigramme="etalab"
# Option 1.3 : Depuis le bati de la bdtopo v3 ==> bdtopo
trigramme="bdtopo"
# Fin Option 1
##############################################################

cadastre="bdtopo"
schemacadastre="r_"$cadastre"_"$millesime
nomschema="p_enveloppe_bati_"$millesime


# Important :
# Pour tout logger taper :
# su postgres
# time sh /home/administrateur/00_gitlab/prod/enveloppe_urbaine/2022_bdtopo/A2_l_enveloppe_bati_bdtopo_ddd_2022.sh 1>/home/administrateur/00_gitlab/prod/enveloppe_urbaine/2022_bdtopo/098_log/A2_enveloppe_bati_etalab_ddd_2021.log 2>&1

$commande "
--DROP SCHEMA IF EXISTS "$nomschema";
CREATE SCHEMA "$nomschema"
"

$commande "
/*
CREATE TABLE "$schematravail".temp_departement_buffer_200m_000_"$millesime" AS
	SELECT '0' || insee_dep AS code_dep, ST_Multi(ST_Buffer(ST_Union(ST_Transform(geom,2154)),200))::geometry(MultiPolygon,2154) as geom
	FROM r_admin_express.n_adm_exp_cog_departement_000
	GROUP BY insee_dep;

CREATE INDEX temp_departement_buffer_200m_000_"$millesime"_code_dep_idx
    ON "$schematravail".temp_departement_buffer_200m_000_"$millesime" USING brin
    (code_dep)
    TABLESPACE index;

CREATE INDEX temp_departement_buffer_200m_000_"$millesime"_geom_gist
    ON "$schematravail".temp_departement_buffer_200m_000_"$millesime" USING gist
    (geom)
    TABLESPACE index;

ALTER TABLE "$schematravail".temp_departement_buffer_200m_000_"$millesime"
    CLUSTER ON temp_departement_buffer_200m_000_"$millesime"_geom_gist;
*/
"

#DEBUG
#for dpt in '090' '02a' '02b'
for dpt in '001' '002' '003' '004' '005' '006' '007' '008' '009' '010' '011' '012' '013' '014' '015' '016' '017' '018' '019' '02b' '02a' '021' '022' '023' '024' '025' '026' '027' '028' '029' '030' '031' '032' '033' '034' '035' '036' '037' '038' '039' '040' '041' '042' '043' '044' '045' '046' '047' '048' '049' '050' '051' '052' '053' '054' '055' '056' '057' '058' '059' '060' '061' '062' '063' '064' '065' '066' '067' '068' '069' '070' '071' '072' '073' '074' '075' '076' '077' '078' '079' '080' '081' '082' '083' '084' '085' '086' '087' '088' '089' '090' '091' '092' '093' '094' '095'
do
# Obligation du cadastre meme si le bati agrégé n a pas été intégré :
batiorigine=$schemacadastre".n_batiment_bdt_"$dpt"_"$millesime 
nomtable="l_enveloppe_bati_"$trigramme"_"$dpt"_"$millesime	
racinedepartement="r_admin_express.n_adm_exp_cog_departement_000"
now=$(date +"%T")
echo "--------------------"
echo "Début à $now"
echo "Département $dpt"
echo "--------------------"
echo "A.1.1] Création de la table à partir d’une dilatation de 50 m / fusion / érosion de 40 m"
$commande "
DROP TABLE IF EXISTS "$nomschema"."$nomtable";
CREATE TABLE "$nomschema"."$nomtable"
(
	id serial,
	nb_batidur integer,
	nb_batileg integer,
	surf_batidur integer,
	surf_batileg integer,
	code_dep character(3) DEFAULT '"$dpt"',  
	geom geometry(Polygon,2154)
);
"
#DEBUG
# Obligation de séparer ces trois commandes car si 2a 2b l’ALTER SEQUENCE echoue et annule les 4 commandes
$commande "
ALTER SEQUENCE "$nomschema"."$nomtable"_id_seq
	RESTART WITH "$dpt"000001;
"
#DEBUG
# Obligation de séparer ces trois commandes car si 2a 2b l’ALTER SEQUENCE echoue et annule les 4 commandes
$commande "
---- Version qui neccessite de générer une table départementale temporaire, mais beaucoup plus rapide
INSERT INTO "$nomschema"."$nomtable" (geom)
	SELECT (ST_Dump(ST_Buffer(ST_Union(ST_Buffer(t1.geom,50)),-40))).geom::geometry(Polygon,2154) as geom
	FROM "$batiorigine" as t1
	JOIN "$nomschema".temp_departement_buffer_200m_000_"$millesime" as t2
	ON ST_Intersects (t1.geom, t2.geom)
	WHERE t2.code_dep = upper('"$dpt"');

/*
---- Version qui tient compte des départements autour mais longue car le buffer et généré à chaque fois sans index
INSERT INTO "$nomschema"."$nomtable" (geom)
	SELECT (ST_Dump(ST_Buffer(ST_Union(ST_Buffer(t1.geom,50)),-40))).geom::geometry(Polygon,2154) as geom
	FROM "$batiorigine" as t1
	JOIN "$racinedepartement" as t2
	ON ST_Intersects (t1.geom,ST_Buffer(t2.geom,100))
	WHERE '0' || t2.insee_dep = '"$dpt"';
	
---- version 1 qui ne tient pas compte des batiments à coté mais hors département
INSERT INTO "$nomschema"."$nomtable" (geom)
	SELECT (ST_Dump(ST_Buffer(ST_Union(ST_Buffer(t1.geom,50)),-40))).geom::geometry(Polygon,2154) as geom
	FROM "$batiorigine" as t1;
*/
"
echo "A.1.2] Clé + Index + Cluster + Optimisation"
$commande "
ALTER TABLE "$nomschema"."$nomtable"
    ADD CONSTRAINT "$nomtable"_pk PRIMARY KEY (id)
    USING INDEX TABLESPACE index;

ALTER TABLE "$nomschema"."$nomtable"
    ADD CONSTRAINT enforce_dims_geom CHECK (ST_ndims(geom) = 2);

ALTER TABLE "$nomschema"."$nomtable"
	ADD CONSTRAINT enforce_srid_geom CHECK (ST_Srid(geom)= 2154);

ALTER TABLE "$nomschema"."$nomtable"
    ADD CONSTRAINT enforce_geotype_geom CHECK (geometrytype(geom) = 'POLYGON'::text);

ALTER TABLE "$nomschema"."$nomtable"
	ADD CONSTRAINT enforce_no_self_intersection_geom CHECK (st_issimple(geom));

ALTER TABLE "$nomschema"."$nomtable"
	ADD CONSTRAINT enforce_no_zero_area_geom CHECK ((st_area(geom) > (0)::double precision));

ALTER TABLE "$nomschema"."$nomtable"
	ADD CONSTRAINT enforce_validite_geom CHECK (st_isvalid(geom));

CREATE INDEX "$nomtable"_code_dep_idx ON "$nomschema"."$nomtable"
	USING brin (code_dep) TABLESPACE index;

CREATE INDEX "$nomtable"_geom ON "$nomschema"."$nomtable"
	USING gist (geom) TABLESPACE index;
ALTER TABLE "$nomschema"."$nomtable" CLUSTER ON "$nomtable"_geom;
"

echo "A.1.3] Peuplement du champs nb_batidur + index"
$commande "
UPDATE "$nomschema"."$nomtable" as t1 SET nb_batidur = (
	SELECT count(*)
	FROM "$batiorigine" as t2
	WHERE 	t1.code_dep = UPPER('"$dpt"')
		and (t2.leger='Non' or t2.leger='non')
		and ST_Intersects(t1.geom, t2.geom));

"
$commande "
CREATE INDEX "$nomtable"_nb_batidur_idx ON "$nomschema"."$nomtable"
	USING brin (nb_batidur) TABLESPACE index;
"

echo "A.1.4] Peuplement du champs nb_batileg + index"
$commande "
UPDATE "$nomschema"."$nomtable" as t1 SET nb_batileg = (
	SELECT count(*)
	FROM "$batiorigine" as t2
	WHERE 	t1.code_dep = UPPER('"$dpt"')
		and (t2.leger='Oui' or t2.leger='oui')
		and ST_Intersects(t1.geom, t2.geom));
"
$commande "
CREATE INDEX "$nomtable"_nb_batileg_idx ON "$nomschema"."$nomtable"
	USING brin (nb_batileg) TABLESPACE index;
"

echo "A.1.5] Peuplement du champs surf_batidur + index"
$commande "
UPDATE "$nomschema"."$nomtable" as t1 SET surf_batidur = (
	SELECT sum(ST_Area(t2.geom))
	FROM "$batiorigine" as t2
	WHERE 	t1.code_dep = UPPER('"$dpt"')
		and (t2.leger='Non' or t2.leger='non')
		and ST_Intersects(t1.geom, t2.geom));
"
$commande "
CREATE INDEX "$nomtable"_surf_batidur_idx ON "$nomschema"."$nomtable"
	USING brin (surf_batidur);
"

echo "A.1.6] Peuplement du champs surf_batileg + index"
$commande "	
UPDATE "$nomschema"."$nomtable" as t1 SET surf_batileg = (
	SELECT sum(ST_Area(t2.geom))
	FROM "$batiorigine" as t2
	WHERE 	t1.code_dep = UPPER('"$dpt"')
		and (t2.leger='Oui' or t2.leger='oui')
		and ST_Intersects(t1.geom, t2.geom));	
"
$commande "
CREATE INDEX "$nomtable"_surf_batileg_idx ON "$nomschema"."$nomtable"
	USING brin (surf_batileg);
"		

echo "A.1.7] Commentaires"
$commande "
COMMENT ON TABLE "$nomschema"."$nomtable"
  IS 'Enveloppe du bati au 1er janvier "$millesime" pour le département ''"$dpt"''.

Il s’agit de fusionner tous les bâtiments issu du cadastre "$trigramme" par une dilatation de 50 m, puis une fusion et ensuite une érosion de 40 m';

COMMENT ON COLUMN "$nomschema"."$nomtable".id
    IS 'Identifiant unique selon méthode COVADIS : n°département.000.000 + id.';

COMMENT ON COLUMN "$nomschema"."$nomtable".nb_batidur
    IS 'Nombre de bâtiments qualifiés de dur par la DGFiP intégrés dans cette enveloppe du bâti.';

COMMENT ON COLUMN "$nomschema"."$nomtable".nb_batileg
    IS 'Nombre de bâtiments qualifiés de legers par la DGFiP intégrés dans cette enveloppe du bâti.';

COMMENT ON COLUMN "$nomschema"."$nomtable".surf_batidur
    IS 'Surface totale en m2 de tous les bâtiments qualifiés de dur par la DGFiP et présents dans cette enveloppe du bâti.';

COMMENT ON COLUMN "$nomschema"."$nomtable".surf_batileg
   IS 'Surface totale en m2 de tous les bâtiments qualifiés de leger par la DGFiP et présents dans cette enveloppe du bâti.';	

COMMENT ON COLUMN "$nomschema"."$nomtable".code_dep
    IS 'Numéro INSEE du département sur 3 charactères.';

COMMENT ON COLUMN "$nomschema"."$nomtable".geom
    IS 'Champs géométrique : Monopolygones en Lambert93 (EPSG:2154).';
"

done

---- Execution du script:
-- time sh /home/administrateur/00_gitlab/prod/enveloppe_urbaine/2022_etalab/A2_l_enveloppe_bati_etalab_ddd_2022.sh 
/*
real	29183m47,638s
user	1m33,196s
sys		0m16,404s
*/

-----------------------------------------------------------
---- A.2] Travail sur la table nationale Enveloppe Urbaine
-----------------------------------------------------------
#!/bin/bash
# "A.2] Travail sur la table nationale Enveloppe Urbaine
export PGPASSWORD='tunelesaurapas!'
commande="psql -h 192.168.1.50 -p 5432 -d ceremabase -c "
millesime="2021"
schematravail="tache_urbaine_"$millesime

############
# Option 1.1 : Depuis le parcellaire express ==> pepci
trigramme="pepci"
# Option 1.2 : Depuis le cadastre de data.gouv.fr ==> etalab
#trigramme="etalab"
# Fin Option 1
############

# Important :
# Pour tout logger taper :
# time sh /home/administrateur/00_gitlab/prod/enveloppe_urbaine/2021_pepci/A2_l_enveloppe_bati_pepci_000_2021.sh 1>/mnt/travaux/prod_enveloppe_bati/add_enveloppe_bati_pepci_2021/80_log/A2_l_enveloppe_bati_pepci_000_2021.log 2>&1

echo "A.2.1] Création de la table temporaire d addition des départements"
now=$(date +"%T")
echo "Début à $now"
echo "-------------------------------------------------------------------"
$commande "
DROP TABLE IF EXISTS "$schematravail".temp_enveloppe_bati_"$trigramme"_000_"$millesime";
"
$commande "
CREATE TABLE "$schematravail".temp_enveloppe_bati_"$trigramme"_000_"$millesime"
(
	code_dep character(3),
	geom geometry(Polygon,2154)
);
"

echo "A.2.2] Ajout des départements dans la table nationale temporaire"
#DEBUG
#for dpt in '001' '038' '069'
for dpt in '001' '002' '003' '004' '005' '006' '007' '008' '009' '010' '011' '012' '013' '014' '015' '016' '017' '018' '019' '02b' '02a' '021' '022' '023' '024' '025' '026' '027' '028' '029' '030' '031' '032' '033' '034' '035' '036' '037' '038' '039' '040' '041' '042' '043' '044' '045' '046' '047' '048' '049' '050' '051' '052' '053' '054' '055' '056' '057' '058' '059' '060' '061' '062' '063' '064' '065' '066' '067' '068' '069' '070' '071' '072' '073' '074' '075' '076' '077' '078' '079' '080' '081' '082' '083' '084' '085' '086' '087' '088' '089' '090' '091' '092' '093' '094' '095'

do

echo "Département $dpt"
$commande "
INSERT INTO "$schematravail".temp_enveloppe_bati_"$trigramme"_000_"$millesime" (code_dep, geom)
	SELECT code_dep, geom
	FROM "$schematravail".l_enveloppe_bati_"$trigramme"_"$dpt"_"$millesime";
"

done

echo "A.2.3] Préparation de la phase A.2.4"
now=$(date +"%T")
echo "Début à $now"
echo "-------------------------------------"
echo "A.2.3.1] Index Géomérique"

$commande "
CREATE INDEX temp_enveloppe_bati_"$trigramme"_000_"$millesime"_geom_gist ON "$schematravail".temp_enveloppe_bati_"$trigramme"_000_"$millesime"
	USING gist (geom) TABLESPACE index;
"
echo "A.2.3.2] Identifiant pour la phase A.3"
$commande "
ALTER TABLE "$schematravail".temp_enveloppe_bati_"$trigramme"_000_"$millesime"
	ADD COLUMN id serial;
"
echo "A.2.3.3] Sauvegarde en cas d'echec de la A.3"
$commande "
DROP TABLE IF EXISTS "$schematravail".sauve_temp_enveloppe_bati_"$trigramme"_000_"$millesime";
CREATE TABLE "$schematravail".sauve_temp_enveloppe_bati_"$trigramme"_000_"$millesime" AS
	SELECT * FROM "$schematravail".temp_enveloppe_bati_"$trigramme"_000_"$millesime";
"


echo "A.2.4] Création de la table nationale"
now=$(date +"%T")
echo "Début à $now"
echo "------------------------------------"
$commande "
DROP TABLE IF EXISTS "$schematravail".l_enveloppe_bati_"$trigramme"_000_"$millesime";
CREATE TABLE "$schematravail".l_enveloppe_bati_"$trigramme"_000_"$millesime"
(
    id_enveloppe serial,
	nb_batidur int4,
	nb_batileg int4,
	surf_batidur int4,
	surf_batileg int4,
	list_code_dep varchar,
    geom geometry(Polygon,2154)
);
"

echo "A.2.4.1] Sélection des enveloppes qui s'intersectent"
now=$(date +"%T")
echo "Début à $now"
echo "------------------------------------"
$commande "
DROP TABLE IF EXISTS "$schematravail".stintersects_enveloppe_bati_"$trigramme"_000_"$millesime";
CREATE TABLE "$schematravail".stintersects_enveloppe_bati_"$trigramme"_000_"$millesime" AS
	SELECT ST_Multi(ST_Union(t1.geom,t2.geom))::geometry(MULTIPOLYGON,2154) as geometrie_union
		FROM "$schematravail".temp_enveloppe_bati_"$trigramme"_000_"$millesime" as t1
		JOIN "$schematravail".temp_enveloppe_bati_"$trigramme"_000_"$millesime" as t2
		ON ST_Intersects(t1.geom, t2.geom)
		WHERE t1.id < t2.id;
-->
" 
$commande "
CREATE INDEX stintersects_enveloppe_bati_"$trigramme"_000_"$millesime"_geom_gist
	ON "$schematravail".stintersects_enveloppe_bati_"$trigramme"_000_"$millesime"
	USING gist (geometrie_union) TABLESPACE index;
"

echo "A.2.4.2] Peuplement par agregation des zones de chevauchement interdépartementales"
now=$(date +"%T")
echo "Début à $now"
echo "------------------------------------"
$commande "
INSERT INTO "$schematravail".l_enveloppe_bati_"$trigramme"_000_"$millesime" (geom)
SELECT (ST_Dump(ST_Union(geometrie_union))).geom::geometry(Polygon,2154) as geom
		FROM "$schematravail".stintersects_enveloppe_bati_"$trigramme"_000_"$millesime";
--> INSERT 0 25040 / Requête exécutée avec succès en 43 hr 58 min.
"

echo "A.2.4.3] Suppression des zones de chevauchement"
echo "         interdépartementales que l'on vient d'inserer"
now=$(date +"%T")
echo "Début à $now"
echo "---------------------------------------------------------------------"
$commande "
DELETE FROM "$schematravail".temp_enveloppe_bati_"$trigramme"_000_"$millesime" WHERE id IN (
		(SELECT t1.id
		FROM "$schematravail".temp_enveloppe_bati_"$trigramme"_000_"$millesime" as t1
		JOIN "$schematravail".temp_enveloppe_bati_"$trigramme"_000_"$millesime" as t2
		ON ST_Intersects(t1.geom, t2.geom)
		WHERE t1.id < t2.id)
	UNION ALL
		(SELECT t2.id
		FROM "$schematravail".temp_enveloppe_bati_"$trigramme"_000_"$millesime" as t1
		JOIN "$schematravail".temp_enveloppe_bati_"$trigramme"_000_"$millesime" as t2
		ON ST_Intersects(t1.geom, t2.geom)
		WHERE t1.id < t2.id)
);
--> 
"

echo "---------------------------------------------------------"
echo "A.2.4.4] Insersion du reste : ce qui ne se supperpose pas :"
echo "         On vérifie qu'il n'y a rien qui s'intersecte"
echo "---------------------------------------------------------"
$commande "
CREATE INDEX l_enveloppe_bati_"$trigramme"_000_"$millesime"_geom_gist
	ON "$schematravail".l_enveloppe_bati_"$trigramme"_000_"$millesime"
	USING gist (geom) TABLESPACE index;

--SELECT *
--FROM "$schematravail".temp_enveloppe_bati_"$trigramme"_000_"$millesime" as t1
--JOIN "$schematravail".l_enveloppe_bati_"$trigramme"_000_"$millesime" as t2
--ON ST_Intersects(t1.geom, t2.geom);
-->
"
echo "---------------------------------------------------------"
$commande "
DROP INDEX "$schematravail".l_enveloppe_bati_"$trigramme"_000_"$millesime"_geom_gist;
"

echo "A.2.4.5] On insere !"
$commande "
INSERT INTO "$schematravail".l_enveloppe_bati_"$trigramme"_000_"$millesime" (geom)
	SELECT geom::geometry(Polygon,2154) as geom
	FROM "$schematravail".temp_enveloppe_bati_"$trigramme"_000_"$millesime";
--> INSERT 0 1535920 / Requête exécutée avec succès en 2 min 29 secs.
"

echo "A.2.4.6] Index Géomérique + Cluster"
$commande "
CREATE INDEX l_enveloppe_bati_"$trigramme"_000_"$millesime"_geom_gist ON "$schematravail".l_enveloppe_bati_"$trigramme"_000_"$millesime"
	USING gist (geom) TABLESPACE index;
ALTER TABLE "$schematravail".l_enveloppe_bati_"$trigramme"_000_"$millesime"
	CLUSTER ON l_enveloppe_bati_"$trigramme"_000_"$millesime"_geom_gist;
"
$commande "
CREATE INDEX sauve_temp_enveloppe_bati_"$trigramme"_000_"$millesime"_geom_gist ON "$schematravail".sauve_temp_enveloppe_bati_"$trigramme"_000_"$millesime"
	USING gist (geom) TABLESPACE index;
ALTER TABLE "$schematravail".sauve_temp_enveloppe_bati_"$trigramme"_000_"$millesime"
	CLUSTER ON sauve_temp_enveloppe_bati_"$trigramme"_000_"$millesime"_geom_gist;
"
#DEBUG : Pas fait sur le referentiel cadastre ETALAB 2020 ! 
#$commande "
#CREATE INDEX n_bati_"$trigramme"_000_"$millesime"_type_idx
#    ON r_cadastre_"$trigramme"_"$millesime".n_bati_"$trigramme"_000_"$millesime" USING brin
#    (type)
#    TABLESPACE index;
#"

echo "A.2.5] Mise à jour des champs"
echo "A.2.5.1] Mise à jour des champs nb_batidur"
now=$(date +"%T")
echo "Début à $now"
echo "------------------------------------"
$commande "
UPDATE "$schematravail".l_enveloppe_bati_"$trigramme"_000_"$millesime" as t1 SET nb_batidur = (
	SELECT count(*)
	FROM r_parcellaire_express_"$millesime".n_batiment_"$trigramme"_000_2021 as t2
	WHERE	ST_Intersects(t1.geom, t2.geom)
	AND (t2.type='Bâtiment en dur' or t2.type='Bâti dur' or t2.type='Bati dur' or t2.type='01')
	);
--> 
"

echo "A.2.5.2] Mise à jour des champs nb_batileg"
now=$(date +"%T")
echo "Début à $now"
echo "------------------------------------"
$commande "
UPDATE "$schematravail".l_enveloppe_bati_"$trigramme"_000_"$millesime" as t1 SET nb_batileg = (
	SELECT count(*)
	FROM r_parcellaire_express_"$millesime".n_batiment_"$trigramme"_000_2021 as t2
	WHERE	ST_Intersects(t1.geom, t2.geom)
	AND	(t2.type='Construction légère' or t2.type='Bâti léger' or t2.type='Bati leger' or t2.type='02')
	);
--> Requête exécutée avec succès en 2 min 8 secs.
"
echo "A.2.5.3] Mise à jour des champs surf_batidur"
now=$(date +"%T")
echo "Début à $now"
echo "------------------------------------"
$commande "
UPDATE tache_urbaine_2021.l_enveloppe_bati_"$trigramme"_000_2021 as t1 SET surf_batidur = (
	SELECT sum(ST_Area(t2.geom))
	FROM r_parcellaire_express_"$millesime".n_batiment_"$trigramme"_000_2021 as t2
	WHERE 	
		ST_Intersects(t1.geom, t2.geom)
		AND 
		(t2.type='01' or t2.type='Bâtiment en dur' or t2.type='Bâti dur' or t2.type='Bati dur')
	);
"
echo "A.2.5.4] Mise à jour des champs surf_batileg"
now=$(date +"%T")
echo "Début à $now"
echo "------------------------------------"
$commande "
UPDATE tache_urbaine_2021.l_enveloppe_bati_"$trigramme"_000_2021 as t1 SET surf_batileg = (
	SELECT sum(ST_Area(t2.geom))
	FROM r_parcellaire_express_"$millesime".n_batiment_"$trigramme"_000_2021 as t2
	WHERE 	
		ST_Intersects(t1.geom, t2.geom)
		AND
		(t2.type='02' or t2.type='Construction légère' or t2.type='Bâti léger' or t2.type='Bati leger')
	);	
"
echo "A.2.5.5] Mise à jour du champs list_code_dep"
now=$(date +"%T")
echo "Début à $now"
echo "------------------------------------"
#DEBUG : si plusieurs taches fusionnées d une même département on a un doublon
#$commande "
#UPDATE "$schematravail".l_enveloppe_bati_"$trigramme"_000_"$millesime" as t1 SET list_code_dep = (
#	SELECT array_agg(code_dep)::varchar
#	FROM "$schematravail".sauve_temp_enveloppe_bati_"$trigramme"_000_"$millesime" as t2
#	WHERE	ST_Intersects(t1.geom, t2.geom)
#	);
#--> UPDATE 38360 / Requête exécutée avec succès en 1 min 25 secs.
#"

#DEBUG : Enleve les doublons de départements, mais on perds l information de la fusion de plusieurs tache dans le même département
$commande "
UPDATE "$schematravail".l_enveloppe_bati_"$trigramme"_000_"$millesime" as t1 SET list_code_dep = (
	WITH resultat1 AS 
		(SELECT DISTINCT t2.code_dep
		FROM "$schematravail".sauve_temp_enveloppe_bati_"$trigramme"_000_"$millesime" as t2
		WHERE ST_Intersects(t1.geom, t2.geom)
		ORDER BY t2.code_dep)
	SELECT array_agg(code_dep)::varchar FROM resultat1
	);
--> 
"

echo "A.2.6] Optimisations"
now=$(date +"%T")
echo "Début à $now"
echo "------------------------------------"
echo "A.2.6.1] Clé primaire"
$commande "
ALTER TABLE "$schematravail".l_enveloppe_bati_"$trigramme"_000_"$millesime"
    ADD CONSTRAINT l_enveloppe_bati_"$trigramme"_000_"$millesime"_id_enveloppe_pk PRIMARY KEY (id_enveloppe)
    USING INDEX TABLESPACE index;
ALTER TABLE "$schematravail".l_enveloppe_bati_"$trigramme"_000_"$millesime"
    ADD CONSTRAINT enforce_dims_geom CHECK (ST_ndims(geom) = 2);
ALTER TABLE "$schematravail".l_enveloppe_bati_"$trigramme"_000_"$millesime"
	ADD CONSTRAINT enforce_srid_geom CHECK (ST_Srid(geom)= 2154);
ALTER TABLE "$schematravail".l_enveloppe_bati_"$trigramme"_000_"$millesime"
    ADD CONSTRAINT enforce_geotype_geom CHECK (geometrytype(geom) = 'POLYGON'::text);
"
echo "A.2.6.2] Index attributaires"
$commande "
CREATE INDEX l_enveloppe_bati_"$trigramme"_000_"$millesime"_nb_batidur_idx ON "$schematravail".l_enveloppe_bati_"$trigramme"_000_"$millesime"
	USING brin (nbatidur) TABLESPACE index;
CREATE INDEX l_enveloppe_bati_"$trigramme"_000_"$millesime"_nb_batileg_idx ON "$schematravail".l_enveloppe_bati_"$trigramme"_000_"$millesime"
	USING brin (nbatileg) TABLESPACE index;
CREATE INDEX l_enveloppe_bati_"$trigramme"_000_"$millesime"_surf_batidur_idx ON "$schematravail".l_enveloppe_bati_"$trigramme"_000_"$millesime"
	USING brin (surf_batidur) TABLESPACE index;
CREATE INDEX l_enveloppe_bati_"$trigramme"_000_"$millesime"_surf_batileg_idx ON "$schematravail".l_enveloppe_bati_"$trigramme"_000_"$millesime"
	USING brin (surf_batileg) TABLESPACE index;
CREATE INDEX l_enveloppe_bati_"$trigramme"_000_"$millesime"_list_code_dep_idx ON "$schematravail".l_enveloppe_bati_"$trigramme"_000_"$millesime"
	USING brin (list_code_dep) TABLESPACE index;
"

echo "A.2.6.3] Index Géomérique + Cluster"
#DEBUG Déjà fait en partie # B.2.1]

echo "A.2.6.4] Commentaires"
$commande "
COMMENT ON TABLE "$schematravail".l_enveloppe_bati_"$trigramme"_000_"$millesime"
  IS 'Enveloppe nationale du bâti de 2020.

Il s’agit de fusionner tous les bâtiments issu du cadastre ETALAB qui sont séparés de moins de 50 m par un Buffer +50, ST_Union, Erosion de 40m';
"
$commande "
COMMENT ON COLUMN "$schematravail".l_enveloppe_bati_"$trigramme"_000_"$millesime".id_enveloppe
    IS 'Identifiant national unique pour une même enveloppe du bati';
COMMENT ON COLUMN "$schematravail".l_enveloppe_bati_"$trigramme"_000_"$millesime".nb_batidur
    IS 'Nombre de bâtiments qualifiés de dur par la DGFiP intégrés dans cette enveloppe du bati.';
COMMENT ON COLUMN "$schematravail".l_enveloppe_bati_"$trigramme"_000_"$millesime".nb_batileg
    IS 'Nombre de bâtiments qualifiés de legers par la DGFiP intégrés dans cette enveloppe du bati.';
COMMENT ON COLUMN tache_urbaine_2021.l_enveloppe_bati_"$trigramme"_000_"$millesime".surf_batidur
    IS 'Surface totale en m2 de tous les bâtiments qualifiés de dur par la DGFiP et présents dans cette enveloppe du bâti.';
COMMENT ON COLUMN tache_urbaine_2021.l_enveloppe_bati_"$trigramme"_000_"$millesime".surf_batileg
    IS 'Surface totale en m2 de tous les bâtiments qualifiés de leger par la DGFiP et présents dans cette enveloppe du bâti.';	
COMMENT ON COLUMN "$schematravail".l_enveloppe_bati_"$trigramme"_000_"$millesime".list_code_dep
    IS 'Liste des départements à l³origine de cette enveloppe du bati.';	
COMMENT ON COLUMN "$schematravail".l_enveloppe_bati_"$trigramme"_000_"$millesime".geom
    IS 'Champs géométrique : Monopolygone en Lambert93 (EPSG:2154).';
"

echo "A.2.7] Netoyage des tables temporaires"
now=$(date +"%T")
echo "Début à $now"
echo "------------------------------------"
$commande "
DROP TABLE IF EXISTS "$schematravail".temp_departement_buffer_200m_000_"$millesime";
----#DEBUG : on garde pour vérifier 
--DROP TABLE IF EXISTS "$schematravail".temp_enveloppe_bati_"$trigramme"_000_"$millesime";
DROP TABLE IF EXISTS "$schematravail".stintersects_enveloppe_bati_"$trigramme"_000_"$millesime";
"
---- Execution :
-- time sh /home/administrateur/00_gitlab/prod/enveloppe_urbaine/2021_pepci/A2_l_enveloppe_bati_pepci_000_2021.sh 1>/mnt/travaux/prod_enveloppe_bati/add_enveloppe_bati_pepci_2021/80_log/A2_l_enveloppe_bati_pepci_000_2021.log 2>&1
/*
Erreur : Geometry type (GeometryCollection) does not match column type (MultiPolygon)
 */
---- Test
SELECT *
FROM p_enveloppe_bati_2022.temp_enveloppe_bati_bdtopo_000_2022
WHERE st_geometrytype(geom) != 'ST_Polygon';
/*
code_dep|geom|id|
--------+----+--+
*/

DROP TABLE IF EXISTS p_enveloppe_bati_2022.stintersects_enveloppe_bati_bdtopo_000_2022;
CREATE TABLE p_enveloppe_bati_2022.stintersects_enveloppe_bati_bdtopo_000_2022 AS
	SELECT ST_Multi(ST_CollectionExtract(ST_Union(t1.geom,t2.geom),3))::geometry(MULTIPOLYGON,2154) as geometrie_union
		FROM p_enveloppe_bati_2022.temp_enveloppe_bati_bdtopo_000_2022 as t1
		JOIN p_enveloppe_bati_2022.temp_enveloppe_bati_bdtopo_000_2022 as t2
		ON ST_Intersects(t1.geom, t2.geom)
		WHERE t1.id < t2.id;
--> Updated Rows	28633

---- Création de la table batiment en lambert93
DROP TABLE IF EXISTS p_enveloppe_bati_2022.n_batiment_bdt_000_2022;
CREATE TABLE p_enveloppe_bati_2022.n_batiment_bdt_000_2022 AS
SELECT construction_legere, ST_Transform(geom,2154)::geometry(MULTIPOLYGON,2154) AS geom
FROM  r_bdtopo_2022.n_batiment_bdt_fra_2022;
---- Index
CREATE INDEX n_batiment_bdt_000_2022_geom_gist ON p_enveloppe_bati_2022.n_batiment_bdt_000_2022 USING gist(geom);
CREATE INDEX n_batiment_bdt_000_2022_construction_legere_idx ON p_enveloppe_bati_2022.n_batiment_bdt_000_2022 USING btree(construction_legere);


------------------------------------------------------------
---- A.3] Synthèse
------------------------------------------------------------
---- 08/05/2021
---- https://gitlab.cerema.fr/centre-est/admin_ceremabase/-/blob/master/fonctions/synthese_geometrie_schema.sql
DROP TABLE IF EXISTS p_enveloppe_bati_2022.synthese_geometrie_schema;
CREATE table p_enveloppe_bati_2022.synthese_geometrie_schema AS
SELECT * FROM w_fonctions.synthese_geometrie_schema('p_enveloppe_bati_2022');
--> Updated Rows	209

/*
# A.3] Sauvegarde
# A.3.1] Sauvegarde du schéma complet
---- Sauvegarde :
time pg_dump --port=5432 --host=192.168.1.50 --dbname="ceremabase" --schema="tache_urbaine_2021" --username "postgres" --role "postgres" --format=plain --encoding UTF8 --inserts --column-inserts --verbose --quote-all-identifiers --file="/mnt/data/sauve_tache_urbaine_2021.sql"
#
#
#

# A.3.2] Sauvegarde de la table finale
#time pg_dump --port=5432 --host=192.168.1.50 --dbname="ceremabase" --table="tache_urbaine_2021.l_enveloppe_bati_pepci_000_2021" --username "postgres" --role "postgres" --format=plain --encoding UTF8 --inserts --column-inserts --verbose --quote-all-identifiers --file="/mnt/data/sauve_tache_urbaine_2021.l_enveloppe_bati_pepci_000_2021.sql"
#real	11m14,760s
#user	0m42,762s
#sys	0m39,493s

# A.3.2] Compression optimisée pour le texte
time 7z a -t7z '/mnt/data/sauve_tache_urbaine_2021.7z' '/mnt/data/sauve_tache_urbaine_2021.sql'  -mmt2 -m0=PPMd -v1024m
real	85m7,176s
user	83m29,647s
sys	0m33,626s


time 7z a -t7z '/mnt/data/sauve_tache_urbaine_2021.l_enveloppe_bati_pepci_000_2021.7z' '/mnt/data/sauve_tache_urbaine_2021.l_enveloppe_bati_pepci_000_2021.sql'  -mmt2 -m0=PPMd -v1024m
#real	20m11,348s
#user	19m47,178s
#sys	0m6,026s

