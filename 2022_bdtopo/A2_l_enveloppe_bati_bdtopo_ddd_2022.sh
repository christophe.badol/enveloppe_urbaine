#!/bin/bash
# 19/06/2022
echo "-----------------------------------------------------"
echo "A.2] Travail sur la table nationale Enveloppe Urbaine"
echo "-----------------------------------------------------"
# Paramètres
export PGPASSWORD='tunelesaurapas!'
commande="psql -h hp-geomatique -p 5432 -U postgres -d ceremabase -c "
millesime="2022"

##############################################################
# Option 1.1 : Depuis le parcellaire express ==> pepci
#trigramme="pepci"
# Option 1.2 : Depuis le cadastre de data.gouv.fr ==> etalab
#trigramme="etalab"
# Option 1.3 : Depuis le bati de la bdtopo v3 ==> bdtopo
trigramme="bdtopo"
# Fin Option 1
##############################################################

cadastre="bdtopo"
schemacadastre="r_"$cadastre"_"$millesime
nomschema="p_enveloppe_bati_"$millesime


# Important :
# Pour tout logger taper :
# su postgres
# time sh /home/administrateur/00_gitlab/prod/enveloppe_urbaine/2022_bdtopo/A2_l_enveloppe_bati_bdtopo_ddd_2022.sh 1>/home/administrateur/00_gitlab/prod/enveloppe_urbaine/2022_bdtopo/098_log/A2_enveloppe_bati_etalab_ddd_2021.log 2>&1

$commande "
--DROP SCHEMA IF EXISTS "$nomschema";
CREATE SCHEMA "$nomschema"
"

$commande "
/*
CREATE TABLE "$schematravail".temp_departement_buffer_200m_000_"$millesime" AS
	SELECT '0' || insee_dep AS code_dep, ST_Multi(ST_Buffer(ST_Union(ST_Transform(geom,2154)),200))::geometry(MultiPolygon,2154) as geom
	FROM r_admin_express.n_adm_exp_cog_departement_000
	GROUP BY insee_dep;

CREATE INDEX temp_departement_buffer_200m_000_"$millesime"_code_dep_idx
    ON "$schematravail".temp_departement_buffer_200m_000_"$millesime" USING brin
    (code_dep)
    TABLESPACE index;

CREATE INDEX temp_departement_buffer_200m_000_"$millesime"_geom_gist
    ON "$schematravail".temp_departement_buffer_200m_000_"$millesime" USING gist
    (geom)
    TABLESPACE index;

ALTER TABLE "$schematravail".temp_departement_buffer_200m_000_"$millesime"
    CLUSTER ON temp_departement_buffer_200m_000_"$millesime"_geom_gist;
*/
"

#DEBUG
#for dpt in '090' '02a' '02b'
for dpt in '001' '002' '003' '004' '005' '006' '007' '008' '009' '010' '011' '012' '013' '014' '015' '016' '017' '018' '019' '02b' '02a' '021' '022' '023' '024' '025' '026' '027' '028' '029' '030' '031' '032' '033' '034' '035' '036' '037' '038' '039' '040' '041' '042' '043' '044' '045' '046' '047' '048' '049' '050' '051' '052' '053' '054' '055' '056' '057' '058' '059' '060' '061' '062' '063' '064' '065' '066' '067' '068' '069' '070' '071' '072' '073' '074' '075' '076' '077' '078' '079' '080' '081' '082' '083' '084' '085' '086' '087' '088' '089' '090' '091' '092' '093' '094' '095'
do
# Obligation du cadastre meme si le bati agrégé n a pas été intégré :
batiorigine=$schemacadastre".n_batiment_bdt_"$dpt"_"$millesime 
nomtable="l_enveloppe_bati_"$trigramme"_"$dpt"_"$millesime	
racinedepartement="r_admin_express.n_adm_exp_cog_departement_000"
now=$(date +"%T")
echo "--------------------"
echo "Début à $now"
echo "Département $dpt"
echo "--------------------"
echo "A.1.1] Création de la table à partir d’une dilatation de 50 m / fusion / érosion de 40 m"
$commande "
DROP TABLE IF EXISTS "$nomschema"."$nomtable";
CREATE TABLE "$nomschema"."$nomtable"
(
	id serial,
	nb_batidur integer,
	nb_batileg integer,
	surf_batidur integer,
	surf_batileg integer,
	code_dep character(3) DEFAULT '"$dpt"',  
	geom geometry(Polygon,2154)
);
"
#DEBUG
# Obligation de séparer ces trois commandes car si 2a 2b l’ALTER SEQUENCE echoue et annule les 4 commandes
$commande "
ALTER SEQUENCE "$nomschema"."$nomtable"_id_seq
	RESTART WITH "$dpt"000001;
"
#DEBUG
# Obligation de séparer ces trois commandes car si 2a 2b l’ALTER SEQUENCE echoue et annule les 4 commandes
$commande "
---- Version qui neccessite de générer une table départementale temporaire, mais beaucoup plus rapide
INSERT INTO "$nomschema"."$nomtable" (geom)
	SELECT (ST_Dump(ST_Buffer(ST_Union(ST_Buffer(t1.geom,50)),-40))).geom::geometry(Polygon,2154) as geom
	FROM "$batiorigine" as t1
	JOIN "$nomschema".temp_departement_buffer_200m_000_"$millesime" as t2
	ON ST_Intersects (t1.geom, t2.geom)
	WHERE t2.code_dep = upper('"$dpt"');

/*
---- Version qui tient compte des départements autour mais longue car le buffer et généré à chaque fois sans index
INSERT INTO "$nomschema"."$nomtable" (geom)
	SELECT (ST_Dump(ST_Buffer(ST_Union(ST_Buffer(t1.geom,50)),-40))).geom::geometry(Polygon,2154) as geom
	FROM "$batiorigine" as t1
	JOIN "$racinedepartement" as t2
	ON ST_Intersects (t1.geom,ST_Buffer(t2.geom,100))
	WHERE '0' || t2.insee_dep = '"$dpt"';
	
---- version 1 qui ne tient pas compte des batiments à coté mais hors département
INSERT INTO "$nomschema"."$nomtable" (geom)
	SELECT (ST_Dump(ST_Buffer(ST_Union(ST_Buffer(t1.geom,50)),-40))).geom::geometry(Polygon,2154) as geom
	FROM "$batiorigine" as t1;
*/
"
echo "A.1.2] Clé + Index + Cluster + Optimisation"
$commande "
ALTER TABLE "$nomschema"."$nomtable"
    ADD CONSTRAINT "$nomtable"_pk PRIMARY KEY (id)
    USING INDEX TABLESPACE index;

ALTER TABLE "$nomschema"."$nomtable"
    ADD CONSTRAINT enforce_dims_geom CHECK (ST_ndims(geom) = 2);

ALTER TABLE "$nomschema"."$nomtable"
	ADD CONSTRAINT enforce_srid_geom CHECK (ST_Srid(geom)= 2154);

ALTER TABLE "$nomschema"."$nomtable"
    ADD CONSTRAINT enforce_geotype_geom CHECK (geometrytype(geom) = 'POLYGON'::text);

ALTER TABLE "$nomschema"."$nomtable"
	ADD CONSTRAINT enforce_no_self_intersection_geom CHECK (st_issimple(geom));

ALTER TABLE "$nomschema"."$nomtable"
	ADD CONSTRAINT enforce_no_zero_area_geom CHECK ((st_area(geom) > (0)::double precision));

ALTER TABLE "$nomschema"."$nomtable"
	ADD CONSTRAINT enforce_validite_geom CHECK (st_isvalid(geom));

CREATE INDEX "$nomtable"_code_dep_idx ON "$nomschema"."$nomtable"
	USING brin (code_dep) TABLESPACE index;

CREATE INDEX "$nomtable"_geom ON "$nomschema"."$nomtable"
	USING gist (geom) TABLESPACE index;
ALTER TABLE "$nomschema"."$nomtable" CLUSTER ON "$nomtable"_geom;
"

echo "A.1.3] Peuplement du champs nb_batidur + index"
$commande "
UPDATE "$nomschema"."$nomtable" as t1 SET nb_batidur = (
	SELECT count(*)
	FROM "$batiorigine" as t2
	WHERE 	t1.code_dep = UPPER('"$dpt"')
		and (t2.leger='Non' or t2.leger='non')
		and ST_Intersects(t1.geom, t2.geom));

"
$commande "
CREATE INDEX "$nomtable"_nb_batidur_idx ON "$nomschema"."$nomtable"
	USING brin (nb_batidur) TABLESPACE index;
"

echo "A.1.4] Peuplement du champs nb_batileg + index"
$commande "
UPDATE "$nomschema"."$nomtable" as t1 SET nb_batileg = (
	SELECT count(*)
	FROM "$batiorigine" as t2
	WHERE 	t1.code_dep = UPPER('"$dpt"')
		and (t2.leger='Oui' or t2.leger='oui')
		and ST_Intersects(t1.geom, t2.geom));
"
$commande "
CREATE INDEX "$nomtable"_nb_batileg_idx ON "$nomschema"."$nomtable"
	USING brin (nb_batileg) TABLESPACE index;
"

echo "A.1.5] Peuplement du champs surf_batidur + index"
$commande "
UPDATE "$nomschema"."$nomtable" as t1 SET surf_batidur = (
	SELECT sum(ST_Area(t2.geom))
	FROM "$batiorigine" as t2
	WHERE 	t1.code_dep = UPPER('"$dpt"')
		and (t2.leger='Non' or t2.leger='non')
		and ST_Intersects(t1.geom, t2.geom));
"
$commande "
CREATE INDEX "$nomtable"_surf_batidur_idx ON "$nomschema"."$nomtable"
	USING brin (surf_batidur);
"

echo "A.1.6] Peuplement du champs surf_batileg + index"
$commande "	
UPDATE "$nomschema"."$nomtable" as t1 SET surf_batileg = (
	SELECT sum(ST_Area(t2.geom))
	FROM "$batiorigine" as t2
	WHERE 	t1.code_dep = UPPER('"$dpt"')
		and (t2.leger='Oui' or t2.leger='oui')
		and ST_Intersects(t1.geom, t2.geom));	
"
$commande "
CREATE INDEX "$nomtable"_surf_batileg_idx ON "$nomschema"."$nomtable"
	USING brin (surf_batileg);
"		

echo "A.1.7] Commentaires"
$commande "
COMMENT ON TABLE "$nomschema"."$nomtable"
  IS 'Enveloppe du bati au 1er janvier "$millesime" pour le département ''"$dpt"''.

Il s’agit de fusionner tous les bâtiments issu du cadastre "$trigramme" par une dilatation de 50 m, puis une fusion et ensuite une érosion de 40 m';

COMMENT ON COLUMN "$nomschema"."$nomtable".id
    IS 'Identifiant unique selon méthode COVADIS : n°département.000.000 + id.';

COMMENT ON COLUMN "$nomschema"."$nomtable".nb_batidur
    IS 'Nombre de bâtiments qualifiés de dur par la DGFiP intégrés dans cette enveloppe du bâti.';

COMMENT ON COLUMN "$nomschema"."$nomtable".nb_batileg
    IS 'Nombre de bâtiments qualifiés de legers par la DGFiP intégrés dans cette enveloppe du bâti.';

COMMENT ON COLUMN "$nomschema"."$nomtable".surf_batidur
    IS 'Surface totale en m2 de tous les bâtiments qualifiés de dur par la DGFiP et présents dans cette enveloppe du bâti.';

COMMENT ON COLUMN "$nomschema"."$nomtable".surf_batileg
   IS 'Surface totale en m2 de tous les bâtiments qualifiés de leger par la DGFiP et présents dans cette enveloppe du bâti.';	

COMMENT ON COLUMN "$nomschema"."$nomtable".code_dep
    IS 'Numéro INSEE du département sur 3 charactères.';

COMMENT ON COLUMN "$nomschema"."$nomtable".geom
    IS 'Champs géométrique : Monopolygones en Lambert93 (EPSG:2154).';
"

done
