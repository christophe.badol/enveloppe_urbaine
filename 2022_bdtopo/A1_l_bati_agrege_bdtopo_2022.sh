#!/bin/bash
# Bati Agrégé
# Version du 14 aout 2022
export PGPASSWORD='tunelesaurapas!'
commande="psql -h hp-geomatique -p 5432 -U postgres -d ceremabase -c "
millesime="2022"
cadastre="bdtopo"
couchebaticadastre="r_"$cadastre"_"$millesime".n_batiment_bdt"
nomschema="p_enveloppe_bati_"$millesime

#DEBUG
echo $couchebaticadastre

# Important :
# Pour tout logger taper :
# time sh /home/administrateur/00_gitlab/prod/enveloppe_urbaine/2022_bdtopo/A1_l_bati_agrege_bdtopo_2022.sh 1>/home/administrateur/1_l_bati_agrege_etalab_2022.log 2>&1

# A] Bati Agrege
#DEBUG
#for dpt in '091'
#for dpt in '02b' '02a'
for dpt in '001' '002' '003' '004' '005' '006' '007' '008' '009' '010' '011' '012' '013' '014' '015' '016' '017' '018' '019' '02a' '02b' '021' '022' '023' '024' '025' '026' '027' '028' '029' '030' '031' '032' '033' '034' '035' '036' '037' '038' '039' '040' '041' '042' '043' '044' '045' '046' '047' '048' '049' '050' '051' '052' '053' '054' '055' '056' '057' '058' '059' '060' '061' '062' '063' '064' '065' '066' '067' '068' '069' '070' '071' '072' '073' '074' '075' '076' '077' '078' '079' '080' '081' '082' '083' '084' '085' '086' '087' '088' '089' '090' '091' '092' '093' '094' '095' 

do

nomtable="l_bati_agrege_"$cadastre"_"$dpt"_"$millesime
echo '-------------------------------------------'
echo "Début à $now"
echo $nomtable
echo '-------------------------------------------'

# A.1] Création des tables Départementales
$commande "
DROP TABLE IF EXISTS "$nomschema"."$nomtable";
"
$commande "
CREATE TABLE "$nomschema"."$nomtable"
	(
	id serial,
	nb_batidur integer,
	nb_batileg integer,
	code_dep character(3) DEFAULT UPPER('"$dpt"'),  
	geom geometry(Polygon,2154)
	);
"

$commande "
ALTER SEQUENCE "$nomschema"."$nomtable"_id_seq
	RESTART WITH "$dpt"000001;
"

# A.2] Buffer +0,01, ST_Union, Buffer -0,01, ST_Dump
$commande "
INSERT INTO "$nomschema"."$nomtable" (geom)
	SELECT (ST_Dump(ST_Buffer(ST_Union(ST_Buffer(geom,0,01)),-0,01))).geom::geometry(Polygon,2154) as geom
	FROM "$couchebaticadastre"_"$dpt"_"$millesime";
"

# A.3] Index Géomérique + Cluster
$commande "
CREATE INDEX "$nomtable"_geom_gist ON "$nomschema"."$nomtable"
	USING gist (geom) TABLESPACE index;
"
$commande "
ALTER TABLE "$nomschema"."$nomtable"
	CLUSTER ON "$nomtable"_geom_gist;
"

# A.4] Mise à jour des champs nb_batidur & nb_batileg
$commande "
UPDATE "$nomschema"."$nomtable" as t1 SET nb_batidur = (
	SELECT count(*)
	FROM "$couchebaticadastre"_"$dpt"_"$millesime" as t2
	WHERE 	t1.code_dep = UPPER('"$dpt"')
		and (t2.leger='Non' or t2.leger='non')
		and ST_Intersects(t1.geom, t2.geom))
	WHERE 	t1.code_dep = UPPER('"$dpt"');
"

$commande "
UPDATE "$nomschema"."$nomtable" as t1 SET nb_batileg = (
	SELECT count(*)
	FROM "$couchebaticadastre"_"$dpt"_"$millesime" as t2
	WHERE 	t1.code_dep = UPPER('"$dpt"')
		and (t2.leger='Oui' or t2.leger='oui')
		and ST_Intersects(t1.geom, t2.geom))
	WHERE 	t1.code_dep = UPPER('"$dpt"');
"

# A.5] Optimisation 
# Clé primaire 
$commande "
ALTER TABLE "$nomschema"."$nomtable"
    ADD CONSTRAINT "$nomtable"_pk PRIMARY KEY (id)
    USING INDEX TABLESPACE index;
"
# Contraintes Géométriques
$commande "
ALTER TABLE "$nomschema"."$nomtable"
    ADD CONSTRAINT enforce_dims_geom CHECK (ST_ndims(geom) = 2);
"
$commande "
ALTER TABLE "$nomschema"."$nomtable"
	ADD CONSTRAINT enforce_srid_geom CHECK (ST_Srid(geom)= 2154);
"
$commande "
ALTER TABLE "$nomschema"."$nomtable"
    ADD CONSTRAINT enforce_geotype_geom CHECK (geometrytype(geom) = 'POLYGON'::text);
"
$commande "
ALTER TABLE "$nomschema"."$nomtable"
    ADD CONSTRAINT enforce_validite_geom CHECK (ST_IsValid(geom));
"
$commande "
ALTER TABLE "$nomschema"."$nomtable"
    ADD CONSTRAINT enforce_no_self_intersection_geom CHECK (ST_IsSimple(geom));
"
$commande "
ALTER TABLE "$nomschema"."$nomtable"
    ADD CONSTRAINT enforce_no_zero_area_geom CHECK (ST_Area(geom) > 0);
"

# Index attributaires
$commande "
CREATE INDEX "$nomtable"_nb_batidur_idx ON "$nomschema"."$nomtable"
	USING brin (nb_batidur) TABLESPACE index;
"
$commande "
CREATE INDEX "$nomtable"_nb_batileg_idx ON "$nomschema"."$nomtable"
	USING brin (nb_batileg) TABLESPACE index;
"
$commande "
CREATE INDEX "$nomtable"_code_dep_idx ON "$nomschema"."$nomtable"
	USING brin (code_dep) TABLESPACE index;
"

# A.6] Commentaires
# Table
$commande "
COMMENT ON TABLE "$nomschema"."$nomtable"
  IS 'Bâti agrégé de "$millesime" pour le département ''"$dpt"''.

Il s’agit de fusionner tous les bâtiments du cadastre "$cadastre" qui sont séparés de moins de 1 cm par un Buffer +0,01, ST_Union, Buffer -0,01, ST_Dump';
"
# Champs
$commande "
COMMENT ON COLUMN "$nomschema"."$nomtable".id
    IS 'Identifiant unique selon méthode COVADIS : n°département.000.000 + id.';
"
$commande "
COMMENT ON COLUMN "$nomschema"."$nomtable".nb_batidur
    IS 'Nombre de bâtiments qualifiés de dur par la DGFiP intégrés dans cette agrégation de bâtiments.';
"
$commande "
COMMENT ON COLUMN "$nomschema"."$nomtable".nb_batileg
    IS 'Nombre de bâtiments qualifiés de legers par la DGFiP intégrés dans cette agrégation de bâtiments.';
"
$commande "
COMMENT ON COLUMN "$nomschema"."$nomtable".code_dep
    IS 'Numéro INSEE du département sur 3 charactères.';
"
$commande "
COMMENT ON COLUMN "$nomschema"."$nomtable".geom
    IS 'Champs géométrique : Monopolygones en Lambert93 (EPSG:2154).';
"

done
