#!/bin/bash
# "A.3] Travail sur la table nationale Enveloppe Urbaine
echo "-----------------------------------------------------"
echo "A.3] Travail sur la table nationale Enveloppe Urbaine"
echo "-----------------------------------------------------"
export PGPASSWORD='tunelesaurapas!'
commande="psql -h hp-geomatique -p 5432 -U postgres -d ceremabase -c "
millesime="2022"
schematravail="p_enveloppe_bati_"$millesime

##############################################################
# Option 1.1 : Depuis le parcellaire express ==> pepci
#trigramme="pepci"
#batimentsmetropolitains="r_cadastre_"$trigramme"_"$millesime".n_batiments_"$trigramme"_000_"$millesime
# Option 1.2 : Depuis le cadastre de data.gouv.fr ==> etalab
trigramme="etalab"
batimentsmetropolitains="r_cadastre_"$trigramme"_"$millesime".n_batiments_"$trigramme"_000_"$millesime
# Option 1.3 : Depuis le bati de la bdtopo v3 ==> bdtopo
#trigramme="bdtopo"
#batimentsmetropolitains="r_"$trigramme"_"$millesime.n_bati_"$trigramme"_000_"$millesime
# Fin Option 1
##############################################################

# Important :
# Pour tout logger taper :
# time sh /home/administrateur/00_gitlab/prod/enveloppe_urbaine/2023_etalab/A3_l_enveloppe_bati_pepci_000_2023.sh

echo "A.3.1] Création de la table temporaire d addition des départements"
now=$(date +"%T")
echo "Début à $now"
echo "-------------------------------------------------------------------"
$commande "
DROP TABLE IF EXISTS "$schematravail".temp_enveloppe_bati_"$trigramme"_000_"$millesime";
"
$commande "
CREATE TABLE "$schematravail".temp_enveloppe_bati_"$trigramme"_000_"$millesime"
(
	code_dep character(3),
	geom geometry(Polygon,2154)
);
"

echo "A.3.2] Ajout des départements dans la table nationale temporaire"
#DEBUG
#for dpt in '001' '038' '069'
for dpt in '001' '002' '003' '004' '005' '006' '007' '008' '009' '010' '011' '012' '013' '014' '015' '016' '017' '018' '019' '02b' '02a' '021' '022' '023' '024' '025' '026' '027' '028' '029' '030' '031' '032' '033' '034' '035' '036' '037' '038' '039' '040' '041' '042' '043' '044' '045' '046' '047' '048' '049' '050' '051' '052' '053' '054' '055' '056' '057' '058' '059' '060' '061' '062' '063' '064' '065' '066' '067' '068' '069' '070' '071' '072' '073' '074' '075' '076' '077' '078' '079' '080' '081' '082' '083' '084' '085' '086' '087' '088' '089' '090' '091' '092' '093' '094' '095'

do

echo "Département $dpt"
$commande "
INSERT INTO "$schematravail".temp_enveloppe_bati_"$trigramme"_000_"$millesime" (code_dep, geom)
	SELECT code_dep, geom
	FROM "$schematravail".l_enveloppe_bati_"$trigramme"_"$dpt"_"$millesime";
"

done

echo "A.3.3] Préparation de la phase A.3.4"
now=$(date +"%T")
echo "Début à $now"
echo "-------------------------------------"
echo "A.3.3.1] Index Géomérique"

$commande "
CREATE INDEX temp_enveloppe_bati_"$trigramme"_000_"$millesime"_geom_gist ON "$schematravail".temp_enveloppe_bati_"$trigramme"_000_"$millesime"
	USING gist (geom) TABLESPACE index;
"
echo "A.3.3.2] Identifiant pour la phase A.3.4"
$commande "
ALTER TABLE "$schematravail".temp_enveloppe_bati_"$trigramme"_000_"$millesime"
	ADD COLUMN id serial;
"
echo "A.2.3.3] Sauvegarde en cas d'echec de la A.3.4"
$commande "
DROP TABLE IF EXISTS "$schematravail".sauve_temp_enveloppe_bati_"$trigramme"_000_"$millesime";
CREATE TABLE "$schematravail".sauve_temp_enveloppe_bati_"$trigramme"_000_"$millesime" AS
	SELECT * FROM "$schematravail".temp_enveloppe_bati_"$trigramme"_000_"$millesime";
"


echo "A.3.4] Création de la table nationale"
now=$(date +"%T")
echo "Début à $now"
echo "------------------------------------"
$commande "
DROP TABLE IF EXISTS "$schematravail".l_enveloppe_bati_"$trigramme"_000_"$millesime";
CREATE TABLE "$schematravail".l_enveloppe_bati_"$trigramme"_000_"$millesime"
(
    id_enveloppe serial,
	nb_batidur int4,
	nb_batileg int4,
	surf_batidur int4,
	surf_batileg int4,
	list_code_dep varchar,
    geom geometry(Polygon,2154)
);
"

echo "A.2.4.1] Sélection des enveloppes qui s'intersectent"
now=$(date +"%T")
echo "Début à $now"
echo "------------------------------------"
$commande "
DROP TABLE IF EXISTS "$schematravail".stintersects_enveloppe_bati_"$trigramme"_000_"$millesime";
CREATE TABLE "$schematravail".stintersects_enveloppe_bati_"$trigramme"_000_"$millesime" AS
	SELECT ST_Multi(ST_Union(t1.geom,t2.geom))::geometry(MULTIPOLYGON,2154) as geometrie_union
		FROM "$schematravail".temp_enveloppe_bati_"$trigramme"_000_"$millesime" as t1
		JOIN "$schematravail".temp_enveloppe_bati_"$trigramme"_000_"$millesime" as t2
		ON ST_Intersects(t1.geom, t2.geom)
		WHERE t1.id < t2.id;
-->
" 
$commande "
CREATE INDEX stintersects_enveloppe_bati_"$trigramme"_000_"$millesime"_geom_gist
	ON "$schematravail".stintersects_enveloppe_bati_"$trigramme"_000_"$millesime"
	USING gist (geometrie_union) TABLESPACE index;
"

echo "A.3.4.2] Peuplement par agregation des zones de chevauchement interdépartementales"
now=$(date +"%T")
echo "Début à $now"
echo "------------------------------------"
$commande "
INSERT INTO "$schematravail".l_enveloppe_bati_"$trigramme"_000_"$millesime" (geom)
SELECT (ST_Dump(ST_Union(geometrie_union))).geom::geometry(Polygon,2154) as geom
		FROM "$schematravail".stintersects_enveloppe_bati_"$trigramme"_000_"$millesime";
--> INSERT 0 25040 / Requête exécutée avec succès en 43 hr 58 min.
"

echo "A.3.4.3] Suppression des zones de chevauchement"
echo "         interdépartementales que l'on vient d'inserer"
now=$(date +"%T")
echo "Début à $now"
echo "---------------------------------------------------------------------"
$commande "
DELETE FROM "$schematravail".temp_enveloppe_bati_"$trigramme"_000_"$millesime" WHERE id IN (
		(SELECT t1.id
		FROM "$schematravail".temp_enveloppe_bati_"$trigramme"_000_"$millesime" as t1
		JOIN "$schematravail".temp_enveloppe_bati_"$trigramme"_000_"$millesime" as t2
		ON ST_Intersects(t1.geom, t2.geom)
		WHERE t1.id < t2.id)
	UNION ALL
		(SELECT t2.id
		FROM "$schematravail".temp_enveloppe_bati_"$trigramme"_000_"$millesime" as t1
		JOIN "$schematravail".temp_enveloppe_bati_"$trigramme"_000_"$millesime" as t2
		ON ST_Intersects(t1.geom, t2.geom)
		WHERE t1.id < t2.id)
);
--> 
"

echo "---------------------------------------------------------"
echo "A.3.4.4] Insersion du reste : ce qui ne se supperpose pas :"
echo "         On vérifie qu'il n'y a rien qui s'intersecte"
echo "---------------------------------------------------------"
$commande "
CREATE INDEX l_enveloppe_bati_"$trigramme"_000_"$millesime"_geom_gist
	ON "$schematravail".l_enveloppe_bati_"$trigramme"_000_"$millesime"
	USING gist (geom) TABLESPACE index;

--SELECT *
--FROM "$schematravail".temp_enveloppe_bati_"$trigramme"_000_"$millesime" as t1
--JOIN "$schematravail".l_enveloppe_bati_"$trigramme"_000_"$millesime" as t2
--ON ST_Intersects(t1.geom, t2.geom);
-->
"
echo "---------------------------------------------------------"
$commande "
DROP INDEX "$schematravail".l_enveloppe_bati_"$trigramme"_000_"$millesime"_geom_gist;
"

echo "A.3.4.5] On insere !"
$commande "
INSERT INTO "$schematravail".l_enveloppe_bati_"$trigramme"_000_"$millesime" (geom)
	SELECT geom::geometry(Polygon,2154) as geom
	FROM "$schematravail".temp_enveloppe_bati_"$trigramme"_000_"$millesime";
--> INSERT 0 1535920 / Requête exécutée avec succès en 2 min 29 secs.
"

echo "A.3.4.6] Index Géomérique + Cluster"
$commande "
CREATE INDEX l_enveloppe_bati_"$trigramme"_000_"$millesime"_geom_gist ON "$schematravail".l_enveloppe_bati_"$trigramme"_000_"$millesime"
	USING gist (geom) TABLESPACE index;
ALTER TABLE "$schematravail".l_enveloppe_bati_"$trigramme"_000_"$millesime"
	CLUSTER ON l_enveloppe_bati_"$trigramme"_000_"$millesime"_geom_gist;
"
$commande "
CREATE INDEX sauve_temp_enveloppe_bati_"$trigramme"_000_"$millesime"_geom_gist ON "$schematravail".sauve_temp_enveloppe_bati_"$trigramme"_000_"$millesime"
	USING gist (geom) TABLESPACE index;
ALTER TABLE "$schematravail".sauve_temp_enveloppe_bati_"$trigramme"_000_"$millesime"
	CLUSTER ON sauve_temp_enveloppe_bati_"$trigramme"_000_"$millesime"_geom_gist;
"
#DEBUG : Pas fait sur le referentiel cadastre ETALAB 2020 ! 
#$commande "
#CREATE INDEX n_bati_"$trigramme"_000_"$millesime"_type_idx
#    ON r_cadastre_"$trigramme"_"$millesime".n_bati_"$trigramme"_000_"$millesime" USING brin
#    (type)
#    TABLESPACE index;
#"

echo "A.3.5] Mise à jour des champs"
echo "A.3.5.1] Mise à jour des champs nb_batidur"
now=$(date +"%T")
echo "Début à $now"
echo "------------------------------------"
$commande "
UPDATE "$schematravail".l_enveloppe_bati_"$trigramme"_000_"$millesime" as t1 SET nb_batidur = (
	SELECT count(*)
	FROM "$batimentsmetropolitains" as t2
	WHERE	ST_Intersects(t1.geom, t2.geom)
	AND (t2.type='Bâtiment en dur' or t2.type='Bâti dur' or t2.type='Bati dur' or t2.type='01')
	);
--> 
"

echo "A.3.5.2] Mise à jour des champs nb_batileg"
now=$(date +"%T")
echo "Début à $now"
echo "------------------------------------"
$commande "
UPDATE "$schematravail".l_enveloppe_bati_"$trigramme"_000_"$millesime" as t1 SET nb_batileg = (
	SELECT count(*)
	FROM "$batimentsmetropolitains" as t2
	WHERE	ST_Intersects(t1.geom, t2.geom)
	AND	(t2.type='Construction légère' or t2.type='Bâti léger' or t2.type='Bati leger' or t2.type='02')
	);
--> Requête exécutée avec succès en 2 min 8 secs.
"
echo "A.3.5.3] Mise à jour des champs surf_batidur"
now=$(date +"%T")
echo "Début à $now"
echo "------------------------------------"
$commande "
UPDATE "$schematravail".l_enveloppe_bati_"$trigramme"_000_"$millesime" as t1 SET surf_batidur = (
	SELECT sum(ST_Area(t2.geom))
	FROM "$batimentsmetropolitains" as t2
	WHERE 	
		ST_Intersects(t1.geom, t2.geom)
		AND 
		(t2.type='01' or t2.type='Bâtiment en dur' or t2.type='Bâti dur' or t2.type='Bati dur')
	);
"
echo "A.3.5.4] Mise à jour des champs surf_batileg"
now=$(date +"%T")
echo "Début à $now"
echo "------------------------------------"
$commande "
UPDATE "$schematravail".l_enveloppe_bati_"$trigramme"_000_"$millesime" as t1 SET surf_batileg = (
	SELECT sum(ST_Area(t2.geom))
	FROM "$batimentsmetropolitains" as t2
	WHERE 	
		ST_Intersects(t1.geom, t2.geom)
		AND
		(t2.type='02' or t2.type='Construction légère' or t2.type='Bâti léger' or t2.type='Bati leger')
	);	
"
echo "A.3.5.5] Mise à jour du champs list_code_dep"
now=$(date +"%T")
echo "Début à $now"
echo "------------------------------------"
#DEBUG : si plusieurs taches fusionnées d une même département on a un doublon
#$commande "
#UPDATE "$schematravail".l_enveloppe_bati_"$trigramme"_000_"$millesime" as t1 SET list_code_dep = (
#	SELECT array_agg(code_dep)::varchar
#	FROM "$schematravail".sauve_temp_enveloppe_bati_"$trigramme"_000_"$millesime" as t2
#	WHERE	ST_Intersects(t1.geom, t2.geom)
#	);
#--> UPDATE 38360 / Requête exécutée avec succès en 1 min 25 secs.
#"

#DEBUG : Enleve les doublons de départements, mais on perds l information de la fusion de plusieurs tache dans le même département
$commande "
UPDATE "$schematravail".l_enveloppe_bati_"$trigramme"_000_"$millesime" as t1 SET list_code_dep = (
	WITH resultat1 AS 
		(SELECT DISTINCT t2.code_dep
		FROM "$schematravail".sauve_temp_enveloppe_bati_"$trigramme"_000_"$millesime" as t2
		WHERE ST_Intersects(t1.geom, t2.geom)
		ORDER BY t2.code_dep)
	SELECT array_agg(code_dep)::varchar FROM resultat1
	);
--> 
"

echo "A.3.6] Optimisations"
now=$(date +"%T")
echo "Début à $now"
echo "------------------------------------"
echo "A.2.6.1] Clé primaire"
$commande "
ALTER TABLE "$schematravail".l_enveloppe_bati_"$trigramme"_000_"$millesime"
    ADD CONSTRAINT l_enveloppe_bati_"$trigramme"_000_"$millesime"_id_enveloppe_pk PRIMARY KEY (id_enveloppe)
    USING INDEX TABLESPACE index;
ALTER TABLE "$schematravail".l_enveloppe_bati_"$trigramme"_000_"$millesime"
    ADD CONSTRAINT enforce_dims_geom CHECK (ST_ndims(geom) = 2);
ALTER TABLE "$schematravail".l_enveloppe_bati_"$trigramme"_000_"$millesime"
	ADD CONSTRAINT enforce_srid_geom CHECK (ST_Srid(geom)= 2154);
ALTER TABLE "$schematravail".l_enveloppe_bati_"$trigramme"_000_"$millesime"
    ADD CONSTRAINT enforce_geotype_geom CHECK (geometrytype(geom) = 'POLYGON'::text);
"
echo "A.3.6.2] Index attributaires"
$commande "
CREATE INDEX l_enveloppe_bati_"$trigramme"_000_"$millesime"_nb_batidur_idx ON "$schematravail".l_enveloppe_bati_"$trigramme"_000_"$millesime"
	USING brin (nb_batidur) TABLESPACE index;
CREATE INDEX l_enveloppe_bati_"$trigramme"_000_"$millesime"_nb_batileg_idx ON "$schematravail".l_enveloppe_bati_"$trigramme"_000_"$millesime"
	USING brin (nb_batileg) TABLESPACE index;
CREATE INDEX l_enveloppe_bati_"$trigramme"_000_"$millesime"_surf_batidur_idx ON "$schematravail".l_enveloppe_bati_"$trigramme"_000_"$millesime"
	USING brin (surf_batidur) TABLESPACE index;
CREATE INDEX l_enveloppe_bati_"$trigramme"_000_"$millesime"_surf_batileg_idx ON "$schematravail".l_enveloppe_bati_"$trigramme"_000_"$millesime"
	USING brin (surf_batileg) TABLESPACE index;
CREATE INDEX l_enveloppe_bati_"$trigramme"_000_"$millesime"_list_code_dep_idx ON "$schematravail".l_enveloppe_bati_"$trigramme"_000_"$millesime"
	USING brin (list_code_dep) TABLESPACE index;
"

echo "A.3.6.3] Index Géomérique + Cluster"
#DEBUG Déjà fait en partie # B.2.1]

echo "A.3.6.4] Commentaires"
$commande "
COMMENT ON TABLE "$schematravail".l_enveloppe_bati_"$trigramme"_000_"$millesime"
  IS 'Enveloppe nationale du bâti de 2020.

Il s’agit de fusionner tous les bâtiments issu du cadastre ETALAB qui sont séparés de moins de 50 m par un Buffer +50, ST_Union, Erosion de 40m';
"
$commande "
COMMENT ON COLUMN "$schematravail".l_enveloppe_bati_"$trigramme"_000_"$millesime".id_enveloppe
    IS 'Identifiant national unique pour une même enveloppe du bati';
COMMENT ON COLUMN "$schematravail".l_enveloppe_bati_"$trigramme"_000_"$millesime".nb_batidur
    IS 'Nombre de bâtiments qualifiés de dur par la DGFiP intégrés dans cette enveloppe du bati.';
COMMENT ON COLUMN "$schematravail".l_enveloppe_bati_"$trigramme"_000_"$millesime".nb_batileg
    IS 'Nombre de bâtiments qualifiés de legers par la DGFiP intégrés dans cette enveloppe du bati.';
COMMENT ON COLUMN "$schematravail".l_enveloppe_bati_"$trigramme"_000_"$millesime".surf_batidur
    IS 'Surface totale en m2 de tous les bâtiments qualifiés de dur par la DGFiP et présents dans cette enveloppe du bâti.';
COMMENT ON COLUMN "$schematravail".l_enveloppe_bati_"$trigramme"_000_"$millesime".surf_batileg
    IS 'Surface totale en m2 de tous les bâtiments qualifiés de leger par la DGFiP et présents dans cette enveloppe du bâti.';	
COMMENT ON COLUMN "$schematravail".l_enveloppe_bati_"$trigramme"_000_"$millesime".list_code_dep
    IS 'Liste des départements à l³origine de cette enveloppe du bati.';	
COMMENT ON COLUMN "$schematravail".l_enveloppe_bati_"$trigramme"_000_"$millesime".geom
    IS 'Champs géométrique : Monopolygone en Lambert93 (EPSG:2154).';
"

echo "A.3.7] Netoyage des tables temporaires"
now=$(date +"%T")
echo "Début à $now"
echo "------------------------------------"
$commande "
--DROP TABLE IF EXISTS "$schematravail".temp_departement_buffer_200m_000_"$millesime";
----#DEBUG : on garde pour vérifier 
--DROP TABLE IF EXISTS "$schematravail".temp_enveloppe_bati_"$trigramme"_000_"$millesime";
DROP TABLE IF EXISTS "$schematravail".stintersects_enveloppe_bati_"$trigramme"_000_"$millesime";
"
