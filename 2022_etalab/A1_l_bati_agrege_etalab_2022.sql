------------------------------------------------
----	CREATION BATI AGREGE
----
----             CEREMABASE
----
----	Version 1 du 19/02/2022
----	c.badol
----	Finalisé oui|X| / non | |
------------------------------------------------
---- Rq : ---- pour les commentaires / -- pour les commandes optionnelles, debuger"



------------------------------------
---- A] Bâti agrégé 2022
------------------------------------
CREATE SCHEMA tache_urbaine_2022 AUTHORIZATION postgres;

---- A1] Script 
#!/bin/bash
# Bati Agrégé
commande="psql -h hp-geomatique -p 5432 -d ceremabase -c "
millesime="2022"
cadastre="etalab"
couchebaticadastre="r_cadastre_etalab_"$millesime".n_batiments_"$cadastre

#DEBUG
#echo $couchebaticadastre

# Important :
# Pour tout logger taper :
# su postgres
# export PGPASSWORD=tunelesaurapas!
# time sh /home/administrateur/00_gitlab/prod/enveloppe_urbaine/2022_etalab/A1_l_bati_agrege_etalab_2022.sh 1>/mnt/data/A1_l_bati_agrege_etalab_2022.log 2>&1

# A] Bati Agrege
#DEBUG
#for dpt in '090' '02b' '02a'
# for dpt in '001' '002' '003' '004' '005' '006' '007' '008' '009' '010' '011' '012' '013' '014' '015' '016' '017' '018' '019' '021' '022' '023' '024' '025' '026' '027' '028' '029' '030' '031' '032' '033' '034' '035' '036' '037' '038' '039' '040' '041' '042' '043' '044' '045' '046' '047' '048' '049' '050' '051' '052' '053' '054' '055' '056' '057' '058' '059' '060' '061' '062' '063' '064' '065' '066' '067' '068' '069' '070' '071' '072' '073' '074' '075' '076' '077' '078' '079' '080' '081' '082' '083' '084' '085' '086' '087' '088' '089' '090' '091' '092' '093' '094' '095' 

do

echo $dpt
echo '----'

# A.1] Création des tables Départementales
$commande "
DROP TABLE IF EXISTS tache_urbaine_"$millesime".l_bati_agrege_"$cadastre"_"$dpt"_"$millesime";
"
$commande "
CREATE TABLE tache_urbaine_"$millesime".l_bati_agrege_"$cadastre"_"$dpt"_"$millesime"
	(
	id serial,
	nb_batidur integer,
	nb_batileg integer,
	code_dep character(3) DEFAULT UPPER('"$dpt"'),  
	geom geometry(Polygon,2154)
	);
"

$commande "
ALTER SEQUENCE tache_urbaine_"$millesime".l_bati_agrege_"$cadastre"_"$dpt"_"$millesime"_id_seq
	RESTART WITH "$dpt"000001;
"

# A.2] Buffer +0,01, ST_Union, Buffer -0,01, ST_Dump
$commande "
INSERT INTO tache_urbaine_"$millesime".l_bati_agrege_"$cadastre"_"$dpt"_"$millesime" (geom)
	SELECT (ST_Dump(ST_Buffer(ST_Union(ST_Buffer(geom,0,01)),-0,01))).geom::geometry(Polygon,2154) as geom
	FROM "$couchebaticadastre"_"$dpt"_"$millesime";
"

# A.3] Index Géomérique + Cluster
$commande "
CREATE INDEX l_bati_agrege_"$cadastre"_"$dpt"_"$millesime"_geom_gist ON tache_urbaine_"$millesime".l_bati_agrege_"$cadastre"_"$dpt"_"$millesime"
	USING gist (geom) TABLESPACE index;
"
$commande "
ALTER TABLE tache_urbaine_"$millesime".l_bati_agrege_"$cadastre"_"$dpt"_"$millesime"
	CLUSTER ON l_bati_agrege_"$cadastre"_"$dpt"_"$millesime"_geom_gist;
"

# A.4] Mise à jour des champs nb_batidur & nb_batileg
$commande "
UPDATE tache_urbaine_"$millesime".l_bati_agrege_"$cadastre"_"$dpt"_"$millesime" as t1 SET nb_batidur = (
	SELECT count(*)
	FROM "$couchebaticadastre"_"$dpt"_"$millesime" as t2
	WHERE 	t1.code_dep = UPPER('"$dpt"')
		and (t2.type='Bâtiment en dur' or t2.type='Bâti dur' or t2.type='Bati dur' or t2.type='01')
		and ST_Intersects(t1.geom, t2.geom))
	WHERE 	t1.code_dep = UPPER('"$dpt"');
"

$commande "
UPDATE tache_urbaine_"$millesime".l_bati_agrege_"$cadastre"_"$dpt"_"$millesime" as t1 SET nb_batileg = (
	SELECT count(*)
	FROM "$couchebaticadastre"_"$dpt"_"$millesime" as t2
	WHERE 	t1.code_dep = UPPER('"$dpt"')
		and (type='Construction légère' or type='Bâti léger' or type='Bati leger' or type='02')
		and ST_Intersects(t1.geom, t2.geom))
	WHERE 	t1.code_dep = UPPER('"$dpt"');
"

# A.5] Optimisation 
# Clé primaire 
$commande "
ALTER TABLE tache_urbaine_"$millesime".l_bati_agrege_"$cadastre"_"$dpt"_"$millesime"
    ADD CONSTRAINT l_bati_agrege_"$cadastre"_"$dpt"_"$millesime"_pk PRIMARY KEY (id)
    USING INDEX TABLESPACE index;
"
# Contraintes Géométriques
$commande "
ALTER TABLE tache_urbaine_"$millesime".l_bati_agrege_"$cadastre"_"$dpt"_"$millesime"
    ADD CONSTRAINT enforce_dims_geom CHECK (ST_ndims(geom) = 2);
"
$commande "
ALTER TABLE tache_urbaine_"$millesime".l_bati_agrege_"$cadastre"_"$dpt"_"$millesime"
	ADD CONSTRAINT enforce_srid_geom CHECK (ST_Srid(geom)= 2154);
"
$commande "
ALTER TABLE tache_urbaine_"$millesime".l_bati_agrege_"$cadastre"_"$dpt"_"$millesime"
    ADD CONSTRAINT enforce_geotype_geom CHECK (geometrytype(geom) = 'POLYGON'::text);
"
$commande "
ALTER TABLE tache_urbaine_"$millesime".l_bati_agrege_"$cadastre"_"$dpt"_"$millesime"
    ADD CONSTRAINT enforce_validite_geom CHECK (ST_IsValid(geom));
"
$commande "
ALTER TABLE tache_urbaine_"$millesime".l_bati_agrege_"$cadastre"_"$dpt"_"$millesime"
    ADD CONSTRAINT enforce_no_self_intersection_geom CHECK (ST_IsSimple(geom));
"
$commande "
ALTER TABLE tache_urbaine_"$millesime".l_bati_agrege_"$cadastre"_"$dpt"_"$millesime"
    ADD CONSTRAINT enforce_no_zero_area_geom CHECK (ST_Area(geom) > 0);
"

# Index attributaires
$commande "
CREATE INDEX l_bati_agrege_"$cadastre"_"$dpt"_"$millesime"_nb_batidur_idx ON tache_urbaine_"$millesime".l_bati_agrege_"$cadastre"_"$dpt"_"$millesime"
	USING brin (nb_batidur) TABLESPACE index;
"
$commande "
CREATE INDEX l_bati_agrege_"$cadastre"_"$dpt"_"$millesime"_nb_batileg_idx ON tache_urbaine_"$millesime".l_bati_agrege_"$cadastre"_"$dpt"_"$millesime"
	USING brin (nb_batileg) TABLESPACE index;
"
$commande "
CREATE INDEX l_bati_agrege_"$cadastre"_"$dpt"_"$millesime"_code_dep_idx ON tache_urbaine_"$millesime".l_bati_agrege_"$cadastre"_"$dpt"_"$millesime"
	USING brin (code_dep) TABLESPACE index;
"

# A.6] Commentaires
# Table
$commande "
COMMENT ON TABLE tache_urbaine_"$millesime".l_bati_agrege_"$cadastre"_"$dpt"_"$millesime"
  IS 'Bâti agrégé de "$millesime" pour le département ''"$dpt"''.

Il s’agit de fusionner tous les bâtiments du cadastre "$cadastre" qui sont séparés de moins de 1 cm par un Buffer +0,01, ST_Union, Buffer -0,01, ST_Dump';
"
# Champs
$commande "
COMMENT ON COLUMN tache_urbaine_"$millesime".l_bati_agrege_"$cadastre"_"$dpt"_"$millesime".id
    IS 'Identifiant unique selon méthode COVADIS : n°département.000.000 + id.';
"
$commande "
COMMENT ON COLUMN tache_urbaine_"$millesime".l_bati_agrege_"$cadastre"_"$dpt"_"$millesime".nb_batidur
    IS 'Nombre de bâtiments qualifiés de dur par la DGFiP intégrés dans cette agrégation de bâtiments.';
"
$commande "
COMMENT ON COLUMN tache_urbaine_"$millesime".l_bati_agrege_"$cadastre"_"$dpt"_"$millesime".nb_batileg
    IS 'Nombre de bâtiments qualifiés de legers par la DGFiP intégrés dans cette agrégation de bâtiments.';
"
$commande "
COMMENT ON COLUMN tache_urbaine_"$millesime".l_bati_agrege_"$cadastre"_"$dpt"_"$millesime".code_dep
    IS 'Numéro INSEE du département sur 3 charactères.';
"
$commande "
COMMENT ON COLUMN tache_urbaine_"$millesime".l_bati_agrege_"$cadastre"_"$dpt"_"$millesime".geom
    IS 'Champs géométrique : Monopolygones en Lambert93 (EPSG:2154).';
"

done

---- Execution :
---- xx février 2023 : de 001 à 073  
time sh /home/administrateur/00_gitlab/prod/enveloppe_urbaine/2022_etalab/A1_l_bati_agrege_etalab_2022.sh 
/*
real	12193m45,480s
user	1m13,022s
sys	0m13,061s
 */
--- 11 mars 2023 de 074 à 095
time sh /home/administrateur/00_gitlab/prod/enveloppe_urbaine/2022_etalab/A1_l_bati_agrege_etalab_2022.sh 1>/mnt/data/A1_l_bati_agrege_etalab_2022.log 2>&1
/*
real	2734m40,320s
user	0m20,925s
sys		0m3,750s
 */


echo "------------------------------------------------------------"
echo "B] Partition "
echo "------------------------------------------------------------"
echo "15/03/2023"
echo "B.1] Partition Nationale "
$commande "
select w_adl_delegue.set_partition_attach_metropole_et_regions('p_enveloppe_bati_2022.l_bati_agrege_etalab','_2022');
/*
set_partition_attach_metropole_et_regions                                     |
------------------------------------------------------------------------------+
2023-03-15 - p_enveloppe_bati_2022.l_bati_agrege_etalab_000_2022 reste visible|
*/


echo "A.2.2] Commentaires "
$commande "
DO $$
DECLARE
liste_valeur 			CHARACTER VARYING[];
millesime				CHARACTER VARYING(4);
cadastre				CHARACTER VARYING;
trigramme				CHARACTER VARYING;
nom_schema				CHARACTER VARYING;
nom_table				CHARACTER VARYING;
req 					text;

BEGIN

------ Paramètres en entrée
liste_valeur := ARRAY  ['000','r11','r24','r27','r28','r32','r44','r52','r53','r75','r76','r84','r93','r94'];
millesime := '2022';
cadastre :='etalab';
trigramme :='etalab';
nom_schema :='p_enveloppe_bati_' || millesime;
------

FOR i_table IN 1..array_length(liste_valeur, 1) LOOP
	
		nom_table :='l_bati_agrege_' || trigramme || '_' || liste_valeur[i_table] || '_' || millesime;

		req := '
				COMMENT ON TABLE ' || nom_schema || '.' || nom_table || '
				  IS ''Bâti agrégé de ' || millesime || '. Il s’agit de fusionner tous les bâtiments du parcellaire express qui sont séparés de moins de 1 cm par un Buffer +0,01, ST_Union, Buffer -0,01, ST_Dump.'';
				COMMENT ON COLUMN ' || nom_schema || '.' || nom_table || '.id
				IS ''Identifiant unique selon méthode COVADIS : n°département.000.000 + id.'';
				COMMENT ON COLUMN ' || nom_schema || '.' || nom_table || '.nb_batidur
				IS ''Nombre de bâtiments qualifiés de dur par la DGFiP intégrés dans cette agrégation de bâtiments.'';
				COMMENT ON COLUMN ' || nom_schema || '.' || nom_table || '.nb_batileg
				IS ''Nombre de bâtiments qualifiés de legers par la DGFiP intégrés dans cette agrégation de bâtiments.'';
				COMMENT ON COLUMN ' || nom_schema || '.' || nom_table || '.code_dep
				IS ''Numéro INSEE du département sur 3 charactères.'';
				COMMENT ON COLUMN ' || nom_schema || '.' || nom_table || '.geom
				IS ''Champs géométrique : Monopolygones en Lambert93 (EPSG:2154).''; 
			';

		RAISE NOTICE '%', req;
		EXECUTE(req);
		COMMIT;
   END LOOP;
END $$;
"
--> Updated Rows	0
